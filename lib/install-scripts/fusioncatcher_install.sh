#!/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh

# USAGE
display_usage() { 
        echo -e "\nUsage:\n\t$0 [DIR] \n" 
        echo -e "\tInstall FusionCatcher in DIR. (Mandatory argument)."
        echo -e "\t-h, --help display this help and exit"
} 
# if less than two arguments supplied, display usage 
if [  $# -le 0 ] 
then 
        display_usage
        exit 1
fi 
# check whether user had supplied -h or --help . If yes display usage 
if [[ ($1 == "--help") ||  $1 == "-h" ]]  
then 
        display_usage
        exit 0
fi 

NAME=fusioncatcher
#VERSION="v0.99.6a"
VERSION="v0.99.7c"
URL="http://sourceforge.net/projects/fusioncatcher/files/fusioncatcher_"${VERSION}".zip"
cd "$( dirname $0 )"
[[ -e ../"$NAME/bin/fusioncatcher" ]] && {
	echo "${NAME} already installed"
	exit 0
}


# INSTALLATION
cd $1
wget http://sf.net/projects/fusioncatcher/files/bootstrap.py -O bootstrap.py && yes | python bootstrap.py -t --download
rm bootstrap.py

##
ln -Tsf "$( pwd )/${NAME}/${NAME}_${VERSION}" "$( pwd )/${NAME}_current"
[[ -e $ANDURIL_HOME/bundles/sequencing/lib/fusioncatcher ]]  || {
	ln -Tsf "$( pwd )/${NAME}/${NAME}_${VERSION}" "$ANDURIL_HOME/bundles/sequencing/lib/$NAME"
}
chmod -R ugo+rx "$( pwd )/${NAME}/"
