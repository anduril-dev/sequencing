#!/bin/bash

. $ANDURIL_HOME/lang/bash/generic.sh
NAME=RSEM
VERSION=1.2.31
URL="https://github.com/deweylab/${NAME}/archive/v${VERSION}.tar.gz"
cd "$( dirname $0 )"
[[ -e ../"${NAME}-${VERSION}/rsem-calculate-expression" ]] && {
    echo "${NAME} already installed"
    exit 0
}
cd ..
rm -rf "${NAME}-${VERSION}"
wget -O $NAME.tgz $URL
tar xvzf $NAME.tgz
rm $NAME.tgz
ln -Tsf "${NAME}-${VERSION}" $NAME

cd $NAME
make
make ebseq
