#!/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh

NAME=SHRiMP
VERSION="2_2_3"
URL="http://compbio.cs.toronto.edu/shrimp/releases/SHRiMP_2_2_3.lx26.x86_64.tar.gz"
cd "$( dirname $0 )"
[[ -e ../"${NAME}_${VERSION}/bin/gmapper-ls" ]] && {
	echo "${NAME} already installed"
	exit 0
}
cd ..
wget -O $NAME.tgz $URL
tar xvzf $NAME.tgz
rm -f $NAME.tgz
chmod -R ugo+rX "${NAME}_${VERSION}"
ln -Tsf "${NAME}_${VERSION}" $NAME
cd "$NAME"
file bin/gmapper
export SHRIMP_FOLDER=$( pwd )
