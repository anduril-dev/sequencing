#!/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh

# USAGE
display_usage() { 
	echo -e "\nUsage:\n\t$0 [DIR] \n" 
	echo -e "\tInstall ChimeraScan in DIR. (Mandatory argument)."
	echo -e "\t-h, --help display this help and exit"
} 
# if less than two arguments supplied, display usage 
if [  $# -le 0 ] 
then 
	display_usage
	exit 1
fi 
# check whether user had supplied -h or --help . If yes display usage 
if [[ ($1 == "--help") ||  $1 == "-h" ]] 
then 
	display_usage
	exit 0
fi 


NAME=chimerascan
VERSION="0.4.6"
URL="https://github.com/genome/chimerascan-vrl.git"
# Check if already installed
cd "$( dirname $0 )"
[[ -e ../"${NAME}/chimerascan/chimerascan_run.py" ]] && {
        echo "${NAME} already installed"
        exit 0
}

# INSTALLATION
cd $1
git clone $URL "$NAME-$VERSION"
chmod -R ugo+rx "${NAME}-${VERSION}"
##
ln -Tsf "$( pwd )/${NAME}-${VERSION}" "$( pwd )/${NAME}"
[[ -e $ANDURIL_HOME/bundles/sequencing/lib/chimerascan ]]  || {
	ln -Tsf "$( pwd )/${NAME}-${VERSION}" "$ANDURIL_HOME/bundles/sequencing/lib/$NAME"
}
cd "${NAME}-${VERSION}"

python setup.py build
python setup.py install	--prefix $( pwd )	#needs root

# Create database
#Download files
wget "http://hgdownload.cse.ucsc.edu/goldenPath/hg38/bigZips/hg38.fa.gz"
wget "http://hgdownload.cse.ucsc.edu/goldenpath/hg38/database/knownGene.txt.gz"
wget "http://hgdownload.cse.ucsc.edu/goldenpath/hg38/database/kgXref.txt.gz"
gunzip *.gz

#Create annotations
cut -f1-10 knownGene.txt > tmpKnownGene
cut -f5 kgXref.txt > tmpkgXref
paste tmpKnownGene tmpkgXref > UCSC.knownGene.txt
rm knownGene.txt tmpKnownGene kgXref.txt tmpkgXref
sed -i '1s/^/#name\tchrom\tstrand\ttxStart\ttxEnd\tcdsStart\tcdsEnd\texonCount\texonStarts\texonEnds\tgeneSymbol\n/' UCSC.knownGene.txt

#Build genome database
mkdir db
export PYTHONPATH=$( pwd )/lib/python2.7/site-packages:$PYTHONPATH
python chimerascan/chimerascan_index.py hg38.fa UCSC.knownGene.txt db 
