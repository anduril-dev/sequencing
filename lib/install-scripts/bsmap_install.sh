#!/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh
NAME=bsmap
VERSION="2.89"
URL="http://lilab.research.bcm.edu/dldcc-web/lilab/yxi/bsmap/bsmap-2.89.tgz"
cd "$( dirname $0 )"
[[ -e ../"${NAME}-${VERSION}/${NAME}" ]] && {
    echo "${NAME} already installed"
    exit 0
}
cd ..
rm -rf "${NAME}-${VERSION}"
wget -O $NAME.tgz $URL
tar xvzf $NAME.tgz
rm -f $NAME.tgz
ln -Tsf "${NAME}-${VERSION}" $NAME
cd $NAME
cp param.cpp param.cpp.tmp
echo '#include<unistd.h>' > param.cpp
cat param.cpp.tmp >> param.cpp
make
chmod a+rx bsmap methratio.py sam2bam.sh samtools/samtools
