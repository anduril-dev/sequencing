#!/bin/bash

. $ANDURIL_HOME/lang/bash/generic.sh
NAME=BitSeq
VERSION=0.7.5
URL="https://github.com/${NAME}/${NAME}/archive/v${VERSION}.tar.gz"
cd "$( dirname $0 )"
[[ -e ../"${NAME}-${VERSION}/parseAlignment" ]] && {
    echo "${NAME} already installed"
    exit 0
}
cd ..
rm -rf "${NAME}-${VERSION}"
wget -O $NAME.tgz $URL
tar xvzf $NAME.tgz
rm $NAME.tgz
ln -Tsf "${NAME}-${VERSION}" $NAME

cd $NAME
make
