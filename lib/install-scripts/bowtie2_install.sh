#!/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh
NAME=bowtie2
VERSION="2.2.5"
URL="http://sourceforge.net/projects/bowtie-bio/files/bowtie2/2.2.5/bowtie2-2.2.5-linux-x86_64.zip"
cd "$( dirname $0 )"
[[ -e ../"${NAME}-${VERSION}/bowtie2" ]] && {
    echo "${NAME} already installed"
    exit 0
}
cd ..
rm -rf "${NAME}-${VERSION}"
wget -O $NAME.zip $URL
unzip $NAME.zip
rm $NAME.zip
ln -Tsf "${NAME}-${VERSION}" $NAME 
