#!/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh

# USAGE
display_usage() {
        echo -e "\nUsage:\n\t$0 [DIR] \n" 
        echo -e "\tInstall Pegasus in DIR. (Mandatory argument)."
        echo -e "\t-h, --help display this help and exit"
}
# if less than two arguments supplied, display usage 
if [  $# -le 0 ]
then
        display_usage
        exit 1
fi
# check whether user had supplied -h or --help . If yes display usage 
if [[ ($1 == "--help") ||  $1 == "-h" ]]
then
        display_usage
        exit 0
fi

NAME="pegasus"
URL="https://github.com/RabadanLab/Pegasus.git"
# Check if already installed
cd "$( dirname $0 )"
[[ -s ../"${NAME}/annotations.gtf" ]] && {
        echo "${NAME} already installed"
        exit 0
}
LIBPATH=$( readlink -f ../ )

# INSTALLATION
mkdir -p "$1"
cd "$1"
git clone $URL "$NAME"
chmod -R ugo+rx "${NAME}"
if [[ "$( pwd )/${NAME}" != "$LIBPATH/$NAME" ]] 
then 
  ln -Tsf "$( pwd )/${NAME}" "$LIBPATH/$NAME"
fi
cd "${NAME}"

# Build annotations $ genome ref
wget -O tmp_annotations.gtf.gz "ftp://ftp.ensembl.org/pub/release-84/gtf/homo_sapiens/Homo_sapiens.GRCh38.84.gtf.gz"	#Ensembl annotations v84
wget -O hg.fa.gz "http://hgdownload.cse.ucsc.edu/goldenPath/hg38/bigZips/hg38.fa.gz"	# UCSC hg38 genome
gunzip tmp_annotations.gtf.gz
gunzip hg.fa.gz
samtools faidx hg.fa
cd learn
python train_model.py
cd ..
perl "$LIBPATH/../components/FusionAnalyzer/resources/format-Pegasus-gtf.pl" tmp_annotations.gtf > annotations.gtf
rm tmp_annotations.gtf
