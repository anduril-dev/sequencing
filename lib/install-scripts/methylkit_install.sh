#!/bin/bash

set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh

NAME=methylKit

echo "library('methylKit')" | R --vanilla && {
    echo "${NAME} already installed"
    exit 0
}
echo '
# dependencies
source("http://bioconductor.org/biocLite.R")
biocLite(c("GenomicRanges","data.table","devtools"))

# install the package from github
library(devtools)
install_github("al2na/methylKit",build_vignettes=FALSE)
' | R --vanilla
