#!/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh

# USAGE
display_usage() {
        echo -e "\nUsage:\n\t$0 [DIR] \n" 
        echo -e "\tInstall Oncofuse in DIR. (Mandatory argument)."
        echo -e "\t-h, --help display this help and exit"
}
# if less than two arguments supplied, display usage 
if [  $# -le 0 ]
then
        display_usage
        exit 1
fi
# check whether user had supplied -h or --help . If yes display usage 
if [[ ($1 == "--help") ||  $1 == "-h" ]]
then
        display_usage
        exit 0
fi

NAME="oncofuse"
URL="https://github.com/mikessh/oncofuse.git"
# Check if already installed
cd "$( dirname $0 )"
[[ -e ../"${NAME}/target/oncofuse-1.1.1.jar" ]] && {
        echo "${NAME} already installed"
        exit 0
}

# INSTALLATION
cd $1
git clone "$URL" "$NAME" --branch legacy
chmod -R ugo+rx "${NAME}"
ln -Tsf "$( pwd )/${NAME}" "$ANDURIL_HOME/bundles/sequencing/lib/$NAME"
cd "${NAME}"

wget -O apache-maven.tar.gz "http://www-eu.apache.org/dist/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.tar.gz"
tar -xzvf apache-maven.tar.gz
rm -f apache-maven.tar.gz
apache-maven-3.3.9/bin/mvn clean install
ln -Tsf "$( pwd )/target/oncofuse-1.1.1.jar" "$( pwd )/Oncofuse.jar"
