#!/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh

NAME=blast
VERSION="2.2.26"
URL="ftp://ftp.ncbi.nlm.nih.gov/blast/executables/legacy/2.2.26/blast-2.2.26-x64-linux.tar.gz" 
cd "$( dirname $0 )"
[[ -s ../"${NAME}-${VERSION}/bin/blastall" ]] && {
    echo "${NAME} already installed"
    exit 0
}
cd ..

wget -O $NAME.tgz $URL
tar xvzf $NAME.tgz
rm -f $NAME.tgz
chmod -R ugo+rX "${NAME}-${VERSION}"
ln -Tsf "${NAME}-${VERSION}" $NAME
