#!/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh

NAME=fastq_screen
VERSION="v0.6.3"
URL="http://www.bioinformatics.babraham.ac.uk/projects/fastq_screen/fastq_screen_v0.6.3.tar.gz"
cd "$( dirname $0 )"
[[ -e ../"${NAME}_${VERSION}/fastq_screen" ]] && {
	echo "${NAME} already installed"
	exit 0
}
cd ..
wget -O $NAME.tgz $URL
tar xvzf $NAME.tgz
rm -f $NAME.tgz
chmod -R ugo+rX "${NAME}_${VERSION}"
ln -Tsf "${NAME}_${VERSION}" $NAME
