#!/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh

# USAGE
display_usage() { 
	echo -e "\nUsage:\n\t$0 [DIR] \n" 
	echo -e "\tInstall deFuse in DIR. (Mandatory argument)."
	echo -e "\t-h, --help display this help and exit"
} 
# if less than two arguments supplied, display usage 
if [  $# -le 0 ] 
then 
	display_usage
	exit 1
fi 
# check whether user had supplied -h or --help . If yes display usage 
if [[ ($1 == "--help") ||  $1 == "-h" ]] 
then 
	display_usage
	exit 0
fi 


NAME=deFuse
VERSION="v0.7.0"
URL="https://bitbucket.org/dranew/defuse.git"
# Check if already installed
cd "$( dirname $0 )"
[[ -e ../"${NAME}/scripts/defuse_run.pl" ]] && {
        echo "${NAME} already installed"
        exit 0
}

# INSTALLATION
cd $1
git clone $URL "$NAME-$VERSION"
chmod -R ugo+rx "${NAME}-${VERSION}"
##
ln -Tsf "$( pwd )/${NAME}-${VERSION}" "$( pwd )/${NAME}"
[[ -e $ANDURIL_HOME/bundles/sequencing/lib/deFuse ]]  || {
	ln -Tsf "$( pwd )/${NAME}-${VERSION}" "$ANDURIL_HOME/bundles/sequencing/lib/$NAME"
}
cd "${NAME}-${VERSION}/tools"
make
cd ..

# Modify configuarion file
sed -i -e '/^ensembl_version/s/[0-9][0-9]/84/g' scripts/config.txt		# Ensembl version = 84
sed -i -e '/^ensembl_genome_version/s/[0-9][0-9]/38/g' scripts/config.txt	# Ensembl genome version = GRCh38
sed -i -e '/^ucsc_genome_version/s/[0-9][0-9]/38/g' scripts/config.txt		# UCSC genome version = hg38
# Create database
mkdir db
scripts/defuse_create_ref.pl -d db -c scripts/config.txt
