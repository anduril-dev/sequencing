#!/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh

NAME=bismark
VERSION="v0.16.1"
URL="http://www.bioinformatics.babraham.ac.uk/projects/bismark/bismark_v0.16.1.tar.gz"
cd "$( dirname $0 )"
[[ -e ../"${NAME}_${VERSION}/bismark" ]] && {
	echo "${NAME} already installed"
	exit 0
}
cd ..
wget -O $NAME.tgz $URL
tar xvzf $NAME.tgz
rm -f $NAME.tgz
chmod -R ugo+rX "${NAME}_${VERSION}"
ln -Tsf "${NAME}_${VERSION}" $NAME
