#!/bin/bash

. $ANDURIL_HOME/lang/bash/generic.sh
NAME=express
VERSION=1.5.1
URL="http://bio.math.berkeley.edu/eXpress/downloads/${NAME}-${VERSION}/${NAME}-${VERSION}-linux_x86_64.tgz"
cd "$( dirname $0 )"
[[ -e ../"${NAME}/${NAME}" ]] && {
    echo "${NAME} already installed"
    exit 0
}
cd ..
rm -rf "${NAME}-${VERSION}"
wget -O $NAME.tgz $URL
tar xvzf $NAME.tgz
rm $NAME.tgz
ln -Tsf "${NAME}-${VERSION}-linux_x86_64" $NAME
