#!/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh
NAME=STAR
VERSION="2.5"
URL="https://github.com/alexdobin/STAR/archive/master.tar.gz"
cd "$( dirname $0 )"
[[ -e ../"${NAME}-${VERSION}/source/STAR" ]] && {
    echo "${NAME} already installed"
    exit 0
}
cd ..
rm -rf "${NAME}-${VERSION}"
wget -O - $URL | tar xz
mv "${NAME}-master" "${NAME}-${VERSION}"
ln -Tsf "${NAME}-${VERSION}" $NAME
cd "$NAME"/source
make STAR
