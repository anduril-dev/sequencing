#!/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh

_install_umi() {
    pip install --upgrade cython
    pip install --upgrade pysam
    pip install --upgrade pandas
    pip install --upgrade numpy
    pip install --upgrade future
    pip install --upgrade umi_tools
}

NAME=umi_tools
VERSION="2.5"
cd "$( dirname $0 )"
[[ -e ../"${NAME}/bin/umi_tools" ]] && {
    . ../"${NAME}"/bin/activate
    python -c "import umi_tools" || {
            echo "$NAME Not installed properly"
            _install_umi
    }
    echo "${NAME} already installed"
    exit 0
}
cd ..
virtualenv "${NAME}"
. "${NAME}"/bin/activate
_install_umi
