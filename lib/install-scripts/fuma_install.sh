#!/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh

# USAGE
display_usage() {
        echo -e "\nUsage:\n\t$0 [DIR] \n" 
        echo -e "\tInstall FusionMatcher in DIR. (Mandatory argument)."
        echo -e "\t-h, --help display this help and exit"
}
# if less than two arguments supplied, display usage 
if [  $# -le 0 ]
then
        display_usage
        exit 1
fi
# check whether user had supplied -h or --help . If yes display usage 
if [[ ($1 == "--help") ||  $1 == "-h" ]]
then
        display_usage
        exit 0
fi


NAME=fuma
VERSION="v3.0.2"
URL="https://github.com/yhoogstrate/fuma.git"
# Check if already installed
cd "$( dirname $0 )"
[[ -e ../"${NAME}/bin/fuma" ]] && {
        echo "${NAME} already installed"
        exit 0
}

# INSTALLATION
cd $1
git clone $URL "$NAME-$VERSION"
chmod -R ugo+rx "${NAME}-${VERSION}"
ln -Tsf "$( pwd )/${NAME}-${VERSION}" "$ANDURIL_HOME/bundles/sequencing/lib/$NAME"
cd "${NAME}-${VERSION}"
#git checkout ${VERSION}
cp "$ANDURIL_HOME/bundles/sequencing/components/FusionAnalyzer/resources/Readers.py" "./fuma/Readers.py"
wget -O annotations.gtf.gz "ftp://ftp.ensembl.org/pub/release-84/gtf/homo_sapiens/Homo_sapiens.GRCh38.84.chr.gtf.gz"	#Ensembl annotations v84

#Create BED annotation file
gunzip annotations.gtf.gz
awk '$3!="gene"' annotations.gtf > tmp_annotations.gtf
python setup.py build
python setup.py install --user
$ANDURIL_HOME/bundles/sequencing/components/FusionAnalyzer/resources/fuma-gencode-gtf-to-bed tmp_annotations.gtf > annotations.bed
rm tmp_annotations.gtf
