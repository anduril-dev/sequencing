#!/bin/bash

. $ANDURIL_HOME/lang/bash/generic.sh
NAME=cufflinks
VERSION=2.2.1.Linux_x86_64
URL="http://cole-trapnell-lab.github.io/cufflinks/assets/downloads/${NAME}-${VERSION}.tar.gz"
cd "$( dirname $0 )"
[[ -e ../"${NAME}-${VERSION}/${NAME}" ]] && {
    echo "${NAME} already installed"
    exit 0
}
cd ..
rm -rf "${NAME}-${VERSION}"
wget -O $NAME.tgz $URL
tar xvzf $NAME.tgz
rm $NAME.tgz
ln -Tsf "${NAME}-${VERSION}" $NAME

