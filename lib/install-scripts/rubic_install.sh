#!/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh

NAME=RUBIC
VERSION="1.0.2"
URL="http://ccb.nki.nl/software/rubic/RUBIC_1.0.2.tar.gz"
cd "$( dirname $0 )"
[[ -e "../rubic_Rlibs/RUBIC/R/RUBIC" ]] && {
	echo "${NAME} already installed"
	exit 0
}
cd ..
mkdir rubic_Rlibs
wget https://cran.r-project.org/src/contrib/pracma_1.9.9.tar.gz
R CMD INSTALL -l rubic_Rlibs pracma_1.9.9.tar.gz
rm -f pracma_1.9.9.tar.gz
wget $URL
R CMD INSTALL -l rubic_Rlibs ${NAME}_${VERSION}.tar.gz
rm -f ${NAME}_${VERSION}.tar.gz
wget https://cran.r-project.org/src/contrib/Archive/data.table/data.table_1.9.4.tar.gz
R CMD INSTALL -l rubic_Rlibs data.table_1.9.4.tar.gz
rm -f data.table_1.9.4.tar.gz
wget https://cran.r-project.org/src/contrib/Archive/ggplot2/ggplot2_1.0.1.tar.gz
R CMD INSTALL -l rubic_Rlibs ggplot2_1.0.1.tar.gz
rm -f ggplot2_1.0.1.tar.gz



