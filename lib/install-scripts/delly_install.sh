#!/bin/bash

set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh

NAME=delly
VERSION=0.6.7
URL="https://github.com/tobiasrausch/delly/releases/download/v${VERSION}/delly_v${VERSION}_linux_x86_64bit"
cd "$( dirname $0 )"
[[ -s ../"${NAME}-${VERSION}" ]] && {
    echo "${NAME} already installed"
    exit 0
}
cd ..

echo Downloading...
mkdir -p bin
wget -O "${NAME}-${VERSION}" "$URL"
echo Change access rights...
chmod ugo+rx "${NAME}-${VERSION}"
ln -Tsf ../"${NAME}-${VERSION}" bin/$NAME

