#!/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh

# USAGE
display_usage() { 
	echo -e "\nUsage:\n\t$0 [DIR] \n" 
	echo -e "\tInstall EricScript in DIR. (Mandatory argument)."
	echo -e "\t-h, --help display this help and exit"
} 
# if less than two arguments supplied, display usage 
if [  $# -le 0 ] 
then 
	display_usage
	exit 1
fi 
# check whether user had supplied -h or --help . If yes display usage 
if [[ ($1 == "--help") ||  $1 == "-h" ]] 
then 
	display_usage
	exit 0
fi 


NAME=ericscript
VERSION="0.5.5"
URL="https://sourceforge.net/projects/ericscript/files/ericscript-0.5.5.tar.bz2"
# Check if already installed
cd "$( dirname $0 )"
[[ -e ../"${NAME}/ericscript.pl" ]] && {
        echo "${NAME} already installed"
        exit 0
}

# INSTALLATION
cd $1
wget -O $NAME.tbz $URL
tar xjf $NAME.tbz
rm -f $NAME.tbz
chmod -R ugo+rx "${NAME}-${VERSION}"
##
ln -Tsf "$( pwd )/${NAME}-${VERSION}" "$( pwd )/${NAME}"
[[ -e $ANDURIL_HOME/bundles/sequencing/lib/ericscript ]] || {
	ln -Tsf "$( pwd )/${NAME}-${VERSION}" "$ANDURIL_HOME/bundles/sequencing/lib/$NAME"
}
cd "${NAME}-${VERSION}"

# Install Samtools 0.1.19
SAMTOOLS_VERSION="0.1.19"
URL="http://downloads.sourceforge.net/project/samtools/samtools/${SAMTOOLS_VERSION}/samtools-${SAMTOOLS_VERSION}.tar.bz2"
wget -N $URL
tar xvjf samtools-${SAMTOOLS_VERSION}.tar.bz2
rm -f samtools-${SAMTOOLS_VERSION}.tar.bz2
chmod -R ugo+rx samtools-${SAMTOOLS_VERSION}
cd samtools-${SAMTOOLS_VERSION}
make
PATH=$( pwd ):$PATH
export PATH
cd ..

# (Ftp2Ensembl.sh) ftp command with -p parameter enabling passive mode for data transfers.
mv lib/bash/Ftp2Ensembl.sh lib/bash/Ftp2Ensembl.sh~
cp $ANDURIL_HOME/bundles/sequencing/components/FusionCaller/resources/Ftp2Ensembl.sh lib/bash

# Create genome database
mkdir db
perl ericscript.pl --downdb --refid homo_sapiens -db db --ensversion 95
