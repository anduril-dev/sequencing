#!/bin/bash

set -e

. $ANDURIL_HOME/lang/bash/generic.sh
NAME=samtools
VERSION=1.3.1
URL="http://downloads.sourceforge.net/project/samtools/samtools/${VERSION}/samtools-${VERSION}.tar.bz2"
cd "$( dirname $0 )"
if iscmd "${NAME}"
then
    echo "${NAME} already installed"
else
    echo "${NAME} not found in PATH"
    cd ..
    [ -d ${NAME}-${VERSION} ] && {
        echo "${NAME}-${VERSION} already installed"
        exit 0
    }

    wget -N $URL
    tar xvjf ${NAME}-${VERSION}.tar.bz2
    rm -f ${NAME}-${VERSION}.tar.bz2
    ln -Tfs ${NAME}-${VERSION} ${NAME}
    cd ${NAME}
    make
fi

     
