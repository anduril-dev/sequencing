#!/bin/bash
set -e
trap "echo $NAME INSTALLATION FAILED" 1 9 15
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh
NAME=fastx_toolkit
VERSION="0.0.14"
URL="https://github.com/agordon/fastx_toolkit/releases/download/$VERSION/fastx_toolkit-$VERSION.tar.bz2"
URLGTEXT="http://hannonlab.cshl.edu/fastx_toolkit/libgtextutils-0.6.1.tar.bz2"
cd "$( dirname $0 )"
[[ -e ../"${NAME}/bin/fastx_clipper" ]] && {
    echo "${NAME} already installed"
    exit 0
}
cd ..
rm -rf "${NAME}-${VERSION}"
wget -O - "$URL" | tar xj
ln -Tsf "${NAME}-${VERSION}" $NAME
cd "${NAME}-${VERSION}"
wget -O - "$URLGTEXT" | tar xj
cd libgtextutils-0.6.1
./configure --prefix=$( pwd )/../gtext/
make && make install
cd ..
PKG_CONFIG_PATH=$( pwd )/gtext/lib/pkgconfig:$PKG_CONFIG_PATH ./configure --prefix=$( pwd )
make && make install
