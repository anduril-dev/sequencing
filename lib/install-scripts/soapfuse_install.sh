#!/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh

# USAGE
display_usage() { 
	echo -e "\nUsage:\n\t$0 [DIR] \n" 
	echo -e "\tInstall SOAPfuse in DIR. (Mandatory argument)."
	echo -e "\t-h, --help display this help and exit"
} 
# if less than two arguments supplied, display usage 
if [  $# -le 0 ] 
then 
	display_usage
	exit 1
fi 
# check whether user had supplied -h or --help . If yes display usage 
if [[ ($1 == "--help") ||  $1 == "-h" ]] 
then 
	display_usage
	exit 0
fi 


NAME=SOAPfuse
VERSION="v1.27"
URL="https://sourceforge.net/projects/soapfuse/files/SOAPfuse_Package/SOAPfuse-v1.27.tar.gz"
# Check if already installed
cd "$( dirname $0 )"
[[ -e ../"${NAME}/SOAPfuse-RUN.pl" ]] && {
        echo "${NAME} already installed"
        exit 0
}

# INSTALLATION
cd $1
wget -O $NAME.tgz $URL
tar xvzf $NAME.tgz
rm -f $NAME.tgz
chmod -R ugo+rx "${NAME}-${VERSION}"
##
ln -Tsf "$( pwd )/${NAME}-${VERSION}" "$( pwd )/${NAME}"
[[ -e $ANDURIL_HOME/bundles/sequencing/lib/SOAPfuse ]] || {
	ln -Tsf "$( pwd )/${NAME}-${VERSION}" "$ANDURIL_HOME/bundles/sequencing/lib/$NAME"
}

cd "${NAME}-${VERSION}"
# Setup configuration file
sed -i -e "/^DB_db_dir/s|=|=    $( pwd )\/db|g" config/config.txt
sed -i -e "/^PG_pg_dir/s|=|=    $( pwd )\/source\/bin|g" config/config.txt
sed -i -e "/^PS_ps_dir/s|=|=    $( pwd )\/source|g" config/config.txt

#Create genome database

wget "http://hgdownload.cse.ucsc.edu/goldenPath/hg38/bigZips/hg38.fa.gz"
#wget "ftp://ftp.ensembl.org/pub/release-84/fasta/homo_sapiens/dna/Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz"
wget "ftp://ftp.ensembl.org/pub/release-84/gtf/homo_sapiens/Homo_sapiens.GRCh38.84.chr.gtf.gz"
wget "http://hgdownload.cse.ucsc.edu/goldenPath/hg38/database/cytoBand.txt.gz"
wget "http://www.genenames.org/cgi-bin/genefamilies/download-all/tsv"
wget "https://sourceforge.net/p/soapfuse/wiki/Construct_SOAPfuse_database/attachment/HumanRef_refseg_symbols_relationship.list"

mv tsv HGNC_Gene_Family_Dataset.tsv
gunzip *.gz

PERL5LIB=$PERL5LIB:$( pwd )/source/bin/perl_module; export PERL5LIB
#echo $PERL5LIB
perl "source/SOAPfuse-S00-Generate_SOAPfuse_database.pl" -wg "hg38.fa"  -gtf "Homo_sapiens.GRCh38.84.chr.gtf" -cbd "cytoBand.txt" -gf "HGNC_Gene_Family_Dataset.tsv" -rft "HumanRef_refseg_symbols_relationship.list" -sd "." --dd "db"
