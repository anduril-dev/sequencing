#!/bin/bash

# get Anduril tools
. "$ANDURIL_HOME"/lang/bash/generic.sh || {
	echo "\$ANDURIL_HOME not set!" >&2
	exit 1
}

# halt on error
set -e 

# install tools
rsm_ver='unknown'
rsm_URL="http://csbi.ltdk.helsinki.fi/p/rsm/rsm-${rsm_ver}.tar.gz"
rsm_path="$(dirname "$0")/../../lib/rsm-${rsm_ver}/"
if [ ! -d "$rsm_path" ]; then
	mkdir -p "$rsm_path"
	curl -L "$rsm_URL" | tar -xz -C "$rsm_path"
	(cd "$(dirname "$rsm_path")" && ln -Tsf "$(basename "$rsm_path")" rsm)
fi
