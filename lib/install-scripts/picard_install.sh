#!/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh
NAME=picard
VERSION="2.20.2"
URL="https://github.com/broadinstitute/picard/releases/download/${VERSION}/picard.jar"
cd "$( dirname $0 )"
[[ -e ../"picard.jar" ]] && {
    echo "${NAME} already installed"
    exit 0
}
cd ..
wget $URL
