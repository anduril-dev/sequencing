// ***************************************************************
// -- DNA Sequencing Network --
//
// - The purpose of the network is to test the functionality
// of the dna part of the sequencing bundle
// 
// Subnetworks:
// - setup: input files and parameters
// - process: alignment procedures
// - call: calling of variants
// - filter: filtering and annotation
//
// @updated 5.2.2014
// @author Rony Lindell
// ***************************************************************

include "setup.and"
include "process.and"
include "call.and"
include "filter.and"
