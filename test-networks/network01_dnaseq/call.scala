// ***************************************************************
// Functions used:
// - VariantCaller: call the snps and indels
// - VariantValidator: validate the format of the calls
// ***************************************************************

// Call variants
calls = VariantCaller(
    reference=reference,
    bam1=reduced,
    dbsnp=dbsnp,
    memory=memory,
    caller=caller,
    variantsOnly=variants_only,
    options=caller_options
    )
OUTPUT(calls.metrics)

// Validate the variants for any vilation of the VCF format
calls_validation = VariantValidator(
    reference=reference,
    variants=calls.snp,
    memory=memory
    )
OUTPUT(calls_validation)
