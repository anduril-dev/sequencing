# README #

Bundle for DNA/RNA sequencing analysis (NGS).  

Components in this bundle can be found in
http://www.anduril.org/anduril/bundles/sequencing/doc/

### How do I get set up? ###

After installing Anduril, run
	anduril install sequencing

### Tutorials ###

Tutorials can be found from Wiki 
https://bitbucket.org/anduril-dev/sequencing/wiki/

### Additional links ###

Anduril webpage including documentation and user guide
http://anduril.org/site/

See also Snippets for additional help
https://bitbucket.org/snippets/anduril-dev/

Discussion forum
https://groups.google.com/forum/m/?fromgroups#!forum/anduril-dev
