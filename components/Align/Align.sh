# running functions.sh 
set -e 
source "$ANDURIL_HOME/lang/bash/functions.sh"
export_command

bwa_str="bwa mem -M -t ${parameter_threads} -R"
rg_str="\"@RG\\tID:${parameter_ID}\\tPL:${parameter_platform}\\tLB:${parameter_library}\\tSM:${parameter_sample}\""

echo -e "RG tag is ${rg_str}"

bwa_str="${bwa_str} ${rg_str}"
if [ "${parameter_options}" != "" ]
then
	bwa_str="${bwa_str} ${parameter_options}"
fi

if [ "${parameter_path}" != "" ]
then
	bwa_str="${parameter_path}/${bwa_str}"
fi

bwa_str="${bwa_str} ${input_reference} ${input_reads} ${input_mates} > ${output_out}"

eval ${bwa_str}
