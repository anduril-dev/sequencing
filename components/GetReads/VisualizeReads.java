import java.util.List;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;

import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.IndexFile;
import org.anduril.component.SkeletonComponent;

import net.sf.samtools.Cigar;
import net.sf.samtools.CigarElement;
import net.sf.samtools.CigarOperator;
import net.sf.samtools.SAMFileReader;
import net.sf.samtools.SAMFileReader.ValidationStringency;
import net.sf.samtools.SAMRecord;
import net.sf.samtools.SAMRecordIterator;
import net.sf.samtools.TextCigarCodec;


public class VisualizeReads extends SkeletonComponent{

	
	protected ErrorCode runImpl(CommandFile cf){
		
		int pos = cf.getIntParameter("position");
		String chr = cf.getParameter("chromosome");
		
		try{
			
			BufferedWriter out = new BufferedWriter(new FileWriter(cf.getOutput("reads")));
			
			if(cf.getInput("alignment") != null){
			
				SAMFileReader sr = new SAMFileReader(cf.getInput("alignment"));
			
				writeReads(out, sr, chr, pos);
				
			}else if(cf.getInput("array") != null){
				
				IndexFile index = IndexFile.read(cf.getInputArrayIndex("array"));
				
				Iterator<String> iter = index.iterator();
				
				while(iter.hasNext()){
					
					String key = iter.next();
					out.write(key);
					out.newLine();
					
					SAMFileReader sr = new SAMFileReader(index.getFile(key));
					writeReads(out, sr, chr, pos);
					
					out.newLine();
				}
			}else{
				System.out.println("No input files");
			}
			out.close();
		}catch (IOException e) {
			e.printStackTrace();
			return ErrorCode.OUTPUT_IO_ERROR;
		}
		
		
		
		
		return ErrorCode.OK;
	}
	
	public static void main (String [] args){
		new VisualizeReads().run(args);
	}
	
	private void writeReads(BufferedWriter out, SAMFileReader sr, String chr, int pos) throws IOException{

		sr.setValidationStringency(ValidationStringency.SILENT);

		SAMRecordIterator iterator = sr.iterator();

		TreeMap<Integer, List<SAMRecord>> sRecs = new TreeMap<Integer, List<SAMRecord>>();

		boolean breakIter = false;
		while(iterator.hasNext()){
			SAMRecord rec = iterator.next();

			if(rec.getAlignmentStart() <= pos && rec.getAlignmentEnd() >= pos && rec.getReferenceName().equals(chr)){

				if(!sRecs.containsKey(rec.getAlignmentStart())){
					sRecs.put(rec.getAlignmentStart(), new ArrayList<SAMRecord>());
				}
				sRecs.get(rec.getAlignmentStart()).add(rec);
				breakIter = true;

			}else if( breakIter && (rec.getAlignmentEnd() < pos || !rec.getReferenceName().equals(chr)))
				break;

		}


		ArrayList<Integer> reference = new ArrayList<Integer>();
		TreeMap<Integer, Set<CigarElement>> cigarCollec = new TreeMap<Integer, Set<CigarElement>>();

		TextCigarCodec ciCod = new TextCigarCodec();

		for(Integer i : sRecs.keySet()){
			for(SAMRecord sRecord : sRecs.get(i)){

				Cigar ci = ciCod.decode(sRecord.getCigarString());

				int length = 0;

				length = 0;

				for(CigarElement ce : ci.getCigarElements()){

					CigarOperator co = ce.getOperator();

					//here we collect all the cigar collections that these reads have

					int index = reference.indexOf(sRecord.getAlignmentStart() + length-1);

					if(co == CigarOperator.INSERTION){

						for(int len = 1; len <= ce.getLength(); len ++){


							if(index + len >= reference.size()){
								reference.add(-1);	
							}else if(reference.get(index + len) != -1){
								reference.add(index + len, -1);	
							}

						}
					}else{ //for matches, mismatches, deletions, soft clippings
						for(int len = 0; len < ce.getLength(); len ++){

							if(!reference.contains(sRecord.getAlignmentStart()+length + len)){

								reference.add(sRecord.getAlignmentStart()+length + len);

							}
						}
						length = length + ce.getLength();
					}


				}

			}
		}

		for(Integer seqPos : cigarCollec.keySet()){
			for(CigarElement ce: cigarCollec.get(seqPos)){
				System.out.print(ce.getOperator());
			}			
		}
		for(Integer position : reference){

			if(position == 0){
				out.write("|");
				//System.out.print("|");
			}else if(position == -1){

				out.write("-");
				//System.out.print("-");
			}else{
				out.write("M");
				//System.out.print("M");
			}
		}

		//System.out.println("");
		out.newLine();



		for(Integer i : sRecs.keySet()){
			for(SAMRecord sRecord : sRecs.get(i)){

				StringBuffer read = new StringBuffer();

				if(pos - sRecord.getAlignmentStart() > 0)
					read.append(sRecord.getReadString().substring(0, pos - sRecord.getAlignmentStart()));

				read.append(sRecord.getReadString().substring(pos - sRecord.getAlignmentStart(), pos - sRecord.getAlignmentStart() + 1).toLowerCase());
				if(pos < sRecord.getAlignmentEnd())
					read.append(sRecord.getReadString().substring(pos - sRecord.getAlignmentStart() + 1));

				StringBuffer sb = new StringBuffer();

				int index = reference.indexOf(sRecord.getAlignmentStart());

				for(int j = 0; j < index; j++)
					sb.append(" ");

				Cigar ci = ciCod.decode(sRecord.getCigarString());

				int length = 0;
				for(CigarElement ce : ci.getCigarElements()){

					CigarOperator co = ce.getOperator();

					//make gap if this if not an insertion
					while(co != CigarOperator.INSERTION && reference.get(sb.length()) == -1)
						sb.append("-");	

					if(co == CigarOperator.MATCH_OR_MISMATCH || co == CigarOperator.INSERTION){

						for(int len = length; len < length + ce.getLength(); len ++){

							while(co != CigarOperator.INSERTION && reference.get(sb.length()) == -1)
								sb.append("-");	

							sb.append(read.charAt(len));//read.substring(length, length + ce.getLength()));
						}
						length = length + ce.getLength();

					}else if(co == CigarOperator.DELETION ){

						for(int len = 0; len < ce.getLength(); len ++){
							sb.append("-");						
						}

					}else if(co == CigarOperator.SKIPPED_REGION){
						for(int len = length; len < length + ce.getLength(); len ++){

							sb.append("i");	
						}
					}
				}
				//System.out.println(sb);
				out.write(sb.toString());
				out.newLine();

			}
		}
	}
}
