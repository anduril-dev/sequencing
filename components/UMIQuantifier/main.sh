# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/lang/bash/functions.sh"
export_command
#set -ex

input=$( getinput alignment )
#$input_alignment
annotation=$( getinput annotation )
myFolder=$( getoutput folder )
#$output_folder
dedupParams=$( getparameter dedupParams )
#$parameter_dedupParams
UMIlen=$( getparameter UMIlen)
featureCountsParams=$( getparameter featureCountsParams )
picard=$( getparameter picard )
table=$( getoutput table )
corrected=$( getoutput corrected)
rmdupAlign=$( getoutput rmdupAlignment )
sampleID=$( getparameter sampleID )
tmpDir=$( gettempdir )

mkdir "$myFolder"
mkdir ${myFolder}/UMItool-dedup
mkdir ${myFolder}/featureCounts

UMIFolder=${myFolder}/UMItool-dedup
fcFolder=${myFolder}/featureCounts

if [[ $input =~ \.bam$ ]]
then    
    rmdupUnsorted=${UMIFolder}/rmdup_unsorted.bam
    rmdup=${UMIFolder}/rmdup.bam
else        
    rmdupUnsorted=${UMIFolder}/rmdup_unsorted.sam
    rmdup=${UMIFolder}/rmdup.sam    
fi
log=${UMIFolder}/Log.log
error=${UMIFolder}/error.txt

#### run UMI-tools ####
echo "UMItools"
echo "Alignment: "$input
echo "Parameters: "$dedupParams
echo "Output folder: "$UMIFolder

#cd "$tmpDir"
cd "$UMIFolder"
#command="umi_tools dedup $dedupParams -I $input -S $rmdupUnsorted > \"${UMIFolder}\"/stats.txt"
#command="umi_tools dedup $dedupParams --output-stats=$sampleID --further-stats -L $log -E $error -I $input -S $rmdupUnsorted"
command="umi_tools dedup $dedupParams -L $log -E $error -I $(realpath $input) -S $rmdupUnsorted"
echo $command
../../lib/umi_tools/bin/$command
command="java -Xmx4g -jar $picard SortSam VALIDATION_STRINGENCY=SILENT SORT_ORDER=coordinate CREATE_INDEX=true CREATE_MD5_FILE=true INPUT=$rmdupUnsorted OUTPUT=$rmdup TMP_DIR=$( gettempdir )"
echo $command
../../lib/umi_tools/bin/$command

#### run featureCounts ####
echo "Now in featureCounts"
echo "input: "$rmdup
echo "Parameters: "$featureCountsParams
echo "Output folder: "$fcFolder

command="featureCounts -a "$annotation" "$featureCountsParams" -o "${fcFolder}/counts.csv" "$rmdup
echo $command
../../lib/umi_tools/bin/$command

#fcOut=${fcFolder}/counts.csv
#### get raw UMI counts ####
tail -n +3 ${fcFolder}/counts.csv |awk -v sampleID="$sampleID" -F"\t" 'BEGIN {OFS="\t"; print "geneID",sampleID}{print $1,$7}' > $table

#### correct UMI counts for collision probabilty####
tail -n +3 ${fcFolder}/counts.csv |awk -v sampleID="$sampleID" -v UMIlen="$UMIlen" -F"\t" 'BEGIN {OFS="\t"; print "geneID",sampleID}{lnC=log(1-$7/4^UMIlen); corrC=-4^UMIlen*lnC; print $1,corrC}' > $corrected
mv *featureCounts ${fcFolder}/readsAssign.csv
ln -s $rmdup $rmdupAlign
