#!/usr/bin/python
import anduril
from anduril.args import *
# TODO: fix import using pip install pyGeno
#from pyGeno.tools.parsers.GTFTools import GTFFile
from GTFTools import GTFFile

# Parse parameter of fields to extract from GFT file
gtf_fields = [i.strip() for i in fields.split(',')]

# Read GTF file, might take a while for the full Genome
in_file = GTFFile(input_in)

# TODO: store only unique
out_file=anduril.TableWriter(output_out,fieldnames=gtf_fields)
for in_row in in_file :
    out_row = []
    # Get the value for each field from the list given as parameter
    for field in gtf_fields:
      try:
    	  out_row.append( in_row[field] )
      except:
        out_row.append("NA")
    # Store the row to tableout
    out_file.writerow( out_row )
    #tableout.append( { "gene_id": line["gene_id"], "gene_name": line["gene_name", "gene_biotype": line["gene_biotype"} )




