import gzip, re

def split_unquoted(s, split = ' ', quote = '"', strip_quotes = True):
	toks = re.findall('((?:{qu}[^{qu}]*{qu}|[^{sp}{qu}])+)'.format(
		sp = re.escape(split), qu = re.escape(quote)), s)
	if strip_quotes:
		toks = [tok.replace(quote, '') for tok in toks]
	return toks

class GTFEntry(object) :
	def __init__(self, gtfFile, lineNumber) :
		"""A single entry in a GTF file"""
		
		self.lineNumber = lineNumber
		self.gtfFile = gtfFile
		self.data = gtfFile.lines[lineNumber].split('\t')
		proto_atts = split_unquoted(self.data[gtfFile.legend['attributes']].strip(), split = ';', strip_quotes = False)
		atts = {}
		for a in proto_atts :
			sa = split_unquoted(a, ' ')
			atts[sa[0]] = ' '.join(sa[1:])
		self.data[gtfFile.legend['attributes']] = atts
	
	def __getitem__(self, k) :
		try :
			return self.data[self.gtfFile.legend[k]]
		except KeyError :
			try :
				return self.data[self.gtfFile.legend['attributes']][k]
			except KeyError :
				#return None
				raise KeyError("Line %d does not have an element %s.\nline:%s\n" %(self.lineNumber, k, self.gtfFile.lines[self.lineNumber]))
	
	def __repr__(self) :
		return "<GTFEntry line: %d>" % self.lineNumber
	
	def __str__(self) :
		return  "<GTFEntry line: %d, %s>" % (self.lineNumber, str(self.data))

def chomp(l):
	return re.sub(r'\r?\n?$', '', l)

class GTFFile(object) :
	"""This is a simple GTF2.2 (Revised Ensembl GTF) parser, see http://mblab.wustl.edu/GTF22.html for more infos"""
	def __init__(self, filename, gziped = False) :
		
		self.filename = filename
		self.legend = {'seqname' : 0, 'source' : 1, 'feature' : 2, 'start' : 3, 'end' : 4, 'score' : 5, 'strand' : 6, 'frame' : 7, 'attributes' : 8}

		if gziped : 
			f = gzip.open(filename)
		else :
			f = open(filename)
		
		self.lines = []
		for l in f :
			l = chomp(l)
			l = re.sub(r'#.*$', '', l)
			if l != '' :
				self.lines.append(l)
		f.close()
		
		self.currentIt = -1

	def get(self, line, elmt) :
		"""returns the value of the field 'elmt' of line 'line'"""
		return self[line][elmt]

	def __iter__(self) :
		self.currentPos = -1
		return self

	def next(self) :
		self.currentPos += 1
		try :
			return GTFEntry(self, self.currentPos)
		except IndexError:
			raise StopIteration

	def __getitem__(self, i) :
		"""returns the ith entry"""
		if self.lines[i].__class__ is not GTFEntry :
			self.lines[i] = GTFEntry(self, i)
		return self.lines[i]

	def __repr__(self) :
		return "<GTFFile: %s>" % (os.path.basename(self.filename))

	def __str__(self) :
		return "<GTFFile: %s, gziped: %s, len: %d>" % (os.path.basename(self.filename), self.gziped, len(self))
	
	def __len__(self) :
		return len(self.lines)
