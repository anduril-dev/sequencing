#!/usr/bin/perl 

my ($INP_F) = @ARGV;

open INP_HANDLE, "<", $INP_F or die $!;

while($line = <INP_HANDLE>){
	if($line =~ /^#/){
		next;
	}else{
		@tmp_line = split("\t",$line);
		($gene_biotype) = $tmp_line[8]=~ /gene_biotype \"(.*?)\";/m;
		if(($tmp_line[2] ne "gene") && ($tmp_line[2] ne "transcript")){
			print "chr$tmp_line[0]\t$gene_biotype\t$tmp_line[2]\t$tmp_line[3]\t$tmp_line[4]\t$tmp_line[5]\t$tmp_line[6]\t$tmp_line[7]\t$tmp_line[8]";
		}
	}
}
close INP_HANDLE;
