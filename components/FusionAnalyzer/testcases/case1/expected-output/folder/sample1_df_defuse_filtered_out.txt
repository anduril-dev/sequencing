gene1_name	gene2_name	chr1	bp1	gene_strand1	chr2	bp2	gene_strand2	span_cnt	tot_cnt	sample	note
CDK12	RP11-390P24.1	17	39556413	+	17	39566788	+	5	1.40361007379181	sample1_df	inconsistency with Ensembl database
PWRN1	NPAP1	15	24624103	+	15	24746702	+	8	2.18896332936579	sample1_df	inconsistency with Ensembl database
RP11-30J20.1	RNU6-144P	8	136984937	+	8	137074251	+	45	1.37019078632057	sample1_df	not annotated genes
