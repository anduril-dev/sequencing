#!/bin/bash
set -e
cd $( dirname $0 )

cd resources
# Download if one of the following missing:
[[ -f geneid.Rdata && -f GO.Rdata && -f homolog_table.Rdata ]] || Rscript download.R
cd ../../../lib/install-scripts

for installer in pegasus_install.sh oncofuse_install.sh fuma_install.sh; do
  echo "You need to manually run $( readlink -f $installer ) TARGET_PATH"
done
echo "These libraries take a lot of space, and therefore not automatically installed"  
  
  
