#!/usr/bin/perl
#Filtering fusions with spanning reads supporting the junction less then "filter" parameter
#Filtering non-sense fusions

my ($INP_FILE,$ID,$FILTER_CNT,$OUT_FILE,$LOG_FILE) = @ARGV;

open INP_FILE_H, "<", "$INP_FILE" or die $!;
open OUT_FILE_H, ">", "$OUT_FILE" or die $!;
open LOG_FILE_H, ">>", "$LOG_FILE" or die $!;
$line = <INP_FILE_H>;    # Header line
if (-z $OUT_FILE) {
  print OUT_FILE_H "gene1_name\tgene2_name\tchr1\tbp1\tgene_strand1\tchr2\tbp2\tgene_strand2\tspan_cnt\ttot_cnt\tsample\n";
}  
while($line = <INP_FILE_H>){
	@tmp_line = split("\t",$line);
	$gene_name1 = $tmp_line[30];	#gene_name1
	$gene_name2 = $tmp_line[31];	#gene_name2
	$gene_chr1 = $tmp_line[24];	#gene_chromosome1
	$gene_chr2 = $tmp_line[25];	#gene_chromosome2
	$gene_strand1 =	$tmp_line[34];	#gene_strand1
	$gene_strand2 =	$tmp_line[35];	#gene_strand2
	$gene_align_strand1 = $tmp_line[22];	#gene_align_strand1
	$gene_align_strand2 = $tmp_line[23];	#gene_align_strand2
	$gene_bp1 = $tmp_line[37];	#genomic_break_pos1
	$gene_bp2 = $tmp_line[38];	#genomic_break_pos2
	$span_cnt = $tmp_line[2];	#splitr_count
	$tot_cnt = $tmp_line[60]; #span_count
  if ($tot_cnt eq "0") {
    $tot_cnt = 0   #if tot_cnt is a string and not number we make it zero
  }
              

	
	if($span_cnt>=$FILTER_CNT){
		if($gene_align_strand1 eq "+"){
			if($gene_align_strand2 eq "-"){
				print OUT_FILE_H "$gene_name1\t$gene_name2\t$gene_chr1\t$gene_bp1\t$gene_strand1\t$gene_chr2\t$gene_bp2\t$gene_strand2\t$span_cnt\t$tot_cnt\t$ID\n";	#5'-gene1|gene2-3'
			}
		}else{
			if($gene_align_strand2 eq "+"){
				print OUT_FILE_H "$gene_name2\t$gene_name1\t$gene_chr2\t$gene_bp2\t$gene_strand2\t$gene_chr1\t$gene_bp1\t$gene_strand1\t$span_cnt\t$tot_cnt\t$ID\n";        #5'-gene2|gene1-3'
			}
		}
	}else{
		print LOG_FILE_H "$gene_name2\t$gene_name1\t$gene_chr2\t$gene_bp2\t$gene_strand2\t$gene_chr1\t$gene_bp1\t$gene_strand1\t$span_cnt\t$tot_cnt\t$ID\n";    
  }
}
close INP_FILE_H;
close OUT_FILE_H;
close LOG_FILE_H;
