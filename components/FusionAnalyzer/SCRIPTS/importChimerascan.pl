#!/usr/bin/perl
#filtering fusions with spanning reads supporting the junction less then "filter" parameter
#Transoforing chimerascan bp coordinates from 0-based to 1-based

my ($INP_FILE,$ID,$FILTER_CNT,$OUT_FILE,$LOG_FILE) = @ARGV;

open INP_FILE_H, "<", "$INP_FILE" or die $!;
open OUT_FILE_H, ">>", "$OUT_FILE" or die $!;
open LOG_FILE_H, ">>", "$LOG_FILE" or die $!;
$line = <INP_FILE_H>;    # Header line
if (-z $OUT_FILE) {
  print OUT_FILE_H "gene1_name\tgene2_name\tchr1\tbp1\tgene_strand1\tchr2\tbp2\tgene_strand2\tspan_cnt\ttot_cnt\tsample\n";
}
while($line = <INP_FILE_H>){
	@tmp_line = split("\t",$line);
	$gene_name1 = $tmp_line[12];	#genes5p
	$gene_name2 = $tmp_line[13];	#genes3p
	$gene_chr1 = $tmp_line[0];
	$gene_chr1 =~ s/chr//;		#chrom5p
	$gene_chr2 = $tmp_line[3];
	$gene_chr2 =~ s/chr//;		#chrom3p
	$gene_strand1 =	$tmp_line[8];	#strand5p
	$gene_strand2 =	$tmp_line[9];	#strand3p
	$gene_start1 = $tmp_line[1]+1;	#start5p
	$gene_start2 = $tmp_line[4]+1;	#start3p
	$gene_end1 = $tmp_line[2]+1;	#end5p
	$gene_end2 = $tmp_line[5]+1;	#end3p
	$span_cnt = $tmp_line[17];	#spanning_frags
  $tot_cnt = $tmp_line[16]-$span_cnt; #total_frags-spanning_frags 

  if ($tot_cnt eq "0") {
    $tot_cnt = 0   #if tot_cnt is a string and not number we make it zero
  }
  

	if($gene_strand1 eq "+"){
		$gene_bp1 = $gene_end1;
	}else{  
        	$gene_bp1 = $gene_start1;
	}

  if($gene_strand2 eq "-"){
   	$gene_bp2 = $gene_end2;
	}else{  
		$gene_bp2 = $gene_start2;
	}
	
  @gene_names1=split(",",$gene_name1);
	@gene_names1=split(",",$gene_name1);
	if($span_cnt>=$FILTER_CNT){
		if((scalar @gene_names1>1) || (scalar @gene_names2>1)){
			foreach $name1 (@gene_names1){
				foreach $name2 (@gene_names2){
					print OUT_FILE_H "$name1\t$name2\t$gene_chr1\t$gene_bp1\t$gene_strand1\t$gene_chr2\t$gene_bp2\t$gene_strand2\t$span_cnt\t$tot_cnt\t$ID\n";
				}
			}
    }else{
			print OUT_FILE_H "$gene_name1\t$gene_name2\t$gene_chr1\t$gene_bp1\t$gene_strand1\t$gene_chr2\t$gene_bp2\t$gene_strand2\t$span_cnt\t$tot_cnt\t$ID\n";
    }
	}else {
     print LOG_FILE_H "$gene_name1\t$gene_name2\t$gene_chr1\t$gene_bp1\t$gene_strand1\t$gene_chr2\t$gene_bp2\t$gene_strand2\t$span_cnt\t$tot_cnt\t$ID\n";
  }
}
close INP_FILE_H;
close OUT_FILE_H;
close LOG_FILE_H;
