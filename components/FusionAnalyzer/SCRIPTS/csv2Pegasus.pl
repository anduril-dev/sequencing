#!/usr/bin/perl
#Usage: csv2general.pl path/to/input_dir path/to/output_dir

my ($INP_DIR,$OUT_DIR) = @ARGV;

if (not defined $INP_DIR) {
  die "Need INPUT DIR path\n";
}
if (not defined $OUT_DIR) {
  die "Need OUTPUT DIR path\n";
}

opendir($INP_DIR_H, $INP_DIR) || die "can't opendir $INP_DIR: $!";
@INP_reports = readdir($INP_DIR_H);
closedir $INP_DIR_H;

for ($r=0;$r<@INP_reports;$r++){
	if ($INP_reports[$r] eq '.' or $INP_reports[$r] eq '..') {	# to avoid reading . and .. dirs
 		   next;
	}
	open NEW_REP_H, ">", "$OUT_DIR/$INP_reports[$r]" or die $!;
	open CSV_REP_H, "<", "$INP_DIR/$INP_reports[$r]" or die $!;
	$line = <CSV_REP_H>;	# read header
	print NEW_REP_H "#5p_symbol\t5p_ensembl\t5p_strand\t5p_chr\t3p_symbol\t3p_ensembl\t3p_strand\t3p_chr\t5p_start\t5p_end\tbp\t3p_start\t3p_end\tsplit_reads\ttot_reads\n"; # write header
	OUTER: while($line = <CSV_REP_H>){
		chomp($line);
	        @tmp_line = split("\t",$line);
		$gene_name1 = $tmp_line[0];
		$gene_name2 = $tmp_line[1];
		$gene_id1 = $tmp_line[2];
		$gene_id2 = $tmp_line[3];
		($gene_start1,$gene_end1,$gene_strand1) = split(":",$tmp_line[4]);
		($gene_start2,$gene_end2,$gene_strand2) = split(":",$tmp_line[5]);
		($tmp_coord1,$tmp_coord2) = split("-",$tmp_line[6]);
		($gene_chr1,$gene_bp1) = split(":",$tmp_coord1);
		($gene_chr2,$gene_bp2) = split(":",$tmp_coord2);
		$span_cnt = $tmp_line[7];
		$encompassing_cnt = $tmp_line[8];
	
    if(($gene_strand1 eq "1") || ($gene_strand1 eq "+")) {
      $gene_end1 = $gene_bp1;
			$gene_strand1 = "+";
    }else{  
      $gene_start1 = $gene_bp1;
			$gene_strand1 = "-";
    }

    if(($gene_strand2 eq "-1") || ($gene_strand2 eq "-")){
      $gene_end2 = $gene_bp2;
			$gene_strand2 = "-";
    }else{  
     	$gene_start2 = $gene_bp2;
			$gene_strand2 = "+";
    }
    print NEW_REP_H $gene_name1."\t".$gene_id1."\t".$gene_strand1."\t".$gene_chr1."\t".$gene_name2."\t".$gene_id2."\t".$gene_strand2."\t".$gene_chr2."\t".$gene_start1."\t".$gene_end1."\t|\t".$gene_start2."\t".$gene_end2."\t"." spanning: $span_cnt"."\t"." encompassing: $encompassing_cnt"."\n";
	}
	close NEW_REP_H;
	close CSV_REP_H;
}
