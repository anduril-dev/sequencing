#!/usr/bin/Rscript

#library(biomaRt)
options(width=200)

args <- commandArgs(TRUE)
var1 <- args[1]	#FusionMatcher output "extensive"
OUT_DIR <- args[2]	#output directory
resource_path<-args[3]   # pre downloaded data from biomart
id_tool<-args[4]
LOG_DIR<-args[5]

## Set the output dir of the pipeline
fusions <- read.table(var1,header=TRUE,sep="\t")
if (nrow(fusions)==0) {
  print("No fusions left")
} else {

## TODO: Try/catch  -> download / error
#~ print("#RETRIEVING DATA FROM DATABASE")
load( paste(resource_path,"homolog_table.Rdata",sep="/") )
load( paste(resource_path,"GO.Rdata",sep="/") )
load( paste(resource_path,"geneid.Rdata",sep="/") )

#FILTERING
fusions_sim_genes<-vector()
reason <- vector()
genes<-list()
genes[[1]]<-fusions[,1]
genes[[2]]<-fusions[,2]

names(homolog_table)[names(homolog_table) == "gene_name_of_homolog" ] <- "homolog_name_gene1"
names(homolog_table)[names(homolog_table) == "gene_name_of_id" ] <- "gene1_name"
#this is a reduced version of the original homolog table
new_homolog_table<-homolog_table[ homolog_table$gene1_name %in% genes[[1]], ]

GO_gene1 <- GO[ GO$external_gene_name %in% genes[[1]], ]
GO_gene2 <- GO[ GO$external_gene_name %in% genes[[2]], ]

gene_name_id1<-gene_name_id[ gene_name_id$external_gene_name %in% genes[[1]], ]
gene_name_id2<-gene_name_id[ gene_name_id$external_gene_name %in% genes[[2]], ]

#~ print("#Finished RETRIEVING DATA FROM DATABASE")

fusions_filtered_in<-list()
fusions_filtered_out<-list()
for(j in 1:length(fusions[,1])){
  print(paste("gene",j,"of ",length(fusions[[1]]),sep=" "))
	#FILTERING FUSIONS WITH HOMOLOGOUS GENES
	my_homologs_table.homologs_name<-new_homolog_table$homolog_name_gene1[new_homolog_table$gene1_name %in% fusions[j,1]]
    if (any(my_homologs_table.homologs_name %in% fusions[j,2])) {
        # at least one found, skip to next iteration
        fusions_filtered_out=rbind(fusions_filtered_out, fusions[j,])
        reason=append(reason,"homologous")
        next
    }
    if (fusions[j,1] == fusions[j,2]) {                                                                                 
        # at least one found, skip to next iteration 
        fusions_filtered_out=rbind(fusions_filtered_out, fusions[j,])
        reason=append(reason,"same gene")
        next                                                                             
    }    
    # check if both without function, skip
	#Filtering fusions with functionally non annotated genes
    
    go1_res<-GO_gene1$external_gene_name %in% fusions[j,1]
    go2_res<-GO_gene2$external_gene_name %in% fusions[j,2]
    if ( !any(go1_res) && !any(go2_res) ) {
        fusions_filtered_out=rbind(fusions_filtered_out, fusions[j,])
        reason=append(reason,"not annotated genes")
        next
    }
    # filter out if inconsistency with ensembl
    g1_id=gene_name_id1[ ( gene_name_id1$external_gene_name == fusions[j,1] &  # name matches
                           gene_name_id1$chromosome_name == fusions$chr1[j] &     # chromosome matches
                           gene_name_id1$start_position <= fusions$bp1[j] &       # starts before breakpoint
                           gene_name_id1$end_position >= fusions$bp1[j]           # ends after breakpoint
                         ),]
    g2_id=gene_name_id2[ ( gene_name_id2$external_gene_name == fusions[j,2] &  # name matches
                           gene_name_id2$chromosome_name == fusions$chr2[j] &     # chromosome matches
                           gene_name_id2$start_position <= fusions$bp2[j] &       # starts before breakpoint
                           gene_name_id2$end_position >= fusions$bp2[j]           # ends after breakpoint
                         ),]
    if (nrow(g1_id)==0 | nrow(g2_id)==0) {
        fusions_filtered_out=rbind(fusions_filtered_out, fusions[j,])
        reason=append(reason,"inconsistency with Ensembl database")
        print(paste("inconsistency with Ensembl database: ",gene_name_id1$external_gene_name,gene_name_id1$chromosome_name,gene_name_id1$start_position,gene_name_id1$end_position,gene_name_id2$external_gene_name,gene_name_id2$chromosome_name,gene_name_id2$start_position,gene_name_id2$end_position,sep=","))
        next
    }
    # Include fusion
    fusions_filtered_in=rbind(fusions_filtered_in,
                             cbind(fusions[j,],
                                    ensembl_gene_id1=g1_id$ensembl_gene_id[1],
                                    start_position1=g1_id$start_position[1],
                                    end_position1=g1_id$end_position[1],
                                    ensembl_gene_id2=g2_id$ensembl_gene_id[1],
                                    start_position2=g2_id$start_position[1],
                                    end_position2=g2_id$end_position[1]
                               ))
}
#~ print("Input fusions")
#~ print(fusions[[1]])
#~ print("Included fusions")
#~ print(fusions_filtered_in)
#~ print("Excluded fusions")
#~ print(cbind(fusions_filtered_out,note=reason))
filenameDiscard <- paste(id_tool,"discarded.txt",sep="_")
filenameKeep <- paste(id_tool,"kept.csv",sep="_")

write.table(cbind(fusions_filtered_out,note=reason), 
           file=paste(LOG_DIR,filenameDiscard,sep="/"),
           sep="\t",  row.names=F,quote=FALSE)
write.table(fusions_filtered_in,
           file=paste(OUT_DIR,filenameKeep,sep="/"),
           row.names=F, sep="\t", quote=FALSE)
}
