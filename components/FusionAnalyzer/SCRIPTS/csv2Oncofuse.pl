#!/usr/bin/perl
#Usage: csv2oncofuse.pl path/to/report.csv path/to/new_report.csv library

my ($INP_DIR,$OUT_DIR,$library) = @ARGV;

if (not defined $INP_DIR) {
  die "Need csv report file path\n";
}
if (not defined $OUT_DIR) {
  die "Need New report file path\n";
}
if (not defined $library) {
  die "Need sample type\n";
}

opendir($INP_DIR_H, $INP_DIR) || die "can't opendir $INP_DIR: $!";
@INP_reports = readdir($INP_DIR_H);
closedir $INP_DIR_H;

for($r=0;$r<@INP_reports;$r++){
	if ($INP_reports[$r] eq '.' or $INP_reports[$r] eq '..') {      # to avoid reading . and .. dirs
                   next;
        }
	open NEW_REP_H, ">", "$OUT_DIR/$INP_reports[$r]" or die $!;
        open CSV_REP_H, "<", "$INP_DIR/$INP_reports[$r]" or die $!;

	print NEW_REP_H "#chr1\tcoord1\tchr2\tcoord2\ttissue\tspanning\tencompassing\n";
	$line = <CSV_REP_H>;	# Header line
	while($line = <CSV_REP_H>){
		chomp($line);
	        @tmp_line = split("\t",$line);
	
		$gene_name1 = $tmp_line[0];
                $gene_name2 = $tmp_line[1];
                $gene_id1 = $tmp_line[2];
                $gene_id2 = $tmp_line[3];
                ($gene_start1,$gene_end1,$gene_strand1) = split(":",$tmp_line[4]);
                ($gene_start2,$gene_end2,$gene_strand2) = split(":",$tmp_line[5]);
                ($tmp_coord1,$tmp_coord2) = split("-",$tmp_line[6]);
                ($gene_chr1,$gene_bp1) = split(":",$tmp_coord1);
                ($gene_chr2,$gene_bp2) = split(":",$tmp_coord2);
		$span_cnt = $tmp_line[7];
		$encompassing_cnt = $tmp_line[8];

	        print NEW_REP_H $gene_chr1."\t".$gene_bp1."\t".$gene_chr2."\t".$gene_bp2."\t".$library."\t".$span_cnt."\t".$encompassing_cnt."\n";
	}
	close CSV_REP_H;
	close NEW_REP_H;
}
