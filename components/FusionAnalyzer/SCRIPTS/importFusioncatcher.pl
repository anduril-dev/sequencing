#!/usr/bin/perl
#Filtering fusions with spanning reads supporting the junction less then "filter" parameter

my ($INP_FILE,$ID,$FILTER_CNT,$OUT_FILE,$LOG_FILE) = @ARGV;

open INP_FILE_H, "<", "$INP_FILE" or die $!;
open OUT_FILE_H, ">>", "$OUT_FILE" or die $!;
open LOG_FILE_H, ">>", "$LOG_FILE" or die $!;
$line = <INP_FILE_H>;    # Header line
if (-z $OUT_FILE) {
    print OUT_FILE_H "gene1_name\tgene2_name\tchr1\tbp1\tgene_strand1\tchr2\tbp2\tgene_strand2\tspan_cnt\ttot_cnt\tsample\n";
}
while($line = <INP_FILE_H>){
	@tmp_line = split("\t",$line);
	$gene_name1 = $tmp_line[0];	#Gene_1_symbol(5end_fusion_partner)
	$gene_name2 = $tmp_line[1];	#Gene_2_symbol(3end_fusion_partner)
	($gene_chr1,$gene_bp1,$gene_strand1) = split(":",$tmp_line[8]);	#Fusion_point_for_gene_1(5end_fusion_partner)
	($gene_chr2,$gene_bp2,$gene_strand2) = split(":",$tmp_line[9]); #Fusion_point_for_gene_2(3end_fusion_partner)
	$span_cnt = $tmp_line[5];	#Spanning_unique_reads
	$tot_cnt = $tmp_line[4]; #Spanning_pairs
  if ($tot_cnt eq "0") {
    $tot_cnt = 0   #if tot_cnt is a string and not number we make it zero
  }
  
	
	if($span_cnt>=$FILTER_CNT){
		print OUT_FILE_H "$gene_name1\t$gene_name2\t$gene_chr1\t$gene_bp1\t$gene_strand1\t$gene_chr2\t$gene_bp2\t$gene_strand2\t$span_cnt\t$tot_cnt\t$ID\n";
	}else {
		print LOG_FILE_H "$gene_name1\t$gene_name2\t$gene_chr1\t$gene_bp1\t$gene_strand1\t$gene_chr2\t$gene_bp2\t$gene_strand2\t$span_cnt\t$tot_cnt\t$ID\n";
  }   
}
close INP_FILE_H;
close OUT_FILE_H;
close LOG_FILE_H;

