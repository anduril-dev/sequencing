#!/usr/bin/perl
#Filtering fusions with spanning reads supporting the junction less then "filter" parameter

my ($INP_FILE,$ID,$FILTER_CNT,$OUT_FILE,$LOG_FILE) = @ARGV;

open INP_FILE_H, "<", "$INP_FILE" or die $!;
open OUT_FILE_H, ">>", "$OUT_FILE" or die $!;
open LOG_FILE_H, ">>", "$LOG_FILE" or die $!;
$line = <INP_FILE_H>;    # Header line
if (-z $OUT_FILE) {
    print OUT_FILE_H "gene1_name\tgene2_name\tchr1\tbp1\tgene_strand1\tchr2\tbp2\tgene_strand2\tspan_cnt\ttot_cnt\tsample\n";
}
while($line = <INP_FILE_H>){
	@tmp_line = split("\t",$line);
	$gene_name1 = $tmp_line[0];	#up_gene
	$gene_name2 = $tmp_line[5];	#dw_gene
	$gene_chr1 = $tmp_line[1];
	$gene_chr1 =~ s/chr//;	#up_chr
	$gene_bp1 = $tmp_line[3];	#up_Genome_pos
	$gene_strand1 = $tmp_line[2];	#up_strand
	$gene_chr2 = $tmp_line[6];
	$gene_chr2 =~ s/chr//;	#dw_chr
	$gene_bp2 = $tmp_line[8];	#dw_Genome_pos
	$gene_strand2 = $tmp_line[7];	#dw_strand
	$span_cnt = $tmp_line[11];	#Junc_reads_num
  $tot_cnt = $tmp_line[10]; #Span_reads_num
	if ($tot_cnt eq "0") {
		$tot_cnt = 0   #if tot_cnt is a string and not number we make it zero
  }
	
	if($span_cnt>=$FILTER_CNT){
		print OUT_FILE_H "$gene_name1\t$gene_name2\t$gene_chr1\t$gene_bp1\t$gene_strand1\t$gene_chr2\t$gene_bp2\t$gene_strand2\t$span_cnt\t$tot_cnt\t$ID\n";
	}else{
		print LOG_FILE_H "$gene_name1\t$gene_name2\t$gene_chr1\t$gene_bp1\t$gene_strand1\t$gene_chr2\t$gene_bp2\t$gene_strand2\t$span_cnt\t$tot_cnt\t$ID\n";
  }
}
close INP_FILE_H;
close OUT_FILE_H;
close LOG_FILE_H;
