#!/usr/bin/bash
set -e
[[ -e $ANDURIL_HOME/lang/bash/functions.sh ]] && . $ANDURIL_HOME/lang/bash/functions.sh $1
export_command

# OUTPUTS
out_file="$output_out"
out_folder="$output_folder"
log_folder="$output_folder/log_folder"
# PARAMETERS
matching_method="$parameter_matching"
tissue_type="$parameter_tissueType"
span_reads="$parameter_filter"
th1="$parameter_th1"
th2="$parameter_th2"
sample_type="$parameter_datasetID"
keepDB="$parameter_keepDB"
pegasus_home="$parameter_pegasus_home"
exclude_db="$parameter_exclude_db"

if [ $keepDB = "false" ]; then
  BOX="boxes -d nuke"
  which boxes &>/dev/null || BOX=cat
  echo "Fusion Database will be DELETED!" | $BOX
  sleep 3
fi

FUSIONANALYZER_HOME="$( pwd )"
LIBPATH=$( readlink -f ../../lib/ )
FUMA_HOME="$LIBPATH/fuma"
if [ "$pegasus_home" == "" ]
then
  PEGASUS_HOME="$LIBPATH/pegasus"
else
  PEGASUS_HOME=$pegasus_home
fi
PEGASUS_DB=$PEGASUS_HOME/resources/hsqldb-2.2.7/hsqldb/mydb

FUSIONCATCHER_HOME="$LIBPATH/fusioncatcher/"
ENSEMBL_HOME="$FUSIONCATCHER_HOME/data/current/"

ONCOFUSE_HOME="$LIBPATH/oncofuse"
[[ -e "$PEGASUS_HOME" ]] || { echo Pegasus folder missing: $PEGASUS_HOME; exit 1; }
[[ -e "$ONCOFUSE_HOME" ]] || { echo Oncofuse folder missing: $ONCOFUSE_HOME; exit 1; }

tmpDir=$( gettempdir )
PEGASUS_OUTPUT="$tmpDir/pegasus_output"
PEGASUS_LOG="$tmpDir/pegasus_log"
ONCOFUSE_OUTPUT="$tmpDir/oncofuse_output"
PEGASUS_FORMATTED="$tmpDir/pegasus_formatted"
ONCOFUSE_FORMATTED="$tmpDir/oncofuse_formatted"

mkdir "$PEGASUS_FORMATTED"
mkdir "$ONCOFUSE_FORMATTED"
mkdir "$PEGASUS_OUTPUT"
mkdir "$PEGASUS_LOG"
mkdir "$ONCOFUSE_OUTPUT"
mkdir "$out_folder"
mkdir "$log_folder"

# These getter functions retrieve tool name from FusionCaller folder inputs
TOOL_LIST=( chimerascan defuse fusioncatcher ericscript soapfuse starfusion)
ALIAS_LIST=( CS DF FC ES SF STAR )
_get_alias() {
  for ((t=0;t<${#TOOL_LIST[@]};t++)); do
    [[ ${TOOL_LIST[$t]} = "$1" ]] && { echo ${ALIAS_LIST[$t]}; return; }
  done
  return 1
}
_get_tool() {
  for ((t=0;t<${#TOOL_LIST[@]};t++)); do
    [[ ${ALIAS_LIST[$t]} = "$1" ]] && { echo ${TOOL_LIST[$t]}; return; }
  done
  return 1
}

# INPUTS

# Input: Array of FusionCaller output folders
if [ -s "$input_in" ]
then
  sample_folders=( $( getarrayfiles in ) ) 
  sample_keys=( $( getarraykeys in ) ) 
	echo "Input: array of FusionCaller folder outputs "

	SPLIT_FILTERED="$tmpDir/split_filtered"
	mkdir "$SPLIT_FILTERED"

  for ((i=0; i<${#sample_keys[@]}; i++ )) {
      sample_folder=${sample_folders[$i]}
      ID=${sample_keys[$i]}
      FILE="$sample_folder"/final
      echo "Sample:" $sample_folder "ID:" $ID "File:" $FILE
      if ! test -e "$sample_folder"/.tool; then
        writeerror "Cannot infer tool used: File '$sample_folder/.tool' is missing!"
          exit 1
      fi
      if test -f "$sample_folder"/.sample; then
          ID=$( cat "$sample_folder"/.sample )
      fi
      if ! test -e "$FILE"; then
          writeerror "Cannot find input data: File '$FILE' is missing!"
          exit 1
      fi
      ALIAS=$( cat "$sample_folder"/.tool )
      TOOL=$( _get_tool "$ALIAS" )
      writelog "Filtering $ID/$TOOL  results with supporting spanning reads less than $span_reads: $FILE"
      case $TOOL in
          chimerascan)
          perl "$FUSIONANALYZER_HOME/SCRIPTS/importChimerascan.pl" "$FILE" "$ID" "$span_reads" "$SPLIT_FILTERED/${ID}_${TOOL}.csv" "$log_folder/${ID}_${TOOL}_count_filter.csv"
          tmpFILE=$SPLIT_FILTERED"/"$ID"_chimerascan.csv"
          ALIAS=$( _get_alias "$TOOL" ) 
          ;;
          defuse)
          perl "$FUSIONANALYZER_HOME/SCRIPTS/importDefuse.pl" "$FILE" "$ID" "$span_reads" "$SPLIT_FILTERED/${ID}_${TOOL}.csv" "$log_folder/${ID}_${TOOL}_count_filter.csv"

          tmpFILE=$SPLIT_FILTERED"/"$ID"_defuse.csv"
          ALIAS=$( _get_alias "$TOOL" ) 
          ;;
          fusioncatcher)
          perl "$FUSIONANALYZER_HOME/SCRIPTS/importFusioncatcher.pl" "$FILE" "$ID" "$span_reads" "$SPLIT_FILTERED/${ID}_${TOOL}.csv" "$log_folder/${ID}_${TOOL}_count_filter.csv"
          
          tmpFILE=$SPLIT_FILTERED"/"$ID"_fusioncatcher.csv"
          ALIAS=$( _get_alias "$TOOL" ) 
          TOOL="fusion-catcher_final"
          ;;
          ericscript)
          perl "$FUSIONANALYZER_HOME/SCRIPTS/importEricscript.pl" "$FILE" "$ID" "$span_reads" "$SPLIT_FILTERED/${ID}_${TOOL}.csv" "$log_folder/${ID}_${TOOL}_count_filter.csv"

          tmpFILE=$SPLIT_FILTERED"/"$ID"_ericscript.csv"
          ALIAS=$( _get_alias "$TOOL" ) 
          ;;
          soapfuse)
          perl "$FUSIONANALYZER_HOME/SCRIPTS/importSoapfuse.pl" "$FILE" "$ID" "$span_reads" "$SPLIT_FILTERED/${ID}_${TOOL}.csv" "$log_folder/${ID}_${TOOL}_count_filter.csv"

          tmpFILE=$SPLIT_FILTERED"/"$ID"_soapfuse.csv"
          ALIAS=$( _get_alias "$TOOL" ) 
          TOOL="soapfuse-final-gene"
          ;;
          starfusion)
          perl "$FUSIONANALYZER_HOME/SCRIPTS/importStarfusion.pl" "$FILE" "$ID" "$span_reads" "$SPLIT_FILTERED/${ID}_${TOOL}.csv" "$log_folder/${ID}_${TOOL}_count_filter.csv"

          tmpFILE=$SPLIT_FILTERED"/"$ID"_starfusion.csv"
          ALIAS=$( _get_alias "$TOOL" ) 
          TOOL="starfusion-final-gene"
          ;;
          *)
          echo "ERROR: \"Tool\" parameter $TOOL in list INPUT not valid. (Possible values: fusioncatcher, chimerascan, defuse, ericscript, soapfuse, starfusion)."
          exit 1
          ;;
      esac
      # Formatting inputs for fusionmatcher
      sample_list_string="$sample_list_string ${ID}_$ALIAS:$TOOL:$FILE"
      link_string="$link_string ${ID}_$ALIAS:hg"
  } # for input array
# Input: folder of csvs
elif [ -s "$input_csvs" ]
then
  SPLIT_FILTERED="$input_csvs"
  echo "Input: Folder of csv files"
fi

if [ $parameter_skipFiltering = "false" ]; then
  #Filter homologous & not annotated genes
  FUSIONS_FILTERED="$tmpDir/fusions_filtered"
  mkdir $FUSIONS_FILTERED
  # Database annotation
  mkdir "$FUSIONS_FILTERED/fusions_annot"
#  mkdir "$FUSIONS_FILTERED/fusions_annot/tmp"
  mkdir "$FUSIONS_FILTERED/fusions_csvs"
  echo "Filter homologous gene fusions, non annotated, or fusions reported in databases"
  echo "Checking "${SPLIT_FILTERED}
  for i in $( ls "${SPLIT_FILTERED}"/*csv )
  do
		FILE=$i
		NAME=$( basename $FILE .csv )

    #After filtering by number of reads some files may not have fusions left, just the header
    actualsize=$(wc -l < "${i}")
    if [ $actualsize -ge "2" ]
    then
	    # Do filtering here: cp "$FILE" "$FUSIONS_FILTERED"/"$ID"."$TOOL".filtered.csv
  	  #echo $NAME
    	#fusions outdir resources
    	Rscript "$FUSIONANALYZER_HOME/SCRIPTS/filter_fusion.r" "$FILE" "$FUSIONS_FILTERED" "$FUSIONANALYZER_HOME/resources/" "${NAME}" "${log_folder}"

			#After filtering by annotation some files may not have fusions left, just the header
    	actualsize=$(wc -l < "$FUSIONS_FILTERED/${NAME}_kept.csv")
    	if [ $actualsize -ge "2" ]
    	then
    
      	#This file contains the fusions that passed the filtering step
      	FILE="$FUSIONS_FILTERED/${NAME}_kept.csv"
      	mkdir "$FUSIONS_FILTERED/fusions_annot/${NAME}_tmp"
      	tsvecho gene1_ID gene2_ID spanning gene1_name gene2_name | tr -d \" > "$FUSIONS_FILTERED/fusions_annot/${NAME}_in"        
      	n1=$( csvcoln $FILE "gene1_name" )
      	n2=$( csvcoln $FILE "gene2_name" )
      	id1=$( csvcoln $FILE "ensembl_gene_id1" )
      	id2=$( csvcoln $FILE "ensembl_gene_id2" )
      	en=$( csvcoln $FILE "tot_cnt" ) 
      	DBS=$( echo ${exclude_db} | tr -d '"' | tr ',' '|' )
      	echo "Terms to exclude: " ${DBS}
		  	tail -n +2 "$FILE" | awk -F $'\t' -v n1=$n1 -v n2=$n2 -v id1=$id1 -v id2=$id2 -v en=$en '{ OFS="\t"; print($id1,$id2,$en,$n1,$n2)}' >> "$FUSIONS_FILTERED/fusions_annot/${NAME}_in"
      	
				"$FUSIONANALYZER_HOME/SCRIPTS/fc_db_annotation.sh" "$FUSIONCATCHER_HOME/bin/" "$ENSEMBL_HOME" "$FUSIONS_FILTERED/fusions_annot/${NAME}_in" "$FUSIONS_FILTERED/fusions_annot/${NAME}_tmp" "$FUSIONS_FILTERED/fusions_annot/${NAME}_out" ${DBS} "$log_folder/${NAME}_db_filter.csv" 2>&1

      	echo "DB annotation completed: "$NAME
      	tsvecho gene1_id gene2_id count gene1_name gene2_name db > "$log_folder/${NAME}_db_annotated.csv" 
      	cat "$FUSIONS_FILTERED/fusions_annot/${NAME}_tmp/prefiltered" >> "$log_folder/${NAME}_db_annotated.csv"
     
      	#Here i need to combine the annotation from fusion catcher to the filtering step
     	 	tail -n +2 "$FILE" | awk -F $'\t' -v n1=$n1 -v n2=$n2 -v id1=$id1 -v id2=$id2 -v en=$en '{ OFS="\t"; print($id1"_"$id2"_"$en"_"$n1"_"$n2,$0)}' OFS="\t" | sort -k1,1 > "$FUSIONS_FILTERED/fusions_annot/${NAME}_tmp1"
      	tail -n +2 "$FUSIONS_FILTERED/fusions_annot/${NAME}_out" | awk -F $'\t' '{print $1,$2,$3,$4,$5"\t"$6}' OFS="_" | sort -k1,1 > "$FUSIONS_FILTERED/fusions_annot/${NAME}_tmp2"
      	head -n1 "$FILE" | sed '1s/^/id\t/' > "$FUSIONS_FILTERED/${NAME}.csv"
      	FILE="$FUSIONS_FILTERED/${NAME}.csv"
      	join -j1 -t '	' "$FUSIONS_FILTERED/fusions_annot/${NAME}_tmp1" "$FUSIONS_FILTERED/fusions_annot/${NAME}_tmp2" | tr ' ' '\t' >> "$FILE"
        #At this point FILE has the fusions that survived the count, ensembl, and FusionCatcher databases filtering
        #In the next step they will be reformatted for pegasus and oncofuse
  
      	tsvecho gene1_name gene2_name gene1_ID gene2_ID gene1_coord gene2_coord bp spanning encompassing | tr -d \" > "$FUSIONS_FILTERED/fusions_csvs/${NAME}"
		  	n1=$( csvcoln $FILE "gene1_name" )
      	n2=$( csvcoln $FILE "gene2_name" )
      	id1=$( csvcoln $FILE "ensembl_gene_id1" )
      	id2=$( csvcoln $FILE "ensembl_gene_id2" )
      	chr1=$( csvcoln $FILE "chr1" )
      	chr2=$( csvcoln $FILE "chr2" )
      	bp1=$( csvcoln $FILE "bp1" )
      	bp2=$( csvcoln $FILE "bp2" )
      	s1=$( csvcoln $FILE "gene_strand1" )
      	s2=$( csvcoln $FILE "gene_strand2" )
      	st1=$( csvcoln $FILE "start_position1" )
      	st2=$( csvcoln $FILE "start_position2" ) 
      	e1=$( csvcoln $FILE "end_position1" ) 
      	e2=$( csvcoln $FILE "end_position2" )
      	sp=$( csvcoln $FILE "span_cnt" )
      	en=$( csvcoln $FILE "tot_cnt" )

      	tail -n+2 "$FILE" | awk -F $'\t' -v n1=$n1 -v n2=$n2 -v id1=$id1 -v id2=$id2 -v c1=$chr1 -v c2=$chr2 -v bp1=$bp1 -v bp2=$bp2 -v s1=$s1 -v s2=$s2 -v st1=$st1 -v st2=$st2 -v e1=$e1 -v e2=$e2 -v sp=$sp -v en=$en -v f=$f  \
          '{ OFS="\t"; 
             bp="chr"$c1":"$bp1"-chr"$c2":"$bp2;
             coord1=$st1":"$e1":"$s1
             coord2=$st2":"$e2":"$s2
             print($n1,$n2,$id1,$id2,coord1,coord2,bp,$sp,$en)  }' \
             >> "$FUSIONS_FILTERED/fusions_csvs/${NAME}"   
    	else
      	echo "No fusions left: ${NAME}"
    	fi
		else
			echo "No fusions left: ${NAME}"
    fi
  done

  echo "Formatting"
	perl "$FUSIONANALYZER_HOME/SCRIPTS/csv2Pegasus.pl" "$FUSIONS_FILTERED/fusions_csvs" "$PEGASUS_FORMATTED"
	perl "$FUSIONANALYZER_HOME/SCRIPTS/csv2Oncofuse.pl" "$FUSIONS_FILTERED/fusions_csvs" "$ONCOFUSE_FORMATTED" "$tissue_type"

else
  FUSIONS_FILTERED="$input_formatted_csvs"

  perl "$FUSIONANALYZER_HOME/SCRIPTS/csv2Pegasus.pl" "$FUSIONS_FILTERED" "$PEGASUS_FORMATTED"
  perl "$FUSIONANALYZER_HOME/SCRIPTS/csv2Oncofuse.pl" "$FUSIONS_FILTERED" "$ONCOFUSE_FORMATTED" "$tissue_type"
fi

# FusionMatcher
[[ $parameter_runFuma = "true" ]] && {
  echo "Matching fusion detection tool results..."
  "$FUMA_HOME/bin/fuma"  -m "$matching_method" -a "hg:$FUMA_HOME/annotations.bed" -s $sample_list_string -l $link_string -f "extensive" -o "$tmpDir/overlapping.coord.txt"
}

#Running PEGASUS
echo "Pegasus execution..."
# Step 0 - delete database
if [ $keepDB = "false" ];
then
    java -jar $PEGASUS_HOME/jars/QueryFusionDatabase.jar -t FUSIONS_COMPLETE_ID -c deleteAll -d "$PEGASUS_DB"
    echo "Fusion database erased"
fi


# Step 1
for file in $PEGASUS_FORMATTED/*; do
    FILE_PATH="$file"
    SAMPLE_ID=$(basename "$FILE_PATH")
    
    echo Pegasus step 1 $PEGASUS_HOME/jars/LoadFusionReport.jar -f "$PEGASUS_DB" -i "$FILE_PATH"  -t "FUSIONS_COMPLETE_ID" -d "xdb" -s "$SAMPLE_ID|$sample_type" -p "bellerophontes"

    ( java -jar "$PEGASUS_HOME/jars/LoadFusionReport.jar" -f "$PEGASUS_DB" \
      -i "$FILE_PATH"  -t "FUSIONS_COMPLETE_ID" -d "xdb" \
      -s "$SAMPLE_ID|$sample_type" -p "bellerophontes" 2>&1 | \
       tee -a "$PEGASUS_LOG/LoadFusionReport.log" ) || true
done

# Step 2
echo Pegasus step 2
java -jar "$PEGASUS_HOME/jars/QueryFusionDatabase.jar" -t "FUSIONS_COMPLETE_ID" -c "all_with_kinases" -s "$sample_type" -d "$PEGASUS_DB" > "$PEGASUS_OUTPUT/samples_info.txt" 2>> "$PEGASUS_LOG/QueryFusionDatabase.log"
cat "$PEGASUS_LOG/QueryFusionDatabase.log"

sh "$PEGASUS_HOME/source/scripts/format_forInframeAnalysis.sh" "$PEGASUS_OUTPUT/samples_info.txt" | sort -u > "$PEGASUS_OUTPUT/samples_info.formatted.sorted.txt"

# Step 3
echo Pegasus step 3
sh "$PEGASUS_HOME/source/scripts/MultiFusionSequenceFromGTF_wrap.sh" -r "$PEGASUS_HOME/hg.fa" -g "$PEGASUS_HOME/annotations.gtf" -s "$PEGASUS_HOME/source/scripts" -j "$PEGASUS_HOME/jars" -o "$PEGASUS_OUTPUT/samples_info.annotated.txt" -t "$PEGASUS_OUTPUT/tmp" -i "$PEGASUS_OUTPUT/samples_info.formatted.sorted.txt" 2>&1 | tee -a "$PEGASUS_LOG/RunAnnotation.log" 

# Step 4
echo Pegasus step 4
perl "$PEGASUS_HOME/source/scripts/merge_res_commonFormat.pl" -i "$PEGASUS_OUTPUT/samples_info.txt" -r "$PEGASUS_OUTPUT/samples_info.annotated.txt" | cut -f1-23,28- | cut -f1-26,28- > "$PEGASUS_OUTPUT/final_results_forXLS.txt" 2>> "$PEGASUS_LOG/Merge_format.log"
cat "$PEGASUS_LOG/Merge_format.log"

perl "$PEGASUS_HOME/source/scripts/wrapper_finalreport.pl" -s "$PEGASUS_HOME/source/scripts" -d "$PEGASUS_OUTPUT" -i "$PEGASUS_OUTPUT/final_results_forXLS.txt" -r "$PEGASUS_HOME/resources" -l "$PEGASUS_LOG/Wrapper_final_report.log" -o "$PEGASUS_OUTPUT/final_results_forXLS.ML.input.txt" 2>&1 | tee -a "$PEGASUS_LOG/Wrapper_final_report.log" 

# To avoid "ValueError: Array contains NaN or infinity" bug, fill empty fields of "final_results_forXLS.ML.input.txt" with 0
awk -F"\t" -v OFS="\t" '{if(NR==1){nc=NF}; for (i=1;i<=nc;i++) {if ($i == "") $i="0"} print $0}' "$PEGASUS_OUTPUT/final_results_forXLS.ML.input.txt" > "$PEGASUS_OUTPUT/final_results_forXLS.ML.input.new.txt"
mv "$PEGASUS_OUTPUT/final_results_forXLS.ML.input.new.txt" "$PEGASUS_OUTPUT/final_results_forXLS.ML.input.txt"

echo Pegasus classify
python "$PEGASUS_HOME/source/scripts/classify.py" -i "$PEGASUS_OUTPUT/final_results_forXLS.ML.input.txt" -m "$PEGASUS_HOME/learn/trained_model_gbc.pk" -o "$PEGASUS_OUTPUT/pegasus.output.txt" -l "$PEGASUS_LOG" >> "$PEGASUS_LOG/ML_module.log" 2>> "$PEGASUS_LOG/ML_module.log"
cat "$PEGASUS_LOG/ML_module.log"


#Running ONCOFUSE
echo "Oncofuse execution..."
for file in $ONCOFUSE_FORMATTED/*
do
        FILE_PATH="$file"
        SAMPLE_ID=$(basename "$FILE_PATH")

        echo "Oncofuse: SAMPLE ID $SAMPLE_ID"

        java -jar "$ONCOFUSE_HOME/Oncofuse.jar" -a "hg38" "$FILE_PATH" "coord" "-" "$ONCOFUSE_OUTPUT/$SAMPLE_ID.oncofuse.txt"
done

#Merging Pegasus Oncofuse and DB annotation results
echo "Merge Oncofuse and Pegasus outputs"
cat "$log_folder/"*"_db_annotated.csv" | awk -F"\t" '{print $4"_"$5"\t"$6}' | grep -v gene_name | sort -u > "$tmpDir/db_annotation"
perl "$FUSIONANALYZER_HOME/SCRIPTS/merge_PegOnc.pl" "$PEGASUS_OUTPUT" "$ONCOFUSE_OUTPUT" "$tmpDir" "$th1" "$th2" "$tmpDir/db_annotation"

cp "$tmpDir/final.candidates.csv" "$out_file"
[[ $parameter_runFuma = "true" ]] && {
  cp "$tmpDir/overlapping.coord.txt" "$out_folder"  #FusionMatcher output format "extensive"
  #cp "$tmpDir/overlapping.summary.txt" "$out_folder"   #FusionMatcher output format "summary"
}
cp "$PEGASUS_OUTPUT/pegasus.output.txt" "$out_folder"   #Pegasus main output file
mkdir "$out_folder/oncofuse"
cp "$ONCOFUSE_OUTPUT/"*".oncofuse.txt" "$out_folder/oncofuse"    #Oncofuse output files

mv $tmpDir "$out_folder/tmpDir"
