#!usr/bin/perl

use strict;
use componentSkeleton;
use Encode::Unicode;
use File::Path qw(make_path);

my $ref_hash_arrayIn;

my $output_basicStats_path;
my $output_perBaseSeqQual_path;
my $output_perSeqQualScore_path;
my $output_perBaseSeqContent_path;
my $output_perBaseGC_path;
my $output_perSeqGC_path;
my $output_seqDuplicateLevel_path;
my $output_overrepresented_path;
my $output_perBaseN_path;

my %hash_basicStats;
my %hash_perBaseSeqQual;
my %hash_perSeqQualScore;
my %hash_perBaseSeqContent;
my %hash_perBaseGC;
my %hash_perSeqGC;
my %hash_perBaseN;
my %hash_seqDuplicateLevel;
my %hash_overrepresented;

sub execute {
	my ($cf_ref) = @_;
	
	# Read inputs and outputs
	$ref_hash_arrayIn = get_InputArrayIndex($cf_ref, "summary");

	$output_basicStats_path = get_output($cf_ref, "basicStats");
	$output_perBaseSeqQual_path = get_output($cf_ref, "perBaseSeqQual");
	$output_perSeqQualScore_path = get_output($cf_ref, "perSeqQualScore");
	$output_perBaseSeqContent_path = get_output($cf_ref, "perBaseSeqContent");
	$output_perBaseGC_path = get_output($cf_ref, "perBaseGC");
	$output_perSeqGC_path = get_output($cf_ref, "perSeqGC");
	$output_seqDuplicateLevel_path = get_output($cf_ref, "seqDuplicateLevel");
	$output_overrepresented_path = get_output($cf_ref, "overrepresented");
	$output_perBaseN_path = get_output($cf_ref, "perBaseN");

	make_path($output_basicStats_path,
		  $output_perBaseSeqQual_path, 
		  $output_perSeqQualScore_path, 
		  $output_perBaseSeqContent_path, 
		  $output_perBaseGC_path, 
		  $output_perSeqGC_path, 
		  $output_seqDuplicateLevel_path, 
		  $output_overrepresented_path, 
		  $output_perBaseN_path);

	# Read parameters
	my $para_paired = get_parameter($cf_ref, "paired");

	# Run
	my $key;

	if($para_paired eq 'true'){
		my @names = ("read", "mate");
		foreach $key (@names){
		   &qc_parser($key);
		}
	}else{
		   &qc_parser("read");
	}
	
	# Output
	&write_array(\%hash_basicStats, $para_paired, $output_basicStats_path);
        &write_array(\%hash_perBaseSeqQual, $para_paired, $output_perBaseSeqQual_path);
        &write_array(\%hash_perSeqQualScore, $para_paired, $output_perSeqQualScore_path);
        &write_array(\%hash_perBaseSeqContent, $para_paired, $output_perBaseSeqContent_path);
        &write_array(\%hash_perBaseGC, $para_paired, $output_perBaseGC_path);
        &write_array(\%hash_perSeqGC, $para_paired, $output_perSeqGC_path);
        &write_array(\%hash_perBaseN, $para_paired, $output_perBaseN_path);
        &write_array(\%hash_seqDuplicateLevel, $para_paired, $output_seqDuplicateLevel_path);
        &write_array(\%hash_overrepresented, $para_paired, $output_overrepresented_path);
}

sub write_array{

	my ($ref_hash_data, $paired, $outputPort) = @_;
	my $indexFile = "_index";

	if($paired eq 'true'){
		$indexFile = $outputPort."/".$indexFile;
		open(FILEIDX, ">".$indexFile);
		print FILEIDX "Key\tFile\n";
		print FILEIDX "read\tread.csv\n";
		print FILEIDX "mate\tmate.csv\n";
		close(FILEIDX);

		open(FILETB, ">".${$ref_hash_data->{"read"}}[0]);
		print FILETB ${$ref_hash_data->{"read"}}[1];
		close(FILETB);

		open(FILETB, ">".${$ref_hash_data->{"mate"}}[0]);
                print FILETB ${$ref_hash_data->{"mate"}}[1];
                close(FILETB);
	}else{
		$indexFile = $outputPort."/".$indexFile;
                open(FILEIDX, ">".$indexFile);
		print FILEIDX "Key\tFile\n";
                print FILEIDX "read\tread.csv\n";
		close(FILEIDX);

		open(FILETB, ">".${$ref_hash_data->{"read"}}[0]);
                print FILETB ${$ref_hash_data->{"read"}}[1];
                close(FILETB);	
	}
}

sub qc_parser{
	my ($key) = @_;
	
	@{$hash_basicStats{$key}} = ($output_basicStats_path."/".$key.".csv", "");
	@{$hash_perBaseSeqQual{$key}} = ($output_perBaseSeqQual_path."/".$key.".csv", "");
        @{$hash_perSeqQualScore{$key}} = ($output_perSeqQualScore_path."/".$key.".csv", "");
        @{$hash_perBaseSeqContent{$key}} = ($output_perBaseSeqContent_path."/".$key.".csv","");
        @{$hash_perBaseGC{$key}} = ($output_perBaseGC_path."/".$key.".csv","");
        @{$hash_perSeqGC{$key}} = ($output_perSeqGC_path."/".$key.".csv","");
        @{$hash_seqDuplicateLevel{$key}} = ($output_seqDuplicateLevel_path."/".$key.".csv","");
        @{$hash_overrepresented{$key}} = ($output_overrepresented_path."/".$key.".csv","");

	open(FILEIN, $ref_hash_arrayIn->{$key});       
  
        # check if the input is empty or not
        my @lines = <FILEIN>;
        my $nline = @lines;
        close(FILEIN);
        if($nline==0){
            ${$hash_basicStats{$key}}[1] = "Measure\tValue\nFilename\tNA\nFile type\tNA\nEncoding\tNA\nTotal Sequences\tNA\nFiltered Sequences\tNA\nSequence length\tNA\n%GC\tNA";

            ${$hash_perBaseSeqQual{$key}}[1] = "Base\tMean\tMedian\tLower Quartile\tUpper Quartile\t10th Percentile\t90th Percentile";

            ${$hash_perSeqQualScore{$key}}[1] = "Quality\tCount";

            ${$hash_perBaseSeqContent{$key}}[1] = "Base\tG\tA\tT\tC";

            ${$hash_perBaseGC{$key}}[1] = "Base\t%GC";

            ${$hash_perSeqGC{$key}}[1] = "GC Content\tCount";

            ${$hash_perBaseN{$key}}[1] = "";

            ${$hash_seqDuplicateLevel{$key}}[1] = "Duplication Level\tRelative count";

            ${$hash_overrepresented{$key}}[1] = "Sequence\tCount\tPercentage\tPossible Source";            
        }else{

          open(FILEIN, $ref_hash_arrayIn->{$key});
          while(<FILEIN>){
	        if($_=~/Basic Statistics/){
			while(<FILEIN>){
                                if($_=~/^>>/){
                                        last;
                                }
				$_=~s/^\#//;
                                ${$hash_basicStats{$key}}[1] = ${$hash_basicStats{$key}}[1].$_;
                        }
        	}elsif($_=~/Per base sequence quality/){
            		while(<FILEIN>){
				if($_=~/^>>/){
					last;
				}
				$_=~s/^\#//;
                    		${$hash_perBaseSeqQual{$key}}[1] = ${$hash_perBaseSeqQual{$key}}[1].$_;
			}
                }elsif($_=~/Per sequence quality scores/){
			while(<FILEIN>){
				if($_=~/^>>/){
					last;
				}
				$_=~s/^\#//;
				${$hash_perSeqQualScore{$key}}[1] = ${$hash_perSeqQualScore{$key}}[1].$_;
			}

                }elsif($_=~/Per base sequence content/){
			while(<FILEIN>){
				if($_=~/^>>/){
					last;
				}
				$_=~s/^\#//;
				${$hash_perBaseSeqContent{$key}}[1] = ${$hash_perBaseSeqContent{$key}}[1].$_;
			}
                }elsif($_=~/Per base GC content/){
			while(<FILEIN>){
				if($_=~/^>>/){
					last;
				}
				$_=~s/^\#//;
				${$hash_perBaseGC{$key}}[1] = ${$hash_perBaseGC{$key}}[1].$_;
			}
                }elsif($_=~/Per sequence GC content/){
			while(<FILEIN>){
				if($_=~/^>>/){
					last;
				}
				$_=~s/^\#//;
				${$hash_perSeqGC{$key}}[1] = ${$hash_perSeqGC{$key}}[1].$_;
			}
                }elsif($_=~/Per base N content/){
			while(<FILEIN>){
				if($_=~/^>>/){
					last;
				}
				$_=~s/^\#//;
				${$hash_perBaseN{$key}}[1] = ${$hash_perBaseN{$key}}[1].$_;
			}
                }elsif($_=~/Sequence Duplication Levels/){
			while(<FILEIN>){
				if($_=~/^>>/){
					last;
				}
				$_=~s/^\#//;
				${$hash_seqDuplicateLevel{$key}}[1] = ${$hash_seqDuplicateLevel{$key}}[1].$_;
			}
                }elsif($_=~/Overrepresented sequences/){
			my $count=0;
			while(<FILEIN>){
				if($_=~/^>>/){
					last;
				}
				$_=~s/^\#//;
				${$hash_overrepresented{$key}}[1] = ${$hash_overrepresented{$key}}[1].$_;
				$count++;
			}
			if($count==0){
				${$hash_overrepresented{$key}}[1] = ${$hash_overrepresented{$key}}[1]."Sequence\tCount\tPercentage\tPossible Source";
			}
                }
	  }
          close(FILEIN);
        }
}


if(scalar @ARGV == 0){
    print "NO_COMMAND_FILE";
    exit;
}
my %cf = parse_command_file($ARGV[0]);

execute(\%cf);



