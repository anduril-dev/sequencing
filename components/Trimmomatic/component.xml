<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>Trimmomatic</name>
    <version>2.1</version>
    <doc>Trimmomatic performs a variety of useful trimming tasks for 
         illumina paired-end and single ended data.

         Adaptor removal is performed first if adapters are provied. After 
         that the trimming operations, as defined by the parameters, are 
         performed in the following order: leading, trailing, slidingWindow, 
         headcrop, crop and minlen.
    </doc>
    <author email="ping.chen@helsinki.fi">Ping Chen</author>
    <author email="javier.nunez-fontarnau@helsinki.fi">Javier Nunez Fontarnau</author>
    <author email="alejandra.cervera@helsinki.fi">Alejandra Cervera</author>
    <author email="erkka.valo@helsinki.fi">Erkka Valo</author>
    <category>Quality control</category>
    <launcher type="java">
        <argument name="class" value="TrimmomaticComponent" />
        <argument name="source" value="TrimmomaticComponent.java" /> 
    </launcher>
    <requires type="jar">trimmomatic-0.32.jar</requires>
	<requires name="installer" optional="false">
         <resource type="bash">install.sh</resource>
    </requires>
    <inputs>
	    <input name="read" type="FASTQ" optional="false">
            <doc>Sequence files in FASTQ format.</doc>
        </input>
        <input name="mate" type="FASTQ" optional="true">
            <doc>Sequence files in FASTQ format.</doc>
        </input>
	    <input name="adapter" type="FASTA" optional="true">
	        <doc> Fasta file for adapters.  </doc>
	    </input>
    </inputs>
    <outputs>
        <output name="trimmedReads" type="FASTQ" array="true">
            <doc>An array with trimmed reads. The keys are 'Reads', 'Mates', and 'Uread' and 'Umate' for unpaired reads and mates.
            </doc>
        </output>
        <output name="stdout" type="BinaryFile" array="false">
            <doc>Stdout of Trimmomatic. </doc>
        </output>
        <output name="stats" type="CSV" array="false">
            <doc>Short statistics. </doc>
        </output>
        <output name="log" type="TextFile" array="false">
            <doc>Log</doc>
        </output>
    </outputs>
    <parameters>
	    <parameter name="leading" type="int" default="30">
            <doc> Remove bases from the start of the read, if quality value is 
                  below the given threshold. Specifies the minimum quality 
                  required to keep a base. No trimming is done if  the value is 
                  set to -1.
            </doc>
        </parameter>
	    <parameter name="trailing" type="int" default="30">
            <doc> Remove bases from the end of the read, if quality value is 
                  below the given threshold. Specifies the minimum quality 
                  required to keep a base. No trimming is done if the value is 
                  set to -1.
            </doc>
        </parameter>
	    <parameter name="crop" type="int" default="-1">
            <doc> Cut the read so it has maximally the specified length. The 
                  number of bases to keep, from the start of the read. No 
                  trimming is done if the value is set to -1.
            </doc>
        </parameter>
	    <parameter name="headcrop" type="int" default="-1">
            <doc> The number of bases to remove from the start of the 
                  read. No trimming is done if the value is set to -1.
            </doc>
        </parameter>
	    <parameter name="minlen" type="int" default="30">
            <doc> Specifies the minimum length of reads to keep. Reads are 
                  dropped if they are below a specified length. No reads are
                  dropped if the value is set to -1.
            </doc>
        </parameter>
	    <parameter name="qual" type="string" default="phred64">
            <doc> Quality scores. Either 'phred33' or 'phred64'.
            </doc>
        </parameter>
	    <parameter name="threads" type="int" default="4">
	        <doc> The number of threads to use. </doc>
	    </parameter>
	    <parameter name="seedMM" type="int" default="2">
            <doc> Seed mismatched for adapters. 
		        It specifies the maximum mismatch count which will still allow a full match to be performed.
	        </doc>
        </parameter>
	    <parameter name="simpleClip" type="int" default="12">
            <doc> A threshold specifies how accurate the match between any adapter etc. sequence must be against a read. 
		  Each matching base adds just over 0.6. Thus, a perfect match of a 20 base sequence will score just 
		  over 12, while 25 bases are needed to score 15.
	        </doc>
        </parameter>
		 <parameter name="palindromeClip" type="int" default="30">
            <doc> Specifies how accurate the match between the two 'adapter ligated' reads must be for PE palindrome read alignment.
            </doc>
        </parameter>

	    <parameter name="slidingWindow" type="string" default="null">
            <doc> Sliding window trimming where the sequence is cut if the 
                  average quality of the bases within the sliding window falls 
                  below the defined threshold. A string specifies the window 
                  size and the average required quality in the sliding window.
                  The format is windowSize:requiredQuality. For example, 4:15 
                  (window size = 4; required quality = 15). No trimming is done
                  if value is set to 'null'.
            </doc>
        </parameter>
        <parameter name="illuminaAdapter" type="string" default="">
            <doc>
              Illumina-specific sequences from the read included with Trimmomatic. Choose TruSeq2-SE.fa, TruSeq2-PE.fa, TruSeq3-SE.fa, or  
              TruSeq3-PE.fa for specifying the file to use; leave it blank otherwise.
            </doc>
        </parameter>
        <parameter name="gzip" type="boolean" default="false">
            <doc>Defines if the output sequences should be gzipped or not.</doc>
        </parameter>
		<parameter name="keepBothReads" type="boolean" default="false">
            <doc>Defines if keep the reverse reads after read-though has been detected by palindrome mode, and the
adapter sequence removed, the reverse read contains the same sequence information as the forward read, albeit in reverse complement.</doc>
        </parameter>
    </parameters>
</component>
