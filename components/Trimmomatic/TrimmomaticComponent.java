import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintStream;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.BufferedWriter;
import java.io.Writer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.IndexFile;
import org.anduril.component.SkeletonComponent;
import org.anduril.asser.io.CSVWriter;

import org.usadellab.trimmomatic.TrimmomaticPE;
import org.usadellab.trimmomatic.TrimmomaticSE;
import org.usadellab.trimmomatic.Trimmomatic;

public class TrimmomaticComponent extends SkeletonComponent {
    
    public static void main(String[] args) {
        new TrimmomaticComponent().run(args);
    }
    
    @Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {
        
        File read = cf.getInput("read");
        
        File mate = null;
        boolean paired = false;

        if (cf.inputDefined("mate")) {
            mate = cf.getInput("mate");
            paired = true;
        }
        
        File adapter = null;
        if (cf.inputDefined("adapter"))
            adapter = cf.getInput("adapter");

        File arrayOutPort = cf.getOutput("trimmedReads");    
        File stdout = cf.getOutput("stdout");
        File stats = cf.getOutput("stats");
        File log = cf.getOutput("log");
        
        //boolean paired = cf.getBooleanParameter("paired");
        int leading = cf.getIntParameter("leading");
        int trailing = cf.getIntParameter("trailing");
        int crop = cf.getIntParameter("crop");
        int headcrop = cf.getIntParameter("headcrop");
        int minlen = cf.getIntParameter("minlen");
        String qual = cf.getParameter("qual");
        int threads = cf.getIntParameter("threads");
        int simpleClip = cf.getIntParameter("simpleClip");
        int palindromeClip = cf.getIntParameter("palindromeClip");
        int seedMM = cf.getIntParameter("seedMM");
        String sliding = cf.getParameter("slidingWindow");
        String adapterFile = cf.getParameter("illuminaAdapter");
        String allAdaptersFile="";
        boolean clip=true;
        boolean newAdapterFile=false;
        boolean gzip=cf.getBooleanParameter("gzip");
        boolean keepBothReads=cf.getBooleanParameter("keepBothReads");
        String filenameReads = "trimmedReads.fq";
        String filenameMates = "trimmedMates.fq";

        if (gzip) {
            filenameReads = "trimmedReads.fq.gz";
            filenameMates = "trimmedMates.fq.gz";
        }

        arrayOutPort.mkdir();
        String aopPath = arrayOutPort.getAbsolutePath();
         
        //if the user wants to use the included adapters for trimming
        if (!adapterFile.equals("")) {
            System.out.println(adapterFile);
            //if also an external adapter file was specified
            if (cf.inputDefined("adapter")) {
                System.out.println(adapter.getAbsolutePath());
                //both files are concatenated
                
                FileWriter fw = null;
                FileReader fr1 = null;
                FileReader fr2 = null;
                PrintWriter out = null;
                BufferedReader in1 = null;
                BufferedReader in2 = null;

                try{
                    fw = new FileWriter(aopPath+"/adapters.fa");
                    fr1 = new FileReader(adapter);
                    fr2 = new FileReader("adapters/"+adapterFile);

                    out = new PrintWriter(fw);
                    in1 = new BufferedReader(fr1);
                    in2 = new BufferedReader(fr2);
                    
                    String line;
                    while((line=in1.readLine())!=null)
                        out.println(line);
                    while((line=in2.readLine())!=null)
                        out.println(line);
                } 
                catch (Exception e) {e.printStackTrace();} 
                finally {
                    try {
                        fw.close();
                        fr1.close();
                        fr2.close();
                    } 
                    catch (Exception e2) {e2.printStackTrace();}
                }
                allAdaptersFile=aopPath+"/adapters.fa";
                newAdapterFile=true;
            }                
            else
                //only included adapters were chosen 
                allAdaptersFile="adapters/"+adapterFile;
        }
        //included adapters not chosen
        else {
            //only trim adapters from external file
            if (cf.inputDefined("adapter")) 
                allAdaptersFile=adapter.getAbsolutePath();
            else
                clip=false;
        }

        
        StringBuffer para = new StringBuffer();
        if(clip) {        
            para.append("ILLUMINACLIP:" + allAdaptersFile + ":" + seedMM + ":" + palindromeClip + ":" + simpleClip + ":8:" + String.valueOf(keepBothReads).toUpperCase() + " ");
        }
        if (leading != -1) {
            para.append("LEADING:" + leading + " ");
        }
        if (trailing != -1) {
            para.append("TRAILING:" + trailing + " ");
        }
        if (!sliding.equals("null")) {
            para.append("SLIDINGWINDOW:" + sliding + " ");
        }
        if (headcrop != -1) {
            para.append("HEADCROP:" + headcrop + " ");
        }
        if (crop != -1) {
            para.append("CROP:" + crop + " ");
        }
        if (minlen != -1) {
            para.append("MINLEN:" + minlen +" ");
        }

        // If there are no trimming operations to perform.
        if(para.length() == 0) {
            cf.writeLog("No trimming operations to perform.");
            Writer output = new BufferedWriter(new FileWriter(log));
            output.write("No trimming operations to perform.");
            output.close();
            if(paired) {
                IndexFile iOut = new IndexFile();
                iOut.add("Reads", read);
                iOut.add("Mates", mate);
                (new File(aopPath + "/unpairedReads.fq")).createNewFile();
                (new File(aopPath + "/unpairedMates.fq")).createNewFile();
                iOut.add("Ureads", new File("unpairedReads.fq"));
                iOut.add("Umates", new File("unpairedMates.fq"));
                iOut.write(cf, "trimmedReads");

                //prepare that stats files when no operations were performed
                String [] heading = new String[5];
                heading[0] = "Input_Reads";
                heading[1] = "Output_Reads";
                heading[2] = "Discarded";
                heading[3] = "Forward_Only_Surviving_Percentage";
                heading[4] = "Reverse_Only_Surviving_Percentage";

                CSVWriter out = new CSVWriter(heading, cf.getOutput("stats"));    
                out.write(out.getMissingValue());
                out.write(out.getMissingValue());
                out.write(out.getMissingValue());
                out.write(out.getMissingValue());
                out.write(out.getMissingValue());
                out.close();
            }else {
                IndexFile iOut = new IndexFile();
                iOut.add("Reads", read);
                iOut.write(cf, "trimmedReads");

                String [] heading = new String[3];
                heading[0] = "Input_Reads";
                heading[1] = "Output_Reads";
                heading[2] = "Discarded";

                CSVWriter out = new CSVWriter(heading, cf.getOutput("stats"));    
                out.write(out.getMissingValue());
                out.write(out.getMissingValue());
                out.write(out.getMissingValue());
                out.write(out.getMissingValue());
                out.close();
            }
            stdout.createNewFile();
            return ErrorCode.OK;
        }

        StringBuffer cmd = new StringBuffer();

        // Trimmomatic prints the basic stats to system error stream.
        PrintStream orig_err = System.err;
        PrintStream printStream = new PrintStream(new FileOutputStream(stdout));
        System.setErr(printStream);
        
        if (paired) {
            TrimmomaticPE triPE = new TrimmomaticPE();
			//Trimmomatic triPE = new Trimmomatic();
            //cmd.append("SE "); 
            cmd.append("-threads " + threads + " -" + qual + " -trimlog ");            
            cmd.append(log.getAbsolutePath() + " ");
            cmd.append(read.getAbsolutePath() + " ");
            cmd.append(mate.getAbsolutePath() + " ");
            cmd.append(aopPath + "/"+filenameReads + " " + aopPath + "/unpairedReads.fq " +aopPath + "/" + filenameMates + " " + aopPath + "/unpairedMates.fq ");
            cmd.append(para);
            cf.writeLog("Executing command: " + cmd.toString());
            triPE.main(cmd.toString().split(" "));
            
            IndexFile iOut = new IndexFile();
            iOut.add("Reads", new File(filenameReads));
            iOut.add("Mates", new File(filenameMates));
            iOut.add("Ureads", new File("unpairedReads.fq"));
            iOut.add("Umates", new File("unpairedMates.fq"));
            if (newAdapterFile)
                iOut.add("adapters", new File("adapters.fa"));
            iOut.write(cf, "trimmedReads");
        } else {
			      TrimmomaticSE triSE = new TrimmomaticSE();
            //Trimmomatic triSE = new Trimmomatic();
            //cmd.append("SE ");
            cmd.append("-threads " + threads + " -" + qual + " -trimlog ");
            cmd.append(log.getAbsolutePath() + " ");
            cmd.append(read.getAbsolutePath() + " ");
            cmd.append(aopPath + "/"+filenameReads+" " + para);
            cf.writeLog("Executing command: " + cmd.toString());
            triSE.main(cmd.toString().split(" "));
            
            IndexFile iOut = new IndexFile();
            iOut.add("Reads", new File(filenameReads));
            if (newAdapterFile)
                iOut.add("adapters", new File("adapters.fa"));
            iOut.write(cf, "trimmedReads");
        }
        
        if (orig_err != null) System.setErr(orig_err);
        
        // Write output of standard error to the log file.
        Scanner in = new Scanner(new FileReader(stdout));
        while(in.hasNext()) cf.writeLog(in.nextLine());
        in.close();
        
        // Parse stats from output of standard error.
        in = new Scanner(new FileReader(stdout));
        boolean found = false;
        
        if(paired) {
            Integer inputReads = null;
            Integer bothSurviving = null;
            Integer forwardSurviving = null;
            Integer reverseSurviving = null;
            Integer dropped = null;

            while (in.hasNext() && !found) {
                
                String line = in.nextLine();
                            
                inputReads = finder(".*Input Read Pairs: (\\d+)", line);
                bothSurviving = finder("Both Surviving: (\\d+)", line);
                forwardSurviving = finder("Forward Only Surviving: (\\d+)", line);
                reverseSurviving = finder("Reverse Only Surviving: (\\d+)", line);
                dropped = finder("Dropped: (\\d+)", line);

                if (inputReads != null) found = true;
            }
            in.close();
            
            // Check if the expected output was found.     
            if(inputReads == null) cf.writeError("Pattern '.*Input Reads: (\\d+)' not found from standard error stream.");
            if(bothSurviving == null) cf.writeError("Pattern 'Both Surviving: (\\d+)' not found from standard error stream.");
            if(forwardSurviving == null) cf.writeError("Pattern 'Forward Only Surviving: (\\d+)' not found from standard error stream.");
            if(reverseSurviving == null) cf.writeError("Pattern 'Reverse Only Surviving: (\\d+)' not found from standard error stream.");
            if(dropped == null) cf.writeError("Pattern 'Dropped: (\\d+)' not found from standard error stream.");
            
            String [] heading = new String[5];
            heading[0] = "Input_Reads";
            heading[1] = "Output_Reads";
            heading[2] = "Discarded";
            heading[3] = "Forward_Only_Surviving_Percentage";
            heading[4] = "Reverse_Only_Surviving_Percentage";

            CSVWriter out = new CSVWriter(heading, cf.getOutput("stats"));    
            out.write(inputReads);
            out.write(bothSurviving);
            out.write(dropped);
            out.write(forwardSurviving);
            out.write(reverseSurviving);
            out.close();
        }else {
            Integer inputReads = null;
            Integer surviving = null;
            Integer dropped = null;

            while (in.hasNext() && !found) {
                
                String line = in.nextLine();
                            
                inputReads = finder(".*Input Reads: (\\d+)", line);
                surviving = finder("Surviving: (\\d+)", line);
                dropped = finder("Dropped: (\\d+)", line);
                
                if (inputReads != null) found = true;
            }        
            in.close();

             // Check if the expected output was found.     
            if(inputReads == null) cf.writeError("Pattern '.*Input Reads: (\\d+)' not found from standard error stream.");
            if(surviving == null) cf.writeError("Pattern 'Surviving: (\\d+)' not found from standard error stream.");
            if(dropped == null) cf.writeError("Pattern 'Dropped: (\\d+)' not found from standard error stream.");
            
            String [] heading = new String[3];
            heading[0] = "Input_Reads";
            heading[1] = "Output_Reads";
            heading[2] = "Discarded";

            CSVWriter out = new CSVWriter(heading, cf.getOutput("stats"));    
            out.write(inputReads);
            out.write(surviving);
            out.write(dropped);
            out.close();
        }
        return ErrorCode.OK;
    }
    
    private Integer finder(String regexp, String line) {
        Pattern pattern = Pattern.compile(regexp);
        Matcher matcher = pattern.matcher(line);
        if (matcher.find()) {
            String m = matcher.group(1);
            return Integer.parseInt(m);
        }
        return null;
    }

}
