#!usr/bin/perl

use strict;
use componentSkeleton;
use File::Path qw(make_path);
use Cwd 'abs_path';

my $ref_arrayIn;
my $adapter;
my $arrayOutPort;
my $paired;
my $leading;
my $tailing;
my $crop;
my $headcrop;
my $minlen;
my $qual;
my $threads;
my $simpleClip;
my $palindromeClip;
my $mm;
my $sliding;
my $keepBothReads;
my $stdout;
sub execute {

	my ($cf_ref) = @_;

	# Read input, output and parameters
	$ref_arrayIn = get_InputArrayIndex($cf_ref, "data");
	$adapter = get_input($cf_ref, "adapter");
	$arrayOutPort = get_output($cf_ref, "trimmedReads");
    $stdout = get_output($cf_ref, "stdout");
	$paired = get_parameter($cf_ref, "paired");
	$leading = get_parameter($cf_ref, "leading");
	$tailing = get_parameter($cf_ref, "tailing");
	$crop = get_parameter($cf_ref, "crop");
	$headcrop = get_parameter($cf_ref, "headcrop");
	$minlen = get_parameter($cf_ref, "minlen");
	$qual = get_parameter($cf_ref, "qual");
	$threads = get_parameter($cf_ref, "threads");
	$simpleClip = get_parameter($cf_ref, "simpleClip");
    $palindromeClip = get_parameter($cf_ref, "palindromeClip");
	$mm = get_parameter($cf_ref, "seedMM");
	$sliding = get_parameter($cf_ref, "slidingWindow");
    $keepBothReads = get_parameter($cf_ref, "keepBothReads")

	# Command parameters
	my $para = "";
	if(input_defined($cf_ref,"adapter")){
		$para = $para."ILLUMINACLIP:".$adapter.":".$mm.":".$palindromeClip.":".$simpleClip.":8:".$keepBothReads." ";
	}
	if($leading!=-1){
		$para = $para."LEADING:".$leading." ";
	}
	if($tailing!=-1){
		$para = $para."TRAILING:".$tailing." ";
	}
	if($crop!=-1){
		$para = $para."CROP:".$crop." ";
	}
	if($headcrop!=-1){
		$para = $para."HEADCROP:".$headcrop." ";
	}
	if($minlen!=-1){
		$para = $para."MINLEN:".$minlen." ";
	}
	if($sliding ne "null"){
		$para = $para."SLIDINGWINDOW:".$sliding." ";
	}

	# Trim sequences
	my $cmd = "java -classpath ";
	my $jar_path = abs_path($0);
	$jar_path =~ s/trimmomatic.pl/trimmomatic-0.20.jar/;
  
	make_path($arrayOutPort);
	if($paired eq "true"){
		$cmd = $cmd.$jar_path." org.usadellab.trimmomatic.TrimmomaticPE -threads ".$threads." -".$qual." ";
		$cmd = $cmd.$ref_arrayIn->{"read"}." ".$ref_arrayIn->{"mate"}." "8:;
		$cmd = $cmd.$arrayOutPort."/trimPEread.fastq ".$arrayOutPort."/trimUPEread.fastq ".$arrayOutPort."/trimPEmate.fastq ".$arrayOutPort."/trimUPEmate.fastq ";
		$cmd = $cmd.$para;
		open(FILEOUT,">".$arrayOutPort."/_index");
		print FILEOUT "Key\tFile\n"."PEread\ttrimPEread.fastq\n"."PEmate\ttrimPEmate.fastq\n"."UPEread\ttrimUPEread.fastq\n"."UPEmate\ttrimUPEmate.fastq\n";
		close(FILEOUT);	
	}else{
		$cmd = $cmd.$jar_path." org.usadellab.trimmomatic.TrimmomaticSE -threads ".$threads." -".$qual." ";
		$cmd = $cmd.$ref_arrayIn->{"read"}." ";
		$cmd = $cmd.$arrayOutPort."/trimSEread.fastq ".$para;
		open(FILEOUT,">".$arrayOutPort."/_index");
		print FILEOUT "Key\tFile\n"."SEread\ttrimSEread.fastq\n";
		close(FILEOUT);
	}
	#print $cmd;
	#system($cmd);
    my $output = `$cmd`; 
    open(FILEOUT,">".$stdout);
    print FILEOUT $output;
    close(FILEOUT);

}

if(scalar @ARGV == 0){
    print "NO_COMMAND_FILE";
    exit;
}
my %cf = parse_command_file($ARGV[0]);

execute(\%cf);
