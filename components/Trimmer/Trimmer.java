import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.IndexFile;
import org.anduril.component.SkeletonComponent;
import java.io.FileNotFoundException;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import rnaSeq.*;

public class Trimmer extends SkeletonComponent{
	private File trimInfo;
	private IndexFile perBaseSeqQualIdx;
	private IndexFile data;
	private IndexFile trimmedReads;
	private File outDir;
	private boolean paired;
	private int score;

    protected ErrorCode runImpl(CommandFile cf) throws Exception {

    	getInsOuts(cf);

    	if(cf.inputDefined("perBaseSeqQual") && cf.inputDefined("trimInfo")){
    		System.err.println("'perBaseSeqQual' and 'trimInfo' can not be both true!");
    		System.exit(0);
    	}

    	if(!cf.inputDefined("perBaseSeqQual") && !cf.inputDefined("trimInfo")){
    		System.err.println("'perBaseSeqQual' and 'trimInfo' can not be both false!");
    		System.exit(0);
    	}

    	Map<String, ArrayList<Integer>> myMap = new HashMap<String, ArrayList<Integer>>();

    	if(cf.inputDefined("perBaseSeqQual")){
    		myMap.put("read", get_trimmed_info(perBaseSeqQualIdx.getFile("read"), score));
    		if(paired){
    			myMap.put("mate", get_trimmed_info(perBaseSeqQualIdx.getFile("mate"), score));
    		}
    	}
    	if(cf.inputDefined("trimInfo")){
    		try {
                Scanner thisFile = new Scanner(trimInfo);
                thisFile.nextLine();
                while(thisFile.hasNext()){
                	ArrayList<Integer> myList = new ArrayList<Integer>();
                	String s = thisFile.next();
                	myList.add(thisFile.nextInt());
                	myList.add(thisFile.nextInt());
                	myMap.put(s, myList);
                }
    		}catch (FileNotFoundException e) {
                        e.printStackTrace();
    		}
    	}

    	File f = data.getFile("read").getAbsoluteFile();
        File fout = new File(outDir.getAbsolutePath()+"/"+f.getName());

        SeqTrimmer trimmer = new SeqTrimmer(f, fout, myMap.get("read").get(0), myMap.get("read").get(1));
        trimmedReads.add("read", new File(f.getName()));

	if(paired){
		f = data.getFile("mate");
        	fout = new File(outDir+"/"+f.getName());

            	trimmer = new SeqTrimmer(f, fout, myMap.get("mate").get(0), myMap.get("mate").get(1));
            	trimmedReads.add("mate", new File(f.getName()));
	}

    	trimmedReads.write(cf, "trimmedReads");

        return ErrorCode.OK;
    }
    public static void main(String[] argv) {
        new Trimmer().run(argv);
    }

    /**
     * Gets trimmed positions
     *
     */
    private ArrayList<Integer> get_trimmed_info(File f, int score){

    	ArrayList<Integer> myMap = new ArrayList<Integer>();
    	List <String> position = new ArrayList<String>();
    	List <Float> qualVal = new ArrayList<Float>();

    	Scanner thisFile;
		try {
			thisFile = new Scanner(f);
			thisFile.nextLine();
 	                while(thisFile.hasNext()){
                		String s = thisFile.nextLine();
            			String [] tmp = s.split("\t");
            			position.add(tmp[0]);
            			qualVal.add(Float.parseFloat(tmp[1]));
            		}
		} catch (FileNotFoundException e) {
			System.out.println("Reading 'perBaseSeqQual' files has errors!");
			e.printStackTrace();
		}

		int left = check_scores(position, qualVal, score, "5end");
		int right = check_scores(position, qualVal, score, "3end");

		myMap.add(left);
		myMap.add(right);

    	return myMap;
    }

    /**
     *  Check read qualities
     */
    private int check_scores(List <String> pos, List <Float> qual, int cutoff, String oriation){
    	int res = 0;
    	int center;
    	if(qual.size()%2==0){
    		center = qual.size()/2;
    	}else{
    		center = (qual.size()-1)/2;
    	}
    	int idx;
    	if(oriation.equals("5end")){
    		for(int i=0;i<center;i++){
    			if(qual.get(i)<cutoff){
    				continue;
    			}else{
    				idx = i;
				String[] tmp = pos.get(idx).split("-");
    				res = Integer.parseInt(tmp[tmp.length-1]);
    				break;
    			}
    		}
    	}else{
    		for(int i=qual.size()-1;i>=center;i--){
    			if(qual.get(i)<cutoff){
    				continue;
    			}else{
    				idx = i;
    				String[] tmp = pos.get(idx).split("-");
    				res = Integer.parseInt(tmp[tmp.length-1]);
    				break;
    			}
    		}
    	}
    	return res;
    }

    /**
     * Gets inputs, outputs and parameters from the command file
     *
     */
    private void getInsOuts(CommandFile cf) throws Exception{
            // Inputs and outputs
            if(cf.inputDefined("perBaseSeqQual")){
            	perBaseSeqQualIdx = cf.readInputArrayIndex("perBaseSeqQual");
	    }

            if(cf.inputDefined("trimInfo")){
                    trimInfo = cf.getInput("trimInfo");
	    }

	    data = cf.readInputArrayIndex("data");

            outDir = cf.getOutput("trimmedReads");
            outDir.mkdirs();
            trimmedReads = new IndexFile();

            // Parameters
            paired = cf.getBooleanParameter("paired");
            score = cf.getIntParameter("score");
    }
}



