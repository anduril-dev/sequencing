#!/usr/bin/env python
import anduril
import os
from anduril.args import (
    output_expected_values, output_map_estimates, output_diff_meth,
    output_params, output_probs, output_pvalues, input_files, sample_compositions,
    probs_format, index_file, pvalues_format, map_format, ev_format, diffmeth_format)
from anduril.args import tempdir as TEMPDIR
import struct
import subprocess
import re
import sys
from util import get_max, list_mul, MultiFileLineIter, gzopen
import numpy as np

sample_count = len(input_files)

DMML_DIR = os.path.join(os.path.dirname(__file__), "../../lib/dmml")
DMML_CMD = os.path.join(DMML_DIR, "dmml")
DMRTOOL_CMD = os.path.join(DMML_DIR, "dmrtool")
PD2ASC_CMD = os.path.join(DMML_DIR, "pd2asc")
PROC_CMD = os.path.join(DMML_DIR, "process_probs")
pvalues_tmp = os.path.join(TEMPDIR, "pvalues")

# Check that everything is OK before running anything
def check():
    for p in [DMML_CMD, DMRTOOL_CMD, PD2ASC_CMD, PROC_CMD]:
        if not os.path.exists(p):
            raise RuntimeError("Program {} doesn't exist".format(p))

    for p in ["probs_format", "pvalues_format", "map_format", "ev_format",
              "diffmeth_format"]:
        v = globals()[p]
        if not v in ["a", "b", ""]:
            raise RuntimeError("Invalid format \"{}\" for arg {}: ".format(v, p))


# Evil global variabales
ev_header = None
map_header = None
diff_meth_header = None
samplecomps = []


def get_mask_prefixes(in_files):
    res = []
    for mask_key, fname in in_files.items():
        try:
            mask = int(mask_key.split(":")[0])
        except:
            res.append("")
            continue
        res.append("{}:".format(mask))
    return res


# Get the DMML cmd from Anduril args.
def get_dmml_cmd(files):
    res = [DMML_CMD]
    output_map = {"params_format": output_params, "probs_format": output_probs,
                  "pvalues_format": pvalues_tmp}
    args = get_anduril_params()
    [args.pop(x) for x in ["param_map_format", "param_ev_format", "param_diffmeth_format"]]
    for akey, aval in args.items():
        if aval == "":
            continue
        p = akey.replace("param_", "")
        if p in output_map:
            # Null output.
            if aval not in ["b", "a"]:
                continue
            name = p.replace("_", "-").replace("-format", "")
            res.append("--output-{}={}:{}".format(name, aval, output_map[p]))
        else:
            res.append("--{}={}".format(p.replace('_', '-'), aval))
    res.extend(files)
    return res


def get_anduril_params():
    pkeys = {p for p in dir(anduril.args) if p.startswith('param_')}
    return {p: getattr(anduril.args, p) for p in pkeys}


def convert_file(path, out):
    with gzopen(out, "wb") as ofile:
        ofile.write(subprocess.check_output([DMRTOOL_CMD, "view", "-b", path]))
    return out


def init_output():
    data = ""
    flist = [output_params, output_probs, output_pvalues,
             output_map_estimates, output_diff_meth]
    for fname in flist:
        with gzopen(fname, "wb") as ofile:
            ofile.write(data)


def is_binary_format(path):
    with gzopen(path, "rb") as f:
        header = f.read(4)
    return struct.unpack("=L", header)[0] == 0x62726d64  # dmrb


def preprocess_files(file_list):
    '''
    Convert input files to binary format, and return list containing paths
    to the converted files consecutively. If a file is already in binary format,
    original path is put into the list and no conversion is done.
    '''
    res = []
    for f in file_list:
        if is_binary_format(f):
            res.append(f)
        else:
            in_f = os.path.basename(f)
            _, ext = os.path.splitext(in_f)
            conv = os.path.join(TEMPDIR, in_f.replace(ext, ".dmrb"))
            convert_file(f, conv)
            res.append(conv)
    return res


def pd2asc(path, field_names, width):
    fnames = ','.join(field_names)
    p = subprocess.Popen([PD2ASC_CMD, "--fieldnames={}".format(fnames),
                         '--width={}'.format(width), path],
                         stdout=subprocess.PIPE)
    return p


# Get the methylation pattern corresponding to the n'th column, as a list of
# 0's and 1's
def get_met_pattern(samples, n):
    fmt = "{{0:0{}b}}".format(samples)
    res = [int(c) for c in fmt.format(n)]
    res.reverse()
    return res


# Get original filename from the input file keys.
def get_orig_fname(fname_k):
    temp = fname_k
    if not os.path.exists(fname_k):
        temp = re.sub("\d+:", "", temp)
    return temp


def calc_evs_from_probs_l(line):
    probs = [float(f.strip()) for f in line.strip().split('\t')]
    sample_count = len(input_files)
    evalues = [0 for s in range(0, sample_count)]
    # The actual calculation.
    for j, prob in enumerate(probs):
        pat = get_met_pattern(sample_count, j)
        list_mul(pat, prob)
        evalues = map(sum, zip(evalues, pat))

    return evalues


def probs_l_to_map_l(line):
    cols = [float(f.strip()) for f in line.split('\t')]
    ind, val = get_max(cols)
    met = (str(m) for m in get_met_pattern(sample_count, ind))
    met_s = '\t'.join(met)
    return met_s


def probs_l_to_diff_meth_l(line):
    evs = calc_evs_from_probs_l(line)
    res = []
    for i in range(0, len(evs)):
        for ii in range(i+1, len(evs)):
            res.append(evs[i]-evs[ii])
    return "\t".join((str(d) for d in res))


def probs_l_to_ev_l(line):
    return "\t".join(("{}".format(e) for e in calc_evs_from_probs_l(line)))


def get_diff_meth_fields(fnames):
    tmp = []
    for i in range(0, len(fnames)):
        for ii in range(i+1, len(fnames)):
            tmp.append("{} vs {}".format(fnames[i], fnames[ii]))
    return tmp


def get_probs_iterable(probs_file):
    sample_count = len(input_files)
    sample_comb = 2**sample_count
    if probs_format == "b":
        # TODO: proper names for methylation patterns
        fields = ['{}'.format(i) for i in range(0, sample_comb)]
        p = pd2asc(probs_file, field_names=fields, width=sample_comb)
        return p.stdout
    else:
        return gzopen(probs_file, "r").get_handle()


def get_header_pvalue(line):
    fnames_d = {}
    for mask, fname in zip(samplecomps, input_files):

        if mask != 1:  # Control
            fnames_d[mask] = fname

    if "3" in fnames_d:
        fnames_d["left"] = fnames_d["3"]
    if "5" in fnames_d:
        fnames_d["right"] = fnames_d["5"]

    def get_fixed_fname(key):
        for k, v in fnames_d.items():
            if key.find(k) != -1:
                return key.strip().replace(k, v)
        return key

    head = line.replace("p-value", "")
    _new_h = []
    new_head = ""
    for h in head.split("\t"):
        left, right = h.strip().split(" vs ")
        entry = " vs ".join(map(get_fixed_fname, [left, right]))
        _new_h.append(entry)

    new_head = "\t".join(_new_h)
    return new_head + "\n"


def repair_pvalues(pvalues_in, pvalues_out, index_file=None):
    '''
    Place original filenames to the header in pvalues file. Only works if
    pvalues are in ASCII and masks are defined explicitly.
    '''
    ind_header = "Chromosome\tSite\tStrand"
    src_iterables = [pvalues_in]
    if index_file:
        src_iterables.append(index_file)
    src_iterables = [open(fname, "r") for fname in src_iterables]

    mfli = MultiFileLineIter(src_iterables)

    def lines_cb_pvalue(i, lp):
        if i == 0:
            header = get_header_pvalue(lp[0])
            if index_file:
                return "\t".join([ind_header, header])
            return header
        if index_file:
            return "{}\t{}".format(lp[1].strip(), lp[0])
        else:
            return lp[0]

    ofile = open(pvalues_out, "w")
    mfli.add_pair(lines_cb_pvalue, ofile)
    mfli.perform()
    ofile.close()

    for si in src_iterables:
        si.close()


# For EV's, MAP estimates and DiffMeth's
def get_header_with_index(fmt, fnames, index=False):
    res = "\t".join(fmt.format(f) for f in fnames)
    if index:
        res = "Chromosome\tSite\tStrand" + "\t" + res
    return res


# Line callbacks
def lines_cb_ev(i, lp):
    if i == 0:
        return ev_header + "\n"
    if index_file:
        return "{}\t{}\n".format(lp[0].strip(), probs_l_to_ev_l(lp[1]))
    else:
        return "{}\n".format(probs_l_to_ev_l(lp[0]))


def lines_cb_map(i, lp):
    if i == 0:
        return map_header + "\n"
    if index_file:
        return "{}\t{}\n".format(lp[0].strip(), probs_l_to_map_l(lp[1]))
    else:
        return "{}\n".format(probs_l_to_map_l(lp[0]))


def lines_cb_diff_meth(i, lp):
    if i == 0:
        return diff_meth_header + "\n"
    if index_file:
        return "{}\t{}\n".format(lp[0].strip(), probs_l_to_diff_meth_l(lp[1]))
    else:
        return "{}\n".format(probs_l_to_diff_meth_l(lp[0]))


# Calculate MAP estimates, expected values and differential methylation.
def do_post_calculations(input_files, output_map_estimates="",
                         output_expected_values="", output_diff_meth="",
                         index_file=None):

    fnames = [get_orig_fname(k) for k in input_files.keys()]
    fnames = map(os.path.basename, fnames)
    ind_header = "Chromosome\tSite\tStrand"
    globals()["ev_header"] = "\t".join(("E({})".format(f) for f in fnames))
    globals()["map_header"] = "\t".join(("Met({})".format(f) for f in fnames))
    globals()["diff_meth_header"] = get_diff_meth_header(fnames)

    if index_file:
        globals()["ev_header"] = ind_header + "\t" + ev_header
        globals()["map_header"] = ind_header + "\t" + map_header
        globals()["diff_meth_header"] = ind_header + "\t" + diff_meth_header

    cb_handle_pairs = []
    map_handle = None
    ev_handle = None
    src_iterables = []

    if index_file:
        src_iterables.append(open(index_file, "r"))
    src_iterables.append(get_probs_iterable(output_probs))

    for cb, o in zip([lines_cb_map, lines_cb_ev, lines_cb_diff_meth], ["map_estimates", "expected_values", "diff_meth"]):
        cb_handle_pairs.append((cb, open(globals()["output_{}".format(o)], "w")))

    mfli = MultiFileLineIter(src_iterables)
    for cb, fhandle in cb_handle_pairs:
        mfli.add_pair(cb, fhandle)

    mfli.perform()

    all_handles = [map_handle, ev_handle] + src_iterables
    for handle in all_handles:
        try:
            handle.close()
        except Exception as e:
            sys.stderr.write(str(e) + "\n")


with open(sample_compositions, "r") as sc:
    samplecomps = sc.read().strip().split("\n")

if len(samplecomps) != len(input_files):
    raise RuntimeError("len(samplecomps) != len(input_files) ({} != {})".format(len(samplecomps), len(input_files)))

def get_samplecount(samplecomps):
    return len(set(samplecomps))

def get_fields_replicated(samplecomps):
    return ["composition {}".format(i) for i in set(samplecomps)]

def process_probs(input_files, output_map_estimates, output_expected_values,
                  output_diff_meth, output_probs, index_file):
    if probs_format != "b":
        raise RuntimeError("Probability matrix must be in binary format")
    cmd = [PROC_CMD]
    ncols = 2**(get_samplecount(samplecomps))
    cmd.extend(["-c", str(ncols)])
    if map_format:
        cmd.extend(["-m", "{}:{}".format(map_format, output_map_estimates)])
    if ev_format:
        cmd.extend(["-e", "{}:{}".format(ev_format, output_expected_values)])
    if diffmeth_format:
        cmd.extend(["-d", "{}:{}".format(diffmeth_format, output_diff_meth)])
    if index_file:
        cmd.extend(["-i", index_file])

    sc = sorted(set(samplecomps))
    res=['a']*len(sc)
    for ind, name in enumerate(input_files):
        res[sc.index(samplecomps[ind])] = name

    asd = res
    if get_samplecount(samplecomps) != len(input_files):
        cmd.extend(["-f", ",".join(get_fields_replicated(samplecomps))])
    else:
        cmd.extend(["-f", ",".join(asd)])
    cmd.append(output_probs)

    print "CMD", " ".join(cmd)
    out = subprocess.check_output(cmd)


check()
init_output()
ifiles = preprocess_files(input_files.values())
masks = ["{}:".format(m) for m in samplecomps]
cmd = get_dmml_cmd(['{}{}'.format(m, f) for m, f in zip(masks, ifiles)])
print "DMML CMD", cmd
out = subprocess.check_output(cmd)
if pvalues_format == "b":
    os.rename(pvalues_tmp, output_pvalues)
elif pvalues_format == "a":
    repair_pvalues(pvalues_tmp, output_pvalues, index_file)

process_probs(input_files, output_map_estimates, output_expected_values, output_diff_meth, output_probs, index_file)

# do_post_calculations(input_files, output_map_estimates, output_expected_values,
#                      output_diff_meth, index_file)

