import gzip

class gzopen(object):

    def __init__(self, filename, mode):
        self.filename = filename
        self.mode = mode
        self.__handle = None

        if filename.endswith('.gz'):
            self.__handle = gzip.GzipFile(filename, mode)
        else:
            self.__handle = open(filename, mode)

    def __enter__(self):
        return self.__handle

    def __exit__(self, exc_type, exc_value, traceback):
        self.__handle.close()

    def get_handle(self):
        return self.__handle


# ONLY WORKS FOR TEXT INPUTS.
class MultiFileLineIter(object):

    def __init__(self, src_iterables=[]):
        self.src_iterables = src_iterables
        self.pairs = []

    def add_pair(self, cb, fhandle):
        self.pairs.append((cb, fhandle))

    def perform(self):
        for i, lines in enumerate(zip(*(self.src_iterables))):
            for cb, fhandle in self.pairs:
                fhandle.write(cb(i, lines))

    def write(self, string, ind):
        self.pairs[ind][1].write(string)


def get_max(arr):
    val = 0
    ind = 0
    for i, v in enumerate(arr):
        if v > val:
            ind = i
            val = v
    return ind, val


def list_mul(l_list, val):
    for i in range(0, len(l_list)):
        l_list[i] = l_list[i]*val

