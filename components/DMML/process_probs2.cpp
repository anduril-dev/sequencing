#include <algorithm>
#include <string>
#include <exception>
#include <vector>
#include <memory>
#include <iostream>
#include <cmath>
#include <unistd.h>
#include <cstdlib>
#include <numeric>
#include <memory>

struct FileMode {
	std::string path;
	std::string mode;
};

void
replaceAll(std::vector<std::string>& v, const char s, const char repl)
{
	for(auto& vv : v)
		std::replace(vv.begin(), vv.end(), s, repl);
}

static
void
_writeRowASCII(const std::vector<std::string>& vec, const std::string sep, FILE *fp)
{
	for (size_t i = 0; i < vec.size()-1; i++)
		fprintf(fp, "%s%s", vec[i].c_str(), sep.c_str());
	fprintf(fp, "%s\n", vec.back().c_str());
}

static
void
_writeRowASCII(const std::vector<double>& vec, const std::string sep, FILE *fp)
{
	for (size_t i = 0; i < vec.size()-1; i++)
		fprintf(fp, "%.15g%s", vec[i], sep.c_str());
	fprintf(fp, "%.15g\n", vec.back());
}

// Get methylation pattern corresponding to column col in the probability matrix
static std::vector<double>
getMetPattern(size_t col, size_t samplecount)
{
	std::vector<double> res(samplecount, 0);
	for(size_t i = 0; i < samplecount; i++)
		res[i] = ((col >> i) & 1);

	return res;
}

template<typename T, typename C>
void
vecMul(std::vector<T>& v, C c)
{
	for(size_t i = 0; i < v.size(); i++)
		v[i] = v[i]*static_cast<T>(c);
}

template<typename T>
void
vecAdd(std::vector<T>& v1, const std::vector<T>& v2)
{
	for(size_t i = 0; i < v1.size(); i++)
		v1[i] = v1[i] + v2[i];
}

std::vector<std::string>
parseFieldNames(const std::string& str, const std::string& sep) {
	size_t beg = 0;
	size_t end = 0;
	std::vector<std::string> s;
	while (end != std::string::npos) {
		end = str.find(sep, beg);
		s.push_back(str.substr(beg, end - beg));
		beg = end + 1;
	}
	return s;
}

std::vector<std::string>
getDMHeader(const std::vector<std::string>& smpl) {
	std::vector<std::string> res;
	for(size_t i = 0; i < smpl.size(); i++)
		for(size_t ii = i + 1; ii < smpl.size(); ii++)
			res.push_back(smpl[i] + " vs " + smpl[ii]);
	return res;
}

template<typename T = double>
struct TableWriter {
	std::vector<std::string> m_columns;
	std::string m_path;
	std::string m_mode;
	FILE *m_fp;
	TableWriter(std::string& name, std::string mode) : m_path(name), m_mode(mode), m_fp(nullptr) {}

	void setColumns(const std::vector<std::string> cols)  { m_columns = cols; }
	void writeHeader() {
		_writeRowASCII(m_columns, "\t", m_fp);
	}
	// Write raw ascii data
	void writeASCII(const char *buf, size_t len) {
		fwrite(buf, sizeof(*buf), len, m_fp);
	}

	void writeRowBinary (T *buf) {
		fwrite(buf, sizeof(T), m_columns.size(), m_fp);
	}

	void open() {
		std::cout << "opening " << m_path << "mode " << m_mode << std::endl;
		if (m_mode == "b")
			m_fp = fopen(m_path.c_str(), "wb");
		else if (m_mode == "a")
			m_fp = fopen(m_path.c_str(), "w");
		if (m_fp == NULL)
			throw std::runtime_error("Couldn't open file for write");
	}

	bool isOpen() { return m_fp != nullptr; }

	void close() {
		fclose(m_fp);
	}
};

template<typename T = double>
struct TableReader {
	std::string m_path;
	std::vector<std::string> m_columns;
	std::string m_mode;
	FILE *m_fp;
	char *m_asciiBuf;
	ssize_t m_asciiBufLen;
	size_t m_asciiBufSize;
	std::unique_ptr<T[]> m_binaryBuf;

	TableReader(std::string path, std::string mode, std::string sep) : m_path(path), m_mode(mode), m_fp(nullptr), m_asciiBuf(nullptr), m_asciiBufLen(0), m_asciiBufSize(0) {}
	size_t readRowASCII() {
		ssize_t res = getline(&m_asciiBuf, &m_asciiBufSize, m_fp);
		m_asciiBufLen = res + 1; // buffer contains also the NULL byte
		return std::max(res, 0L);
	}
	size_t readRowBinary () {
		return fread(m_binaryBuf.get(), sizeof(T), m_columns.size(), m_fp);
	}
	// Returns number of bytes read
	size_t readRow() {
		if (m_mode == "a")
			return readRowASCII();
		else if (m_mode == "b")
			return readRowBinary();
		return 0;
	}
	// Setup columns by reading a header
	std::vector<std::string> readColumns() {
		readRowASCII();
		m_columns = parseFieldNames(std::string(m_asciiBuf), "\t");
		m_binaryBuf.reset(new T[m_columns.size()]);
		return m_columns;
	}
	void setColumns(const std::vector<std::string> cols)  {
		m_columns = cols;
		m_binaryBuf.reset(new T[m_columns.size()]);
	}
	void open() {
		std::cout << "opening " << m_path << "mode " << m_mode << std::endl;
		if (m_mode == "b")
			m_fp = fopen(m_path.c_str(), "rb");
		else if (m_mode == "a")
			m_fp = fopen(m_path.c_str(), "r");
		if (m_fp == NULL)
			throw std::runtime_error("Couldn't open file for read");
	}
	void close() {
		fclose(m_fp);
	}

	~TableReader() {
		if (m_asciiBuf != nullptr)
			free(m_asciiBuf);
	}
	bool isOK() { return m_fp != nullptr; }
};

template<typename T>
struct CallBacks {
	static std::vector<T> MAPEstimate(T *buf, size_t len) {
		auto it = std::max_element(&buf[0], &buf[len]);
		// Get methylation pattern corresponding to the index of max value
		return getMetPattern(it - &buf[0], std::log2(len));
	}

	static std::vector<T> DiffMeth(T *buf, size_t len) {
		auto evs = calculateEV(buf, len);
		std::vector<T> res;
		for(size_t i = 0; i < evs.size(); i++)
			for(size_t ii = i + 1; ii < evs.size(); ii++)
				res.push_back(evs[i] - evs[ii]);
		return res;
	}

	static std::vector<T> calculateEV(T *buf, size_t len)
	{
		size_t sampleCount = (size_t) std::log2(len);
		std::vector<T> evalues(sampleCount, 0);
		for (size_t i = 0; i < len; i++) {
			// get the methylation pattern as vector of 0's and 1's corresponding to specific column
			auto metPattern = getMetPattern(i, sampleCount);
			// multiply the vector by the specific probability
			vecMul(metPattern, buf[i]);
			if (evalues.size() != metPattern.size()) {
				fprintf(stderr, "size mismatch: %lu != %lu", evalues.size(), metPattern.size());
				std::abort();
			}
			vecAdd(evalues, metPattern);
		}
		return evalues;
	}

	static std::vector<T> ExpectedValues(T *buf, size_t len) {
		std::vector<T> evalues(len);
		return calculateEV(buf, len);
	}
};

typedef CallBacks<double> CD;
static constexpr std::array<decltype(&CD::MAPEstimate), 3> CBs = { CD::MAPEstimate, CD::ExpectedValues, CD::DiffMeth };

void perform(FileMode probs, FileMode index, FileMode map, FileMode ev, FileMode diffmeth, size_t ncols, std::vector<std::string> sampleNames)
{
	TableReader<double> Tprobs(probs.path, probs.mode, "\t");
	TableReader<double> Tindex(index.path, index.mode, "\t");
	Tprobs.open();
	if (Tindex.m_path != "")
		Tindex.open();

	// Set columns
	if (Tindex.isOK())
		Tindex.readColumns();

	if (Tprobs.m_mode == "a")
		Tprobs.readColumns();
	else if (Tprobs.m_mode == "b")
		Tprobs.setColumns(std::vector<std::string>(ncols));

	// Setup writers and write headers
	TableWriter<double> Wmap(map.path, map.mode), Wev(ev.path, ev.mode), Wdm(diffmeth.path, diffmeth.mode);
	static std::array<TableWriter<double>*, CBs.size() > writers = {&Wmap, &Wev, &Wdm};
	for (auto w : writers) {
		if (w->m_path.empty())
			continue;

		std::vector<std::string> cols;
		if (w->m_mode == "a")
			cols = Tindex.m_columns;
		std::vector<std::string> colss(sampleNames);
		if (w == &Wdm)
			colss = getDMHeader(sampleNames);
		cols.insert(cols.end(), colss.begin(), colss.end());
		replaceAll(cols, '\n', '\0');
		w->open();
		w->setColumns(cols);
		w->writeHeader();
	}

	// Read all rows
	while (Tprobs.readRow() != 0) {
		if (Tindex.isOK() && (Tindex.readRow() == 0))
			throw std::runtime_error("Index is too short");

		// Invoke callbacks and write
		for (size_t i = 0; i < CBs.size(); i++) {
			std::vector<double> res = CBs[i](Tprobs.m_binaryBuf.get(), Tprobs.m_columns.size());

			if (writers[i]->m_mode == "a") {
				if (Tindex.isOK()) {
					writers[i]->writeASCII(Tindex.m_asciiBuf, std::max(Tindex.m_asciiBufLen - 2, 0L)); // Skip newline and null byte
					fputc('\t', writers[i]->m_fp);
				}
				// Convert to string and write
				_writeRowASCII(res, "\t", writers[i]->m_fp);
			}
			else if (writers[i]->m_mode == "b")
				writers[i]->writeRowBinary(&res[0]); // specs guarantee contiguous memory
		}
	}

	// Close readers and writers
	Tprobs.close();
	if (Tindex.isOK())
		Tindex.close();
	for (auto w : writers)
		if (w->isOpen())
			w->close();
}

struct Opts {
	FileMode probs, index, map, diffmeth, ev;
	std::string inPath, indexPath;
	long long ncols;
	std::vector<std::string> sampleNames;
};

std::ostream& operator<<(std::ostream& os, const FileMode& o) {
	os << "path: " << o.path << ", format: " << o.mode;
	return os;
}

void parseOutPath(const std::string& in, std::string& outPath, std::string& fmt) {
	fmt = "b";
	if (in.find("a:") == 0) {
		fmt = "a";
		outPath = in.substr(2, in.length()-2);
	}
	else if (in.find("b:") == 0) {
		fmt = "b";
		outPath = in.substr(2, in.length()-2);
	}
}

void
parse_args(int argc, char *argv[], Opts& opts) {
	std::vector<std::string> args;
	args.assign(&argv[1], &argv[1] + (argc - 1));
	std::string outPath;
	std::string fmt = "b";
	opts.inPath = args.back();
	opts.probs.path = args.back();
	opts.probs.mode = "b";
	for (size_t i = 0; i < args.size(); i++) {
		if (args[i][0] == '-' && i < (args.size() - 1))
			switch(args[i][1]) {
				case 'i':
					opts.index.path.assign(args[i+1]);
					opts.index.mode = "a";
					break;
				case 'm':
					parseOutPath(args[i+1], opts.map.path, opts.map.mode);
					break;
				case 'd':
					parseOutPath(args[i+1], opts.diffmeth.path, opts.diffmeth.mode);
					break;
				case 'e':
					parseOutPath(args[i+1], opts.ev.path, opts.ev.mode);
					break;
				case 'f':
					opts.sampleNames = parseFieldNames(args[i+1], ",");
					break;
				case 'c':
					opts.ncols = atoll(args[i+1].c_str());
					break;
				default:
					break;
			}
	}
}

int main (int argc, char **argv) {
	if (argc < 2) {
		fprintf(stderr, "Usage: %s cols probs_file", argv[0]);
		return 0;
	}

	Opts opts;
	parse_args(argc, argv, opts);

	if (opts.sampleNames.size() == 0)
		for(size_t i = 0; i < std::log2(opts.ncols); i++)
			opts.sampleNames.push_back(std::to_string(i));

	perform(opts.probs, opts.index, opts.map, opts.ev, opts.diffmeth, opts.ncols, opts.sampleNames);
}
