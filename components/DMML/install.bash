#!/bin/bash

. $ANDURIL_HOME/lang/bash/generic.sh || {
    echo "ANDURIL_HOME may not be set!"
    exit 1
    }

myDir=$(dirname $(realpath $0))
cd $myDir

iscmd hg || {
    echo Mercurial not installed'!'
    exit 1
}

NAME=dmml
URL="https://bitbucket.org/anthakki/dmml"
TGTDIR=../../lib/dmml

[[ -d "$TGTDIR" ]] && {
    echo $NAME already installed. Updating.
    cd "$TGTDIR"
    [[ -h dmml ]] && rm dmml 
    hg pull
    hg up -C
	make clean
	make CFLAGS.EXTRA="-DHAVE_ERFC -DHAVE_ZLIB" LIBS.EXTRA="-lz"
    exit $?
}

hg clone "$URL" "$TGTDIR"
cd "$TGTDIR"
make CFLAGS.EXTRA="-DHAVE_ERFC -DHAVE_ZLIB" LIBS.EXTRA="-lz"
cd $myDir
make
cp process_probs "$TGTDIR"

exit $?
