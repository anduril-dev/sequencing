#!/bin/bash -x

# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/lang/bash/functions.sh"

# use these functions to read command file
bam=$( getinput in ) 
fq=$( getoutput out )
picard=$( getparameter picard )

picard_jar="${picard}/picard.jar"
writelog "Path used: ${picard_jar}"

# Create a temporary directory
tempdir=$( gettempdir )

# Extract just the mapped reads as a sub-BAM file
samtools view -b -F 4 $bam > "$tempdir"/subBam

# Convert the subBam file to a Fastq file
java -Xmx4g -jar $picard_jar SamToFastq VALIDATION_STRINGENCY=SILENT INPUT="$tempdir"/subBam FASTQ=$fq || writelog "Error in picard.jar SamToFASTQ"

if [ -f $fq ]
then 
        writelog "Successful conversion."
else  
        writeerror "Missing output file!"
        exit 1
fi
