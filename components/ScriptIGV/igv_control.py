#!/usr/bin/env python

from __future__ import print_function

import anduril
from anduril.arrayio import *
from anduril.args import *
args=anduril.args

import os, errno, sys
import subprocess, threading
import itertools
import shlex
import subprocess
import socket

LIBPATH=os.path.abspath('../../lib/grok/python')
if os.path.exists(LIBPATH):
    sys.path.append(LIBPATH)
try:
    import grok
except ImportError as err:
    print('Install grok to '+LIBPATH)
    print('Or add it to python path.')
    print(err)
    sys.exit(1)

# Controlling IGV through a Port.
# This option is turned off by default but can be enabled from the Advanced tab of the Preferences window. 

# To create scripts, try e.g. https://github.com/brentp/bio-playground/blob/master/igv/igv.py
# https://raw.github.com/brentp/bio-playground/master/igv/igv.py

# Bash equivalent is something like: bash igv.sh input.bam -g hg19 -b igv_batch_script.txt

class IGVControl:
    def __init__(self, TCP_IP = "127.0.0.1", TCP_PORT = 60151):
        if TCP_IP:
            self.s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.s.connect((TCP_IP, TCP_PORT))
        else:
            self.s=None

    def process(self, msg, BUFFER_SIZE=1024):
        print("Sending", msg, "\n")
        self.s.send(msg)
        data = self.s.recv(BUFFER_SIZE) # Mostly this is just "OK"
        if data.startswith("OK"):
            return None
        return data

    def send_script(self, script, stop_on_error=True):
        """ Either communicates over the given TCP/IP port or starts IGV from the command line, using the -b flag.
        A third option would be to start IGV and communicate over network - however it may be the preferences aren't
        set so that networking works, so using the batch flag is the safer option. """
        if self.s: # Network
            lineno=0
            for line in script.split("\n"):
                if not line.strip(): # Empty lines are an error
                    continue
                lineno+=1
                try:
                    result=self.process(line+"\n")
                except Exception as e:
                    result=e.what()
                if stop_on_error and result:
                    raise Exception("An error occurred on line %d, stopping:\n"%lineno + line + "\n" + result)
        else: # cmd line
            ''' IGV manual
                    You can  specify optionally specify a session file or a comma-delimited list of
                    files to load and a locus in the form of a locus string (e.g. chr1:100-200) or gene name.
                    These 2 arguments are order dependent, you cannot specify a locus without specifying a file to load.
                    Other arguments that you can use from the command line include:

                        -b <file>, --batch=<file> Immediately run the supplied batch script after start up.
                        -g <genomeId>, --genome=<genomeId> Specify the genome by ID.
                                You can locate the genomeId as the last entry in each line of the genome registry.
                                NEW since 2.1.28:  the -g option can specify a path to a .genome or indexed fasta file.
                        -d <URL>, --dataServerURL=<URL> Specify URL to data registry file.
                        -u <URL>, --genomeServerURL=<URL> Specify URL to genome registry file.
                        -p <port>, --port=<port> Specify port IGV opens for port commands.
                        -o <file>, --preferences=<file> A user preference properties file. This can override any properties IGV supports.
            '''
            script+="\nexit\n" # Want IGV to exit
            import tempfile
            fd,pathname=tempfile.mkstemp(text=True) # "secure" temp file
            file = open(pathname, 'w')
            file.write(script)
            print("Wrote script", script)
            file.close()

            if not component.input.igv:
                component.input.igv="igv.sh"
            cmd = [component.input.igv, "-b", pathname]+shlex.split(component.param.igvOptions)
            print(cmd)
            proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            proc.communicate()[0] # This will block if there is an IGV NullPointerException or such, because the exit command doesn't get there. So, using the network seems to always be the better option. Well, unless the error stream here provides the necessary information.

            os.close(fd)

def bedtools_script(cf):
    global component
    print("Creating bedtools script.")
    script=prepend_header("")
    if component.param.bedtoolsOptions=="default":
        component.param.bedtoolsOptions=""
    print(bedtoolsOptions)
    bedtools_cmd="bedtools igv %s -path %s -i INPUT_FILE"%(component.param.bedtoolsOptions, output_screenshots)

    if input_file:
        print("Running bedTools with command %si"%bedtools_cmd.replace("INPUT_FILE", input_file))
        proc = subprocess.Popen(shlex.split(bedtools_cmd.replace("INPUT_FILE", input_file)), stdout=subprocess.PIPE)
        script += proc.communicate()[0]
        print("Bedtools igv result:", script)
    return script

def prepend_header(script):
    # Initialization. Loading all tracks may be not what the user wants?
    script = "snapshotDirectory %s\n"%(component.output.screenshots) \
        + ("load %s\n"%(",".join(component.input.tracks.values())) if component.input.tracks else "") \
        + script
    print("Prepending header",script)
    return script

def script_substitute(cf, script):
    # FIXME: Create anduril_environment.sh... source ..../tools/vnc/vnc_functions
    global component

    class grok_readers:
        def __init__(self, name, fn):
            self.reader=grok.reader(fn)
            self.name=name
            self.filename=fn
    readers=[]

    import pdb
    #pdb.set_trace()

    if not component.input.file: # Ensure there isn't a @file@ tag in the script if the input isn't defined
        if -1!=script.find("@file@"):
            raise Exception("Script contains @file@ but the otherwise optional input is not defined. Either remove the tag from the script or provide the input.")
    else:
        script=script.replace("@file@", os.path.basename(component.input.file))
        print("GROK reader from", component.input.file)
        readers.append(grok_readers("input_file", component.input.file))

    if component.input.array:
        for key, value in component.input.array.items():
            print("Array input", key, value)
            readers.append(grok_readers(key, grok.reader(value)))
            
    script=prepend_header(script)

    script=script.replace("@snapshotDirectory@", component.output.screenshots)

    start=0

    snippets=[]
    coords=[]
    while True:
        start=script.find("<loop>") # This becomes quadratically slower
        print("Start", start)
        if start==-1:
            break
        
        next_loop=script.find("<loop>", start+1)
        end=script.find("</loop>", start)
        if end==-1:
            raise Exception("Closing loop tag missing for loop starting at character %d"%(start) )
        if next_loop!=-1 and next_loop<end:
            raise Exception("Nested loops aren't supported %r %r %r"%(start, end, next_loop) )

        s=script[start+len("<loop>"):end] # This should be repeated to replace whatever in the middle

        # scan for tags
        to_insert=""
        for r in readers:
            for reg in r.reader:
                coord="%s:%d-%d"%(reg["seqid"],reg["left"],reg["right"])
                insert=s.replace("@coord@",coord)
                insert=insert.replace("@filename@", r.name)
                to_insert+="\n"+insert+"\n"

        script=script[:start]+to_insert+script[end:]
        anduril.trace(script)
        script=script.replace("<loop>","",1)
        script=script.replace("</loop>","",1)

        break


    return script

def execute(cf):
    global component
    print(args.get_iop_dict(cf))
    print(component)

    if not os.path.exists(component.output.screenshots):
        os.mkdir(component.output.screenshots)

    ctl=IGVControl( TCP_IP = component.param.host , TCP_PORT = param_port )

    if component.param.bedtoolsOptions:
        script=bedtools_script(cf)
    elif component.input.scriptFile:
        f=open(component.input.scriptFile)
        script=f.read()
        f.close()
        script=script_substitute(cf, script)
    else:
        script=component.param.script
        script=script_substitute(cf, script)

    scriptpath=os.path.join(component.output.screenshots, "..", "scriptfile.txt")
    print("Writing script to", scriptpath)
    with open(scriptpath, "w") as scriptfile:
        scriptfile.write(script)

    ctl.send_script(script)

    print("Done processing scripts")

    return 0

def main():
    if False:
        script='''new
    load continuous_pairs_collapsed.bed
    expand continuous_pairs_collapsed.bed
    genome hg19
    region chr1 169093767 170116083
    snapshotDirectory /home/llyly/screenshots
    snapshot'''

        ctl=IGVControl( TCP_IP = '127.0.0.1', TCP_PORT = 60151 )
        ctl.send_script(script)

    else:
        anduril.main(execute)

if __name__=="__main__":
    main()

"""
From IGV manual

Commands
Command           Params                Description
new                                     Create a new session.  Unloads all tracks except the default genome annotations.
load              file                  Loads data or session files.  Specify a comma-delimited list of full paths or URLs.
collapse          trackName             Collapses a given trackName. trackName is optional, however, and if it is not supplied all tracks are collapsed.
echo                                    Writes "echo" back to the response.  (Primarily for testing)
exit                                    Exit (close) the IGV application.
expand            trackName             Expands a given trackName. trackName is optional, however, and if it is not supplied all tracks are expanded.
genome            genomeId              Selects a genome. 
goto              locus or listOfLoci   Scrolls to a single locus or a space-delimited list of loci. If a list is provided, these loci will be displayed in a split screen view.  Use any syntax that is valid in the IGV search box.
region            chr start end         Defines a region of interest bounded by the two loci (e.g., region chr1 100 200).
maxPanelHeight    height                Sets the number of vertical pixels (height) of each panel to include in image. Images created from a port command or batch script are not limited to the data visible on the screen. Stated another way, images can include the entire panel not just the portion visible in the scrollable screen area. The default value for this setting is 1000, increase it to see more data, decrease it to create smaller images.
setSleepInterval  ms                    Sets a delay (sleep) time in milliseconds.  The sleep interval is invoked between successive commands.
snapshotDirectory path                  Sets the directory in which to write images.

snapshot          filename              Saves a snapshot of the IGV window to an image file.
                                        If filename is omitted, writes a PNG or SVG file with a filename generated based on the locus.
                                        If filename is specified, the filename extension determines the image file format, which must be .png, or .svg.

sort              option locus
                                        Sorts an alignment track by the specified option.  Recognized values for the option parameter are: base, position, strand, quality, sample,  readGroup, AMPLIFICATION, DELETION, EXPRESSION, SCORE, and MUTATION_COUNT.  The locus option can define a single position, or a range.  If absent sorting will be perfomed based on the region in view, or the center position of the region in view, depending on the option.
squish            trackName             Squish a given trackName. trackName is optional, and if it is not supplied all annotation tracks are squished.
viewaspairs       trackName             Set the display mode for an alignment track to "View a pairs".  trackName is optional.
preference        key value             Temporarily set the preference named key to the specified value. This preference only lasts until IGV is shut down.
"""

