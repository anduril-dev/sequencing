library(componentSkeleton)

execute <- function(cf) {

    suppressPackageStartupMessages(suppressWarnings(library(tcltk)))
    # Parameters + input
    param3 <- get.parameter(cf,"TPtables", "string")
    param2 <- get.parameter(cf,"TPdb", "string")
    param1 <- get.parameter(cf, "pairedKeys", "string")
    ref <- get.parameter(cf, "reference", "string")
    table1 <- CSV.read(get.input(cf, "refTable"))

    # array or single file output?
    if (ref != "") {
        REF = TRUE
    } else {
        REF = FALSE
    } 
    # Create array output dir.
    results <- get.output(cf, "array")
    if (!file.exists(results)) {
        dir.create(results, recursive=TRUE)
    }
    # Array object
    results.array.output <- Array.new()
    

    # If required package not installed, install it
    is.installed <- function(pkg) {
                           is.element(pkg, installed.packages()[,1])
                           }

    if (!is.installed("sqldf")) {
                        print("Downloading required R package SQLDF")
                        update.packages(checkBuilt=TRUE, ask=FALSE)
                        install.packages("sqldf",repos="http://cran.us.r-project.org", dependencies=TRUE)
                        }

    library(sqldf)

    # Extract SQLite query info to iterate over. pairedKeys are strictly REFkey=TPkey
    pairedKeys <- do.call(rbind,strsplit(unique(unlist(strsplit(gsub("[[:blank:]]","",param1),","))),"="))
    TPdb <- param2
    TPtables <- unique(unlist(strsplit(gsub("[[:blank:]]","",param3),",")))

    annotadded <- table1

    if (!REF) { # Create single merged file
        for (Table in TPtables) {
            # Retrieve column names of Table
            tableRefs <- sqldf(paste("PRAGMA table_info(",Table,")",sep=""),dbname=TPdb)[,2]

            # Find overlap of colnames in annotadded and pairedKeys[,1]
            annotKeys <- intersect(colnames(annotadded),pairedKeys[,1])
            if (length(annotKeys) == 0) {
                write.error("Could not find any of ",paste(pairedKeys[,1], collapse=", "), " in ", paste(colnames(annotadded), collapse=", "), ". Verify   pairedKeys is correct.")
                return(1)
            }

            # Retrieve corresponding TPkey by EXACT whole-word match.
            TPkeys <- c()
            for (key in annotKeys) {
                TPkeys <- c(TPkeys, pairedKeys[grep(paste("\\b",key,"\\b",sep=""),pairedKeys[,1]),2])
            }

            # Find overlap of column names in Table and REFkeys
            TPkeys <- intersect(tableRefs, TPkeys)
            
            # Find corresponding TPkeys
            REFkeys <- c()
            for (key in TPkeys) {
                REFkeys <- c(REFkeys, pairedKeys[grep(paste("\\b",key,"\\b",sep=""),pairedKeys[,2]),1])
            }        

            # CHECK: same number of keys, and all keys correspond to a valid column name
            if (length(TPkeys) != length(REFkeys) | length(grep(paste(REFkeys,collapse="|"),colnames(annotadded))) < length(REFkeys)) { 
                write.error(paste("TPkeys != REFkeys. Please verify colnames. They are case-sensitive and you cannot repeat colnames for REFkeys. Available TPkeys: ",paste(tableRefs, collapse=", "), ". Available REFkeys: ", paste(colnames(annotadded),collapse=", "),sep=""))
                return(1)
                }

            # Create query string
            query <- "SELECT * FROM"
            for (n in 1:length(TPkeys)) {
                if (n == 1) {
                    query <- paste(query, " main.", Table, " WHERE ", TPkeys[n], " IN ('", paste(unique(annotadded[,REFkeys[n]]),collapse="', '",sep=""), "')",sep="")
                } else {
                    query <- paste(query," AND ", TPkeys[n], " IN ('", paste(unique(annotadded[,REFkeys[n]]),collapse="', '",sep=""), "')",sep="")
                }
            }

            # SQLquery
            queried <- unique(sqldf(query,dbname=TPdb))

            if (nrow(queried) != 0) {
                # merge with table if data extracted from table. Otherwise, skip.
                annotadded <- merge(annotadded,queried,by.x=REFkeys, by.y=TPkeys,all.x =TRUE, all.y=FALSE, suffixes = c("",paste("_",Table,sep="")))
            }

            # Remove NAs from REFkeys
            for (n in grep(paste(pairedKeys[,1],collapse="|"),colnames(annotadded))) {
                nas <- which(is.na(annotadded[,n]))
                if (length(nas) != 0) {
                    annotadded <- annotadded[-nas,]
                }
            }
        }

        # Write outputs
        CSV.write(get.output(cf,"annot"),annotadded) 
        return(0)

    ##################
    } else { # Create array results

        # Merge reference with input file
        tableRefs1 <- sqldf(paste("PRAGMA table_info(",ref,")",sep=""),dbname=TPdb)[,2]

        # Find overlap of colnames in annotadded and pairedKeys[,1]
        annotKeys <- intersect(colnames(annotadded),pairedKeys[,1])
        if (length(annotKeys) == 0) {
            write.error("Could not find any of ",paste(pairedKeys[,1], collapse=", "), " in ", paste(colnames(annotadded), collapse=", "), ". Verify pairedKeys is correct.")
            return(1)
        }

       # Retrieve corresponding TPkey by EXACT whole-word match.
       TPkeys <- c()
       for (key in annotKeys) {
            TPkeys <- c(TPkeys, pairedKeys[grep(paste("\\b",key,"\\b",sep=""),pairedKeys[,1]),2])
       }

       # Find overlap of column names in Table and REFkeys
       TPkeys <- intersect(tableRefs1, TPkeys)

       # Find corresponding TPkeys
       REFkeys <- c()
       for (key in TPkeys) {
           REFkeys <- c(REFkeys, pairedKeys[grep(paste("\\b",key,"\\b",sep=""),pairedKeys[,2]),1])
        }        

        # CHECK: same number of keys, and all keys correspond to a valid column name
        if (length(TPkeys) != length(REFkeys) | length(grep(paste(REFkeys,collapse="|"),colnames(annotadded))) < length(REFkeys)) { 
           write.error(paste("TPkeys != REFkeys. Please verify colnames. They are case-sensitive and you cannot repeat colnames for REFkeys. Available TPkeys: ",paste(tableRefs1, collapse=", "), ". Available REFkeys: ", paste(colnames(annotadded),collapse=", "),sep=""))
           return(1)
           }

        # Create query string
        query <- "SELECT * FROM"
        for (n in 1:length(TPkeys)) {
           if (n == 1) {
                query <- paste(query, " main.", ref, " WHERE ", TPkeys[n], " IN ('", paste(unique(annotadded[,REFkeys[n]]),collapse="', '",sep=""), "')",sep="")
           } else {
                query <- paste(query," AND ", TPkeys[n], " IN ('", paste(unique(annotadded[,REFkeys[n]]),collapse="', '",sep=""), "')",sep="")
           }
        }

        # SQLquery
        queried <- unique(sqldf(query,dbname=TPdb))

       if (nrow(queried) != 0) {
           # merge with table if data extracted from table. Otherwise, skip.
           Annot <- merge(annotadded,queried,by.x=REFkeys, by.y=TPkeys,all.x =TRUE, all.y=FALSE, suffixes = c("",paste("_",Table,sep="")))
       }

        # Remove NAs from REFkeys
        for (n in grep(paste(pairedKeys[,1],collapse="|"),colnames(Annot))) {
            nas <- which(is.na(Annot[,n]))
            if (length(nas) != 0) {
                Annot <- Annot[-nas,]
            }
        }

        # Merge reference file (Annot) to each TPtable
        for (Table in TPtables) {
           # Retrieve column names of Table
            tableRefs <- sqldf(paste("PRAGMA table_info(",Table,")",sep=""),dbname=TPdb)[,2]

            # Find overlap of colnames in Annot and pairedKeys[,1]
            annotKeys <- intersect(colnames(Annot),pairedKeys[,1])

           # Retrieve corresponding TPkey by EXACT whole-word match.
           TPkeys <- c()
           for (key in annotKeys) {
                TPkeys <- c(TPkeys, pairedKeys[grep(paste("\\b",key,"\\b",sep=""),pairedKeys[,1]),2])
           }

            # Find overlap of column names in Table and REFkeys
            TPkeys <- intersect(tableRefs, TPkeys)

           # Find corresponding TPkeys
           REFkeys <- c()
           for (key in TPkeys) {
               REFkeys <- c(REFkeys, pairedKeys[grep(paste("\\b",key,"\\b",sep=""),pairedKeys[,2]),1])
            }        

            # CHECK: same number of keys, and all keys correspond to a valid column name
            if (length(TPkeys) != length(REFkeys) | length(grep(paste(REFkeys,collapse="|"),colnames(Annot))) < length(REFkeys)) { 
               write.error(paste("TPkeys != REFkeys. Please verify colnames. They are case-sensitive and you cannot repeat colnames for REFkeys. Available TPkeys: ",paste(tableRefs, collapse=", "), ". Available REFkeys: ", paste(colnames(Annot),collapse=", "),sep=""))
               return(1)
               }


            
            # Create query string
            query <- "SELECT * FROM"
            for (n in 1:length(TPkeys)) {
                if (n == 1) {
                    query <- paste(query, " main.", Table, " WHERE ", TPkeys[n], " IN ('", paste(unique(Annot[,REFkeys[n]]),collapse="', '",sep=""), "')",sep="")
                } else {
                    query <- paste(query," AND ", TPkeys[n], " IN ('", paste(unique(Annot[,REFkeys[n]]),collapse="', '",sep=""), "')",sep="")
                }
            }

            # SQLquery
            queried <- unique(sqldf(query,dbname=TPdb))

            if (nrow(queried) != 0) {
                # merge with table if data extracted from table. Otherwise, skip.
                annotFile <- merge(Annot[,union(intersect(tableRefs1,colnames(Annot)),union(intersect(TPkeys,colnames(Annot)), intersect(REFkeys,colnames(Annot))))],queried,by.x=REFkeys[grep(paste(TPkeys,collapse="|"),TPkeys)], by.y=TPkeys,all.x =TRUE, all.y=FALSE, suffixes = c("",paste("_",Table,sep="")))
            }

        # Save to arrays
        CSV.write(paste(results,"/",Table,".csv",sep=""), annotFile)
        results.array.output <- Array.add(results.array.output,Table,paste(Table,".csv",sep=""))
        }

        Array.write(cf,results.array.output,"array")    
        CSV.write(get.output(cf,"annot"),Annot) 
        return(0)
    }
}

main(execute)
