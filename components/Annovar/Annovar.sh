# running functions.sh 
set -e
source "$ANDURIL_HOME/lang/bash/functions.sh"
export_command

# inputs
echo csvIn    				$input_csvIn
echo vcfIn 					$input_vcfIn

# parameters
echo buildver 				$parameter_buildver
echo protocol	 			$parameter_protocol
echo operation				$parameter_operation
echo argument				$parameter_argument
echo threads				$parameter_threads
echo options				$parameter_options
echo annovardb				$parameter_annovardb
echo annovarPath			$parameter_annovarPath
echo inputType				$paramteter_inputType

mkdir "${output_analysis}"
temp_folder=$( gettempdir )

if [ "${parameter_annovarPath}" == "" ]
then
	parameter_annovarPath="${ANNOVAR_HOME}"   
fi

if [ "${parameter_annovardb}" == "" ]
then
	parameter_annovardb="${ANNOVARDB_HOME}"   
fi

command="${parameter_annovarPath}/table_annovar.pl"
## which input
## vcf input
if [ "${parameter_inputType}" != "vcf" -a "${parameter_inputType}" != "csv" ]
then
	writeerror "unknown input type"
fi

if [ "$parameter_inputType" == "vcf" ]
then
	if [ ! -e "${input_vcfIn}" ]
	then
		writeerror "vcfIn input does not exist"
		exit 1
	fi
	command="${command} ${input_vcfIn} ${parameter_annovardb} -vcfinput"
else
## csv input
	if [ "$parameter_inputType" == "csv" ]
	then
		if [ ! -e "${input_csvIn}" ]
		then
			writeerror "csvIn input does not exist"
			exit 1
		fi
	cat ${input_csvIn} | awk 'NR>1' | tr -d '"' > ${temp_folder}/csvInput.csv	
	command="${command} ${temp_folder}/csvInput.csv ${parameter_annovardb}"
	fi
fi

command="${command} -buildver ${parameter_buildver} -protocol ${parameter_protocol} -operation ${parameter_operation} -out ${output_analysis}/annotated "

if [ "${parameter_argument}" != "" ]
then
	command="${command} --argument ${parameter_argument}"
fi

if [ "${parameter_options}" != "" ]
then
	command="${command} ${parameter_options}"
fi


echo "Annovar command is: "$command
$command 2>> "${logfile}"
if [ $? != 0 ]
then
	writeerror "Annovar failed"
	exit 1
fi

if [ "$parameter_inputType" == "csv" ]
then
	output="${output_analysis}/annotated.${parameter_buildver}_multianno.txt"
	NF=$( cat @var1@ | head -n 1 | awk '{print NF}' )
	paste ${output} <(cut -f 6-$NF ${input_csvIn}) > ${output_csvOut}
	touch ${output_vcfOut}
else
	output="${output_analysis}/annotated.${parameter_buildver}_multianno.vcf"
	cp ${output} ${output_vcfOut}
	touch ${output_csvOut}	
fi
