#!/usr/bin/env python

import anduril.args, pycloneqc
import os

# get input files
trace_files = pycloneqc.get_trace_files(anduril.args.input_config,
	anduril.args.input_trace)
traces = pycloneqc.load_trace_matrix(trace_files)

# get options
options = {'tol': anduril.args.tolerance}

# find known variants?
known_variants = ()
if anduril.args.infCoverage < 2**31: # NB. yaml starts to produce garbage above this anyway..
	known_variants = pycloneqc.get_known_variants(anduril.args.input_config,
		anduril.args.input_mutationFiles, anduril.args.infCoverage)

# perform analyses
analyses = (('corr', anduril.args.output_corr, pycloneqc.estimate_corr_lags),
	('conv', anduril.args.output_conv, pycloneqc.estimate_conv_lags))
tables = {}
for out_type, filename, method in analyses:
	# compute table & export
	tables[out_type] = method(traces, known_variants = known_variants, **options)
	pycloneqc.to_csv(filename, tables[out_type])

# create output directory for the plots
plot_dir = anduril.args.output_plots
os.makedirs(plot_dir)

# output plots
plots = (('corr', pycloneqc.make_corr_plot),
	('conv', pycloneqc.make_conv_plot))
for plot_type, method in plots:
	if plot_type in [arg.strip() for arg in anduril.args.plotType.split(',')]:
		for j in xrange(traces.shape[1]):
			# get trace & table
			trace = traces.iloc[:, j:(j+1)]
			table = tables[plot_type].iloc[j:(j+1), :]

			# create figure & export
			fig = method(trace, table, **options)
			pycloneqc.to_pdf(os.path.join(plot_dir, '{}-{:03d}-{}.pdf'.format(
				plot_type, j + 1, traces.columns[j])), fig)
			fig.close()
