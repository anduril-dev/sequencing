#!/usr/bin/env python

#
# Quality control tool for PyClone 0.13.
#
# This file is in public domain, but the restrictions in the PyClone license
# may apply. For details, see:
#   https://bitbucket.org/aroth85/pyclone/src/tip/LICENSE.txt
#

import glob, os.path, re

import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt

import numpy as np, pandas as pd

import yaml
import pyclone.paths, pyclone.trace, pyclone.post_process.loci

def get_trace_dir(config_file):
	''' Get the trace directory from the configuration file '''

	return pyclone.paths.get_trace_dir(config_file)

def get_trace_files(config_file, trace_dir = None):
	''' Look into a config file and return the interesting trace files '''

	# get trace directory
	if not trace_dir:
		trace_dir = get_trace_dir(config_file)

	# get all traces except the labels 
	files = set(glob.glob(os.path.join(trace_dir, '*.tsv.bz2')))
	files.remove(os.path.join(trace_dir, 'labels.tsv.bz2'))

	return sorted(files)

def _strip_extensions(filename, extensions):
	''' Strips file extensions, in-order '''

	for extension in reversed(extensions):
		if not filename.endswith('.{}'.format(extension)):
			break
		filename = filename[:-(len(extension)+1)]

	return filename

def _get_trace_name(trace_file):
	''' Get name for a trace file '''

	return _strip_extensions(os.path.basename(trace_file), ('tsv', 'bz2'))
	
def load_trace(trace_file):
	''' Load a trace file '''

	# read first line
	head = pd.read_csv(trace_file, header = None, compression = 'bz2',
		sep = '\t', nrows = 1)
	has_head = not all(t.kind in 'bifc' for t in head.dtypes)

	# load raw trace file (no burn-in & thinnig)
	data = pd.read_csv(trace_file, header = (None, 0)[has_head],
		compression = 'bz2', sep = '\t').astype(float)

	# set header using the file name if one is missing
	if not has_head:
		data.columns = (_get_trace_name(trace_file),)

	return data

def load_trace_matrix(trace_files):
	''' Load all trace files as a matrix '''

	def _load_trace_with_fn(fn):
		# augment file name in the index
		label = _get_trace_name(fn)
		t = load_trace(fn)
		t.columns = pd.MultiIndex.from_tuples([(label, colname) for colname in t.columns])
		return t

	return pd.concat([_load_trace_with_fn(fn) for fn in trace_files], axis = 1)

def get_samples_files(samples_dir):
	''' Find sample files in a directory '''

	if not samples_dir:
		return None

	return dict((re.sub(r'\.yaml$', '', os.path.basename(fn)), fn)
		for fn in glob.glob(os.path.join(samples_dir, '*.yaml')))

def get_mutations_files(config_file, samples_files = None):
	''' Find mutation files using the config and a sample directory '''

	# load config
	config = pyclone.paths.load_config(config_file)

	# override sample files?
	if samples_files:
		for sample in config['samples'].keys():
			config['samples'][sample]['mutations_file'] = samples_files[sample]

	return ((sample, config['samples'][sample]['mutations_file'])
		for sample in config['samples'].keys())

def _load_vafs_and_coverage(file_name):
 	with open(file_name) as fh:
		config = yaml.load(fh)
    
	data = []
	for mutation_dict in config['mutations']:
		mutation = pyclone.post_process.loci.load_mutation_from_dict(mutation_dict)
        
		coverage = mutation.ref_counts + mutation.var_counts
		try:
			freq = float(mutation.var_counts) / coverage
		except ZeroDivisionError:
			freq = pd.np.nan

		data.append((mutation.id, freq, coverage))

	return pd.DataFrame(data, columns = ('mutation_id', 'variant_allele_frequency', 'coverage'))

def get_known_variants(config_file, samples_files, inf_coverage):
	''' Find known variants marked by very high coverage '''

	results = []
	for sample, mutations_file in get_mutations_files(config_file, samples_files):
		vaf_data = _load_vafs_and_coverage(mutations_file)
		for mutation_id in vaf_data['mutation_id'][vaf_data['coverage'] >= inf_coverage]:
			results.append((sample, mutation_id))

	return results

def zscore(x):
	''' Standardize x into z-scores '''

	return (x - np.mean(x)) / np.std(x)

def xcorr(a, b = None):
	''' Compute cross-correlation '''

	# compute autocorrelation?
	b_is_a = b is None
	if b_is_a:
		b = a

	# form lag array
	lags = np.arange(-b.size+1, a.size)

	# FFT convolution
	def _fxcorr(a, b, s):
		fa = np.fft.fft(a, s)
		if b_is_a:
			fb = fa
		else:
			fb = np.fft.fft(b, s)
		return np.fft.ifft(fa * fb.conj()).real

	# compute cross-correlation via FFT
	s = a.size + b.size - 1
	p = 1
	while p < s: # NB. pad to a power of 2-- numpy fft is dog slow for primes
		p *= 2
	if b_is_a:
		z = zscore(a)
		c = _fxcorr(z, z, p)
	else:
		c = _fxcorr(zscore(a), zscore(b), p)

	# shift DC to middle & scale by the sample sizes
	c = c[np.concatenate((np.arange(a.size, s)+(p-s), np.arange(0, a.size)))]
	c = c / np.minimum(a.size - np.maximum(lags, 0),
		b.size - np.maximum(-lags, 0))

	return c, lags

def medw(x, w):
	''' Compute the weighted median '''

	# sort data
	perm = np.argsort(x)
	cumw = np.cumsum(np.array(w)[perm])

	# find first & last median
	sig = cumw - .5 * cumw[-1]
	a = sig.size - np.sum(sig >= 0)
	b = sig.size - np.sum(sig[a:] > 0)

	# average
	return x[perm[a]] + .5 * (x[perm[b]] - x[perm[a]])

def ldlin1(a, b, w = 1):
	''' Solves the linear least deviations problem: min sum(w*abs(c*a-b)) wrt c '''

	x = b / a
	y = w / abs(a)
	return medw(x[a != 0], y[a != 0])

def fit_decay_0(x, w = 1):
	''' Fit x ~ exp(-t / b) with weights w '''

	# TODO: more sophisticated algorithm 

	best_lag, best_r = np.nan, np.inf
	t = np.arange(x.size)

	# TODO: adjustable grid size at least..

	lags = np.logspace(0, np.log10(0.05 * x.size), 101)

	for lag in lags:
		r = np.sum(w * np.abs(np.exp(-t / lag) - x))
		if r < best_r:
			best_lag, best_r = lag, r

	return best_lag

def fit_decay_1(x, w = 1):
	''' Fit x ~ a * exp(-t / b) '''

	# TODO:

	best_parm, best_r = np.nan * np.ones(2), np.inf
	t = np.arange(x.size)

	lags = np.logspace(0, np.log10(0.05 * x.size), 101)

	for lag in lags:
		s = np.exp(-t / lag)
		a = ldlin1(s, x, w)

		r = np.sum(w * np.abs(a * s - x))
		if r < best_r:
			best_parm, best_r = (a, lag), r

	return best_parm

def fit_decay_2(x, w = 1):
	''' Fit x ~ a * exp(-t / b) + c with x[0] fixed '''

	# TODO:

	best_parm, best_r = np.nan * np.ones(3), np.inf
	t = np.arange(x.size)

	lags = np.logspace(0, np.log10(0.05 * x.size), 101)

	for lag in lags:
		s = np.exp(-t / lag)
		c = ldlin1(1 - s, x - x[0] * s, w)
		a = x[0] - c

		r = np.sum(w * np.abs(a * s + c - x))
		if r < best_r:
			best_parm, best_r = (a, lag, c), r

	return best_parm

def estimate_corr_lags(traces, tol, known_variants = (), cov = 'auto'):
	''' Estimate cross-correlation lengths at a given tolerance '''

	m, n = traces.shape

	# create output array
	results = pd.DataFrame(columns = ('source-left', 'left', 'source-right', 'right', 'a', 'b', 'c', 'lag'))
	t = 0

	if cov == 'auto':
		# analyze autocovariances only 
		for j in xrange(n):
			# get autocorrelation
			c, lags = xcorr(traces.ix[:, j])
			mask = (lags >= 0) & (lags < .5 * m)

			# fit model
			b = fit_decay_0(c[mask], m - lags[mask])
			lag = -b * np.log(tol)

			# append results
			source, trace = traces.columns[j]
			results.loc[t] = [source, trace, source, trace, 1., b, 0., lag]
			t = t + 1
	else:
		# 
		# TODO: this part has never been tested
		# 

		# analyze all n^2 cross-covariances
		 # NOTE: loop both (a,b) and (b,a), as we analyze the positive lag only
		for j in xrange(n):
			for jj in xrange(n):
				# get cross-correlation
				c, lags = xcorr(traces[:, j].values, traces[:, jj].values)
				mask = (lags >= 0) & (lags < .5 * m)

				# estimate model
				if not j == jj:
					a, b = fit_decay_1(c[mask], m - lags[mask])
				else:
					a = 1
					b = fit_decay_0(c[mask], m - lags[mask])
				lag = -b * np.log(tol / abs(a))

			 	# append
				source_l, trace_l = traces.columns[j]
				source_r, trace_r = traces.columns[jj]
				results.loc[t] = [source_l, trace_l, source_r, trace_r, a, b, 0., lag]
				t = t + 1

	# flag known variants
	if len(known_variants) > 0:
		bad_set = set(('{}.cellular_prevalence'.format(sample_id), mutation_id)
			for sample_id, mutation_id in known_variants)

		mask = [ (row['source-left'], row['left']) in bad_set or
			(row['source-right'], row['right']) in bad_set for _, row in results.iterrows() ]
		results.loc[mask, 'lag'] = np.nan

	return results

def estimate_conv_lags(traces, tol, known_variants = ()):
	''' Estimate convergence of the trace series '''

	m, n = traces.shape

	results = pd.DataFrame(columns = ('source', 'trace', 'a', 'b', 'c', 'lag'))
	t = 0

	for j in xrange(n):
		# estimate decay
		a, b, c = fit_decay_2(traces.ix[:, j].values)
		lag = -b * np.log(tol)

		# append
		source, trace = traces.columns[j]
		results.loc[t] = [source, trace, a, b, c, lag]
		t = t + 1

	# flag known variants
	if len(known_variants) > 0:
		bad_set = set(('{}.cellular_prevalence'.format(sample_id), mutation_id)
			for sample_id, mutation_id in known_variants)

		mask = [ (row['source'], row['trace']) in bad_set for _, row in results.iterrows() ]
		results.loc[mask, 'lag'] = np.nan

	return results

def add_ext(filename, ext):
	''' Add file extension if one is missing '''
	if not '.' in os.path.basename(filename):
		filename = '{}.{}'.format(filename, ext)

	return filename

def to_csv(filename, table):
	''' Save a data frame as Anduril-style "csv" '''

	# add extension
	filename = add_ext(filename, 'csv')

	# write
	import csv
	return table.to_csv(filename, sep = '\t', na_rep = 'NA', index = False,
		quoting = csv.QUOTE_NONNUMERIC)

def _decay_plot(ax, t, x, label, abc, lag):
	''' Plots a decaying curve and the associated model '''

	# plot curve
	ax.plot(t, x, 'b-', label = label)

	# plot model
	a, b, c = abc
	ax.plot(t, a * np.exp(-t / b) + c, 'g--',
		label = '{:.3g}*exp(-t/{:.3g}) + {:.3g}'.format(a, b, c))

	# set limits
	ylim = np.amin((np.amin(x), 0, a + c, c)), np.amax((np.amax(x), 0, a + c, c))
	ax.set_ylim(ylim[0], ylim[-1])

	# plot threshold
	ax.plot((lag, lag), (ylim[0], ylim[-1]), 'g:')

def make_corr_plot(traces, results, **options):
	''' Plots the correlation curves & their models '''

	# plot traces
	fig, ax = plt.subplots()
	for j in xrange(traces.shape[1]):
		# get correlation
		x, lags = xcorr(traces.iloc[:, j].values)
		mask = (lags >= 0) & (lags < .5 * traces.shape[0])

		# get model
		a, b, c = [results.iloc[j, :][key] for key in ('a', 'b', 'c')]
		lag = results.iloc[j, :]['lag']

		# plot curves
		_decay_plot(ax, lags[mask], x[mask], traces.columns[j],
			(a, b, c), lag)

	# put legends
	plt.xlabel('lag')
	plt.ylabel('correlation')
	plt.legend(loc = 'best')

	fig.close = lambda: plt.close(fig)
	return fig

def make_conv_plot(traces, results, **options):
	''' Plots the traces & their convergence models '''

	# create time 
	t = np.arange(traces.shape[0])

	# plot traces
	fig, ax = plt.subplots()
	for j in xrange(traces.shape[1]):
		# get parameters
		a, b, c = [results.iloc[j, :][key] for key in ('a', 'b', 'c')]
		lag = results.iloc[j, :]['lag']

		# plot curves
		_decay_plot(ax, t, traces.iloc[:, j].values, traces.columns[j],
			(a, b, c), lag)

	# put legends
	plt.xlabel('iteration')
	plt.ylabel('value')
	plt.legend(loc = 'best')

	fig.close = lambda: plt.close(fig)
	return fig

def to_pdf(filename, plot):
	''' Converts a Matplotlib figure into a pdf '''

	# add extension
	filename = add_ext(filename, 'pdf')
	# write
	plot.savefig(filename, format = 'pdf', bbox_inches = 'tight')

def split_names(string):
	''' Convert a comma separated string into a list of names '''

	if not string:
		return ()
	return tuple(arg.strip() for arg in string.split(','))

def _format_column(column):
	''' Converts a MultiIndex column name into a string ''' 

	if isinstance(column, basestring):
		return column
	else:
		return '-'.join(column)

if __name__ == '__main__':
	import argparse, csv, sys

	# create parser
	parser = argparse.ArgumentParser()
	parser.add_argument('--config_file', default = './config.yaml')
	parser.add_argument('--trace_dir')
	parser.add_argument('--samples_dir')
	parser.add_argument('--out_dir', default = '.')
	parser.add_argument('--plot_type', default = '')
	parser.add_argument('--table_type', default = '')
	parser.add_argument('--tolerance', type = float, default = 0.05)
	parser.add_argument('--inf_coverage', type = float, default = float('inf'))

	# parse arguments
	args = parser.parse_args()

	# get inputs
	trace_files = get_trace_files(args.config_file, args.trace_dir)
	traces = load_trace_matrix(trace_files)
	options = {'tol': args.tolerance}

	# do we need to parse the variants for coverage?
	known_variants = ()
	if args.inf_coverage < float('inf'):
		known_variants = get_known_variants(args.config_file,
			get_samples_files(args.samples_dir), args.inf_coverage)

	# results cache
	cache = {}

	# print tables
	tables = {'corr': estimate_corr_lags,
		'conv': estimate_conv_lags}
	for table_type in split_names(args.table_type):
		cache[table_type] = tables[table_type](traces, known_variants = known_variants, **options)
		to_csv(os.path.join(args.out_dir, table_type), cache[table_type])

	# make plots
	plots = {'corr': make_corr_plot,
		'conv': make_conv_plot}
	for plot_type in split_names(args.plot_type):
		table = cache[plot_type]

		# loop over traces
		for j in xrange(traces.shape[1]):
			fig = plots[plot_type](traces.iloc[:, j:(j+1)], table.iloc[j:(j+1), :],
				**options)
			to_pdf(os.path.join(args.out_dir, '{}-{:03}-{}.pdf'.format(
				plot_type, j + 1, _format_column(traces.columns[j]))), fig)
			fig.close()
