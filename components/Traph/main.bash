
#!/bin/bash
# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/lang/bash/functions.sh"
export_command
tempdir=$( gettempdir ) # nice to have it always since it's the scratch disk


# Run Traph

runtraph.py -i "${input_fragments}" -o "${output_folder}" -l "${parameter_readLength}" ${parameter_options}

