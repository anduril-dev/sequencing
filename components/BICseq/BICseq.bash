# running functions.sh 
source "$ANDURIL_HOME/lang/bash/functions.sh"
export_command

# inputs
input_test_bam=${input_test%.bai}
echo test    $input_test_bam
input_control_bam=${input_control%.bai}
echo control $input_control_bam

# parameters
echo lambda			 "${parameter_lambda}"		# default 4
echo insertSD 		 "${parameter_insertSD}"	# default 500,50
echo binSize 		 "${parameter_binSize}"		# default 100
echo bicseqHome 	 "${parameter_bicseqHome}" 	# default /mnt/csc-gc/opt/BICseq
echo aligner 		 "${parameter_aligner}" 	# BWA or Bowtie
echo pairedEnd		 "${parameter_pairedEnd}"	# boolean default true
echo chromosomes	 "${parameter_chromosomes}" # comma delimited or * for all
echo outputName		 "${parameter_outputName}"	# base name for output files
echo threshold		 "${parameter_threshold}"	# threshold for amplifications and deletions
echo runTHetA		 "${parameter_runTHetA}"	# whether to infer clone-specific copy number with THetA
echo thetaDir		 "${parameter_thetaDir}"	# Directory path to THetA binaries
echo threads		 "${parameter_threads}"		# number of threads for THetA

# creating temp structure
temp_folder=$( gettempdir )
temp_test="${temp_folder}"/test
temp_control="${temp_folder}"/control
mkdir "${temp_test}" "${temp_control}"

# required binaries/scripts
BICSEQ_HOME=$parameter_bicseqHome
samtools_special=$( find "${BICSEQ_HOME}" -type f -executable -name samtools 2>"${temp_folder}/foo" )  
BICseq_perl=$( find "${BICSEQ_HOME}" -type f -name BIC-seq.pl 2>>"${temp_folder}/foo" )  

if [ -z "$samtools_special" ] || [ -z "$BICseq_perl" ]
then
	writeerror "check bicseqHome parameter"
	exit
fi

if [ "${parameter_runTHetA}" == "true" -a "${parameter_thetaDir}" == "" ]
then
	writeerror "thetaDir should be specified"
fi

# getting unique read counts
chrFile=$temp_folder/chrFile.csv
existingChrFile=$temp_folder/existingChrFile.csv

if [ "$parameter_chromosomes" == "*" ]
	then
 	# parallel run
	$samtools_special view -U $parameter_aligner,$temp_test/,N,N $input_test_bam &
	$samtools_special view -U $parameter_aligner,$temp_control/,N,N $input_control_bam
	wait

	else
	samtools view -H "${input_test_bam}" | awk '{if($2 ~ "SN:")print substr($2,4)}' > $existingChrFile
	echo $parameter_chromosomes | tr "," "\n" > $chrFile
	checkChr=$(awk 'FNR==NR {a[$1]; next} ($1 in a)' $existingChrFile $chrFile | wc -l)
	
	if [ $checkChr == "0" ]
		then
		writeerror "Unknown chromosomes"
		exit
	fi
	
	chr=$(echo $parameter_chromosomes | tr "," " ")
	# parallel run
	samtools view -b $input_test_bam $chr | $samtools_special view -U $parameter_aligner,$temp_test/,N,N - &
	samtools view -b $input_control_bam $chr | $samtools_special view -U $parameter_aligner,$temp_control/,N,N - 
	wait 
fi


# constructing config file
configFile=$temp_folder/config.csv
unfilteredConfig=$temp_folder/unfilteredConfig.csv

paste <( find $temp_test -type f -printf %P\\n | sort | sed "s,.seq$,," ) \
      <( find $temp_test -type f  | sort ) \
      <( find $temp_control -type f | sort ) > $unfilteredConfig

echo -e "chr\ttest\tcontrol" > $configFile

if [ "$parameter_chromosomes" == "*" ]
	then
	cat $unfilteredConfig >> $configFile
	else
	awk 'FNR==NR {a[$1]; next} ($1 in a)' $chrFile $unfilteredConfig >> $configFile 
fi



# running BICseq
if [ $parameter_pairedEnd == "true" ]
	then
	perl $BICseq_perl --lambda $parameter_lambda --I $parameter_insertSD \
				  --bin_size $parameter_binSize --paired "${configFile}" "${output_results}"/ $parameter_outputName
	else
	perl $BICseq_perl --lambda $parameter_lambda --I $parameter_insertSD \
				  --bin_size $parameter_binSize "${configFile}" "${output_results}"/ $parameter_outputName
fi

if [ ! -f "${output_results}/${parameter_outputName}.bicseg" ]
	then
	writeerror "BICseq run failed"
	exit	
fi

# calling amplifications and deletions
awk 'NR==1 {OFS="\t";$8="state"; print}' "${output_results}/${parameter_outputName}.bicseg" > "${output_calls}" 
awk -v t=$parameter_threshold 'NR>1 { if ($6 > t || $6 < (-t)) print}' "${output_results}/${parameter_outputName}.bicseg" \
	| awk ' BEGIN { OFS = "\t" }{if ($6 >t) $8="amplification"; if($6<(-t)) $8="deletion"; print}' 	 >> "${output_calls}"

# Running THetA
if [ "${parameter_runTHetA}" == "true" ] 
then
	bicseqOut="${output_results}/${parameter_outputName}.bicseg"
	ThetaResults="${output_results}/THetA"
	mkdir $ThetaResults
	java -jar "${parameter_thetaDir}"/runBICSeqToTHetA.jar $bicseqOut -OUTPUT_Prefix "${ThetaResults}/${parameter_outputName}"
	ThetaIn="${ThetaResults}/${parameter_outputName}.all_processed"
	ThetaCommand="${parameter_thetaDir}/RunTHetA --OUTPUT_PREFIX ${ThetaResults}/${parameter_outputName} --NUM_PROCESSES ${parameter_threads} ${ThetaIn}"
	echo "THetA command is: ${ThetaCommand}"
	eval $ThetaCommand
	ThetaOut="${ThetaResults}/${parameter_outputName}.BEST.results"
	Rscript script.R ${ThetaIn} ${ThetaOut} ${output_cloneCalls}
else
	touch ${output_cloneCalls}
fi
