<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>DESeqExpr</name>
    <version>1.0</version> 
    <doc>
        Provides a gene expression matrix using DESeq.

        You can use <a href="../HTSeqBam2Counts/index.html">HTSeqBam2Counts</a> to prepare the suitable inputs for this function.
    </doc>
    <author email="marko.laakso@significo.fi">Marko Laakso</author>
    <category>Expression</category>
    <category>Short-read Sequencing</category>
    <launcher type="R">
        <argument name="file" value="DESeqExpr.r" />
    </launcher>
    <requires type="manual" URL="http://www.r-project.org/">R</requires>
    <requires type="R-bioconductor" URL="http://www.bioconductor.org/packages/release/bioc/html/DESeq2.html">DESeq2</requires>
    <inputs>
        <input name="geneCounts" type="TextFile" array="true">
            <doc>The alignment counts for each gene</doc>
        </input>
        <input name="sampleNames" type="CSV" optional="true">
            <doc>Sample names corresponding the gene counts</doc>
        </input>
    </inputs>
    <outputs>
        <output name="expr" type="CSV">
            <doc>Expression data of gene rows and sample columns</doc>
        </output>   
        <output name="log2" type="LogMatrix">   
            <doc>Log2 expression data of gene rows and sample columns</doc>
        </output>
    </outputs>
    <parameters>
		<parameter name="maxNA" type="float" default="1.00">
            <doc>Maximum proportion of missing values accepted for a gene.
                 Genes with more sample values missing are excluded.</doc>
        </parameter>
		<parameter name="counts" type="boolean" default="false">
            <doc>Provides the bin counts instead of counts per million
                 mapped fragments.</doc>
        </parameter>
		<parameter name="normalized" type="boolean" default="true">
            <doc>Use robust fpm or normalized counts.</doc>
        </parameter>
		<parameter name="colIn" type="string" default="">
            <doc>Column name for the original sample names in the annotation file.
                 An empty string refers to the first column.</doc>
        </parameter>
		<parameter name="colOut" type="string" default="">
            <doc>Column name for the intended sample names in the annotation file.
                 An empty string refers to the second column.</doc>
        </parameter>
        <parameter name="add1" type="boolean" default="false">
            <doc>Add 1 before doing log2 to avoid negative values.</doc>
        </parameter>
    </parameters>
</component>

