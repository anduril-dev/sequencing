#!/bin/bash -x

# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/lang/bash/functions.sh"

# methratio.py Command Locations
METHRATIOPATH=$( readlink -f ../../lib/bsmap/methratio.py )
SAMTOOLSPATH=$( readlink -f ../../lib/bsmap/samtools )

# Get inputs
reference=$( getinput reference ) 
alignment=$( getinput alignment )

# Get parameters
trim=$( getparameter trim ) # trim N fill-in nucleotides in DNA fragment end-repairing. [default:2] (This option is only for pair-end mapping. For RRBS, N could be detetmined by the distance between
							# cuttings sites on forward and reverse strands. For WGBS, N is usually between 0~3.)
ctSNP=$( getparameter ctSNP ) 
optionsMethylCall=$( getparameter optionsMethylCall ) 
zeroMeth=$( getparameter zeroMeth )
unique=$( getparameter unique ) # process only unique mappings/pairs.
pair=$( getparameter pair ) # process only properly paired mappings.
removeDuplicate=$( getparameter removeDuplicate ) # remove duplicated mappings to reduce PCR bias. (This option should not be used on RRBS data. For WGBS, sometimes 
												  # it's hard to tell if duplicates are caused by PCR due to high seqeuncing depth.)
chr=$( getparameter chr )
  
# Get outputs
methylationCalling=$( getoutput methylationCalling ) 

# Get the file extension from reference, reads and mate
suffix_reference=$(echo ${reference} | awk -F\. '{print $NF}' | tr [:upper:] [:lower:] )
suffix_alignment=$(echo ${alignment} | awk -F\. '{print $NF}' | tr [:upper:] [:lower:] )

# Check reference file extension
if [ "${suffix_reference}" != "fasta" ] && [ "${suffix_reference}" != "fa" ]
then
    writeerror "Format \"${suffix_reference}\" not recognized. Reference file must be in FASTA format!"
    exit 1
fi

# Check alignment file extension
if [ "${suffix_alignment}" != "bam" ]
then
    writeerror "Format \"${suffix_alignment}\" not recognized. Alignment file must be in BAM format!"
    exit 1
fi

# Check parameters
# trim
if [ "${trim}" -lt 0 ] || [ "${trim}" -gt 3 ]
then
    writeerror "Incorrect trim value \"${trim}\". The trim value must be a positive number between 0 and 3!"
    exit 1
fi

# ctSNP
if [ "${ctSNP}" != "no-action" ] && [ "${ctSNP}" != "correct" ] && [ "${ctSNP}" != "skip" ]
then
    writeerror "Incorrect ctSNP value \"${ctSNP}\". The ctSNP parameter must be either \"no-action\", \"correct\" or \"skip\"!"
    exit 1
fi

# chr
declare -a chrm=('chr1' 'chr2' 'chr3' 'chr4' 'chr5' 'chr6' 'chr7' 'chr8' 'chr9' 'chr10' 'chr11' 'chr12' 'chr13' 'chr14' 'chr15' 'chr16' 'chr17' 'chr18' 'chr19' 'chr20' 'chr21' 'chr22' 'chrX' 'chrY' 'chrMT' )
if [ "${chr}" != "all" ]
then
	chrArray=$(echo "${chr}" | tr "," "\n")
	for ch in $chrArray
	do
		flag=true
		for c in $chrm
		do
			if ["${c}" == "${ch}" ] 
			then
				flag=false
			fi
		done
		
		if $flag 
		then
			writeerror "Incorrect chr value  \"${chr}\". Chromosomes must be listed as comma separated values without spaces and in the form chr1,chrX and not only the chromosome number or identifier (X, Y or MT)!"
			exit 1
		fi
	done
fi

# Methylation Calling
# Command strings initialization
methratioSwitches=""

# Check that the binary exists
PYTHONPATH=$( which python ) # python Command Locations
if [ ! -z "${PYTHONPATH}" ]
then
    writelog "Python found!"
else
    writeerror "Python not found! ( \"${pythonLocation}\" )"
    exit 1
fi

# Construction of methratio command
methratioSwitches="$methratioSwitches ${alignment} -d ${reference} -o ${methylationCalling} -t ${trim} -i ${ctSNP} -s ${SAMTOOLSPATH}"

# zeroMeth
if $zeroMeth
then
    methratioSwitches="$methratioSwitches -z"
fi

# unique
if $unique
then
    methratioSwitches="$methratioSwitches -u"
fi

# pair
if $pair
then
    methratioSwitches="$methratioSwitches -p"
fi

# removeDuplicate
if $removeDuplicate
then
    methratioSwitches="$methratioSwitches -r"
fi

# chr
if [ "${chr}" != "all" ]
then
	methratioSwitches="$methratioSwitches -c ${chr}"
fi

# optionsMethylCall
if [ "${optionsMethylCall}" != "" ]
then
    methratioSwitches="$methratioSwitches $optionsMethylCall"
fi
 
$PYTHONPATH $METHRATIOPATH ${methratioSwitches} 2>&1 >> "$logfile"
exitcode=$?

exit $exitcode
