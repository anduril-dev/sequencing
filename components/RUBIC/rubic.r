#!/usr/bin/Rscript

args <- commandArgs(TRUE)
library(biomaRt)
library(RUBIC, lib.loc=args[1])
library(data.table, lib.loc=args[1])
library(ggplot2, lib.loc=args[1])

if(packageVersion("data.table") == "1.9.6") {
    stop("Bug present in data.table version 1.9.6, downgrade your data.table to 1.9.4 or upgrade to version 1.9.8 (if possible), in which this bug should be corrected.")
}
if(packageVersion("ggplot2") != "1.0.1") {
    stop("Older version ggplot2 1.0.1 needed.")
}

hg <- args[2] 
fdr <- args[3]
segcna <- args[4]
markers <- args[5]
samples <- args[6]
ampLevel <- args[7]
delLevel <- args[8]
minMean <- args[9]
maxMean <- args[10]
colChr <- args[11]
colStart <- args[12]
colEnd <- args[13]
colLogR <- args[14]
outGain <- args[15]
outLosses <- args[16]
outPlots <- args[17]
genesList <- args[18]

if (hg=="hg19"){
	human = useMart(biomart= "ENSEMBL_MART_ENSEMBL",host="grch37.ensembl.org", dataset = "hsapiens_gene_ensembl")
}else{
	human = useMart(biomart= "ENSEMBL_MART_ENSEMBL",host="www.ensembl.org", dataset = "hsapiens_gene_ensembl")
}

genes<-getBM(attributes = c('ensembl_gene_id','external_gene_name','chromosome_name','start_position','end_position'), mart = human)
genes<-setNames(genes,c("ID","Name","Chromosome","Start","End"))
genes_dt<-data.table(genes)

if (minMean=="0.0"){
	minMean<-NA_real_
}else{
	minMean<-as.numeric(minMean)
}
if (maxMean=="0.0"){
        maxMean<-NA_real_
}else{
	maxMean<-as.numeric(maxMean)
}

rbc <- rubic(as.numeric(fdr), segcna, markers, samples=samples, genes=genes_dt, amp.level=as.numeric(ampLevel), del.level=as.numeric(delLevel), min.mean=minMean, max.mean=maxMean, col.sample=1, col.chromosome=as.numeric(colChr), col.start=as.numeric(colStart), col.end=as.numeric(colEnd), col.log.ratio=as.numeric(colLogR))

rbc$save.focal.gains(outGain)
rbc$save.focal.losses(outLosses)

if (genesList==''){
	rbc$save.plots(outPlots)
}else{
	gene_ids<-read.table(genesList)
	genes_plot<-getBM(attributes=c('ensembl_gene_id','external_gene_name','chromosome_name','start_position','end_position'),filters ='ensembl_gene_id',values=gene_ids,mart=human)
	genes_plot<-setNames(genes_plot,c("ID","Name","Chromosome","Start","End"))
	genes_plot<-data.table(genes_plot)

	rbc$save.plots(outPlots,genes=genes_plot)
}
