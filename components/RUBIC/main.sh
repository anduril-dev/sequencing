#!/usr/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/functions.sh ]] && . $ANDURIL_HOME/bash/functions.sh $1
[[ -e $ANDURIL_HOME/lang/bash/functions.sh ]] && . $ANDURIL_HOME/lang/bash/functions.sh $1
export_command

# INPUTS
markers="$input_markers"
genes="$input_genes"
# PARAMETERS
hg="$parameter_assembly"
fdr="$parameter_fdr"
ampLevel="$parameter_ampLevel"
delLevel="$parameter_delLevel"
minMean="$parameter_minMean"
maxMean="$parameter_maxMean"
colChr=$((parameter_colChr + 1))
colStart=$((parameter_colStart + 1))
colEnd=$((parameter_colEnd + 1))
colLogR=$((parameter_colLogR + 1))
# OUTPUTS
tmpDir=$( gettempdir )

CNFiles=( $( getarrayfiles segments ) )
CNKeys=( $( getarraykeys segments ) )

SEQUENCING_PATH=$( getbundlepath sequencing )
# Concatenate CSVs
printf '%s\n' "${CNKeys[@]}" >> "$tmpDir/samples.txt"
awk -F"\t" -v OFS="\t" '{if(NR==1){print "ID",$0}}' "${CNFiles[0]}" > "$tmpDir/segcna.tsv"
for i in "${!CNFiles[@]}"
do
	awk -F"\t" -v OFS="\t" -v k=${CNKeys[$i]} '{if(NR!=1){print k,$0}}' "${CNFiles[$i]}" >> "$tmpDir/segcna.tsv"
done
sed -i 's/\"//g' "$tmpDir/segcna.tsv"	#remove (eventually) quotes

Rscript "$SEQUENCING_PATH/components/RUBIC/rubic.r" "$SEQUENCING_PATH/lib/rubic_Rlibs" $hg $fdr "$tmpDir/segcna.tsv" "$markers" "$tmpDir/samples.txt" "$ampLevel" "$delLevel" "$minMean" "$maxMean" "$colChr" "$colStart" "$colEnd" "$colLogR" "$tmpDir/focal_gains.tsv" "$tmpDir/focal_losses.tsv" "$output_plots" "$genes"

cp "$tmpDir/focal_gains.tsv"  "$output_gains"
cp "$tmpDir/focal_losses.tsv"  "$output_losses"
