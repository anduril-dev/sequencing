
#!/bin/bash
# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/lang/bash/functions.sh"
export_command
tempdir=$( gettempdir ) # nice to have it always since it's the scratch disk

# $parameter_mode # snp, indel or cnv
# $input_variants # variants to annotate
# $output_array # output folder, and working directory
# $parameter_format # output folder, and working directory
# $output_log # rest of files

set -e

if [[ "${parameter_mode}" == "snp" ]]; then
    parameter_mode=""
else
    parameter_mode="_$parameter_mode"
fi

script="$parameter_anntoolsPath"/driver"$parameter_mode".py

# Enable wildcard negation 
shopt -s extglob

if [[ -f "${script}" ]]; then
    mkdir -p $output_array
    mkdir -p $output_log
    cd "$output_array"

    # Anntools wants to write to inputs dir, and a config file in cwd
    ln -s "$parameter_anntoolsPath"/config.txt
    ln -s "$input_variants"

    input_variants=$(basename $input_variants)

    echo "Running AnnTools with" "python" "${script}" "$input_variants" "$parameter_format"
    python "${script}" "$input_variants" "$parameter_format"

    # Clean up non-output files
    rm $input_variants
    rm config.txt

    # Move other than VCF files aside
    mv !(*.vcf) $output_log
    echo "Generating array with" anduril-array-generate $annot *.annot.vcf
    anduril-array-generate -R $annot *.annot.vcf 
else
    echo "Please check that the anntoolsPath parameter corresponds to an AnnTools installation path. Cannot find script at $script "
    exit 1
fi

