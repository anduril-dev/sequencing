
import org.anduril.component.SkeletonComponent;
import org.anduril.component.ErrorCode;
import org.anduril.component.CommandFile;
import org.anduril.component.IndexFile;
import org.anduril.core.utils.IOTools;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.File;
import java.io.FilenameFilter;

public class Cuffmerge extends SkeletonComponent {

    private File assembly_list;
/*	private File arrayIn;*/
	private IndexFile arrayIn;
    private File merged;
    private File ref_gtf;
    private File ref_seq;
    
    private String command="";
    private String outdir;
    
   	private boolean help;
    private int num_threads;
    
	private File tempDir;	
	
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        // Get inputs, outputs and parameters from command file
        getInsOuts(cf);
        getParameters(cf);
        // Create the command for Cufflinks
        createCommand(cf);
        System.out.println("Execution command is: " + command);       
        int status = IOTools.launch(command);
        // Check for error in execution
        if (status > 0) {
            return ErrorCode.ERROR;
        }
         // Rename output files
         
        try {
            renameFiles();
        } catch (IOException exception) {
            throw exception;
        }
        // Return success
        return ErrorCode.OK;
    }
    
    /**
     * Gets inputs and outputs from the command file
     *
     */
    private void getInsOuts(CommandFile cf) throws Exception{
        int i=1;
/*        if (cf.inputDefined("assembly_list")) 
            assembly_list = cf.getInput("assembly_list");*/

        if (cf.inputDefined("ref_gtf")) 
            ref_gtf = cf.getInput("ref_gtf");
        if (cf.inputDefined("ref_seq")) 
            ref_seq = cf.getInput("ref_seq");

 		arrayIn = cf.readInputArrayIndex("arrayIn");
        merged = cf.getOutput("merged");
        tempDir = cf.getTempDir();

        outdir= merged.getParent();        
		
		assembly_list= new File (tempDir+"/assembly_list.txt");
 		BufferedWriter bufferedWriter = null;
		for(String key : arrayIn) {
            File file = arrayIn.getFile(key);
        
        	try {
            
            	//Construct the BufferedWriter object
            	bufferedWriter = new BufferedWriter(new FileWriter(assembly_list, true));
            
            	//Start writing to the output stream
            	bufferedWriter.write(file.getAbsolutePath());
            	bufferedWriter.newLine();
            
        	} catch (FileNotFoundException ex) {
            	ex.printStackTrace();
        	} catch (IOException ex) {
            	ex.printStackTrace();
        	} finally {
            	//Close the BufferedWriter
            	try {
                	if (bufferedWriter != null) {
                    	bufferedWriter.flush();
                    	bufferedWriter.close();
                	}
            	} catch (IOException ex) {
                	ex.printStackTrace();
            	}
        	}
    	}
    }
     /**
     * Gets parameters from the command file
     *
     */
    private void getParameters(CommandFile cf) {
        help = cf.getBooleanParameter("help");
        num_threads = cf.getIntParameter("num_threads");  
    }
    
    private void createCommand(CommandFile cf) {
        command = "cuffmerge";
	    command += " -o "+outdir;
        if (help) 
            command +=" -h";   
        if (num_threads != -1)
            command +=" -p "+num_threads;
        if (cf.inputDefined("ref_gtf"))
            command +=" -g "+ref_gtf;
	    if (cf.inputDefined("ref_seq")) 
	        command +=" -s "+ref_seq;    
		/*System.out.println(assembly_list.getName());*/
		command += " ";
		try {
			if (assembly_list != null) {
			    command += assembly_list.getAbsolutePath();
	    	    System.out.println(command);
            }
        } catch (SecurityException ex) {
                ex.printStackTrace();
            }
    }
    
    /**
	 * Rename Cuffdiff output files to the component output files
	 *
	 */
	private void renameFiles() throws Exception {
	    File a = new File(outdir+"/merged.gtf");
	   
	    // Rename files or create new empty files
	    
        try {
            if (a.exists()) 
                a.renameTo(merged);
            
	    } catch (Exception exception) {
	        throw exception;
	    }
	}
    /**
     * @param args
     */
    public static void main(String[] args) {
        new Cuffmerge().run(args);
    }

}
