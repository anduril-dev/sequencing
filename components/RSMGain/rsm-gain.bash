#!/bin/bash

# set up Anduril stuff
source "$ANDURIL_HOME/lang/bash/functions.sh" "$1"

## build up the arguments

# set up an array
args=()

# list the standard parameters and their switches
declare -A params
params=([algorithm]='a' [quantile]='q')

# add the standard parameters
for param in "${!params[@]}"; do
	key="${params[$param]}"
	arg="$(getparameter "$param")"
	[ -n "$arg" ] && args+=("-$key$arg")
done

# scale
arg="$(getparameter 'use_unity')"
[ "$arg" = "true" ] && args+=("-n$(getparameter 'unity')")

# get input/output file names
outfn="$(getoutput 'out')"
infn="$(getinput 'in')"

## execute

# get path to tools
rsm_path="$(dirname "$0")/../../lib/rsm"

# strip header
strip_header() {
	noheader "$1" | cut -f2-
}

# restore header
restore_header() {
	"$rsm_path/tsv_meta_restore.py" -G "$2" <( header "$1" ) "$1"
}

# execute
strip_header "$infn" | \
	"$rsm_path/rsm-gain" "${args[@]}" '-G/dev/stdout' '/dev/stdin' | \
	restore_header "$infn" '/dev/stdin' >"$outfn"
