<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>ExpressionStats</name>
    <version>0.1</version>
    <doc>Given an expression matrix and a CSV containing sample IDs and treatment groups, calculate basic statistics by treatment group: mean, median, and standard deviation of genes. Visualizations (histogram, density, boxplot, hierarchical clustering) are generated if parameter <code>makeVisuals</code> is true.
    </doc>
    <author email="katherine.icay@helsinki.fi">Katherine Icay</author>
    <author email="alejandra.cervera@helsinki.fi">Alejandra Cervera</author>
    <category>Expression</category>
    <category>smallRNA</category>
    <launcher type="R">
        <argument name="file" value="ExpressionStats.r" />
    </launcher>
    <requires URL="http://www.r-project.org/" type="manual">R</requires>
    <requires type="R-package" URL="https://cran.r-project.org/web/packages/MASS/index.html">MASS</requires>
    <requires type="R-package" URL="https://cran.r-project.org/web/packages/ggplot2/index.html">ggplot2</requires>
    <requires type="R-package" URL="https://cran.r-project.org/web/packages/reshape/index.html">reshape</requires>
    <inputs>
        <input name="expr" type="CSV">
            <doc>Expression matrix. If matrix has an additional bio_type column annotated, parameter <code>biotype</code> should not be "skip".</doc>
        </input>
        <input name="ref" type="CSV">
            <doc>CSV file containing sample names and treatment groups. Sample names must match column names of <code>expr</code>.</doc>
        </input>
        <input name="geneSet" type="CSV" optional="true">
            <doc>2-column list of interesting genes (gene id, gene name) to create heatmap with. Id column should be the same name as defined in parameter <code>exprID</code>.</doc>
        </input>
        <input name="bodyMap" type="CSV" optional="true">
            <doc>Illumina body map. CSV file of geneIds (rows) per tissue (columns). Ids used should be the same as in expr (e.g. Ensembl gene id).</doc>
        </input>
    </inputs>
    <outputs>
        <output name="stats" type="CSV">
            <doc>Rows of genes with columns containing the mean, median and standard deviation of its expression by treatment type.</doc>
        </output>
        <output name="statsArray" type="CSV" array="true">
            <doc>Additional statistics (topExpressed, topExpressedByTissue, zero, low, high, and summary) when <code>makeVisuals</code> is true and <code>biotype</code> is not skipped.</doc>
        </output>
        <output name="report" type="Latex">
            <doc>Produced only if parameter <code>makeVisuals</code> is true. Latex report containing expression visualizations using additional inputs <code>geneSet</code> and <code>bodyMap</code>.</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="makeVisuals" type="boolean" default="false">
            <doc>Perform additional analysis of geneSet. When true, geneSet and bodyMap should be defined.</doc>
        </parameter>
        <parameter name="refID" type="string" default="">
            <doc>Column name of <code>ref</code> file containing the reference names to match to the <code>expr</code> columns. If blank, the first column is used.
            </doc>
        </parameter>
        <parameter name="refGroup" type="string" default="Treatment">
            <doc>Column name of <code>ref</code> file containing the treatment information corresponding to each sample ID. If blank, the component will lookfor a "Treatment" column.
            </doc>
        </parameter>
        <parameter name="exprID" type="string" default="">
            <doc>Column name of <code>expr</code> file containing the unique gene IDs that statistics are calculated for. If blank, the first column is used.
            </doc>
        </parameter>
        <parameter name="topNum" type="int" default="10">
            <doc>Number of top genes to be reported</doc>
        </parameter>
        <parameter name="bodySite" type="string" default="body">
            <doc>Any body tissue from the Illumina Body Map. Default is an empty string, but other options are heart, stomach, brain, etc.</doc>
        </parameter>
        <parameter name="biotype" type="string" default="skip">
            <doc>Is bio_type annotation added to input expr file? Either blank "" or "skip". Default is to skip this step. </doc>
        </parameter>
        <parameter name="biotype_min" type="float" default="0">
            <doc>When biotype is blank "", this is the minimum expression value to consider when calculating additional statistics. </doc>
        </parameter>
    </parameters>
</component>
