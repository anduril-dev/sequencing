#!/usr/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/functions.sh ]] && . $ANDURIL_HOME/bash/functions.sh $1
[[ -e $ANDURIL_HOME/lang/bash/functions.sh ]] && . $ANDURIL_HOME/lang/bash/functions.sh $1
export_command
export PATH=$PATH:$( readlink -f $ANDURIL_HOME/bundles/sequencing/lib/STAR/bin/Linux_x86_64 )
[[ -d "$STAR_FOLDER" ]] || export STAR_FOLDER=$( readlink -f $ANDURIL_HOME/bundles/sequencing/lib/STAR )

gene5p_id="$parameter_gene5pID"
gene3p_id="$parameter_gene3pID"
gene5p_bp="$parameter_gene5pBP"
gene3p_bp="$parameter_gene3pBP"
AS="$parameter_as"
ref_genome="$parameter_release"
threads="$parameter_threads"
sample="$parameter_sample"

annotation_file="$input_annotation"
reads1="$input_reads"
reads2="$input_mates"
fusion_list="$input_fusionList"

out_dir="$output_folder";
metaData=$( gettempdir )

mkdir "$out_dir"

if [ -s "$reads2" ]; then
	paired_end="true"
else
	paired_end="false"
fi

if [ -s "$input_annotation" ]
then
  echo "annotation file provided: recreate fusions using breakpoints"
  iscmd STAR || {
        echo STAR not installed.
        exit 1
  }
  iscmd samtools || {
	  echo Samtools not installed.
	  exit 1
  }

  FUSION_VAL="$( pwd )/fusion_validation.pl"

  [[ $paired_end = "true" ]] && {
	  reads1_format="${reads1##*.}"
	  reads2_format="${reads2##*.}"
	  mkdir "$metaData/genomeIdx1"
	  mkdir "$metaData/genomeIdx2"
	
	  # FUSION VISUALIZATION PERL SCRIPT
	  "$FUSION_VAL" "$gene5p_id" "$gene5p_bp" "$gene3p_id" "$gene3p_bp" "$fusion_list" "$annotation_file" "$ref_genome" "$metaData/genomeIdx1" "$reads1" "$metaData" "$threads" "$AS" "$reads1_format" "$metaData/genomeIdx2" "$reads2"
} || {
	  reads1_format="${reads1##*.}"
	  mkdir "$metaData/genomeIdx1"
	  mkdir "$metaData/genomeIdx2"
	
	  # FUSION VISUALIZATION PERL SCRIPT
	  "$FUSION_VAL" "$gene5p_id" "$gene5p_bp" "$gene3p_id" "$gene3p_bp" "$fusion_list" "$annotation_file" "$ref_genome" "$metaData/genomeIdx1" "$reads1" "$metaData" "$threads" "$AS" "$reads1_format" "$metaData/genomeIdx2"
  }

  cp "$metaData"/virtual_ref1.fa "$out_dir"
  cp "$metaData"/virtual_ref2.fa "$out_dir"
  cp "$metaData"/align1.sorted.bam "$out_dir"
  cp "$metaData"/align2.sorted.bam "$out_dir"
  cp "$metaData"/align1.sorted.bam.bai "$out_dir"
  cp "$metaData"/align2.sorted.bam.bai "$out_dir"
  cp "$metaData"/annotation1.gff3 "$out_dir"
  cp "$metaData"/annotation1.gtf "$out_dir"
  cp "$metaData"/annotation2.gff3 "$out_dir"
  cp "$metaData"/annotation2.gtf "$out_dir"
  cp "$metaData"/tmp_unique_align1.sam "$out_dir"/align1.sam
  cp "$metaData"/tmp_unique_align2.sam "$out_dir"/align2.sam

else
  echo "Running FusionInspector"
  STARFUSION_HOME=$( readlink -f $ANDURIL_HOME/bundles/sequencing/lib/STAR-Fusion )
  echo "$STARFUSION_HOME"
#"$LIBPATH/STAR-Fusion"

  [[ $paired_end = "true" ]] && {
#    echo "paired"
#    } || { 
#      echo "no" 
#    }
    "$STARFUSION_HOME"/FusionInspector/FusionInspector --examine_coding_effect --annotate --include_Trinity --write_intermediate_results --fusions "$input_fusionList" --genome_lib "$STARFUSION_HOME"/ctat_genome_lib_build_dir --left_fq "$reads1" --right_fq "$reads2" --out_dir "$out_dir" --out_prefix "$sample" --prep_for_IGV
    } || {
      echo "no"
      }
#    "$STARFUSION_HOME"/FusionInspector/FusionInspector --examine_coding_effect --annotate --include_Trinity --write_intermediate_results --fusions "$input_fusionList" --genome_lib "$STARFUSION_HOME"/ctat_genome_lib_build_dir --left_fq "${reads1}" --out_dir  "$out_dir" --out_prefix "$sample" "--prep_for_IGV"
 #   }
fi

