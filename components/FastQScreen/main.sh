#!/usr/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/functions.sh ]] && . $ANDURIL_HOME/bash/functions.sh $1
[[ -e $ANDURIL_HOME/lang/bash/functions.sh ]] && . $ANDURIL_HOME/lang/bash/functions.sh $1
export_command

# INPUTS
reads="$input_reads"
mates="$input_mates"
dbList="$input_dbList"
# OUTPUTS
out_folder="$output_folder"
nohit="$output_NoHitPercentage"
# PARAMETERS
aligner="$parameter_aligner"
bisulfite="$parameter_bisulfite"
illumina1_3="$parameter_illumina1_3"
nohits="$parameter_nohits"
subset="$parameter_subset"
threads=$( getmetadata "custom_cpu" )

tmpDir=$( gettempdir )
mkdir "$out_folder"

#threads

FASTQSCREEN_HOME="$ANDURIL_HOME/bundles/sequencing/lib/fastq_screen"
BISMARK_HOME="$ANDURIL_HOME/bundles/sequencing/lib/bismark"
# Create configuration file
tail -n+2 $dbList| awk -v FS='\t' -v OFS='\t' '{$1="DATABASE\t"$1} 1'> $out_folder/fastq_screen.conf
# Execute FastQ Screen
[[ -z $threads ]] && {
	threads="1"
}

[[ $bisulfite == "false" ]] && {
	bisulfite=""
} || {
	bisulfite="--bisulfite $BISMARK_HOME"
}

[[ $illumina1_3 == "false" ]] && {
	illumina1_3=""
} || {
	illumina1_3="--illumina1_3"
}

[[ $nohits == "false" ]] && {
	nohits=""
} || {
	nohits="--nohits"
}

echo -e """"$FASTQSCREEN_HOME/fastq_screen" "$bisulfite" "$illumina1_3" "$nohits" --subset $subset --conf "$out_folder/fastq_screen.conf" --aligner $aligner --threads $threads --outdir "$out_folder" "$reads" "$mates""""
"$FASTQSCREEN_HOME/fastq_screen" "$bisulfite" "$illumina1_3" "$nohits" --subset $subset --conf "$out_folder/fastq_screen.conf" --aligner $aligner --threads $threads --outdir "$out_folder" "$reads" "$mates"

reads_name=$(basename "$reads")
reads_name=""${reads_name%%.*}""
mates_name=$(basename "$mates")
mates_name=""${mates_name%%.*}""

echo -e "%Hit_no_genomes_reads" > "$out_folder/$reads_name"_hit_no_genomes.csv
tail -n1 "$out_folder/$reads_name"_screen.txt | cut -d " " -f2 >> "$out_folder/$reads_name"_hit_no_genomes.csv
tail -n+2 "$out_folder/$reads_name"_screen.txt | head -n-2 >> "$out_folder/$reads_name"_screen.csv
cp "$out_folder/$reads_name"_hit_no_genomes.csv "$output_NoHitPercentage"
