library(componentSkeleton)
library(iSeq)

execute <- function(cf){
	# Inputs
	chip <- get.input(cf, 'treatment')
	mock <- get.input(cf, 'control' )

	# Params

	# For mergetag
	method <- get.parameter(cf, 'method', type = 'string')
	maxlen <- get.parameter(cf, 'maxlen', type = 'int')
	minlen  <- get.parameter(cf, 'minlen', type = 'int')

	# For iSeq
	ntagcut  <- get.parameter(cf, 'ntagcut', type = 'int' )
	gap  <- get.parameter(cf, 'gap', type = 'int' )
	burnin  <- get.parameter(cf, 'burnin', type = 'int' )
	sampling  <- get.parameter(cf, 'sampling', type = 'int' )
	ctcut  <- get.parameter(cf, 'ctcut', type = 'float' )
	a0  <- get.parameter(cf, 'a0', type = 'float' )
	b0  <- get.parameter(cf, 'b0', type = 'float' )
	a1  <- get.parameter(cf, 'a1', type = 'float' )
	b1  <- get.parameter(cf, 'b1', type = 'float' )
	k0  <- get.parameter(cf, 'k0', type = 'float' )
	mink  <- get.parameter(cf, 'mink', type = 'float' )
	maxk  <- get.parameter(cf, 'maxk', type = 'float' )
	normsd  <- get.parameter(cf, 'normsd', type = 'float' )
	verbose  <- get.parameter(cf, 'verbose', type = 'boolean' )

	# For peakreg
	cutoff  <- get.parameter(cf, 'cutoff', type = 'float' )
	peakreg_method <- get.parameter(cf, 'peakreg_method', type = 'string')
	maxgap <- get.parameter(cf, 'maxgap', type = 'int' )

	# Call algorithm
	chip_csv = CSV.read(chip)
	mock_csv = CSV.read(mock)
	tagct = mergetag(chip= chip_csv, control= mock_csv)
	if (method=='iSeq1') {
		res = iSeq1(Y=tagct[,1:4], gap=gap, burnin=burnin, sampling=sampling, ctcut=ctcut, a0=a0, b0=b0, a1=a1, b1=b1, k0=k0, mink=mink, maxk=maxk, normsd=normsd, verbose=verbose)
	} else {
		res = iSeq2(Y=tagct[,1:4], gap=gap, burnin=burnin, sampling=sampling, ctcut=ctcut, a0=a0, b0=b0, a1=a1, b1=b1, k=k0, verbose=verbose)
	}

	reg = peakreg(tagct[,1:3],tagct[,5:6]-tagct[,7:8],res$pp, cutoff, method=peakreg_method, maxgap=maxgap)

	# Order the regions based on ’sym’ and ’ct12’ to get the ’top’ binding regions.
	reg = reg[order(reg$sym,reg$ct12,decreasing = TRUE),]

	# Output
	out.dir     <- get.output(cf, 'report')
	docName     <- file.path(out.dir, LATEX.DOCUMENT.FILE)
	dir.create(out.dir, recursive=TRUE)

	fig.name <- sprintf('an_enriched_region.png', get.metadata(cf, 'instanceName'))
	fig.path <- file.path(out.dir, fig.name)
	png(fig.path, height=1000, width=1500, res=150)
	### plot an enriched region ############
	ID = (reg[1,4]):(reg[1,5])
	plotreg(tagct[ID,2:3],tagct[ID,5:6],tagct[ID,7:8],peak=reg[1,6])

	cat( latex.figure(fig.name, quote.capt = FALSE, image.width=12), "\\clearpage", sep="\n", file=docName, append=TRUE)

	# Plot the model parameters to see whether they converge. In general, the MCMC chains
	# have converged when the parameters fluctuate around the modes of their distributions. If
	# there is an obvious trend(e.g. continuous increase or decrease), the user should increase the
	# number of iterations in the burn-in and/or sampling phases. If the chains do not mix well,
	# the user can adjust the argument normsd to see how it affects the results.
	par(mfrow=c(2,2), mar=c(4.1, 4.1, 2.0, 1.0))
	hist(res$pp)
	fig.name <- sprintf('pp_histogram.png', get.metadata(cf, 'instanceName'))
	fig.path <- file.path(out.dir, fig.name)
	png(fig.path, height=1000, width=1500, res=150)
	cat( latex.figure(fig.name, quote.capt = FALSE, image.width=12), "\\clearpage", sep="\n", file=docName, append=TRUE)

	fig.name <- sprintf('kappa convergence.png', get.metadata(cf, 'instanceName'))
	fig.path <- file.path(out.dir, fig.name)
	png(fig.path, height=1000, width=1500, res=150)
	plot(res$kappa, pch=".", xlab="Iterations", ylab="kappa")
	cat( latex.figure(fig.name, quote.capt = FALSE, image.width=12), "\\clearpage", sep="\n", file=docName, append=TRUE)

	fig.name <- sprintf('lambda0 convergence.png', get.metadata(cf, 'instanceName'))
	fig.path <- file.path(out.dir, fig.name)
	png(fig.path, height=1000, width=1500, res=150)
	plot(res$lambda0, pch=".", xlab="Iterations", ylab="lambda0")
	cat( latex.figure(fig.name, quote.capt = FALSE, image.width=12), "\\clearpage", sep="\n", file=docName, append=TRUE)

	fig.name <- sprintf('lambda1 convergence.png', get.metadata(cf, 'instanceName'))
	fig.path <- file.path(out.dir, fig.name)
	png(fig.path, height=1000, width=1500, res=150)
	plot(res$lambda1, pch=".", xlab="Iterations", ylab="lambda1")
	cat( latex.figure(fig.name, quote.capt = FALSE, image.width=12), "\\clearpage", sep="\n", file=docName, append=TRUE)

	# Add columns to make output compatible with MACS
	df <- as.data.frame(reg)
	# Originally column names are
	# "chr" "gstart"  "gend" "rstart" "rend" "peakpos" "meanpp" "ct1"     "ct2" "ct12" "sym"
	# Rename columns
	colnames(df) <- c("chr", "start", "end", "rstart", "rend", "summit", "meanpp", "tagcount_fwd", "tagcount_rev", "tagcount", "sym")

	# Reorder columns for outputting to CSV
	df <- df[ c("chr", "start", "end", "summit", "meanpp", "tagcount", "tagcount_fwd", "tagcount_rev", "sym", "rstart", "rend") ]

	# MACS has "chr" "start" "end" "length" "summit" "tags" "pvalue" "fold_enrichment" "fdr", so renaming:
	# peakpos -> summit
	# gstart -> start, gend -> end
	# The closest thing to fold enrichment in iSeq is the tag count ct2
	# Renaming ct1 -> tagcount_fwd, etc.

	CSV.write(get.output(cf, 'peaks'), as.data.frame(df))
}

main(execute)

