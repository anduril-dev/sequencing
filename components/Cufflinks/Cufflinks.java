import org.anduril.component.SkeletonComponent;
import org.anduril.component.ErrorCode;
import org.anduril.component.CommandFile;
import org.anduril.core.utils.IOTools;
import org.anduril.core.utils.Logger;

import java.io.IOException;
import java.io.File;


public class Cufflinks extends SkeletonComponent {
    private File alignment;
    private File transcripts;
    private File isoforms;
    private File genes;
    private File reference_annotation1;
    private File reference_annotation2;
    private File mask;
    private File genome;
    
    private String command="";
    private String outdir;
    
   	private boolean help;
    private int num_threads;
	private boolean multi_read_correct;	
	private String library_type;
	private int frag_len_mean;
	private int frag_len_std_dev;
	private boolean upper_quartile_norm;
	private boolean total_hits_norm;
	private boolean compatible_hits_norm;
	private int num_importance_samples;
	private String label;
	private int max_mle_iterations;
	private double min_isoform_fraction;
	private double pre_mrna_fraction;
	private int max_intron_length;
	private double junc_alpha;
	private double small_anchor_fraction;
	private int min_frags_per_transfrag;
	private int overhang_tolerance;
	private int max_bundle_length;
	private int min_intron_length;
	private int trim_3_dropoff_frac;
	private int trim_3_avgcov_thresh;
	private int overhang_tolerance_3;
	private int intron_overhang_tolerance;
	private boolean no_faux_reads;
	private boolean verbose;
	private boolean quiet;
	private boolean no_update_check;
	
	

    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        // Get inputs, outputs and parameters from command file
        getInsOuts(cf);
        getParameters(cf);
        // Create the command for Cufflinks
        createCommand(cf);
        System.out.println("Execution command is: " + command);       
        int status = IOTools.launch(command, null, null, null, CufflinksLogger);
        // Check for error in execution
        if (status > 0) {
            return ErrorCode.ERROR;
        }
         // Rename output files
         
        try {
            renameFiles();
        } catch (IOException exception) {
            throw exception;
        }
        // Return success
        return ErrorCode.OK;
    }
    
    /**
     * Gets inputs and outputs from the command file
     *
     */
    private void getInsOuts(CommandFile cf) {
        int i=1;
        if (cf.inputDefined("alignment")) 
            alignment = cf.getInput("alignment");
        if (cf.inputDefined("reference_annotation1")) 
            reference_annotation1 = cf.getInput("reference_annotation1");
        if (cf.inputDefined("reference_annotation2")) 
            reference_annotation2 = cf.getInput("reference_annotation2");
        if (cf.inputDefined("mask")) 
            mask = cf.getInput("mask");
        if (cf.inputDefined("genome")) 
            genome = cf.getInput("genome");
            
        transcripts = cf.getOutput("transcripts");
        isoforms = cf.getOutput("isoforms");      
        genes = cf.getOutput("genes");
        outdir= transcripts.getParent();
    }
    
     /**
     * Gets parameters from the command file
     *
     */
    private void getParameters(CommandFile cf) {
        help = cf.getBooleanParameter("help");
        num_threads = cf.getIntParameter("num_threads");
        multi_read_correct = cf.getBooleanParameter("multi_read_correct");
        library_type = cf.getParameter("library_type");
        frag_len_mean = cf.getIntParameter("frag_len_mean");
        frag_len_std_dev = cf.getIntParameter("frag_len_std_dev");
        upper_quartile_norm = cf.getBooleanParameter("upper_quartile_norm");
        total_hits_norm = cf.getBooleanParameter("total_hits_norm");
        compatible_hits_norm = cf.getBooleanParameter("compatible_hits_norm");
        num_importance_samples = cf.getIntParameter("num_importance_samples");
        max_mle_iterations = cf.getIntParameter("max_mle_iterations");
        label = cf.getParameter("label");
        min_isoform_fraction = cf.getDoubleParameter("min_isoform_fraction");
        pre_mrna_fraction = cf.getDoubleParameter("pre_mrna_fraction");
        max_intron_length = cf.getIntParameter("max_intron_length");
        junc_alpha = cf.getDoubleParameter("junc_alpha");
        small_anchor_fraction = cf.getDoubleParameter("small_anchor_fraction");
        min_frags_per_transfrag = cf.getIntParameter("min_frags_per_transfrag");
        overhang_tolerance = cf.getIntParameter("overhang_tolerance");
        max_bundle_length = cf.getIntParameter("max_bundle_length");
        min_intron_length = cf.getIntParameter("min_intron_length");
        trim_3_avgcov_thresh = cf.getIntParameter("trim_3_avgcov_thresh");
        trim_3_dropoff_frac = cf.getIntParameter("trim_3_dropoff_frac");
        overhang_tolerance_3 = cf.getIntParameter("overhang_tolerance_3");
        intron_overhang_tolerance = cf.getIntParameter("intron_overhang_tolerance");
        no_faux_reads = cf.getBooleanParameter("no_faux_reads");
        verbose = cf.getBooleanParameter("verbose");
        quiet = cf.getBooleanParameter("quiet");
        no_update_check = cf.getBooleanParameter("no_update_check");

    }
    
    private void createCommand(CommandFile cf) {
        command = "cufflinks";
        command += " -o "+outdir;
        if (help) 
            command +=" -h";
        if (num_threads != -1)
            command +=" -p "+num_threads;
	    if (cf.inputDefined("reference_annotation1"))
	        command += " -G "+reference_annotation1.getAbsolutePath();
	    if (cf.inputDefined("reference_annotation2")) 
	        command += " -g "+reference_annotation2.getAbsolutePath();
	    if (cf.inputDefined("mask")) 
	        command += " -M "+mask.getAbsolutePath();
        if (cf.inputDefined("genome")) 
	        command += " -b "+genome.getAbsolutePath();
	    if (multi_read_correct)
	        command +=" -u";	
	    if (!library_type.equals(""))
	        command += " --library-type "+library_type;
	    if (frag_len_mean != -1)
	        command += " -m "+frag_len_mean;
	    if (frag_len_std_dev != -1)
	        command += " -s "+frag_len_std_dev;
	    if (upper_quartile_norm)
	        command += " -N";
	    if (!total_hits_norm)
	        command += " --total-hits-norm=false";
	    if (compatible_hits_norm)
	        command += " --compatible-hits-norm";
	    if (num_importance_samples != -1)
	        command += " --num-importance-samples "+num_importance_samples;
	    if (max_mle_iterations != -1)
	        command += " --max-mle-iterations "+max_mle_iterations;
	    if (!label.equals(""))
	        command += " -L "+label;
    	if (min_isoform_fraction != -1)
	        command += " -F "+min_isoform_fraction;
    	if (pre_mrna_fraction != -1)
	        command += " -j "+pre_mrna_fraction;
	    if (max_intron_length != -1)
	        command += " -I "+max_intron_length;
	    if (junc_alpha != -1)
	        command += " -a "+junc_alpha;
    	if (small_anchor_fraction != -1)
	        command += " -A "+small_anchor_fraction;
	    if (min_frags_per_transfrag != -1)
	        command += " --min-frags-per-transfrag "+min_frags_per_transfrag;
	    if (overhang_tolerance != -1)
	        command += " --overhang-tolerance "+overhang_tolerance;
	    if (max_bundle_length != -1)
	        command += " --max-bundle-length "+max_bundle_length;
	    if (min_intron_length != -1)
	        command += " --min-intron-length "+min_intron_length;
	    if (trim_3_dropoff_frac != -1)
	        command += " --trim-3-dropoff-frac "+trim_3_dropoff_frac;
	    if (trim_3_avgcov_thresh != -1)
	        command += " --trim-3-avgcov-thresh "+trim_3_avgcov_thresh;
	    if (overhang_tolerance_3 != -1)
	        command += " --3-overhang-tolerance "+overhang_tolerance_3;
	    if (intron_overhang_tolerance != -1)
	        command += " --intron-overhang-tolerance "+intron_overhang_tolerance;
	    if (no_faux_reads)
	        command +=" --no-faux-reads";	
	    if (verbose)
	        command +=" -v";	
	    if (quiet)
	        command +=" -q";	
	    if (no_update_check)
	        command +=" --no-update-check";	
	    command += " " + alignment.getAbsolutePath();
    }
    
    	/**
	 * Rename Cufflinks output files to the component output files
	 *
	 */
	private void renameFiles() throws Exception {
	    System.out.println("in rename files ");
	    File t = new File(outdir+"/transcripts.gtf");
	    File i = new File(outdir+"/isoforms.fpkm_tracking");
	    File g = new File(outdir+"/genes.fpkm_tracking");
	   
	    // Rename files or create new empty files
        try {
            if (t.exists()) 
                t.renameTo(transcripts);
            else System.out.println("hello hello");
            //else transcripts.createNewFile(); 
	        if (i.exists()) i.renameTo(isoforms);
	          //  else isoforms.createNewFile();
	        if (g.exists()) g.renameTo(genes);
	            //else genes.createNewFile();
	    } catch (Exception exception) {
	        throw exception;
	    }
	}
	private static Logger CufflinksLogger =
         Logger.getLogger(Cufflinks.class.getName());
    /**
     * @param args
     */
    public static void main(String[] args) {
        new Cufflinks().run(args);
    }

}
