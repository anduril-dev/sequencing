package SeedMatchLib;

use strict;
require Exporter;
our $version = 0.01;
our @ISA = qw(Exporter);
our @EXPORT = qw(&SeedMatch);
our %EXPORT_TAGS= ();

sub SeedMatch{
	my ($id,$transID,$transSeed,$ref_pairs,$ref_mature_seq) = @_;
	my $nMirna = @{$ref_pairs};
	my $out_mirna="";
	my $out_site_type = "";

	for(my $i=0;$i<$nMirna;$i++){
		my $mirna = $ref_pairs->[$i];
		my $seq = $ref_mature_seq->{$mirna};
		my $type = ifMatch($seq,$transSeed);
		if($type ne ""){
			$out_mirna=$out_mirna.$mirna.";";
			$out_site_type=$out_site_type.$type.";";
		}
	}
	if($out_mirna ne ""){
		$out_mirna=~s/;$//;
		$out_site_type=~s/;$//;
		return $id."\t".$transID."\t".$out_mirna."\t".$out_site_type."\n";
	}
}


sub ifMatch{
	my ($mature_seq,$transSeed) = @_;
	
	# Get 1-8nt from 5' mature miRNA
	my $seed = substr($mature_seq,0,8);
	$seed = scalar reverse $seed;
	$seed =~ tr/AUGC/TACG/;
	my $type;

	# 8mer or 7mer-m8
	my $start_pos = index $transSeed, substr($seed,0,7);
	if($start_pos>=1 && $start_pos<=7){
		my $site_1 = substr($transSeed,$start_pos+7,1);
		if($site_1 eq "A"){
                        $type = "8mer";
                }else{
                        $type = "7mer-m8";
                }
		return $type;
	}

	# 7mer-a1 or 6mer
	$start_pos = index $transSeed, substr($seed,1,6);
	if($start_pos>=2 && $start_pos<=7){
		my $site_1 = substr($transSeed,$start_pos+6,1);
		if($site_1 eq "A"){
                        $type = "7mer-a1";
                }else{
			$type = "6mer";
		}
		return $type;
	}
	
	# offset 6mer
	$start_pos = index $transSeed, substr($seed,0,6);
	if($start_pos>=2 && $start_pos<=7){
		$type = "offset-6mer";
	}else{
		$type =""
	}
	return $type;	
}


1;
