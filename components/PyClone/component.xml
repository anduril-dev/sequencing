<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>PyClone</name>
    <version>0.1</version>
    <doc>
	Component to run PyClone variant clustering tool. Component has been tested with PyClone version 0.13.0. 

See: https://bitbucket.org/aroth85/pyclone/wiki/Home for more information. Always cite 
Roth et al. PyClone: statistical inference of clonal population structure in cancer PMID: 24633410 if you use PyClone.


Config file (config.yaml) is generated, 
	mutation files are built and PyClone analysis is run. Optionally, 
	the outputs of the clusterig results are output.  
	
	Step1: Mutation files are generated with command: PyClone build_mutations_file. 
	Prior for this command is defined with mutationPrior parameter. 
	Step2: PyClone analysis is run with command: PyClone run_analysis. 
	Seed value for the run is defined in parameter seed. Step3: 
	Optional: Clustering results are generated in a table format if 
	clusterTables is true. Wanted output types are defined with a 
	comma-separated list in clusterTableType parameter. 
    </doc>
    <author email="mikko.kivikoski@helsinki.fi">Mikko Kivikoski</author>
    <category>Analysis</category>
    <launcher type="bash">
        <argument name="file" value="pyclone_run.sh" />
    </launcher>
    <requires URL="https://bitbucket.org/aroth85/pyclone/wiki/Home">PyClone</requires>
    <type-parameters>
		<type-parameter name="T1"/>
	</type-parameters>
    <inputs>
        <input name="in" type="T1" array="true">
            <doc>Array of variant tables in tab-separated format.</doc>
        </input>
        <input name="purity" type="CSV" optional="true">
			<doc>Optional. Tumor purity estimates for each sample in a 
			two column csv file. Column 'Key' must match to the Key in 
			the input array. Column 'Purity' is the purity estimate for 
			the sample. This overrides the purityDefault parameter.
            </doc>
        </input>
    </inputs>
    <outputs>
        <output name="config" type="YAML">
            <doc>Output port for the config file.</doc>
        </output>
        <output name="trace" type="BinaryFolder">
            <doc>Output folder for trace files.</doc>
        </output>
        <output name="mutationFiles" type="YAML" array="true">
            <doc>Mutation prior files in yaml format.</doc>
        </output>
        <output name="clusteringResults" type="CSVList">
            <doc>Results of clustering, if executed.</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="densityFunction" type="string" default="pyclone_beta_binomial">
	        <doc>Density function</doc>
        </parameter>
        <parameter name="iterations" type="int" default="50">
	        <doc>Number of iterations used.</doc>
        </parameter>
        <parameter name="alpha" type="int" default="1">
			<doc>Alpha value</doc>
        </parameter>
        <parameter name="beta" type="int" default="1">
			<doc>Beta value</doc>
        </parameter>
        <parameter name="concentration" type="float" default="1.0">
			<doc>Concentration parameter</doc>
        </parameter>
        <parameter name="shape" type="float" default="1.0">
			<doc>Shape parameter</doc>
        </parameter>
        <parameter name="rate" type="float" default="0.001">
			<doc>Rate parameter</doc>
        </parameter>
        <parameter name="mutationPrior" type="string" default="major_copy_number">
			<doc>Mutation prior for building mutation files</doc>
        </parameter>
        <parameter name="seed" type="string" default="3">
			<doc>Seed value for analysis.</doc>
        </parameter>
        <parameter name="purityDefault" type="float" default="1.0">
				<doc>Tumor purity estimate. Parameter's value is used as tumor purity estimate
				for all samples. Purity input overrides this parameter. </doc>
        </parameter>
        <parameter name="clusterTables" type="boolean" default="false">
				<doc>Boolean, default = false. If true, PyClone clustering results are produced
				by using PyClone build_table command.</doc>
        </parameter>
        <parameter name="clusterTableType" type="string" default="cluster,loci,old_style">
				<doc>Comma-separated list of wanted clustering tables. Possible options:
				cluster,loci and old_style. Default: all.</doc>
        </parameter>
        <parameter name="tableName" type="string" default="">
				<doc>String to be added as a prefix to the file names of clustering results. The prefix will be separate with hyphen. Default: ""</doc>
        </parameter>
        <parameter name="initMethod" type="string" default="disconnected">
				<doc>Initial clustering. The value "connected" starts with all loci in a single cluster, subsequent iterations likely splitting the clusters; while "disconnected" starts with each loci in a separate cluster, iterations likely merging them. The former can be beneficial for large datasets. Default: "disconnected".</doc>
        </parameter>
        <parameter name="oldFormat" type="string" default="true">
				<doc>Specifies if the outputs should be converted to the old format when using PyClone 0.13.1 and newer. The cluster labels and trace files are off by one. Default: true</doc>
        </parameter>
        <parameter name="burnin" type="int" default="5">
				<doc>Number of MCMC samples to discard from the beginning. Prior to convergence, the MCMC series features an initial transient which is controlled by the initial parameters and not the data. This initial transient should be specified such that it can be discarded from the subsequent analysis.</doc>
        </parameter>
        <parameter name="thin" type="int" default="1">
				<doc>Thinning ratio for the MCMC series in samples. In an MCMC series, the consecutive samples are correlated. These correlations will skew higher-order statistics such as variance estimates. This can be mitigated by specifying T such that each T-sample string is thinned into a single sample.</doc>
        </parameter>
        <parameter name="meshSize" type="int" default="101">
				<doc>Number of mesh points for density estimation. The default of 101 allows subdivision of 1% in cellular prevalences.</doc>
        </parameter>
    </parameters>
</component>
