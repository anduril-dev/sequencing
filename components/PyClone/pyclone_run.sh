source "$ANDURIL_HOME/lang/bash/functions.sh"
export_command
#set -x
#set -e

iscmd PyClone || { writeerror "PyClone executable not found." ; exit 1; }
version_str="$( PyClone --version 2>&1 )"
writelog "$version_str"

version_ge() {
    ge="$(echo "$2" && echo "$1")"
    result="$(echo "$ge" | sort -V)"
    [ "$result" = "$ge" ]
}

tempDir=$( gettempdir )
inFiles=( $( getarrayfiles in ) )
inKeys=( $( getarraykeys in ) )
inPurity=$input_purity

cd "$tempDir"
# Generate config.yaml file for running.
echo -e "working_dir: $tempDir

trace_dir: trace

density: ${parameter_densityFunction}

beta_binomial_precision_params: 
  value: 1000

  prior:
    shape: 1.0
    rate: 0.0001

  proposal:
    precision: 0.01

num_iters: $( getparameter iterations )

base_measure_params:
  alpha: $( getparameter alpha )
  beta: $( getparameter beta )

concentration:

  value: $( getparameter concentration )
  
  prior:
    shape: $( getparameter shape )
    rate: $( getparameter rate )
  
samples:
" > config.yaml

mkdir yaml
mkdir ${output_clusteringResults}
mkdir ${output_mutationFiles}
[[ -z "$inPurity" ]] || {
  # input exists
  key_col=$( csvcoln "$inPurity" "Key" )
  purity_col=$( csvcoln "$inPurity" "Purity" )
}
for (( a=0; a<${#inKeys[@]}; a++ ))
do aidx=$(( $a + 1 ))
   
 #   echo input $aidx , ${inFiles[$a]} : ${inKeys[$a]}
    purity_value=${parameter_purityDefault}
    [[ -z "$inPurity" ]] || {
      # input exists
      # TODO awk oneliner?   tsvquery?
      purity_value=$( cut -f $key_col,$purity_col "$inPurity" | tr -d \" | grep -w "${inKeys[$a]}" | head -n 1 | cut -f $purity_col )
    }
    # Add sample specific parts to the config file.
    echo -e "  ${inKeys[$a]}:
    mutations_file: yaml/${inKeys[$a]}.yaml
    
    tumour_content:
      value: ${purity_value}
      
    error_rate: 0.001
    " >> config.yaml
   
   PyClone build_mutations_file --in_file ${inFiles[$a]} --out_file yaml/${inKeys[$a]}.yaml --prior $( getparameter mutationPrior )
   addarray mutationFiles ${inKeys[$a]} ${inKeys[$a]}.yaml
done

# write initialization method, iff it differs from the default
 # older PyClone don't have this flag
init_method="$( getparameter initMethod )"
if [ "$init_method" != "disconnected" ]; then
   echo "init_method: $init_method
" >>config.yaml
fi

# rewrites a top-level parameter in the configuration file
 # rewrite_parameter file key value
rewrite_parameter() {
  sed -i 's@^\('"$2"'\).*$@\1: '"$3"'@' "$1"
}

# figure out if we should apply the conversion hacks
convertFormat="$( [ "$( getparameter oldFormat )" = "true" ] && version_ge "$version_str" "PyClone-0.13.1" && echo "y" )"

if [ -n "$convertFormat" ]; then
  rewrite_parameter config.yaml num_iters "$(( $( getparameter iterations ) + 1 ))"
fi

PyClone run_analysis --config_file config.yaml --seed $( getparameter seed )

if [ -n "$convertFormat" ]; then
    rewrite_parameter config.yaml num_iters "$( getparameter iterations )"
    find trace -name '*.tsv.bz2' | while read file; do
        first_line="$(( $(bzcat "$file" | sed -n '$=') - $( getparameter iterations ) ))"
        bzcat "$file" | sed "${first_line}d" | bzip2 -cz | tee "$file" >/dev/null
    done
fi

## TO DO: mesh_size, thin. Consult Antti
if [ ${parameter_clusterTables} = true ] ; then
	table_types=${parameter_clusterTableType}
    for i in $( echo $table_types | sed "s/,/ /g" )
    do
        if [[ -z "${parameter_tableName}" ]] ; then
            OUTTABLE=$( echo $i.csv )
        else
            OUTTABLE=$( echo ${parameter_tableName}-$i.csv )
        fi
        PyClone build_table --config_file config.yaml --out_file $OUTTABLE --table_type $i --burnin ${parameter_burnin} --thin ${parameter_thin} --mesh_size ${parameter_meshSize}
       
        if [ -n "$convertFormat" ]; then
            cat "$OUTTABLE" | awk '
BEGIN {
    OFS = "\t"
}
NR == 1 {
    for (i = 1; i <= NF; ++i) {
        idx[$i] = i
    }
    print
}
NR > 1 {
    $idx["cluster_id"] += 1
    print
}
                ' | tee "$OUTTABLE" >/dev/null
        fi
    done
    cp *.csv ${output_clusteringResults}	
fi
#cp -r $tempDir ${output_out}

cp config.yaml ${output_config}

cp -r yaml/* ${output_mutationFiles}
cp -r trace ${output_trace}

