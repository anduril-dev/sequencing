# running functions.sh 
set -e
source "$ANDURIL_HOME/lang/bash/functions.sh"
export_command

# inputs
echo reference    			$input_reference
echo bam1 					$input_bam1
echo bam2					$input_bam2
echo knownTargets			$input_knownTargets
echo indels1				$input_indels1
echo indels2				$input_indels2
echo indels3				$input_indels3
echo intervals				$input_intervals

# parameters
echo exclude 				$parameter_exclude
echo gatk	 				$parameter_gatk
echo memory					$parameter_memory
echo optionsRealigner		$parameter_optionsRealigner
echo optionsTargets			$parameter_optionsTargets
echo region					$parameter_region
echo java8                  $parameter_java

if [ "${parameter_java}" == "" ]
then
    parameter_java=java
fi

JavaVersion=$(${parameter_java} -version 2>&1 | head -n 1 | cut -d'"' -f2 | cut -d'.' -f2)
if [ $JavaVersion != "8" ]; then
  echo "Java 8 is required."
  exit 1
fi

temp_folder=$( gettempdir )
ln -s "${input_bam1}" "${temp_folder}/input.bam"
inputBam="${temp_folder}/input.bam"
if [ -e "${input_bam1}.bai" ]
then
	ln -s "${input_bam1}.bai" "${temp_folder}/input.bam.bai"
else
	
	bai1=$( dirname "${input_bam1}" )
	bai1="${bai1}/$( echo $( basename ${input_bam1} ) | awk 'gsub("bam", "bai" )' )"
	if [ -e "${bai1}" ]
	then
	ln -s "${bai1}" "${temp_folder}/input.bam.bai"
	fi
fi

## finding gatk
if [ "${parameter_gatk}" == "" ]
then
	parameter_gatk="${GATK_HOME}"   
fi

## building commands

#print gatk version
version=$( ${parameter_java} -jar ${parameter_gatk}/GenomeAnalysisTK.jar --version )
echo "GATK version used is ${version}"

#basic commands
targetsCommand="${parameter_java} -Xmx${parameter_memory} -jar ${parameter_gatk}/GenomeAnalysisTK.jar -T RealignerTargetCreator -R ${input_reference} -I ${inputBam} -o ${output_targets}"
realignerCommand="${parameter_java} -Xmx${parameter_memory} -jar ${parameter_gatk}/GenomeAnalysisTK.jar -T IndelRealigner -R ${input_reference} -I ${inputBam}"

## adding control if exits
if [ -e "${input_bam2}" ]
then 
	ln -s "${input_bam2}" "${temp_folder}/control.bam"
	if [ -e "${input_bam2}.bai" ]
	then
		ln -s "${input_bam2}.bai" "${temp_folder}/control.bam.bai"
	else
		bai2=$( dirname "${input_bam2}" )
		bai2="${bai2}/$( echo $( basename  ${input_bam2} ) | awk 'gsub("bam", "bai" )' )"
		if [ -e "${bai2}" ]
		then
			ln -s "${bai2}" "${temp_folder}/control.bam.bai"
		fi
	fi

	controlBam="${temp_folder}/control.bam"
	echo -e "input.bam\t${output_realignedCase}" > "${temp_folder}/input.map"
	echo -e "control.bam\t${output_realignedControl}" >> "${temp_folder}/input.map"
	targetsCommand="${targetsCommand} -I ${controlBam}"
	realignerCommand="${realignerCommand} -I ${controlBam} --nWayOut ${temp_folder}/input.map"
else
	realignerCommand="${realignerCommand} -o ${output_realignedCase}"
	touch "${output_realignedControl}"
fi


## restricting analysis on specified regions from file or parameter
if [ "${parameter_region}" != "" ]
then
	regionStr="-L ${parameter_region}"
	regionStr=$( echo ${regionStr} | tr ',' ' -L ')
	targetsCommand="${targetsCommand} ${regionStr}"
	realignerCommand="${realignerCommand} ${regionStr}"
fi

if [  -e "${input_intervals}" ]
then
	targetsCommand="${targetsCommand} -L ${input_intervals}"
	realignerCommand="${realignerCommand} -L ${input_intervals}"
fi

## adding known indels inputs
if [  -e "${input_indels1}" ]
then
	targetsCommand="${targetsCommand} -known ${input_indels1}"
	realignerCommand="${realignerCommand} -known ${input_indels1}"
fi

if [  -e "${input_indels2}" ]
then
	targetsCommand="${targetsCommand} -known ${input_indels2}"
	realignerCommand="${realignerCommand} -known ${input_indels2}"
fi

if [  -e "${input_indels3}" ]
then
	targetsCommand="${targetsCommand} -known ${input_indels3}"
	realignerCommand="${realignerCommand} -known ${input_indels3}"
fi

# excluding specified regions from analysis
if [ "${parameter_exclude}" != "" ]
then
	excludeStr="-XL ${parameter_exclude}"
	excludeStr=$( echo $excludeStr | tr ',' ' -XL ')
	targetsCommand="${targetsCommand} ${excludeStr}"
	realignerCommand="${realignerCommand} ${excludeStr}"

fi

# adding other options
if [ "${parameter_optionsTargets}" != "" ]
then
	targetsCommand="${targetsCommand} ${parameter_optionsTargets}"
fi

if [ "${parameter_optionsRealigner}" != "" ]
then
	realignerCommand="${realignerCommand} ${parameter_optionsRealigner}"
fi

# if known targets are not specified run realigner target creator
if [ ! -e "${input_knownTargets}" ]
then
	echo "Realigner target creator command is ${targetsCommand}"
	$targetsCommand 2>> "${logfile}"
	if [ $? != 0 ]
	then
		writeerror "Realigner target creator failed"
		exit 1
	fi
	
	indelModel="USE_READS"
	indelTargets="${output_targets}"
fi

# if known targets are specified run indel realigner directly
if [  -e "${input_knownTargets}" ]
then
	indelModel="USE_READS"
	indelTargets="${input_knownTargets}"
	touch "${output_targets}"
fi



realignerCommand="${realignerCommand} -model ${indelModel} -targetIntervals ${indelTargets}"
echo "Indel realigner command is ${realignerCommand}"

# run indel realigner
$realignerCommand 2>> "${logfile}"
if [ $? != 0 ]
then
	writeerror "Indel Realigner failed"
	exit 1
fi
