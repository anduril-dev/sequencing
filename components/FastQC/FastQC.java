import org.anduril.component.CSVReader;
import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;
import org.anduril.asser.io.CSVParser;
import org.anduril.component.LatexTools;
import org.anduril.component.IndexFile;
import java.io.*;
import javax.swing.UIManager;
import java.io.FileNotFoundException;
import java.io.IOException;
import rnaSeq.*;
import java.util.*;
import java.util.regex.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.Writer;
import java.nio.channels.FileChannel;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FastQC extends SkeletonComponent {
    private File read;
    private File mate;
    private IndexFile summary;
    private File report;
    private File extra;
    private File outDir;
    private File folder;

    private boolean mode;
    private String metaDataDir;
    private String reportPath;
    private boolean clean;
    private String sectionType;
    private String suffix;
    private boolean pagebreak;
    private boolean isPaired=false;
    private String threads="";
     
    private static final String NO_OVERREPRESENTED_KMERS_FIGURE = "resources/kmer_profiles.png";
    private static final String NO_PER_TILE_QUALITY_FIGURE = "resources/per_tile_quality.png";
    private static final String NO_ADAPTER_CONTENT_FIGURE = "resources/adapter_content.png";

    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        getInsOuts(cf);

        Vector <String> shortNames= new Vector <String>();
        Vector <String> filenames= new Vector <String>();
        filenames.add(read.getAbsolutePath());
        
        int num;
        num = read.getName().length();
        if (read.getName().indexOf(".") > 0) {
            num = read.getName().indexOf(".");
        }

        shortNames.add(read.getName().substring(0,num));
        System.out.println("Read in: "+shortNames.get(0));

        if(cf.inputDefined("mate")){
            isPaired=true;
            filenames.add(mate.getAbsolutePath());
            
            num = mate.getName().length();
            if (mate.getName().indexOf(".") > 0) {
                num = mate.getName().indexOf(".");
            }

            shortNames.add(mate.getName().substring(0,num));
            System.out.println("Mate in: "+shortNames.get(1));
        }
        
        String readDir="",mateDir="";

        // Quality control execution //
        if(mode){
            FastQCApplication app = new FastQCApplication(filenames,extra,metaDataDir);
            app.setVisible(true);
            do{}while(true);
        }else{
            
            String cmdRoot="fastqc";
            int cmdExitVal;
            Process cmdProc;
            try {
                cmdProc = Runtime.getRuntime().exec(cmdRoot+" -v");
                cmdProc.waitFor();
                cmdExitVal = cmdProc.exitValue();
            } catch (IOException e) {
                System.err.println("FastQC threw an exception:\n" + e);
                return ErrorCode.ERROR;
            }
            if (cmdExitVal>0) {
                try { 
                    cmdRoot="../../lib/FastQC/fastqc";
                    cmdProc=Runtime.getRuntime().exec(cmdRoot+" -v");
                    cmdProc.waitFor();
                } catch (IOException e) {
                    throw new IOException("Cannot find command fastqc in PATH or BundleRoot/lib/FastQC");
                }
            }
            
            String cmd = cmdRoot;
            for(int i=0;i<filenames.size();i++){
                cmd = cmd + " " + filenames.get(i);
            }
            cmd = cmd + " " + "--outdir=" + metaDataDir;
            cmd = cmd + " " + "--extract --threads "+threads;

            System.out.println("Command");
            System.out.println(cmd);

            try
            {            
                Runtime rt = Runtime.getRuntime();
                Process version = rt.exec(cmdRoot+" -v");
                InputStream verIn= version.getInputStream();
                InputStreamReader verInR = new InputStreamReader(verIn);
                BufferedReader vbr = new BufferedReader(verInR);
                String line = null;
                while ( (line = vbr.readLine()) != null)
                    System.out.println(line);

                Process proc = rt.exec(cmd);
                InputStream stdin = proc.getInputStream();
                InputStreamReader isr = new InputStreamReader(stdin);
                BufferedReader br = new BufferedReader(isr);
                line = null;
                // Fix to find the exceptions from FastQC that are printed in its output but not thrown /julia
                boolean throwException = false;

                while ( (line = br.readLine()) != null) {
                    if ( line.matches(".*Exception.*")) {
                        throwException = true;
                    }
                    System.out.println("<FASTQC>"+line+"<FASTQC>");
                }

                InputStream stderr = proc.getErrorStream();
                isr = new InputStreamReader(stderr);
                br = new BufferedReader(isr);
                line = null;

                while ( (line = br.readLine()) != null) {
                    if ( line.matches(".*Exception.*")) {
                        throwException = true;
                    }
                    System.err.println("<FASTQC>"+line+"<FASTQC>");
                }

                if ( throwException ) {
                    System.err.println("FastQC threw a silent exception");
                    return ErrorCode.ERROR;
                }

                int exitVal = proc.waitFor();            
                if (exitVal != 0 ) {
                    System.err.println("FastQC exited with error: " + exitVal);
                    return ErrorCode.ERROR;
                }
            } catch (Throwable t)
            {
                t.printStackTrace();
                return ErrorCode.ERROR;
            }
           
            Path dir = Paths.get(metaDataDir);
            try {
                DirectoryStream<Path> stream = Files.newDirectoryStream(dir);
                for (Path file: stream) {
                    System.out.println("Files created: "+file.toAbsolutePath().toString());
                    if (file.getFileName().toString().contains(shortNames.get(0)) && !file.getFileName().toString().contains("zip") && !file.getFileName().toString().contains("html")) {
                       readDir = file.toAbsolutePath().toString();
                       System.out.println("read: "+readDir);
                    }
                    else if (isPaired && file.getFileName().toString().contains(shortNames.get(1)) && !file.getFileName().toString().contains("zip") && !file.getFileName().toString().contains("html")){
                        mateDir = file.toAbsolutePath().toString();
                        System.out.println("mate: "+mateDir);
                   }
                }                
            } catch (IOException e) {
                System.err.println("directory empty "+dir.toString());
                return ErrorCode.ERROR;
            } 
            
            try {
                Writer output = new BufferedWriter(new FileWriter(extra));
                output.write("");
                output.close();
            } catch (IOException e) {
                System.err.println("Error occurs in generating extra file!");
                e.printStackTrace();
                return ErrorCode.ERROR;
            }
        }

        // Output //
        try {

            List<String> latexOut = new ArrayList<String>();
            latexOut.addAll(outputResult(cf, readDir, reportPath, "read", true, true));

            if(isPaired)
                latexOut.addAll(outputResult(cf, mateDir, reportPath, "mate", pagebreak, false));
            

            summary.write(cf, "summary");

            LatexTools.writeDocument(report, latexOut);

            // Clean metadata
            if(clean)
                for(File file: folder.listFiles()) file.delete();

        } 
        catch (Throwable t) {
            t.printStackTrace();
            return ErrorCode.ERROR;
        }

        return ErrorCode.OK;
    }

    /**
     * Report generator
     */
    private List<String> outputResult(CommandFile cf, String filename, String reportPath, String key, boolean addClearPage, boolean isRead) throws Throwable {
        
      
        //filename contains the output data from FastQC for either the read or the mate 
        String statsOrig = filename+"/fastqc_data.txt";

        //outDir will contain the statistics in text format and the warnings
        File statsNew = new File( outDir.getAbsolutePath() + "/" + key + ".txt");
        File warns = new File( outDir.getAbsolutePath() + "/" + key  + "_warnings.txt");
        copyFile(new File(statsOrig), statsNew);
        copyFile(new File(filename + "/summary.txt"),warns);

        // Check if summary statistics was copied (if it existed)
        if(!statsNew.exists()) {
            statsNew.createNewFile();
        }

        summary.add(key, new File(key + ".txt"));
        summary.add(key + "_warnings", new File(key + "_warnings.txt"));
        
        // Latex report
        //Original images folder from fastqc
        File figFolder = new File(filename+"/Images/");

        String [] figs = figFolder.list();
        File target;

        String targetFileName = report.getAbsolutePath() + "/";

        if(!suffix.equals("")) {
            targetFileName = targetFileName + suffix + "_";
        }

        targetFileName = targetFileName + key;

        target = new File(targetFileName);

        if(!target.exists()){
            target.mkdirs();
        }
        System.out.println("Folder "+figFolder.getAbsolutePath());
        try {
            if (figs != null) {
                for(int i=0;i<figs.length;i++){
                    copyFile(new File(figFolder.getAbsolutePath()+"/"+figs[i]), new File(target.getAbsolutePath()+"/"+figs[i]));
                }
                // KMER profiles figure is not produced if there is no overrepresented
                // KMERs.
                File kmerProfiles = new File(target.getAbsolutePath()+"/" + "kmer_profiles.png");
                if(!kmerProfiles.exists()) {
                    copyFile(new File(cf.getMetadata(CommandFile.METADATA_COMPONENT_PATH) + "/" + FastQC.NO_OVERREPRESENTED_KMERS_FIGURE), kmerProfiles);
                    copyFile(new File(cf.getMetadata(CommandFile.METADATA_COMPONENT_PATH) + "/" + FastQC.NO_OVERREPRESENTED_KMERS_FIGURE), new File(figFolder.getAbsolutePath()+"/kmer_profiles.png"));

                }
                File tileQuality = new File(target.getAbsolutePath()+"/" + "per_tile_quality.png");
                if(!tileQuality.exists()){
                    copyFile(new File(cf.getMetadata(CommandFile.METADATA_COMPONENT_PATH) + "/" + FastQC.NO_PER_TILE_QUALITY_FIGURE), tileQuality);
                    copyFile(new File(cf.getMetadata(CommandFile.METADATA_COMPONENT_PATH) + "/" + FastQC.NO_PER_TILE_QUALITY_FIGURE), new File(figFolder.getAbsolutePath()+"/per_tile_quality.png"));
                }
                File adapterContent = new File(target.getAbsolutePath()+"/" + "adapter_content.png");
                if(!adapterContent.exists()){
                    copyFile(new File(cf.getMetadata(CommandFile.METADATA_COMPONENT_PATH) + "/" + FastQC.NO_ADAPTER_CONTENT_FIGURE), adapterContent);
                    copyFile(new File(cf.getMetadata(CommandFile.METADATA_COMPONENT_PATH) + "/" + FastQC.NO_ADAPTER_CONTENT_FIGURE), new File(figFolder.getAbsolutePath()+"/adapter_content.png"));
                }
            }
            else {
                System.err.println("Figure folder is empty");
            }
        }
        catch (IOException e) {   
            e.printStackTrace();
        }   
        List<String> latexOutTmp = new ArrayList<String>();
        latexOutTmp.add("\\"+ sectionType +"{" + LatexTools.quote(filename) + "}");

        latexOutTmp.addAll(LatexTools.formatFigure(target + "/" + "per_base_quality.png",
                                                           null,
                                                           "Per base quality" + " (" + filename.replace("_","\\_") + ")",
                                                           "!ht", 15.0));

        latexOutTmp.addAll(LatexTools.formatFigure(target + "/" + "duplication_levels.png",
                                                           null,
                                                           "Duplication levels" + " (" + filename.replace("_","\\_") + ")",
                                                           "!ht", 15.0));

        latexOutTmp.addAll(LatexTools.formatFigure(target + "/" + "per_base_gc_content.png",
                                                           null,
                                                           "Per base GC content" + " (" + filename.replace("_","\\_") + ")",
                                                           "!ht", 15.0));

        latexOutTmp.addAll(LatexTools.formatFigure(target + "/" + "per_base_n_content.png",
                                                           null,
                                                           "Per base n content" + " (" + filename.replace("_","\\_") + ")",
                                                           "!ht", 15.0));

        latexOutTmp.addAll(LatexTools.formatFigure(target + "/" + "per_base_sequence_content.png",
                                                           null,
                                                           "Per base sequence content" + " (" + filename.replace("_","\\_") + ")",
                                                           "!ht", 15.0));

        latexOutTmp.addAll(LatexTools.formatFigure(target + "/" + "per_sequence_gc_content.png",
                                                           null,
                                                           "Per sequence GC content" + " (" + filename.replace("_","\\_") + ")",
                                                           "!ht", 15.0));

        latexOutTmp.addAll(LatexTools.formatFigure(target + "/" + "sequence_length_distribution.png",
                                                           null,
                                                           "Sequence length distribution" + " (" + filename.replace("_","\\_") + ")",
                                                           "!ht", 15.0));

        latexOutTmp.addAll(LatexTools.formatFigure(target + "/" + "kmer_profiles.png",
                                                           null,
                                                           "KMER profiles" + " (" + filename.replace("_","\\_") + ")",
                                                           "!ht", 15.0));

        if(addClearPage){
            latexOutTmp.add("\\clearpage");
        }

            return latexOutTmp;

    }

    public static void copyFile(File sourceFile, File destFile)
                throws IOException {
        if (!sourceFile.exists()) {
            System.err.println("Source file doesn't exist: "+sourceFile.getAbsolutePath());
            System.err.println(sourceFile);
            return;
        }
        if (!destFile.exists()) {
                destFile.createNewFile();
        }
        System.out.println("Copying "+sourceFile.getAbsolutePath()+" to "+destFile.getAbsolutePath());
        FileChannel source = null;
        FileChannel destination = null;
        source = new FileInputStream(sourceFile).getChannel();
        destination = new FileOutputStream(destFile).getChannel();
        if (destination != null && source != null) {
                destination.transferFrom(source, 0, source.size());
        }
        if (source != null) {
                source.close();
        }
        if (destination != null) {
                destination.close();
        }
    }


    /**
     * Delete meta-data
     */

    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    } 

    /**
     * Gets inputs, outputs and parameters from the command file
     *
     */
    private void getInsOuts(CommandFile cf) {
        // Inputs and outputs
        read = cf.getInput("read");
        if(cf.inputDefined("mate"))
            mate = cf.getInput("mate");
        outDir = cf.getOutput("summary");
        report = cf.getOutput("report");
        folder = cf.getOutput("folder");
        extra = cf.getOutput("extra");
        outDir.mkdirs();
        summary = new IndexFile();
        folder.mkdirs();
        metaDataDir = folder.getAbsolutePath();
        if(!report.exists())
            report.mkdirs();
        reportPath=report.getAbsolutePath();
        System.out.println("Report "+reportPath);

        // Parameters
        mode = cf.getBooleanParameter("interactive");
        clean = cf.getBooleanParameter("clean");
        sectionType = cf.getParameter("sectionType");
        pagebreak = cf.getBooleanParameter("pagebreak");
        suffix = cf.getParameter("suffix");
        threads = cf.getMetadata("custom_cpu");
        if (threads==null) { threads="2"; }
        try { int threads_int = Integer.parseInt(threads);
        } catch (NumberFormatException e) {
            cf.writeError("Error: only integer numbers can be used for 'cpu' annotation");
            throw new NumberFormatException();
        }

    }


    /**
    * @param argv Pipeline arguments
    */
    public static void main(String[] argv) {
        new FastQC().run(argv);
    }
}

