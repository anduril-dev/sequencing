<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>SISSRs</name>
    <version>1.0</version>
    <doc>SISSRs is a software application for precise identification of genome-wide transcription factor binding
    sites from ChIP-Seq data. It is essentially a perl implementation of the SISSRs algorithm outlined in
    Jothi et al [1], with several new features that were not fully described in the original paper.
    [1] Jothi R, Cuddapah S, Barski A, Cui K, Zhao K. Genome-wide identification of in vivo protein-DNA
    binding sites from ChIP-Seq data. Nucleic Acids Research (2008) 36(16):5221-31.

    The component installation folder contains a PDF version of the original manual.
    </doc>
    <author email="lauri.lyly@helsinki.fi">Lauri Lyly</author>
    <launcher type="bash">
	<argument name="file" value="sissr.sh"/>   
    </launcher>

    <inputs>
    	<input name="treatment" type="BED">
            <doc>The aligned ChIP-Seq reads for the sample in BED format.</doc>
        </input>
    	<input name="control" type="BED" optional="true">
            <doc>The aligned ChIP-Seq reads for the control in BED format.
	    [-b ]	background file containing tags in BED format to be
		used as control; -e and -p can be set to desired
		values to control specificity and specificity, resp.
</doc>
        </input>
        <input name="exclude" type="TextFile" optional="true"> <doc>
 [-q ]	file containing genomic regions to exclude; reads
		mapped to these regions will be ignored; file
		format: 'chr startCoord endCoord'
	</doc> </input>

    </inputs>
    <outputs>
        <output name="output" type="TextFile">
            <doc>Output file</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="genome_size" type="float" default="2700000000">
            <doc>Genome size.</doc>
        </parameter>
        <parameter name="keep_single" type="boolean" default="false"> <doc>
		[-a] only one read is kept if multiple reads align to the same genomic coordinate (minimizes amplification bias)
	</doc> </parameter>
        <parameter name="fraglen" type="int" default="-1"> <doc>
 [-F]	average length of DNA fragments that were sequenced
		(default: estimated from reads, i.e. -1)
	</doc> </parameter>
        <parameter name="fdr" type="float" default="0.001"> <doc>
 [-D]	false discovery rate (default: 0.001) if random
		background model based on Poisson probabilities need
		to be used as control (also check option -b below)
	</doc> </parameter>
        <parameter name="background_sites" type="float" default="10"> <doc>
 [-e]	e-value (>=0); it is the number of binding sites
		one might expect to infer by chance (default: 10);
		this option is irrelevant if -b option is NOT used
	</doc> </parameter>
        <parameter name="p_value" type="float" default="0.001"> <doc>
 [-p]	p-value threshold for fold enrichment of ChIP tags
		at a binding site location compared to that at the
		same location in the control data (default: 0.001);
		this option is irrelevant if -b option is NOT used
	</doc> </parameter>
        <parameter name="mappable_fraction" type="float" default="0.8"> <doc>
 [-m]	fraction of genome mappable by reads (default: 0.8
		for hg18, assuming ELAND was used to map the reads;
		could be different for different organisms and other
		alignment algorithms)
	</doc> </parameter>
        <parameter name="window_size" type="int" default="20"> <doc>
 [-w]	scanning window size (even number > 1), which
		controls for noise (default: 20)
	</doc> </parameter>
        <parameter name="min_reads" type="int" default="2"> <doc>
 [-E]	min number of 'directional' reads required on each
		side of the inferred binding site (>0); (default: 2)
	</doc> </parameter>
        <parameter name="maxlen" type="int" default="500"> <doc>
 [-L]	upper-bound on the DNA fragment length (default: 500)
	</doc> </parameter>
        <parameter name="report_narrow" type="boolean" default="false"> <doc>
 [-t]	reports each binding site as a single genomic
		coordinate (transition point t in Fig 1A)
	</doc> </parameter>
        <parameter name="report_wide" type="boolean" default="false"> <doc>
 [-r]		reports each binding site as an X-bp binding region
		centered on inferred binding coordinate; X denotes
		the distance from the start of the right-most red
		bar (see Fig 1A in the manuscript; lower-left)
		to the end of the left-most blue bar surrounding the
		actual binding site (transition point t in Fig 1A)
	</doc> </parameter>
        <parameter name="merge_regions" type="boolean" default="false"> <doc>
 [-c]		same as the -r (report_region) option, except that it reports binding
		sites that are clustered within F bp of each other as
		a single binding region; this is the default option
	</doc> </parameter>
        <parameter name="keep_monostrand_sites" type="boolean" default="false"> <doc>
 [-u]		(also) reports binding sites supported only by reads
		mapped to either sense or anti-sense strand; this
		option will recover binding sites whose sense or
		anti-sense reads were not mapped for some reason
		(e.g., falls in unmappable/repetitive regions)
	</doc> </parameter>
        <parameter name="print_progress" type="boolean" default="true"> <doc>
 [-x]		do not print progress report (default: prints report)
	</doc> </parameter>
    </parameters>
</component>
