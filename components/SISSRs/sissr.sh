#!/bin/bash -x

# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/lang/bash/functions.sh"

# use these functions to read command file
treatment=$( getinput treatment ) 
control=$( getinput control ) 
exclude=$( getinput exclude ) 
outfile=$( getoutput output )
peaks=$( getoutput peaks )

cutoff=$( getparameter cutoff )
genome_size=$( getparameter genome_size)
keep_single=$( getparameter keep_single)
fraglen=$( getparameter fraglen)
fdr=$( getparameter fdr)
background_sites=$( getparameter background_sites)
p_value=$( getparameter p_value)
mappable_fraction=$( getparameter mappable_fraction)
window_size=$( getparameter window_size)
min_reads=$( getparameter min_reads)
maxlen=$( getparameter maxlen)
report_narrow=$( getparameter report_narrow)
report_wide=$( getparameter report_wide)
merge_regions=$( getparameter merge_regions)
keep_monostrand_sites=$( getparameter keep_monostrand_sites)
print_progress=$( getparameter print_progress)
    
# you can use a special temporary directory:
# tempdir=$( gettempdir )
#echo "Hello" > "$tempdir"/tempfile

opt_multi="-c" # The default from either -t, -r or -c
if [ "$report_narrow" == "true" ]; then
	opt_multi="-t"
elif [ "$report_wide" == "true" ]; then
	opt_multi="-r"
fi

opt_a=""
if [ "$keep_single" == "true" ]; then
	opt_a="-a"
fi

opt_F=""
if [ "$fraglen" != -1 ]; then
	opt_F="-F $fraglen"
fi

opt_u=""
if [ "$keep_monostrand_sites" == "true" ]; then
	opt_u="-u"
fi

opt_x=""
if [ "$print_progress" == "true" ]; then
	opt_x="-x"
fi

if [ "$control" != "" ]; then
	control="-b $control"
fi
if [ "$exclude" != "" ]; then
	exclude="-q $exclude"
fi

./sissrs.pl -i $treatment $control $exclude -s $genome_size -o $outfile $opt_F -e $background_sites -p $p_value -m $mappable_fraction -w $window_size -E $min_reads -L $maxlen $opt_u $opt_x $opt_a $opt_multi

# Grep the embedded CSV file
echo -e '"chr"\t"start"\t"end"\t"tags"\t"fold"\t"p_value"' > $peaks
cat $outfile | grep "^chr" >> $peaks

if [ -f $outfile ];
then echo "Successful run." >> $logfile
     echo $slsep >> $logfile
# messages should be separated with $slsep in log/error file.
fi


