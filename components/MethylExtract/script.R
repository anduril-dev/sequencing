methylConvert = function(file, merge=T,removeChr=T,filterCov=T,covThr=5){
	colClasses = rep(NA,9)
	colClasses[c(6,9)] = "NULL"
	chr = c(paste0("chr",c(1:22,"X","Y","M","MT")),c(1:22,"X","Y","M","MT"))
	meth = read.table(file,header=F,stringsAsFactors=F,sep="\t",na.strings=".",colClasses=colClasses)
	if (removeChr)
		meth = meth[meth[,1] %in% chr,]
	if (merge)
		meth = data.frame(chr = meth[,1],start=meth[,2],end = meth[,2]+1, meth = (rowSums(meth[,c(4,6)],na.rm=T)/rowSums(meth[,c(5,7)],na.rm=T))*100, nC = rowSums(meth[,c(4,6)],na.rm=T), nT = rowSums(meth[,c(5,7)],na.rm=T)-rowSums(meth[,c(4,6)],na.rm=T))
	if (!merge){
		meth = data.frame(chr = rep(meth[,1],2), start = c(meth[,2],meth[,2]+1), end = c(meth[,2],meth[,2]+1), meth = c((meth[,4]/meth[,5])*100,(meth[,6]/meth[,7])*100), nC = c(meth[,4],meth[,6]),nT = c(meth[,5]-meth[,4],meth[,7]-meth[,6]))
		meth = meth[!is.na(meth[,4]),]
	}

	if (filterCov)
		meth = meth[meth$nC + meth$nT >=covThr,]
	return(meth)
}

args = commandArgs(trailingOnly=T)
input = args[1]
output = args[2]
destrand = args[3]
removeChr = args[4]
if (destrand=="false")
	merge=F
if (destrand=="true")
	merge=T
if (removeChr=="false")
	removeChr=F
if (removeChr=="true")
	removeChr=T
converted = methylConvert(input,filterCov=F,merge=merge,removeChr=removeChr)
write.table(format(converted,scientific=F),file=output,sep="\t",quote=F,col.names=F,row.names=F)
