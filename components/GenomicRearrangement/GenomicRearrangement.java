import org.anduril.component.SkeletonComponent;
import org.anduril.component.ErrorCode;
import org.anduril.component.CommandFile;
import org.anduril.core.utils.IOTools;
import org.anduril.asser.io.CSVWriter;
import org.anduril.asser.io.CSVParser;

import java.io.FileReader;
import java.io.IOException;
import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Tool for analyzing genomic rearrangements with DELLY
 *
 * @author Riku Louhimo
 *
 */
public class GenomicRearrangement extends SkeletonComponent {
    /* Command  */
    private String  COMMAND;

    /* Input files */
    private File reference;  // Input reference fasta file
    private File input;      // Input file
    
    /* Output files */
    private File    PE_out;
    private File    SR_out;
    private File    PE_tmp;
    private File    SR_tmp;
    private File    PE_tmp2;
    private File    SR_tmp2;
    private File    OUT_FOLDER;

    /* Parameters */
    private String   algorithm;
    private String   dellyEnv;
    private String[] PE_columnHeaders;
    private String[] SR_columnHeaders;
    
    private int     minMapQuality;
    private int     minFlankSize;
    private int     inversionOverlapMerge;
    private int     distMergeCutoff;
    private int     madCutoff;
    
    private double  maxEpsilon;
    
    private boolean verbose;
    
   
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        try {
            getParameters(cf);
        } catch (IOException ie){
            cf.writeError(ie);
            return ErrorCode.PARAMETER_ERROR;
        }
        // Get inputs, outputs and parameters from command file
        getInsOuts(cf);
        // Create the command for DELLY algorithms
        createCommand(cf);
        // Execute DELLY
        if (!verbose) System.out.println("Execution command is:\n" + COMMAND);
        int status;
        Map<String,String> env = new HashMap<String,String>(System.getenv());
        env.put("TMP", cf.getTempDir().getAbsolutePath());

        if (verbose){
            status = IOTools.launch(COMMAND, null, env, "", "");
        } else {
            status = IOTools.launch(COMMAND, null, env, null, "");
        }
        if (status > 0){
            return ErrorCode.ERROR;
        } else {
            if (algorithm.equals("duppy")){
                String sortCMD     = "grep -i Duplication "+PE_tmp2.getAbsolutePath()+" > "+PE_tmp.getAbsolutePath();
                FileWriter cmdFile = new FileWriter(cf.getTempDir().getAbsolutePath()+File.separator+"_cmd.sh");
                cmdFile.write(sortCMD);
                cmdFile.close();
                cf.writeLog("Invoking: "+sortCMD);
                int value=IOTools.launch("bash "+cf.getTempDir().getAbsolutePath()+File.separator+"_cmd.sh");
            } else if (algorithm.equals("delly")){
                String sortCMD     = "grep -i Deletion "+PE_tmp2.getAbsolutePath()+" > "+PE_tmp.getAbsolutePath();
                FileWriter cmdFile = new FileWriter(cf.getTempDir().getAbsolutePath()+File.separator+"_cmd.sh");
                cmdFile.write(sortCMD);
                cmdFile.close();
                cf.writeLog("Invoking: "+sortCMD);
                int value=IOTools.launch("bash "+cf.getTempDir().getAbsolutePath()+File.separator+"_cmd.sh");
            }
        }
        
        // Reformat output files to contain headers
        try {
            checkOutput(cf);
        } catch (IOException e){
            cf.writeError(e);
            return ErrorCode.OUTPUT_IO_ERROR;
        }

        return ErrorCode.OK;
	}
	
	/**
	 * Checks the output
	 *
	 */
	private void checkOutput(CommandFile cf) throws IOException {
        try {
            if (!PE_out.exists()) PE_out.createNewFile();
            if (!PE_tmp.exists()) PE_tmp.createNewFile();
            if (!SR_out.exists()) SR_out.createNewFile();
            if (!SR_tmp.exists()) SR_tmp.createNewFile();
            
            makeHeader(PE_tmp, PE_out, PE_columnHeaders);
            makeHeader(SR_tmp, SR_out, SR_columnHeaders);

        } catch (IOException exception) {
            throw exception;
        }
	}
	
	private void makeHeader(File fileIn, File fileOut, String[] columnHeaders) throws IOException {
	    CSVWriter writer = null;
	    CSVParser parser = new CSVParser(new FileReader(fileIn), "\t", 0, "NA", false, false);
        try {
            writer = new CSVWriter(columnHeaders, fileOut, false);
            for (String[] row : parser){
                writer.writeRow(false, row);
            }
        } catch (IllegalArgumentException e) {
            for (String[] row : parser){
                System.out.println("An error has occured when trying to write the following line: ");
                for (int i = 0; i < row.length-1; i++){
                    System.out.print(row[i]+"  ");
                }
                System.out.println();
                throw (e);
            }
        } finally {
            try {
                writer.close();
                parser.close();
                //fileIn.delete();
            } catch (NullPointerException e) {
                throw (e);
            }
        }
	}

    /**
     * Gets inputs and outputs from the command file
     *
     */
    private void getInsOuts(CommandFile cf) throws IOException {
        if (cf.inputDefined("input")){
            input = cf.getInput("input");
        } else if (cf.inputDefined("folder")){
            input   = cf.getInput("folder");
        } else {
            cf.writeError("Input file is not present.");
        }
        if (cf.inputDefined("reference")) reference = cf.getInput("reference");
                
        OUT_FOLDER = cf.getOutput("delly");
        if (!OUT_FOLDER.exists() && !OUT_FOLDER.mkdirs())
            cf.writeError("Cannot create output folder: "+OUT_FOLDER.getCanonicalPath());
        PE_out  = new File(OUT_FOLDER.getAbsolutePath()+"/"+algorithm+".csv");
        PE_tmp  = new File(cf.getTempDir().getAbsolutePath()+"/"+algorithm+".csv");
        PE_tmp2 = new File(cf.getTempDir().getAbsolutePath()+"/tmp1.txt");
        SR_out  = new File(OUT_FOLDER.getAbsolutePath()+"/"+algorithm+"Breaks.csv");
        SR_tmp  = new File(cf.getTempDir().getAbsolutePath()+"/"+algorithm+"Breaks.csv");
        SR_tmp2 = new File(cf.getTempDir().getAbsolutePath()+"/tmp2.txt");
    }

    /**
     * Gets parameters from the command file
     *
     */
    private void getParameters(CommandFile cf) throws IOException{
        algorithm        = cf.getParameter("algorithm");
        if (algorithm.equals("delly") || algorithm.equals("jumpy") ||
            algorithm.equals("invy")  || algorithm.equals("duppy")){
            // Do nothing
        } else {
            throw(new IOException("Invalid command. Must be one of 'delly', 'jumpy', 'invy' or 'duppy'."));
        }
        dellyEnv              = cf.getParameter("dellyEnv");
        if (dellyEnv.equals("")) dellyEnv = System.getenv("DELLY_HOME");
        minMapQuality         = cf.getIntParameter("minMapQuality");
        minFlankSize          = cf.getIntParameter("minFlankSize");
        inversionOverlapMerge = cf.getIntParameter("inversionOverlapMerge");
        distMergeCutoff       = cf.getIntParameter("distMergeCutoff");
        madCutoff             = cf.getIntParameter("madCutoff");
        verbose               = cf.getBooleanParameter("verbose");
        maxEpsilon            = cf.getDoubleParameter("maxEpsilon");
    }

   
    /**
     * Creates the command string.
     */
    private void createCommand(CommandFile cf){
        // Create the command for DELLY
        COMMAND   = dellyEnv+"/"+algorithm;
        
        // Parameters
        if (minMapQuality > 0)  COMMAND = COMMAND+" --map-qual "+minMapQuality;
        if (minFlankSize != 13) COMMAND = COMMAND+" --min-flank "+minFlankSize;

        // Variable part of the command
        String VAR_COM = "";
        if (algorithm == "delly"){
            if (madCutoff != 5)    VAR_COM = VAR_COM+" --mad-cutoff "+madCutoff;
            if (maxEpsilon >= 0.0) VAR_COM = VAR_COM+" --epsilon "+maxEpsilon;
        } else if (algorithm == "jumpy"){
            if (distMergeCutoff != 300) VAR_COM = VAR_COM+" --distance "+distMergeCutoff;
        } else if (algorithm == "invy"){
            if (maxEpsilon >= 0)             VAR_COM = VAR_COM+" --epsilon "+maxEpsilon;
            if (inversionOverlapMerge != 80) VAR_COM = VAR_COM+" --percent-overlap "+inversionOverlapMerge;
        } else if (algorithm == "duppy"){
            if (maxEpsilon >= 0) VAR_COM = VAR_COM+" --epsilon "+maxEpsilon;
        }
        COMMAND = COMMAND+VAR_COM;

        // Outputs
        if (algorithm.equals("invy") || algorithm.equals("jumpy")){
            COMMAND = COMMAND + " --outfile "+PE_tmp2.getAbsolutePath();
            COMMAND = COMMAND + " --merge-file "+PE_tmp.getAbsolutePath();
            COMMAND = COMMAND + " --break-merge "+SR_tmp.getAbsolutePath();
            COMMAND = COMMAND + " --breakpoints "+SR_tmp2.getAbsolutePath();
            if (algorithm.equals("invy")){
                String[] columnHeaders1 = {"chr", "start", "end", "size", 
                                           "supporting_pairs", "avg_mapping_quality",
                                           "id" };
                PE_columnHeaders = columnHeaders1;
                String[] columnHeaders2 = {"chr", "start", "end", "size", 
                                           "overlap_of_merged_left/right_inversion_calls",
                                           "avg_mapping_quality_of_both_calls",
                                           "id" };
                SR_columnHeaders = columnHeaders2;
            } else {
                String[] columnHeaders1 = {"chrA", "startA", "endA", "sizeA", "" +
                                           "chrB/insertion_point/inverted(0=no,1=yes)", 
                                           "avg_mapping_quality_of_both_calls/avg_paired-end_support",
                                           "id" };
                PE_columnHeaders = columnHeaders1;
                String[] columnHeaders2 = {"chrA", "startA", "endA", "sizeA", "" +
                		                   "chrB/insertion_point/inverted(0=no,1=yes)", 
                                           "perc_aligment_quality/nro_split_reads",
                                           "id" };
                SR_columnHeaders = columnHeaders2;
            }
        } else {
            COMMAND = COMMAND + " --outfile "+PE_tmp2.getAbsolutePath();
            COMMAND = COMMAND + " --breakpoints "+SR_tmp.getAbsolutePath();
            String[] columnHeaders1 = {"chr", "start", "end", "size", 
                                       "supporting_pairs", "avg_mapping_quality",
                                       "id" };
            PE_columnHeaders = columnHeaders1;
            String[] columnHeaders2 = {"chr", "start", "end", "size", 
                                       "supporting_splits", "perc_alignment_quality",
                                       "id" };
            SR_columnHeaders = columnHeaders2;
        }
        
        // Inputs
        if (cf.inputDefined("reference")) COMMAND = COMMAND + " -g "+reference.getAbsolutePath();
        if (input.isDirectory()){
            for (File in : input.listFiles()){
                COMMAND = COMMAND+" "+in.getAbsolutePath();
            }
        } else {
            COMMAND = COMMAND+" "+input.getAbsolutePath();
        }
    }
    

    public static void main(String[] args) {
        new GenomicRearrangement().run(args);
    }
}

