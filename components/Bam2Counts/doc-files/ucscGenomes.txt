db	organism	date	name
hg19	Human	Feb. 2009	Genome Reference Consortium GRCh37
hg18	Human	Mar. 2006	NCBI Build 36.1
hg17	Human	May 2004	NCBI Build 35
hg16	Human	Jul. 2003	NCBI Build 34
felCat4	Cat	Dec. 2008	NHGRI catChrV17e
felCat3	Cat	Mar. 2006	Broad Institute Release 3
galGal3	Chicken	May 2006	WUSTL Gallus-gallus-2.1
galGal2	Chicken	Feb. 2004	WUSTL Gallus-gallus-1.0
panTro3	Chimp	Oct. 2010	CGSC Build 2.1.3
panTro2	Chimp	Mar. 2006	CGSC Build 2.1
panTro1	Chimp	Nov. 2003	CGSC Build 1.1
bosTau4	Cow	Oct. 2007	Baylor College of Medicine HGSC Btau_4.0
bosTau3	Cow	Aug. 2006	Baylor College of Medicine HGSC Btau_3.1
bosTau2	Cow	Mar. 2005	Baylor College of Medicine HGSC Btau_2.0
canFam2	Dog	May 2005	Broad Institute v2.0
canFam1	Dog	Jul. 2004	Broad Institute v1.0
loxAfr3	Elephant	Jul. 2009	Broad loxAfr3
fr2	Fugu	Oct. 2004	JGI v4.0
fr1	Fugu	Aug. 2002	JGI v3.0
cavPor3	Guinea pig	Feb. 2008	Broad cavPor3
equCab2	Horse	Sep. 2007	Broad EquCab2
equCab1	Horse	Jan. 2007	Broad EquCab1
petMar1	Lamprey	Mar. 2007	WUSTL v3.0
anoCar2	Lizard	May 2010	Broad AnoCar2
anoCar1	Lizard	Feb. 2007	Broad AnoCar1
calJac3	Marmoset	Mar. 2009	WUSTL Callithrix_jacchus-v3.2
calJac1	Marmoset	Jun. 2007	WUSTL Callithrix_jacchus-v2.0.2
oryLat2	Medaka	Oct. 2005	NIG v1.0
mm9	Mouse	Jul. 2007	NCBI Build 37
mm8	Mouse	Feb. 2006	NCBI Build 36
mm7	Mouse	Aug. 2005	NCBI Build 35
monDom5	Opossum	Oct. 2006	Broad Institute release MonDom5
monDom4	Opossum	Jan. 2006	Broad Institute release MonDom4
monDom1	Opossum	Oct. 2004	Broad Institute release MonDom1
ponAbe2	Orangutan	Jul. 2007	WUSTL Pongo_albelii-2.0.2
ailMel1	Panda	Dec. 2009	BGI-Shenzhen AilMel 1.0
susScr2	Pig	Nov. 2009	SGSC Sscrofa9.2
ornAna1	Platypus	Mar. 2007	WUSTL v5.0.1
oryCun2	Rabbit	Apr. 2009	Broad Institute release oryCun2
rn4	Rat	Nov. 2004	Baylor College of Medicine HGSC v3.4
rn3	Rat	Jun. 2003	Baylor College of Medicine HGSC v3.1
rheMac2	Rhesus	Jan. 2006	Baylor College of Medicine HGSC v1.0 Mmul_051212
oviAri1	Sheep	Feb. 2010	ISGC Ovis aries 1.0
gasAcu1	Stickleback	Feb. 2006	Broad Release 1.0
tetNig2	Tetraodon	Mar. 2007	Genoscope v7
tetNig1	Tetraodon	Feb. 2004	Genoscope v7
xenTro2	X. tropicalis	Aug. 2005	JGI v.4.1
xenTro1	X. tropicalis	Oct. 2004	JGI v.3.0
taeGut1	Zebra finch	Jul. 2008	WUSTL v3.2.4
danRer7	Zebrafish	Jul. 2010	Sanger Institute Zv9 
danRer6	Zebrafish	Dec. 2008	Sanger Institute Zv8 
danRer5	Zebrafish	Jul. 2007	Sanger Institute Zv7 
danRer4	Zebrafish	Mar. 2006	Sanger Institute Zv6 
danRer3	Zebrafish	May 2005	Sanger Institute Zv5 
ci2	C. intestinalis	Mar. 2005	JGI v2.0
ci1	C. intestinalis	Dec. 2002	JGI v1.0
braFlo1	Lancelet	Mar. 2006	JGI v1.0
strPur2	S. purpuratus	Sep. 2006	Baylor College of Medicine HGSC v. Spur 2.1
strPur1	S. purpuratus	Apr. 2005	Baylor College of Medicine HGSC v. Spur_0.5
apiMel2	A. mellifera	Jan. 2005	Baylor College of Medicine HGSC v.Amel_2.0 
apiMel1	A. mellifera	Jul. 2004	Baylor College of Medicine HGSC v.Amel_1.2 
anoGam1	A. gambiae	Feb. 2003	IAGP v.MOZ2
droAna2	D. ananassae	Aug. 2005	Agencourt Arachne release
droAna1	D. ananassae	Jul. 2004	TIGR Celera release
droEre1	D. erecta	Aug. 2005	Agencourt Arachne release
droGri1	D. grimshawi	Aug. 2005	Agencourt Arachne release
dm3	D. melanogaster	Apr. 2006	BDGP Release 5
dm2	D. melanogaster	Apr. 2004	BDGP Release 4
dm1	D. melanogaster	Jan. 2003	BDGP Release 3
droMoj2	D. mojavensis	Aug. 2005	Agencourt Arachne release
droMoj1	D. mojavensis	Aug. 2004	Agencourt Arachne release
droPer1	D. persimilis	Oct. 2005	Broad Institute release
dp3	D. pseudoobscura	Nov. 2004	Flybase Release 1.0
dp2	D. pseudoobscura	Aug. 2003	Baylor College of Medicine HGSC Freeze 1
droSec1	D. sechellia	Oct. 2005	Broad Release 1.0
droSim1	D. simulans	Apr. 2005	WUSTL Release 1.0
droVir2	D. virilis	Aug. 2005	Agencourt Arachne release
droVir1	D. virilis	Jul. 2004	Agencourt Arachne release
droYak2	D. yakuba	Nov. 2005	WUSTL Release 2.0
droYak1	D. yakuba	Apr. 2004	WUSTL Release 1.0
caePb2	C. brenneri	Feb. 2008	WUSTL 6.0.1
caePb1	C. brenneri	Jan. 2007	WUSTL 4.0
cb3	C. briggsae	Jan. 2007	WUSTL Cb3
cb1	C. briggsae	Jul. 2002	WormBase v. cb25.agp8
ce6	C. elegans	May 2008	WormBase v. WS190
ce4	C. elegans	Jan. 2007	WormBase v. WS170
ce2	C. elegans	Mar. 2004	WormBase v. WS120
caeJap1	C. japonica	Mar. 2008	WUSTL 3.0.2
caeRem3	C. remanei	May 2007	WUSTL 15.0.1
caeRem2	C. remanei	Mar. 2006	WUSTL 1.0
priPac1	P. pacificus	Feb. 2007	WUSTL 5.0
aplCal1	Sea Hare	Sep. 2008	Broad Release Aplcal2.0
sacCer2	Yeast	June 2008	SGD June 2008 sequence
sacCer1	Yeast	Oct. 2003	SGD 1 Oct 2003 sequence
