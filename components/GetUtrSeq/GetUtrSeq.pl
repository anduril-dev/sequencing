#!usr/bin/perl

use strict;
use componentSkeleton;
use Encode::Unicode;
use Bio::EnsEMBL::Registry;

sub execute {

   my ($cf_ref) = @_;
   my $input = get_input($cf_ref,"transcripts");
   my $type = get_parameter($cf_ref,"primeType");

   my $db_dir = get_input($cf_ref,"connection");
   open(FILE_DB,$db_dir);
   my @lines = <FILE_DB>;
   my @tmp1;
   my $line;
   my %db_info;
   foreach $line (@lines){
        chomp($line);
        @tmp1 = split(/ *= */,$line);
        $db_info{$tmp1[0]} = $tmp1[1];
   }
   close(FILE_DB);

   open(FILEIN,$input);
   my @lines = <FILEIN>;
   my $nline = @lines;
   close(FILEIN);

   my $registry = 'Bio::EnsEMBL::Registry';

   $registry->load_registry_from_db(
         -host => $db_info{'host'},
         -user => $db_info{'user'}
   );

   my $transcript_adaptor = $registry->get_adaptor('Human', 'Core', 'Transcript');

   open(FILEOUT1, ">".get_output($cf_ref,"UTR5"));
   open(FILEOUT2, ">".get_output($cf_ref,"UTR3"));

   if($type eq "both"){
	for(my $i=1;$i<$nline;$i++){
        	chomp($lines[$i]);
		$lines[$i]=~s/"//g;
        	my $transcript = $transcript_adaptor->fetch_by_stable_id($lines[$i]);
		if(!defined($transcript)){next}
		my $fiv_utr = $transcript->five_prime_utr();
	        my $thr_utr = $transcript->three_prime_utr();
		if(defined($fiv_utr)){
			print FILEOUT1 ">".$lines[$i]."\n".$fiv_utr->seq()."\n";
		}
		if(defined($thr_utr)){
			print FILEOUT2 ">".$lines[$i]."\n".$thr_utr->seq()."\n";
		}
   	}
   }elsif($type eq "5prime"){
	for(my $i=1;$i<$nline;$i++){
                chomp($lines[$i]);
		$lines[$i]=~s/"//g;
                my $transcript = $transcript_adaptor->fetch_by_stable_id($lines[$i]);
		if(!defined($transcript)){next}
                my $fiv_utr = $transcript->five_prime_utr();
                if(defined($fiv_utr)){
			print FILEOUT1 ">".$lines[$i]."\n".$fiv_utr->seq()."\n";
		}
        }
   }elsif($type eq "3prime"){
	for(my $i=1;$i<$nline;$i++){
                chomp($lines[$i]);
		$lines[$i]=~s/"//g;
                my $transcript = $transcript_adaptor->fetch_by_stable_id($lines[$i]);
		if(!defined($transcript)){next}
                my $thr_utr = $transcript->three_prime_utr();
		if(defined($thr_utr)){	
                	print FILEOUT2 ">".$lines[$i]."\n".$thr_utr->seq()."\n";
		}
        }
   }

   close(FILEOUT1);
   close(FILEOUT2);
}

if(scalar @ARGV == 0){
    print "NO_COMMAND_FILE";
    exit;
}
my %cf = parse_command_file($ARGV[0]);
execute(\%cf);
