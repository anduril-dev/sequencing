# running functions.sh 
set -e
source "$ANDURIL_HOME/lang/bash/functions.sh"
export_command

# inputs
echo reads    				$input_reads
echo mates					$input_mates

# parameters
echo paired 				$parameter_paired
echo stringency				$parameter_stringency
echo minQuality				$parameter_minQuality
echo minLength				$parameter_minLength
echo clipR1					$parameter_clipR1
echo clipR2					$parameter_clipR2
echo extraArgs				$parameter_extraArgs
echo fastqcArgs				$parameter_fastqcArgs
echo qualityScore			$parameter_qualityScore
echo runFastqc				$parameter_runFastqc
echo adapter				$parameter_adapter
echo gzip					$parameter_gzip

mkdir $output_report
temp_folder=$( gettempdir )
mkdir $output_trimmed

### basic command ###
Command=$( readlink -f trim_galore )
fastqcArgs="--fastqc_args \"--outdir ${output_report} --extract ${fastqcArgs}\""
Command="${Command} --stringency ${parameter_stringency} --quality ${parameter_minQuality} --length ${parameter_minLength} --output_dir ${output_report} ${parameter_extraArgs}"

### print version ###
commandVersion="${Command} -v"
eval $commandVersion
cutadaptVersion=$( cutadapt --version )
echo "cutadapt version is ${cutadaptVersion}"

### runFastqc ###
if [ "${parameter_runFastqc}" == "true" ]
then
	Command="${Command} --fastqc ${fastqcArgs}"
fi

### adapter ###
if [ "${parameter_adapter}" != "" ]
then
	Command="${Command} -a '${parameter_adapter}'"
fi
### clipping ###
if [ "$parameter_clipR1" != "0" ]
then
	Command="${Command} --clip_R1 ${parameter_clipR1}"
fi

if [ "$parameter_clipR2" != "0" -a "${parameter_paired}" == "true" ]
then
	Command="${Command} --clip_R2 ${parameter_clipR2}"
fi

if [ "$parameter_qualityScore" == "phred33" ]
then
	Command="${Command} --phred33"
elif [ "$parameter_qualityScore" == "phred64" ]
then
	Command="${Command} --phred64"
else
	writeerror "unknown quality score"
	exit 1
fi

### gzip ###
if [ "$parameter_gzip" == "true" ]
then
	Command="${Command} --gzip"
else
	Command="${Command} --dont_gzip"
fi

### paired ###
if [ "$parameter_paired" == "true" ]
then
	if [ ! -e "${input_mates}" ]
	then
		writeerror "mates input does not exist"
		exit 1
	fi
	Command="${Command} --paired ${input_reads} ${input_mates}"
else
## single-end alignment
	Command="${Command} ${input_reads}"
fi

## running trim_galore
echo "trim_galore command is: "$Command
eval ${Command} 2>> "${logfile}"
if [ $? != 0 ]
then
	writeerror "trim_galore failed"
	exit 1
fi

### organising outputs
if [ "$parameter_paired" == "true" ]
then
	if [ "${parameter_runFastqc}" == "true" ]
	then
		rm "${output_report}"/*.zip
		rm "${output_report}"/*.html
		mv "${output_report}"/*val_1*fastqc "${output_report}"/reads_quality_report
		mv "${output_report}"/*val_2*fastqc "${output_report}"/mates_quality_report
	fi
	mv "${output_report}"/$(basename ${input_reads})_trimming_report.txt "${output_report}"/reads_trimming_report
	mv "${output_report}"/$(basename ${input_mates})_trimming_report.txt "${output_report}"/mates_trimming_report
	if [ "$parameter_gzip" == "true" ]
	then
		mv "${output_report}"/*val_1* "${output_trimmed}"/TrimmedReads.fq.gz
		mv "${output_report}"/*val_2* "${output_trimmed}"/TrimmedMates.fq.gz
		addarray trimmed Reads TrimmedReads.fq.gz
		addarray trimmed Mates TrimmedMates.fq.gz
        if [ -e "${output_report}"/*unpaired_1.fq.gz ]
        then 
            mv "${output_report}"/*unpaired_1.fq.gz "${output_trimmed}"/unpairedReads.fq.gz
            mv "${output_report}"/*unpaired_2.fq.gz "${output_trimmed}"/unpairedMates.fq.gz        
            addarray trimmed Ureads unpairedReads.fq.gz
            addarray trimmed Umates unpairedMates.fq.gz
        fi
	else
		mv "${output_report}"/*val_1* "${output_trimmed}"/TrimmedReads.fq
		mv "${output_report}"/*val_2* "${output_trimmed}"/TrimmedMates.fq
		addarray trimmed Reads TrimmedReads.fq
		addarray trimmed Mates TrimmedMates.fq
        if [ -e "${output_report}"/*unpaired_1.fq ]
        then 
            mv "${output_report}"/*unpaired_1.fq "${output_trimmed}"/unpairedReads.fq
            mv "${output_report}"/*unpaired_2.fq "${output_trimmed}"/unpairedMates.fq
            addarray trimmed Ureads unpairedReads.fq
            addarray trimmed Umates unpairedMates.fq
        fi  
	fi
else
	if [ "${parameter_runFastqc}" == "true" ]
	then
		rm "${output_report}"/*.html
		rm "${output_report}"/*.zip
		mv "${output_report}"/*fastqc "${output_report}"/reads_quality_report
	fi
	mv "${output_report}"/$(basename ${input_reads})_trimming_report.txt "${output_report}"/reads_trimming_report
	if [ "$parameter_gzip" == "true" ]
	then
		mv "${output_report}"/*trimmed* "${output_trimmed}"/TrimmedReads.fq.gz
		addarray trimmed Reads TrimmedReads.fq.gz
	else
		mv "${output_report}"/*trimmed* "${output_trimmed}"/TrimmedReads.fq
		addarray trimmed Reads TrimmedReads.fq
	fi
fi

### making stats output
#if [ -e ${output_report}/reads_trimming_report ]
#then 
#  report = ${output_report}/mates_trimming_report
#else
#  report = ${output_report}/reads_trimming_report
#fi

total=$( grep "Total reads processed" ${output_report}/reads_trimming_report | cut -d":" -f2 | sed 's/^[ ]*//' | tr -d ',' )
#out=$( cat ${output_trimmed}/TrimmedReads.fq | wc -l )
#written=$(( written/4 ))
written=$( grep "Reads written" ${output_report}/reads_trimming_report | cut -d":" -f2 | sed 's/^[ ]*//' | cut -d" " -f1 | tr -d ',' )
discarded=$((total-written))

if [ -e ${output_trimmed}/unpairedReads.fq.gz ] 
then
  fwd=$( zcat ${output_trimmed}/unpairedReads.fq.gz | wc -l )
  rev=$( zcat ${output_trimmed}/unpairedMates.fq.gz | wc -l )
  f=$(( fwd/4 ))
  r=$(( rev/4 ))
  echo -e "Forward_Only_Surviving\tReverse_Only_Surviving" > $temp_folder/tmp2
  echo -e $f"\t"$r >> $temp_folder/tmp2
fi
if [ -e ${output_trimmed}/unpairedReads.fq ] 
then
  fwd=$( cat ${output_trimmed}/unpairedReads.fq | wc -l )
  rev=$( cat ${output_trimmed}/unpairedMates.fq | wc -l )
  f=$(( fwd/4 ))
  r=$(( rev/4 ))
  echo -e "Forward_Only_Surviving\tReverse_Only_Surviving" > $temp_folder/tmp2
  echo -e $f"\t"$r >> $temp_folder/tmp2
fi

echo -e "Input_Reads\tOutput_Reads\tDiscarded" > $temp_folder/tmp1
echo -e $total"\t"$written"\t"$discarded >> $temp_folder/tmp1
if [ -e $temp_folder/tmp2 ]
then
  paste $temp_folder/tmp1 $temp_folder/tmp2 > ${output_stats}
else 
  mv $temp_folder/tmp1 ${output_stats}
fi

