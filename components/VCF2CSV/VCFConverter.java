import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.IndexFile;
import org.anduril.component.SkeletonComponent;
import org.anduril.asser.io.CSVParser;
import org.anduril.asser.io.CSVWriter;


public class VCFConverter extends SkeletonComponent {
	
	public static final String CHROM_COLUMN 	= "#CHROM";
	public static final String POS_COLUMN 		= "POS";
	public static final String ID_COLUMN 		= "ID";
	public static final String REF_COLUMN 		= "REF";
	public static final String ALT_COLUMN 		= "ALT";
	public static final String FORMAT_COLUMN 	= "FORMAT";
	public static final String QUAL_COLUMN 		= "QUAL";
	public static final String FILTER_COLUMN 	= "FILTER";
	public static final String INFO_COLUMN 		= "INFO";

	private static final int HEADER_SIZE 		= 9;
	private static final int STANDARD_COLS		= 11;
	private static final int SAMPLE_COLS		= 4;
	

	private void vcf2csv(File vcfFile, 
					File regionFile, 
					File sampleDir, 
					IndexFile ifile,
					Map<String, String> infoCols, 
					Map<String, String> gtCols,
					boolean onlyAnnotations,
					boolean useIds) throws FileNotFoundException, IOException{
		
		
		BufferedReader vcf = new BufferedReader(new FileReader(vcfFile));
		
		String [] regCols = new String[STANDARD_COLS + infoCols.size()];
		
		regCols[0] = "Id"; 
		regCols[1] = "Chromosome"; 
		regCols[2] = "Start"; 
		regCols[3] = "Length"; 
		regCols[4] = "Strand"; 
		regCols[5] = "Quality";
		regCols[6] = "Filter";
		regCols[7] = "Reference"; 
		regCols[8] = "Alleles"; 
		regCols[9] = "NSamples";
		regCols[10] = "AlleleCount";
		
		int i = STANDARD_COLS;
		for(String key :  infoCols.keySet()){
			regCols[i] = key;
			i ++;
		}
		CSVWriter dnaRegion = new CSVWriter(regCols, regionFile);
		
		String [] sampleColumns = new String[SAMPLE_COLS + gtCols.size()];
		sampleColumns[0] = "Id";
		sampleColumns[1] = "Allele1";
		sampleColumns[2] = "Allele2";
		sampleColumns[3] = "Phased";
		i = SAMPLE_COLS;
		for(String key :  gtCols.keySet()){
			sampleColumns[i] = key;
			i ++;
		}
		
		String line = vcf.readLine();

		while(line != null && line.substring(0, 2).equals("##")){
			line = vcf.readLine();
		}
		
		List<String> headings = Arrays.asList(line.split("\t"));
		
		List<CSVWriter> samples = new ArrayList<CSVWriter>();
		
		for(i = HEADER_SIZE; i < headings.size(); i++){
			
			//create file named by the sample
			String filename = sampleDir.getAbsolutePath() + "/" + headings.get(i) + ".csv";
			String iFileName = headings.get(i) + ".csv";
			
			samples.add(new CSVWriter(sampleColumns, filename));
			
			ifile.add(headings.get(i), new File(iFileName));
		}
		
		i = 1;
		String lineId = "";
		
		line = vcf.readLine();
		
		while(line != null){
			String [] values = line.split("\t");
			
			//id for dna region
			if(values[headings.indexOf(ID_COLUMN)].equals(".") || !useIds)
				lineId = i+"";
			else
				lineId = values[headings.indexOf(ID_COLUMN)];
			
			dnaRegion.write(lineId);
			
			dnaRegion.write(values[headings.indexOf(CHROM_COLUMN)]);
			
			dnaRegion.write(values[headings.indexOf(POS_COLUMN)]);
			
			dnaRegion.write("0"); //length
			dnaRegion.write("NA"); //strand
			
			dnaRegion.write(values[headings.indexOf(QUAL_COLUMN)]);
			
			dnaRegion.write(values[headings.indexOf(FILTER_COLUMN)]);
			
			dnaRegion.write(values[headings.indexOf(REF_COLUMN)]);
			
			dnaRegion.write(values[headings.indexOf(ALT_COLUMN)]);
			
			//get allele indecies
			String [] alt = values[headings.indexOf(ALT_COLUMN)].split(",");
			String [] alleles = new String[alt.length +1];
			int [] aCount = new int[alleles.length];
			//zero points to reference
			alleles[0] = values[headings.indexOf(REF_COLUMN)];
			aCount[0] = 0;
			
			int k = 1;
			for(String s : alt){

				alleles[k] = s;
				aCount[k] = 0;
				
				k++;
			}
			
			
			int nSamples = 0;
			
			if(!onlyAnnotations){
				List<String> gtFormat =  Arrays.asList(values[headings.indexOf(FORMAT_COLUMN)].split(":"));
				int j = HEADER_SIZE;

				for(CSVWriter sampleOut : samples){

					String [] data = values[j].split(":");

					sampleOut.write(lineId);

					if(data[gtFormat.indexOf("GT")].matches("\\.([\\|/]\\.)?")){ //missing
						for(int m = 0; m < sampleColumns.length - 1; m++)
							sampleOut.write(null);
					}else{

						if(data[gtFormat.indexOf("GT")].matches(".*[\\|/].*")){ //diploid

							String a1 = data[gtFormat.indexOf("GT")].split("\\||/")[0];
							String a2 = data[gtFormat.indexOf("GT")].split("\\||/")[1];

							sampleOut.write(alleles[Integer.parseInt(a1)]);
							aCount[Integer.parseInt(a1)] ++;

							sampleOut.write(alleles[Integer.parseInt(a2)]);
							aCount[Integer.parseInt(a2)] ++;

							if(data[gtFormat.indexOf("GT")].matches(".*\\|.*"))
								sampleOut.write(true);
							else
								sampleOut.write(false);



						}else{ //haploid
							sampleOut.write(alleles[Integer.parseInt( data[gtFormat.indexOf("GT")] )]);
							aCount[Integer.parseInt( data[gtFormat.indexOf("GT")])] ++;

							sampleOut.write(null);

						}

						for(int m = SAMPLE_COLS; m < sampleColumns.length; m++){

							gtCols.get(sampleColumns[m]);
							if(gtFormat.contains(gtCols.get(sampleColumns[m])))
								sampleOut.write(data[gtFormat.indexOf(gtCols.get(sampleColumns[m]))]);
							else
								sampleOut.write(null);
						}


						nSamples ++;

					}

					j++;

				}
			}
			dnaRegion.write(nSamples);
			StringBuilder ac = new StringBuilder();

			for(int l = 0; l < aCount.length; l++){

				ac.append(aCount[l]);
				if(l < aCount.length -1)
					ac.append(",");
			}
			dnaRegion.write(ac.toString());

			//extract data from INFO
			for(int l = STANDARD_COLS; l < regCols.length; l ++){

				Pattern p = Pattern.compile("((.*;)||\\b)" + infoCols.get(regCols[l]) + "=([\\w!\"#\\$%\\&'\\(\\)\\*\\+,\\-\\./:\\<\\>\\?@\\[\\\\\\]\\^_`\\{\\|\\}~]*)((;.*)|\\b)", Pattern.CASE_INSENSITIVE);
				Matcher match = p.matcher(values[headings.indexOf(INFO_COLUMN)]);
				
				if(match.matches()){
					dnaRegion.write(match.group(3));
				}else{
					dnaRegion.write(null);					
				}
			}			
			line = vcf.readLine();
			
			i ++;
		}
		
		for(CSVWriter sampleOut : samples){
			sampleOut.close();
		}
		
		dnaRegion.close();
	}
	
	private void csv2vcf() throws FileNotFoundException, IOException{
		BufferedWriter vcf = new BufferedWriter(new FileWriter(""));
		IndexFile ifile = new IndexFile();
		
		Map<String, CSVParser> samples = new HashMap<String, CSVParser>();
		
		for(String name : ifile){
			ifile.getFile(name);
		}
	}

	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {
		
		cf.getOutput("samples").mkdir();
		File samples = cf.getOutputArrayIndex("samples");
		
		IndexFile ifile = new IndexFile();
		
		String [] infoData = cf.getParameter("infoData").split(",");
		Map<String, String> infoColumns = new HashMap<String, String>();
		
		for(String data : infoData){
			infoColumns.put(data.split("=")[1], data.split("=")[0]);
		}
		
		String [] sampleData = cf.getParameter("sampleData").split(",");
		Map<String, String> sampleColumns = new HashMap<String, String>();
		
		if(!cf.getParameter("sampleData").equals("")){
			for(String data : sampleData){
				sampleColumns.put(data.split("=")[1], data.split("=")[0]);
			}
		}
		boolean onlyAnnotations = cf.getBooleanParameter("onlyAnnotations");
		
		vcf2csv(cf.getInput("vcf"), 
				cf.getOutput("annotations"), 
				cf.getOutput("samples"), 
				ifile, 
				infoColumns, 
				sampleColumns,
				onlyAnnotations,
				cf.getBooleanParameter("useIds"));

		ifile.write(cf, "samples");
		
		return ErrorCode.OK;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		new VCFConverter().run(args);
	}
}
