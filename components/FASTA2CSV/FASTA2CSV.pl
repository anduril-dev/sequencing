#!usr/bin/perl

use strict;
use componentSkeleton;
use Encode::Unicode;

sub execute {
    my ($cf_ref) = @_;
    my $fasta = get_input($cf_ref,"in");
    my $output = get_output($cf_ref,"out");
    my $idCol = get_parameter($cf_ref,"idCol");
    my $seqCol = get_parameter($cf_ref,"seqCol");

    open(FILEOUT,">".$output);
    print FILEOUT $idCol."\t".$seqCol."\n";   
    open(FILEIN,$fasta);
    my $key = "";
    my $seq = "";
    while(<FILEIN>){
	chomp($_);
	if($_=~/^>/){
		if($key ne ""){
			print FILEOUT $key."\t".$seq."\n";
		}
		$_ =~ s/^>//;
		$key = $_;
		$seq = "";
	}else{
		$_ =~s/\n//g;
		$seq = $seq.$_;
	}
    }
    print FILEOUT $key."\t".$seq."\n";
    close(FILEIN);
    close(FILEOUT);
}

if(scalar @ARGV == 0){
    print "NO_COMMAND_FILE";
    exit;
}
my %cf = parse_command_file($ARGV[0]);
execute(\%cf);
