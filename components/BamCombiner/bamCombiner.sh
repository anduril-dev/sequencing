# running functions.sh 
set -e 
source "$ANDURIL_HOME/lang/bash/functions.sh"
export_command


if [ "${parameter_picard}" == "" ]
then
	parameter_picard="${PICARD_HOME}"   
fi

picard_Str="java -Xmx${parameter_memory} -jar ${parameter_picard}/picard.jar MergeSamFiles VALIDATION_STRINGENCY=SILENT SORT_ORDER=coordinate CREATE_INDEX=true O=${output_out}"

if [ "${parameter_options}" != "" ]
then
	picard_Str="${picard_Str} ${parameter_options}" 
fi

picard_Str="${picard_Str} $( paste -d ' '  <(getarrayfiles in)  | sed 's,^, I=,' | tr -d '\n' )"

echo "Picard MergeSamFiles command is ${picard_Str}"

$picard_Str 2>> "${logfile}"
if [ $? != 0 ]
then
	writeerror "MergeSamFiles failed"
	exit 1
fi
