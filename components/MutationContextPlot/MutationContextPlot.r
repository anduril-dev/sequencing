library(componentSkeleton)
library(SomaticSignatures)
library(VariantAnnotation)
library(ggplot2)

# Obvious improvements:
# - support directly using mutect 'context' input
# - support vcf input files
processSingle <- function(chr.col, locus.col, alt.col, ref.col, build, sample, cf, one.sample=TRUE) { 
  instanceName <- get.metadata(cf, 'instanceName')
  document.dir <- get.output(cf, 'plot')
  docName      <- file.path(document.dir, LATEX.DOCUMENT.FILE)

  chrs <- unique(chr.col)
  if (length(grep('chr', chrs)) == 0){
    chrs <- paste('chr', chrs, sep='')
    chr.col <- paste('chr', chr.col, sep='')
  } 

  if (one.sample){
    vr <- VRanges(seqnames = chr.col,
                  ranges = IRanges(locus.col, locus.col),
                  ref = ref.col, alt = alt.col)
  } else {
    vr <- VRanges(seqnames = chr.col,
                  ranges = IRanges(locus.col, locus.col),
                  ref = ref.col, alt = alt.col, 
                  sampleNames = sample,
                  study = sample)
  }
  vr   <- ucsc(vr)
  seql <- seqlengths(get(build))[1:24]
  seql <- seql[which(names(seql) %in% chrs)]

  for (i in 1:length(seqlengths(vr))){
    seqlengths(vr)[i] <- seql[which(names(seql) == names(seqlengths(vr))[i])]
  }

  vr <- trim(vr)
  motifs <- mutationContext(vr, get(build), unify = TRUE)
  if (one.sample){
    motifs$study <- sample
    plotMutationSpectrum(motifs, 'study')
    fig.name     <- sprintf('%s-mutationContext-%s.png', instanceName, sample)
    fig.capt <- paste('Mutation context for', sample, sep=' ')
    fig.path     <- file.path(document.dir, fig.name)
     ggsave(filename=fig.path, width=10, height=5)
  } else {
    plotMutationSpectrum(motifs, 'study', colorby='alteration')
    fig.name     <- sprintf('%s-mutationContext.png', instanceName)
    fig.capt <- 'Mutation context for the sample set'
    fig.path     <- file.path(document.dir, fig.name)
    ggsave(filename=fig.path, width=10, height=10)
  }
  cat( latex.figure(fig.name, caption = fig.capt, quote.capt = FALSE, image.scale=1),
       "\\clearpage",
       sep="\n", file=docName, append=TRUE)
}

execute <- function(cf) {

  build        <- get.parameter(cf, 'build')
  sampleCol    <- get.parameter(cf, 'sampleCol')
  s.by.s       <- get.parameter(cf, 'sampleBySample', type='boolean')
  dataCols     <- unlist(strsplit(get.parameter(cf, 'dataCols'), ','))
  document.dir <- get.output(cf, 'plot')
  dir.create(document.dir, recursive=TRUE)

  if (build == 'hg38'){
    library(BSgenome.Hsapiens.UCSC.hg38)
    build <- 'BSgenome.Hsapiens.UCSC.hg38'
  } else if (build == 'hg19'){
    library(BSgenome.Hsapiens.UCSC.hg19)
    build <- 'BSgenome.Hsapiens.UCSC.hg19'
  } else if (build == 'hg18') {
    library(BSgenome.Hsapiens.UCSC.hg18)
    build <- 'BSgenome.Hsapiens.UCSC.hg18'
  } else {
    write.error(cf, 'Unsupported genome build. Must be one of hg18, hg19, or hg38')
    return(PARAMETER_ERROR)
  }

  if (input.defined(cf, 'array')) {
    array <- Array.read(cf, 'array')
  } else {
    array <- NULL
  }

  if (input.defined(cf, 'in')) {
    csv <- CSV.read(get.input(cf, 'in'))
  } else {
    csv <- NULL
  }

  if (is.null(csv) && is.null(array)){
    write.error(cf, 'No inputs defined. Either array or in.csv must be defined. Quitting...')
    return(INVALID_INPUT)
  }

  if (is.null(array)){
    samples <- unique(csv[,sampleCol])
    if (s.by.s){
      for (sample in samples){
        s.csv <- csv[which(csv[,sampleCol] == sample),]
        processSingle(chr.col   = s.csv[,dataCols[1]],
                      locus.col = s.csv[,dataCols[2]],
                      alt.col   = s.csv[,dataCols[3]],
                      ref.col   = s.csv[,dataCols[4]],
                      sample    = sample,
                      build     = build, cf=cf)
      }
    } else {
        s.csv <- csv
        processSingle(chr.col   = s.csv[,dataCols[1]],
                      locus.col = s.csv[,dataCols[2]],
                      alt.col   = s.csv[,dataCols[3]],
                      ref.col   = s.csv[,dataCols[4]],
                      sample    = s.csv[,sampleCol],
                      build     = build, cf=cf, one.sample=s.by.s)
    }
  } else {
    for (i in 1:Array.size(array)) {
      sample <- Array.getKey(array, i)
      file  <- Array.getFile(array, sample)
      s.csv <- CSV.read(file)
      if (sampleCol!='') s.csv  <- s.csv[which(s.csv[,sampleCol] == sample),]
      processSingle(chr.col   = s.csv[,dataCols[1]],
                    locus.col = s.csv[,dataCols[2]],
                    alt.col   = s.csv[,dataCols[3]],
                    ref.col   = s.csv[,dataCols[4]],
                    sample    = sample,
                    build     = build, cf=cf)
      rm(s.csv)
    }
  }

}


main(execute)
