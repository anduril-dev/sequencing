# running functions.sh 
set -e 
source "$ANDURIL_HOME/lang/bash/functions.sh"
export_command


if [ "${parameter_picard}" == "" ]
then
	parameter_picard="$( getbundlepath sequencing )/lib/picard/" 
fi

picard_Str="java -Xmx${parameter_memory} -jar ${parameter_picard}/picard.jar MarkDuplicates VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true TMP_DIR=$( gettempdir ) O=${output_outBam} I=${input_in} METRICS_FILE=${output_outMetrics}"

if [ "${parameter_options}" != "" ]
then
	picard_Str="${picard_Str} ${parameter_options}" 
fi

if [ "${parameter_remove}" == "true" ]
then
	picard_Str="${picard_Str} REMOVE_DUPLICATES=true" 
fi


echo "Picard MarkDuplicates command is ${picard_Str}"

$picard_Str 2>> "${logfile}"
if [ $? != 0 ]
then
	writeerror "MarkDuplicates failed"
	exit 1
fi
