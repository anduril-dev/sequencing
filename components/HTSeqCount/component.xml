<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>HTSeqCount</name>
    <version>1.0</version>
    <doc> Quantify reads mapped to EnsemblID transcripts for a given sample. Requires <code>htseq-count</code> and <code>picard</code> in PATH. Similar to <code>HTSeqBam2Counts</code>, but developed for smallRNA, with more htseq-count parameters available, and with the option to use custom annotation files (e.g. known and novel mature miRNAs).
    </doc>
    <author email="katherine.icay@helsinki.fi">Katherine Icay</author>
    <category>Expression</category>
    <launcher type="bash">
        <argument name="file" value="htseq.sh" />
    </launcher>
    <requires type="DEB">python2.7-dev</requires>
    <requires type="python">HTSeq</requires>
    <requires>picard</requires>
    <requires name="installer" optional="false">
         <resource type="bash">install.sh</resource>
    </requires>
    <inputs>
        <input name="bam" type="BAM" optional="false">
            <doc>Sample file containing reads aligned to Ensembl transcripts.</doc>
        </input>
		<input name="annotation_mirbase" type="BinaryFile" optional="false">
            <doc>GFF/GFF3 file containing feature information on the alignment of the <code>bam</code> input. This can be the 'File' column of the <code>AnnotMirbase</code> output or a filtered Ensembl GFF file of ncRNAs.</doc>
        </input>
    </inputs>
    <outputs>
        <output name="counts" type="CSV">
            <doc>Quantified expression of reads in a sample.</doc>
        </output>      
    </outputs>
    <parameters>
        <parameter name="order" type="string" default="pos">
            <doc>Alignment can either be sorted by <code>name</code> or by <code>pos</code> (alignment position).</doc>
        </parameter>
		<parameter name="overlap_rule" type="string" default="intersection-nonempty">
            <doc>One of three decision-making modes used by <code>htseq-count</code> for counting reads. Other options are 'union' and 'intersection-strict.' See <a href="http://www-huber.embl.de/users/anders/HTSeq/doc/count.html" target="_BLANK">HTSeq</a> for more details.</doc>
        </parameter>
		<parameter name="mirbase_gff" type="boolean" default="true">
            <doc>Is the annotation file from mirbase (GFF3) or Ensembl (GFF)?</doc>
        </parameter>
		<parameter name="byStrand" type="string" default="no">
            <doc>Should expression be strand-specific? If "yes", sequences were processed with strand information and reads will be counted only if they overlap the same strand and region as an annotation feature. If "no", reads are counted whether it is mapped to the same or the opposite strand as the feature.</doc>
        </parameter>
        <parameter name="featureType" type="string" default="miRNA">
            <doc> Feature in 3rd column of GFF/GFF3 file to quantify reads to, all other types ignored. For miRBase GFF3, this is either (mature) "miRNA" or "miRNA_primary_transcript". For Ensembl transcripts, this is usually "exon". </doc>
        </parameter>
        <parameter name="featureID" type="string" default="Name">
            <doc>GFF/GFF3 attribute to use as feature ID to count. Counts will be combined for rows in the GFF/GFF3 file sharing the same feature ID. Default is to quantify reads aligned to mature sequences of a miRBase GFF3 file. To quantify on a hairpin/transcript level of a miRBase GFF3 file, use <code>'type=miRNA_primary_transcript'</code>.</doc>
        </parameter>
		<parameter name="picardPath" type="string" default="../../lib/picard">
            <doc>Path to picard installation.</doc>
        </parameter>
    </parameters>
</component>
