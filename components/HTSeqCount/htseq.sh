# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/lang/bash/functions.sh"
set -e

# use these functions to read command file
order=$( getparameter order )
bam=$( getinput bam )
annotation_mirbase=$( getinput annotation_mirbase )
counts=$( getoutput counts )
overlap_rule=$( getparameter overlap_rule )
mirbase_gff=$( getparameter mirbase_gff )
byStrand=$( getparameter byStrand )
featureType=$( getparameter featureType )
featureID=$( getparameter featureID )
picard=$( getparameter picardPath )

# Create a temporary directory
tempdir=$( gettempdir )

# Check validity of string parameters
if [ "$overlap_rule" != "union" ] && [ "$overlap_rule" != "intersection-strict" ] && [ "$overlap_rule" != "intersection-nonempty" ]
then
    writeerror "Invalid HTSeq parameter:'$overlap_rule'. Please check HTSeq documentation for valid inputs."
    exit 1
fi
if [ "$byStrand" != "yes" ] && [ "$byStrand" != "no" ] && [ "$byStrand" != "reverse" ]
then
    writeerror "Invalid HTSeq parameter:'$byStrand'. Please check HTSeq documentation for valid inputs."
    exit 1
fi

# Ensure BAM is sorted
samtools sort $bam OUTPUT="$tempdir"/sorted TMP_DIR="$tempdir"
# Convert BAM to SAM
java -Xmx4g -jar "${picard}/picard.jar" SamFormatConverter VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true CREATE_MD5_FILE=true INPUT="$tempdir"/sorted.bam OUTPUT="$tempdir"/aligned_sorted.sam TMP_DIR="$tempdir"

# Reformat GFF file if needed (e.g. for miRbase)
if [ "$mirbase_gff" = "true" ]
then
    if [ "$featureType" != "exon" ]
    then
        sed 's/chr//g' $annotation_mirbase > "$tempdir"/_temp.gff3
        sed -i 's/\=/ "/g' "$tempdir"/_temp.gff3
        sed -i 's/\;/";/g' "$tempdir"/_temp.gff3
        sed 's/$/"/' "$tempdir"/_temp.gff3 > "$tempdir"/clean.gff3
    else
        cp $annotation_mirbase "$tempdir"/clean.gff3
    fi
    htseq-count --mode=$overlap_rule --stranded=$byStrand --order=$order --type=$featureType --quiet --idattr=$featureID "$tempdir"/aligned_sorted.sam "$tempdir"/clean.gff3 > $counts

else # map to whole genome
   htseq-count --mode=$overlap_rule --stranded=$byStrand --order=$order --type=$featureType --idattr=$featureID "$tempdir"/aligned_sorted.sam $annotation_mirbase > $counts
fi

#if [ -f $fq ]
counted=$(wc -l < $counts)
if [ $counted -gt 1 ]
then 
    if [ -f $fq ]
    then
        writelog "HTSeq-count complete."
    else 
        writeerror "Missing output file!"
        exit 1
    fi
else
    writeerror "counts.csv file is empty."
    exit 1
fi
