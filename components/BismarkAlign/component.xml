<?xml version="1.1" encoding="UTF-8" standalone="yes"?>
<component>
    <name>BismarkAlign</name>
    <version>1.0</version>
    <doc> Performs alignment of bisulfite sequencing data in addition to deduplication
    and methylation extraction using <code>Bismark Bisulfite Mapper</code>
    </doc>
    <author email="Amjad.Alkodsi@Helsinki.FI">Amjad Alkodsi</author>
    <category>Alignment</category>
    <category>DNA Methylation</category>
    <launcher type="bash">
        <argument name="file" value="BismarkAlign.sh" />
    </launcher>
    <requires URL="http://www.bioinformatics.babraham.ac.uk/projects/bismark/">bismark</requires>
    <requires URL="http://samtools.sourceforge.net/">samtools</requires>
    <requires URL="http://bowtie-bio.sourceforge.net/index.shtml">bowtie</requires>
    <requires URL="http://bowtie-bio.sourceforge.net/bowtie2/index.shtml">bowtie2</requires>
    <requires name="installer" optional="false">
         <resource type="bash">install.sh</resource>
    </requires>
    <inputs>
        <input name="reads" type="SequenceSet" >
            <doc>Input reads in fastq or fasta format</doc>
        </input>
        <input name="refGenome" type="BinaryFolder">
            <doc>The folder containing the converted reference genome. The conversion can be
            performed using <code>bismark_genome_preparation</code> tool. 
            </doc>
        </input>
        <input name="mates" type="SequenceSet" optional="true">
            <doc>Input mates in fastq or fasta format</doc>
        </input>
    </inputs>
    <outputs>
        <output name="methylationCalls" type="CSV">
            <doc>Methylation calls in CpG context. For other contexts,
            check bismark documentation and include appropriate parameters in 
            <code>extractExtra</code> parameter. The CSV contains the following columns:
            chr, position, strand, coverage, MethC.</doc>            
        </output>
        
        <output name="bedGraph" type="BED">
            <doc>BedGraph output for methylation calls in CpG context. For other contexts,
            check bismark documentation and include appropriate parameters in 
            <code>extractExtra</code> parameter.</doc>
        </output>
        
        <output name="alignment" type="BAM">
            <doc>Aligned reads in BAM format. If the parameter <code>dupRemoval</code> is set to true,
            this output will be deduplicated.</doc>
        </output>
        <output name="Mbias" type="BinaryFile">
            <doc>M-bias file reporting methylation percentage at different positions of reads in different contexts.</doc>
        </output>
        <output name="report" type="HTMLFile">
            <doc>The report of alignment, methylation extraction and deduplication (if performed).</doc>
        </output>
        <output name="analysis" type="BinaryFolder">
            <doc>Folder containing all produced files other than outputs. 
            </doc>
        </output>
     </outputs>
    <parameters>
        <parameter name="inputFormat" type="string" default="fastq">
            <doc>Input format, fastq or fasta.</doc>
        </parameter>
        <parameter name="aligner" type="string" default="bowtie2">
            <doc>The aligner to be used, can be "bowtie" or "bowtie2". The input <code>refGenome</code> should 
            be constructed with the same aligner.
            </doc>
        </parameter>
        <parameter name="alignExtra" type="string" default="">
            <doc>Extra flags to be passed to the align function. Check bisrmak documentation.</doc>
        </parameter>
        <parameter name="paired" type="boolean" default="true">
            <doc>Set true for paired-end reads.</doc>
        </parameter>
        <parameter name="extract" type="boolean" default="true">
            <doc>Whether to extract methylation from bam file.</doc>
        </parameter>
        <parameter name="directional" type="boolean" default="true">
            <doc>Set true for directional BS-Seq libraries. In directional alignment, reads will
            be aligned to the original top and bottom strands. In non-directional alignment, reads
            will be aligned to original top and bottom strands in addition to the strands complementary
            to original top and bottom strands.</doc>
        </parameter>
        <parameter name="dupRemoval" type="boolean" default="true">
            <doc>Set true for performing duplicate removal. Do not use with RRBS data!</doc>
        </parameter>
        <parameter name="deleteNonCpG" type="boolean" default="true">
            <doc>Set true for deleting non-CpG context files.</doc>
        </parameter>
        <parameter name="extractExtra" type="string" default="">
            <doc>Extra flags to be passed to the methylation extraction function. Check bismark documentation.</doc>
        </parameter>
		<parameter name="ignoreR1" type="int" default="1">
            <doc>Ignore the first "number" of bp from the 5’ end of reads when 
            processing the methylation call string. </doc>
        </parameter>
		<parameter name="ignoreR2" type="int" default="2">
            <doc>Ignore the first "number" of bp from the 5’ end of mates when 
            processing the methylation call string. (only with paired end). </doc>
        </parameter>
    </parameters>
</component>
