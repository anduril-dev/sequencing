#!/bin/bash

# set up Anduril stuff
source "$ANDURIL_HOME/lang/bash/functions.sh" "$1"

## build up the arguments

# set up an array
args=()

# list the standard parameters and their switches
declare -A params
params=([algorithm]='a' [quantile]='q')

# add the standard parameters
for param in "${!params[@]}"; do
	key="${params[$param]}"
	arg="$(getparameter "$param")"
	[ -n "$arg" ] && args+=("-$key$arg")
done

# scale
arg="$(getparameter 'use_unity')"
[ "$arg" = "true" ] && args+=("-n$(getparameter 'unity')")

# get number of threads
threds="$(getparameter 'threads')"

# get input files
b_expr_fn="$(getinput 'in')"
s_expr_fn="$(getinput 'scExpr')"
s_weights_fn="$(getinput 'scWeights')"
s_gains_fn="$(getinput 'scGains')"

# get output files
Z_fn="$(getoutput 'out')"
W_fn="$(getoutput 'bulkWeights')"
G_fn="$(getoutput 'bulkGains')"

## execute

# get path to tools
rsm_path="$(dirname "$0")/../../lib/rsm"

# strip header
strip_header() {
	noheader "$1" | cut -f2-
}

# restore header
restore_header() {
	"$rsm_path/tsv_meta_restore.py" "$@"
}

# set up a stack of temporary files
tmp_files=()

# compute single-cell gains if missing
if [ -z "$s_gains_fn" ]; then
	# dump message 
	writelog 'estimating single-cell gains..'

	# get file
	s_gains_fn="$(mktemp -p "$(gettempdir)")"
	tmp_files+=("$s_gains_fn")

	# estimate gain
	"$rsm_path/rsm-gain" -G"$s_gains_fn" -n1. <( strip_header "$s_expr_fn" )
fi

# run decomposition
NUMBER_OF_PROCESSORS="$threads" "$rsm_path/rsm-decom-ref" "${args[@]}" \
	<( strip_header "$b_expr_fn" ) \
	-w <( strip_header "$s_weights_fn" ) \
	<( strip_header "$s_expr_fn" ) \
	-Z >( restore_header -Z '/dev/stdin' "$b_expr_fn" "$s_weights_fn" >"$Z_fn" ) \
	-W >( restore_header -W '/dev/stdin' "$b_expr_fn" "$s_weights_fn" >"$W_fn" ) \
	-G >( restore_header -G '/dev/stdin' "$b_expr_fn" "$s_weights_fn" >"$G_fn" )

# clean up temporary files
for file in "${tmp_files[@]}"; do
	rm "$file"
done
