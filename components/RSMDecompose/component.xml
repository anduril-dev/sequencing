<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
	<name>RSMDecompose</name>
	<version>0.9</version>
	<doc>Decompose bulk RNA-seq data using single-cell references</doc>
	<author email="antti.e.hakkinen@helsinki.fi">Antti Hakkinen</author>
	<category>Expression</category>
	<launcher type="bash">
		<argument name="file" value="rsm-decom-ref.bash" />
	</launcher>
	<requires name="installer">rsm
		<resource type="bash">install.bash</resource>
	</requires>
	<inputs>
		<input name="in" type="CSV">
			<doc>Expression matrix for the bulk data. Rows represent genes and columns represent samples.</doc>
		</input>
		<input name="scExpr" type="CSV">
			<doc>Expression matrix for single-cell data. Rows are genes and columns are samples. The gene names must match that of the bulk expression matrix and be in the same order.</doc>
		</input>
		<input name="scWeights" type="CSV">
			<doc>Composition matrix for single-cell data. Rows are samples and columns are cell types. The samples must match that of the single-cell expression matrix and be in the same order.</doc>  
		</input>
		<input name="scGains" type="CSV" optional="true">
			<doc>Gain vector for single-cell data. The single row contains the scaling factors and columns are samples. The samples must match that of the single-cell expression matrix and be in the same order. If the gains are not provided, they are automatically estimated. Use the RSMGain component to customize the procedure.</doc>
		</input>
	</inputs>
	<outputs>
		<output name="out" type="CSV">
			<doc>Decomposed bulk expression matrix. Rows are genes and columns are sample-cell type combinations.</doc>
		</output>
		<output name="bulkWeights" type="CSV">
			<doc>Composition matrix of the bulk data. Rows are samples and columns are cell types.</doc>
		</output>
		<output name="bulkGains" type="CSV">
			<doc>Gain vector for bulk data. The single row contains the scaling factors and columns are samples.</doc>
		</output>
	</outputs>
	  <parameters>
		<parameter name="maxIters" type="int" default="1000">
			<doc>Maximum number of EM iterations (default: 1000). A larger number can give more accurate estimates, but takes linearly more time.</doc>
		</parameter>
		<parameter name="minDelta" type="float" default="1.49e-8">
			<doc>Minimum objective change to detect a stall (default: 1.49e-8). A smaller number may prevent early exits.</doc>
		</parameter>
		<parameter name="threads" type="int" default="1">
			<doc>Maximum number of threads to use for processing (default: 1).</doc>
		</parameter>
	</parameters>
</component>
