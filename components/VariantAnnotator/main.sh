#!/bin/bash

# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/lang/bash/functions.sh"

export_command

set -e

# ------------------------------------------------------------
# Establish outputs
# ------------------------------------------------------------

mkdir ${output_calls}
mkdir ${output_log}
mkdir ${output_raw}
echo -e '"Key"'"\t"'"File"' >> "${output_calls}/_index"
echo -e '"Key"'"\t"'"File"' >> "${output_log}/_index"
echo -e '"Key"'"\t"'"File"' >> "${output_raw}/_index"

cd ${output_calls}/..


# ------------------------------------------------------------
# Use environment variables as defaults.
# ------------------------------------------------------------

if [[ ${parameter_annovar} == "" ]]; then
    annovar="${ANNOVAR_HOME}"
else
    annovar="${parameter_annovar}"
fi

if [[ "${input_reference}" != "" ]]; then
    reference=${input_reference}
else
    reference=${parameter_referenceDir}
fi

# Depending on the number of additional fields in the result file,
# "cut -f 3-" can be used to retrieve the original VCF rows

annovarInput="annovarFormatInput.txt"
if [[ ${parameter_convertFromType} != "none" ]]; then
    ${annovar}/convert2annovar.pl -includeinfo -format ${parameter_convertFromType} "${input_variantQuery}" > "${annovarInput}"
else
    ln -s "${input_variantQuery}" "${annovarInput}"
fi

query="${annovarInput}"

convertToCSV=true # // Not needed for all methods
options="${parameter_options}"

if [[ ${parameter_annotator} == "annovar" ]]; then
  # Default mode of running VariantAnnotator
    if [[ "${parameter_options}" == "defaults" ]]; then
        options=""
    fi

    ${annovar}/annotate_variation.pl ${query} "${reference}" --buildver ${parameter_index} --outfile ${output_calls}/results $options
elif [[ ${parameter_annotator} == "variant_reduction" ]] ; then
    # http://www.openbioinformatics.org/annovar/annovar_accessary.html
    if [[ "${parameter_options}" == "defaults" ]]; then
        options="--protocol nonsyn_splicing,esp5400_aa,recessive --operation g,f,m"
    fi

    ${annovar}/variants_reduction.pl "${query}" "${reference}" --buildver ${parameter_index}  --outfile ${output_calls}/results $options
elif [[ ${parameter_annotator} == "summarize" ]] ; then
    if [[ "${parameter_options}" == "defaults" ]]; then
        options="--verdbsnp 135"
    fi

    ${annovar}/summarize_annovar.pl "${query}" "${reference}" --buildver ${parameter_index}  --outfile ${output_calls}/results $options
    convertToCSV=false
elif [[ ${parameter_annotator} == "table" ]] ; then
    if [[ "${parameter_options}" == "defaults" ]]; then
        # From quickstart example
        options="-protocol refGene,phastConsElements46way,genomicSuperDups,esp6500si_all,1000g2012apr_all,snp135,ljb2_all -operation g,r,r,f,f,f,f"
    fi

    ${annovar}/table_annovar.pl --remove -nastring NA "${query}" "${reference}" --buildver ${parameter_index}  --outfile ${output_calls}/results $options

    convertToCSV=false
fi

# Produces three arrays - one with output as CSV and one with the corresponding log files, one with extra output

# Move log files out of the way
for f in $(ls ${output_calls}/results* | grep \.log$ ); do
    fname=$(basename ${f})
    fname=$(echo $fname | sed -e 's/results.//')

    mv ${f} ${output_log}/
    echo -e $fname"\t"$(basename $f) >> ${output_log}/_index
done

if $convertToCSV ; then
    echo "Converting to CSV."
else
    # Move non-CSV files i.e. raw output out of the way
    for f in $(ls ${output_calls}/results* | grep -v \.csv$ ); do
        fname=$(basename ${f})
        fname=$(echo $fname | sed -e 's/results.//')
        mv ${f} ${output_raw}/
        echo -e $fname"\t"$(basename $f) >> ${output_raw}/_index
    done
fi

for f in $(ls ${output_calls}/results* | grep -v \.log$ ); do
    fname=$(basename ${f})
    if $convertToCSV ; then
        # Column names are from VCF spec

        num_fields=$(( $(head -1 ${f} | awk -F'\t' ' { print NF }') - 8 ))

        # The last fields are same as in VCF when --includeinfo is used in convert2annovar.pl
        # Before that, something else is happening.
        header='"CHROM"\t"POS"\t"ID"\t"REF"\t"ALT"\t"QUAL"\t"FILTER"\t"INFO"'
        prefix=""
        for num in $(seq 1 $num_fields); do
            prefix="${prefix}Annotation$num\t"
        done
        header=${prefix}${header}

        cat <(echo -e $header) ${f} > ${f}.tmp
        mv ${f}.tmp ${f}
    else
        # The files are already CSV but they are comma-separated and they lack VCF fields
        # sed only operates on the header line
        perl -pe 'while (s/(,"[^"]+),/\1<COMMA>/g) {1}; s/"//g; s/,/\t/g; s/<COMMA>/,/g' < ${f} | sed -e '/1/{s/^/"/;s/\t/"\t"/g;s/Otherinfo/CHROM"\t"POS"\t"ID"\t"REF"\t"ALT"\t"QUAL"\t"FILTER"\t"INFO"/;:a;n;ba}' > ${f}.tmp
        mv ${f}.tmp ${f}
    fi
    fname=$(echo $fname | sed -e 's/results.//')
    echo -e $fname"\t"$(basename $f) >> ${output_calls}/_index
done

#python "$DIR"/count_alts.py annovarized_info.txt > alts.txt
#paste annovarized.txt.${BUILDVER}_multianno.txt alts.txt > "$OUT"

