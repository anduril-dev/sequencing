#!/usr/bin/env python

#
# Reclustering tool for PyClone 0.13.
#
# This file is in public domain, but the restrictions in the PyClone license
# may apply. For details, see:
#   https://bitbucket.org/aroth85/pyclone/src/tip/LICENSE.txt
#

import glob, os.path, re

import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt

import numpy as np, pandas as pd
import scipy.stats

import yaml

import pyclone.paths, pyclone.post_process.clusters, pyclone.trace
import pydp.cluster, pydp.data, pydp.utils

def load_config(config_file, trace_dir = None, samples_files = None):
	''' Load configuration with path customizations '''

	# load config
	config = pyclone.paths.load_config(config_file)

	# override trace directory?
	if trace_dir:
		config['trace_dir'] = trace_dir

	# override sample files?
	if samples_files:
		for sample in config['samples'].keys():
			config['samples'][sample]['mutations_file'] = samples_files[sample]

	return config

def get_samples_files(samples_dir):
	''' Find sample files in a directory '''

	if not samples_dir:
		return None

	return dict((re.sub(r'\.yaml$', '', os.path.basename(fn)), fn)
		for fn in glob.glob(os.path.join(samples_dir, '*.yaml')))

def compute_create_lr(labels, sim_mat):
	''' Compute likelihood ratio for cluster creation '''

	# eliminate isolated points: 
	 # null ~ LL w/o singleton, alt ~ LL including singleton
	s = np.sum(sim_mat, axis = 1)
	ll1 = np.bincount(labels, weights = s - 1) / np.bincount(labels, weights = s)

	return ll1[labels]

def compute_assign_lr(labels, sim_mat):
	''' Compute likelihood ratio for cluster assigment '''

	# eliminate points where assignment is not clear
	 # null ~ LL w/o map cluster, alt ~ LL including map estimate
	s = np.sum(sim_mat, axis = 1)
	ll2 = np.sum(sim_mat * (labels[:, None] != labels), axis = 0) / (s-1)

	return np.maximum(1 - ll2, 0)

def _sqfsubs(m):
	''' Computes subscripts to "squareform" vector. '''
	jumps = np.zeros(m*(m-1)//2, dtype = np.intp)
	jumps[np.cumsum(np.arange(m-1, 1, -1))] = np.arange(m-1, 1, -1)
	ii = np.cumsum(jumps > 0)
	jj = np.arange(m*(m-1)//2) - np.cumsum(jumps) + ii + 1
	return ii, jj

def _sqfinds(m, ii, jj):
	''' Computes indices to "squareform" vector. '''
	ii, jj = np.minimum(ii, jj), np.maximum(ii, jj)
	return ii * (m+(m-ii)-1)//2 + (jj-ii) - 1

def link_av_minsize_fast(D, minsize):
	if minsize == 1:
		return scipy.cluster.hierarchy.average(D)  # use native version from scipy
	else:
		return link_av_minsize(D, minsize) 

def link_av_minsize(D, minsize = 1):
	''' Construct UPGMA trees from trees of at least minsize. This is useful for
		obtaining a clustering with >=minsize elements in each cluster if possible.''' 

	# normalize distances to vector form
	if D.ndim > 1:
		D = scipy.spatial.distance.squareform(D)

	# figure out dimension
	m = np.intp(np.ceil(np.sqrt(2*D.size)))  # = (1+sqrt(1+8*y))/2 for valid numbers

	# cluster labels in D
	c = np.arange(m, dtype=np.intp)

	# linkage information goes here
	Z = np.zeros((m-1, 4))

	# get cluster sizes
	def get_n(cc):
		nn = np.ones(cc.shape)
		nn[~(cc < m)] = Z[cc[~(cc < m)]-m, 3]
		return nn

	# loop
	for u in range(m-1):
		# dimension of D @ this iteration
		m1 = m - u

		# compute indices of the pairs in D
		ii, jj = _sqfsubs(m1)

		# compute number of pairs
		n1, n2 = get_n(c[ii]), get_n(c[jj])
		n12 = n1 * n2
		cost = D / n12  # UPGMA cost

		# high priority merges pending?
		prio = (n1 < minsize) & (n2 < minsize)
		has_prio = np.any(prio)
		if has_prio:
			cost[~prio] = np.inf

		# select pair to merge
		kmin = np.argmin(cost)
		i, j = ii[kmin], jj[kmin]
		cost_kmin = cost[kmin]
		if has_prio:
			cost_kmin = 0. # pretend the cost is zero

		# record cluster
		Z[u, :] = ( c[i], c[j], cost_kmin, n1[kmin] + n2[kmin] )
# now, we must permute i,j out of D,c
		perm = np.arange(m1)
		perm[j], perm[-1] = perm[-1], perm[j]  # swap hole to end
		perm[j] = perm[i]  # permute i,j to same

		# permute D
		mask = perm[ii] != perm[jj]
		D = np.bincount(_sqfinds(m1-1, perm[ii[mask]], perm[jj[mask]]), weights = D[mask])
		# permute c
		c[perm] = c[:]
		c[perm[i]] = m + u

	# sort indices
	Z[:, (0, 1)] = np.sort(Z[:, (0, 1)], axis=1)

	return Z

def cluster_many_with_mpear(sim_mat, groups, min_cluster_size, max_clusters):
	''' This is like PyDP's cluster_with_mpear(), but does not allow
	  cross-group clusters to form '''

	dist_mat = 1. - sim_mat

	for _, ii in groups.items():
		mask = np.ones(len(dist_mat), dtype = bool)
		mask[ii] = False

		for i in ii:
			dist_mat[mask, i] = 1e100 # NB. should be inf, but average() pukes
			dist_mat[i, mask] = 1e100

	Z = link_av_minsize_fast(
		scipy.spatial.distance.squareform(dist_mat), minsize = min_cluster_size)

	best_cluster_labels = None
	max_pear = 0

	if max_clusters is None or max_clusters > len(sim_mat):
		max_clusters = len(sim_mat)

	for i in range(len(groups), max_clusters):
		cluster_labels = pydp.cluster._get_flat_clustering(Z, i)
		pear = pydp.cluster._compute_mpear(cluster_labels, sim_mat)

		# NB. older PyClone use 1-based labels, newer 0-based
		 # hence, pack the labels before bincount, but keep them for output
		_, cluster_sizes = np.unique(cluster_labels, return_counts = True)

		if not np.all(cluster_sizes >= min_cluster_size):
			# stop here, min_cluster_size constraint cannot be satisfied
			break

		if pear > max_pear:
			best_cluster_labels = cluster_labels
			max_pear = pear

	return best_cluster_labels

def normalize_labels(labels):
	''' Rewrites & packs labels into minimal ordered values '''

	# NB. this returns values in [0, n) which is pythonic 
	 # older PyClone use 1..n and PyCloneRC uses 1..n and 0 for outliers

	_, index, inverse = np.unique(labels,
		return_index = True, return_inverse = True)

	inv_perm = np.zeros(len(index), dtype = index.dtype)
	inv_perm[np.argsort(index)] = np.arange(len(index))

	return inv_perm[inverse]

def get_original_labels(config, options):
	''' Get original label assignment & similarity matrix '''

	# get trace
	labels_trace = pyclone.trace.load_cluster_labels_trace(
		pyclone.paths.get_labels_trace_file(config), options['burnin'],
			options['thin'])

	# move variants with fixed VAF into their separate subspaces such 
	 # that they won't conflict with other variants
	 # conflict with other variants
	shifts = np.zeros(labels_trace.shape[1], dtype = int)

	if options['inf_coverage'] < float('inf'):
		for index, sample_id in enumerate(config['samples']):
			# get VAF & coverage
			vaf_data = _load_vafs_and_coverage(
				config['samples'][sample_id]['mutations_file'])

			# find columns in labels_trace where these variants live
			inds, = np.nonzero(vaf_data['coverage'] >= options['inf_coverage'])
			colindices = [labels_trace.columns.get_loc(key) for key in vaf_data['mutation_id'][inds]]

			# split into separate subspace 
			shifts[colindices] += shifts[colindices] + (2 ** index)

	# group each column in labels_trace
	groups = {}
	for i, s in enumerate(shifts):
		if s in groups:
			groups[s].append(i)
		else:
			groups[s] = [i]

	# get similarity matrix
	X = labels_trace.values
	sim_mat = 1 - pydp.cluster.squareform(pydp.cluster.pdist(np.array(X).T,
		metric = 'hamming'))

	# prune similarity matrix for cross-space interactions
	for _, ii in groups.items():
		mask = np.ones(X.shape[1], dtype = bool)
		mask[ii] = False

		for i in ii:
			sim_mat[i, mask] = 0.
			sim_mat[mask, i] = 0.

	# perform clustering
	labels = cluster_many_with_mpear(sim_mat, groups, options['min_cluster_size'], options['max_clusters'])

	# normalize labels to 1:n and rewrite them in deterministic order
	 # keep first label as 1, as we use 0 for outliers
	labels = normalize_labels(labels) + 1

	return labels, labels_trace.columns, sim_mat

def filter_labels(labels, sim_mat, options):
	''' Filter labels '''

	# create a copy of the array
	labels = labels.copy()

	# we use cluster 0 for outliers
	invalid_label = 0

	# zero out some of the labels
	labels[compute_create_lr(labels, sim_mat) < options['min_create_lr']] = invalid_label
	labels[compute_assign_lr(labels, sim_mat) < options['min_assign_lr']] = invalid_label

	return labels

def _cluster_traces(config, options):
	''' Cluster traces & filter the clustering '''

	# get labels & similarity matrix
	labels, colnames, sim_mat = get_original_labels(config, options)

	# filter
	labels = filter_labels(labels, sim_mat, options)

	# propagate metadata
	labels = pd.Series(labels, index = colnames)
	labels = labels.reset_index()
	labels.columns = 'mutation_id', 'cluster_id'
    
	return labels

def pmf_trimmed_stat(x, p, q):
	''' Compute trimmed mean & std from a PMF '''

	#
	# TODO: this could be a bit more sophisticated
	#  but ideally, I don't want to do this from a pmf anyway..
	# 

	# get CDF
	c = np.cumsum(p)

	# find ranges with mass q
	ranges = -1 * np.ones((len(c), 2), dtype = int)
	t = 0
	for j in xrange(len(c)):

		# NOTE: use mass >= q, not mass ~ q
		 # so q = 1 uses all data etc.

		while t < len(c) and c[t] < c[j] - p[j] + q:
			t = t + 1
		if t < len(c):
			ranges[j, :] = (j, t)

	# no results..?
	if not np.sum(ranges[:, 0] > 0) > 0:
		ranges = np.array(((0, len(c) - 1),))

	# compute moments
	l, r = ranges[ranges[:, 0] > -1, 0], ranges[ranges[:, 1] > -1, 1]
	m0 = c[r] - (( c[l]-p[l] ))
	t = x * p
	s = np.cumsum(t)
	m1 = s[r] - (( s[l]-t[l] ))
	t = x * t
	s = np.cumsum(t)
	m2 = s[r] - (( s[l]-t[l] ))

	# find minimum
	obj = (m2 - m1 * (m1 / m0)) / m0
	imin = np.argmin(obj)

	# estimate mean & variance
	est_m = m1[imin] / m0[imin]
	est_v = np.maximum(obj[imin], 0)

	# find scale for std
	 # this assumes the data is normal
	k = -scipy.stats.norm.ppf((1 - q) / 2)
	t = scipy.stats.norm.pdf(k)
	coe = 1 / (( 1 - 2 * k * t / q ))
	if not t > 0:
		coe = 1

	return est_m, coe * est_v

def _compute_cluster_summary(config, options):
	''' Compute aggregate cluster statistics '''

	# get cluster data
	df = pyclone.post_process.clusters.load_table(config, **options)
	df = df.set_index(['sample_id', 'cluster_id', 'size'])

	# get densities
	x = df.columns.astype(float).values[np.newaxis, :]
	p = np.exp(df.values)

	# estimate moments
	m_1 = np.nan * np.ones((p.shape[0], 1))
	var = np.nan * np.ones((p.shape[0], 1))
	for i in xrange(p.shape[0]):
		m_1[i], var[i] = pmf_trimmed_stat(x.flatten(), p[i, :], 1 - options['trim'])
	std = np.sqrt(var)
    
	# create output
	out_df = pd.concat([pd.DataFrame(m_1, index = df.index),
		pd.DataFrame(std, index = df.index)], axis = 1)
	out_df.columns = 'mean', 'std'
	out_df = out_df.reset_index()

	#
	# NOTE: maybe we don't want this after all
	#
#	# strip the "cluster" of rejected variants
#	out_df = out_df[out_df['cluster_id'] > 0]
    
	return out_df

def trimmed_stat(x, q):
	''' Compute trimmed mean & std '''

	# get window size
	w = int(np.ceil(q * len(x)))

	# sort data
	perm = np.argsort(x)
	c = np.concatenate(((0,), np.cumsum(x[perm])))
	l = np.arange(len(x) - (w - 1))

	# compute moments
	m1 = c[l + w] - c[l]
	t = np.concatenate(((0,), np.cumsum(x[perm] * x[perm])))
	m2 = t[l + w] - t[l]

	# find minimum
	obj = (m2 - m1 * (m1 / w)) / w
	imin = np.argmin(obj)

	# estimate mean & variance
	est_m = m1[imin] / w
	est_v = np.maximum(obj[imin], 0)

	# find scale for std
	 # this assumes the data is normal
	k = -scipy.stats.norm.ppf((1 - q) / 2)
	t = scipy.stats.norm.pdf(k)
	coe = 1 / (( 1 - 2 * k * t / q ))
	if not t > 0:
		coe = 1

	return est_m, coe * est_v

def _load_vafs_and_coverage(file_name):
 	with open(file_name) as fh:
		config = yaml.load(fh)
    
	data = []
	for mutation_dict in config['mutations']:
		mutation = pyclone.post_process.loci.load_mutation_from_dict(mutation_dict)
        
		coverage = mutation.ref_counts + mutation.var_counts
		try:
			freq = float(mutation.var_counts) / coverage
		except ZeroDivisionError:
			freq = pd.np.nan

		data.append((mutation.id, freq, coverage))

	return pd.DataFrame(data, columns = ('mutation_id', 'variant_allele_frequency', 'coverage'))

def _sample_id_from_prevalence_filename(filename, config):
	match = re.match(r'^(.*)\.cellular_prevalence\.tsv\.bz2$', os.path.basename(filename))
	assert(match)

	return match.group(1)

def _solve_prevalence_from_vafs(vaf_data):
	''' Solves prevalences from VAF & related data '''

	#
	# TODO: in general, we need purity, "error" parameters, and the 
	#   various prior, but as we only fixed VAFs at zero, this is 
	#   not yet implemented
	# 

	vafs = np.array(vaf_data['variant_allele_frequency'])
	assert(all(vafs == 0))

	return vafs # for zero VAF, prevalence ~ VAF

def _load_prevalences(filename, config, options):
	# load data
	data = pyclone.post_process.loci.load_cellular_frequencies_trace(filename,
		burnin = options['burnin'], thin = options['thin'])

	# force prevalence for known data
	if options['inf_coverage'] < float('inf'):
		# load vaf data
		mutations_file = config['samples'][_sample_id_from_prevalence_filename(
				filename, config)]['mutations_file']
		vaf_data = _load_vafs_and_coverage(mutations_file)

		# find boogies
		inds, = np.nonzero(vaf_data['coverage'] >= options['inf_coverage'])
	
		# reset prevalence
		data[vaf_data['mutation_id'][inds]] = _solve_prevalence_from_vafs(
			vaf_data.ix[inds, :])

	# estimate moments
	m_1 = np.nan * np.ones((data.shape[1], 1))
	var = np.nan * np.ones((data.shape[1], 1))
	for i in xrange(data.shape[1]):
		m_1[i], var[i] = trimmed_stat(data.ix[:, i].values, 1 - options['trim'])

	# label
	out_df = pd.DataFrame(np.concatenate([m_1, np.sqrt(var)], axis = 1),
		index = data.columns)
	out_df.columns = 'cellular_prevalence', 'cellular_prevalence_std'
	out_df.index.name = 'mutation_id'
	out_df = out_df.reset_index()
    
	return out_df

# !!! monkey patch pyclone methods !!!

def _add_cookie(x, cookie):
	class __arg(x.__class__):
		def __init__(self, value):
			value.__class__.__init__(self, value)
		def _get_cookie(self):
			return cookie

	return __arg(x)

def _add_options(options, **kwargs):
	for key, value in kwargs.items():
		if key in options:
			assert options[key] == value
		else:
			options[key] = value

	return options

def _options_for(options, fun):
	import inspect
	args, _, _, _ = inspect.getargspec(fun)
	return dict((arg, options[arg]) for arg in args if arg in options)

def _sel_options(options, keep):
	new_options = {}
	for key, value in options.items():
		if key in keep:
			new_options[key] = value

	return new_options

# NB. we forward PyCloneRC options in a cookie attached to config

pyclone.paths.load_config = (lambda loader:
	lambda config:
		loader(config) if isinstance(config, basestring) else config)(
			pyclone.paths.load_config)

pyclone.post_process.clusters.cluster_pyclone_trace = (
	lambda config, burnin, thin, **kwargs:
		_cluster_traces(config, _add_options(config._get_cookie(),
			burnin = burnin, thin = thin, **kwargs)))

pyclone.post_process.clusters.load_table = (lambda _load_table:
	lambda config, **kwargs:
		_load_table(config, **_options_for(_add_options(config._get_cookie(),
			**kwargs), _load_table)))(
				pyclone.post_process.clusters.load_table)

pyclone.post_process.clusters.load_summary_table = (
	lambda config, **kwargs:
		_compute_cluster_summary(config, _add_options(config._get_cookie(),
			**kwargs)))

pyclone.post_process.loci.cluster_pyclone_trace = (    # NB. alias
	pyclone.post_process.clusters.cluster_pyclone_trace)

# NB. we need to pass config through to _load_prevalences..
 # wrap it in the burnin parameter, which is not dropped

pyclone.post_process.loci.load_table = (lambda _load_table:
	lambda config, burnin, thin, **kwargs:
		_load_table(config, _add_cookie(burnin, config), thin,
			**_options_for(kwargs, _load_table)))(
				pyclone.post_process.loci.load_table)

pyclone.post_process.loci._load_sample_cellular_prevalences = (
	lambda file_name, burnin, thin:
		_load_prevalences(file_name, burnin._get_cookie(), _add_options(
			burnin._get_cookie()._get_cookie(), burnin = burnin, thin = thin)))

# !!! end monkey patching !!!

def compute_cluster_table(config_file, **options):
	''' Construct summary table of the clusters '''

	# create config structure
	config = pyclone.paths.load_config(config_file)
	# attach cookie
	config = _add_cookie(config, options)

	# get table
	return pyclone.post_process.clusters.load_summary_table(config,
		burnin = options['burnin'], mesh_size = options['mesh_size'], thin = options['thin'])

def compute_locus_score(y, x, logf):
	''' Computes a membership score for locus with prevalence y for the 
		multivariate density that is the outer product of exp(logf)(x) '''

	# TODO: use a 3-region approach: certainly < BB, certainly > BB, and 
	 # the interesting part 

	# evaluate log-probability at y
	 # TODO: interpolate in exp-space, as log is nonlinear
	logfy = 0.
	for i in xrange(len(y)):
		logfyi = np.interp(y[i], x, logf[i])
		logfy += logfyi

	# maximum length for the table
	 # the outer product can take a lot of space if the threshold is
	 # small and the dimension is high (i.e. ~ meshsize ** numsamples)
	max_len = 101L ** 3.5
	max_c = np.sqrt( float(len(x)) ** len(y) / max_len )

	# accumulate 
	outer_logf = np.array([0.])
	c = 1.
	for i in xrange(len(y)):
		# get values to mix in, drop eagerly
		logfi = logf[i][ logf[i] > logfy ]
		# get outer sum
		outer_logf = np.add.outer(outer_logf, logfi).flatten()

		# drop invalid elements
		outer_logf = outer_logf[outer_logf > logfy]

		# the array grows too large?
		if len(outer_logf) > max_len:
			# print warning 
			if not c > 1.:
				import sys
				sys.stderr.write('warning: outer product too large, subsampling..' + '\n')
	
			# subsample
			old_len = len(outer_logf)
			while len(outer_logf) > max_len:
				# drop even elements & flip
				outer_logf = outer_logf[::-2]

			# update scaling factor
			c = c * (( float(old_len) / len(outer_logf) ))

			# TODO: this hits some good variants as well..

			# cut out crazy values, they are too noisy
			if c > max_c:
				outer_logf = np.nan
				break

	# get "p-value"
	mass = np.maximum( 1. - c * np.sum(np.exp(outer_logf)), 0. )

	return mass

def compute_loci_table(config_file, **options):
	''' Construct summary table of the loci '''

	# create config structure
	config = pyclone.paths.load_config(config_file)
	# attach cookie
	config = _add_cookie(config, options)

	# get table of loci
	loci_table = pyclone.post_process.loci.load_table(config,	**options)

	# computer locus scores?
	if options['cluster_pvalues']:
		# get cluster densities
		density_table = pyclone.post_process.clusters.load_table(config, **options)

		# compute scores for each locus
		joint_table = pd.merge(loci_table, density_table, on = ('sample_id', 'cluster_id'), how = 'left')
		x_cols = np.where( (0. <= joint_table.columns) & (joint_table.columns <= 1.) )[0]
		x = joint_table.columns[x_cols].astype(float).values
		scores = joint_table.groupby(['mutation_id']).apply(lambda row:
			compute_locus_score(row['cellular_prevalence'].astype(float).values, x, row.ix[:, x_cols].values))
	
		# add scores into the table
		loci_table = pd.merge(loci_table, pd.DataFrame(scores, columns = ['cluster_pvalue']).reset_index(),
			on = ['mutation_id'], how = 'left' )

	return loci_table

def compute_density_table(config_file, **options):
	''' Construct a table of the cluster densities '''

	# create config structure
	config = pyclone.paths.load_config(config_file)
	# add cookie
	config = _add_cookie(config, options)

	# get cluster data
	return pyclone.post_process.clusters.load_table(config, **options)

def add_ext(filename, ext):
	''' Add file extension if one is missing '''
	if not '.' in os.path.basename(filename):
		filename = '{}.{}'.format(filename, ext)

	return filename

def to_csv(filename, table):
	''' Save a data frame as Anduril-style "csv" '''

	# add extension
	filename = add_ext(filename, 'csv')

	# write
	import csv
	return table.to_csv(filename, sep = '\t', na_rep = 'NA', index = False,
		quoting = csv.QUOTE_NONNUMERIC)

def _ismember_loc(a, b):
	''' For each a[i] in a, return lower index in b st. a[i] = b[index[i]] '''

	# sort data & extract indices
	_, vals, inds = np.unique(np.concatenate((b, a)),
		return_index = True, return_inverse = True)

	# map to canonical element in b
	result = vals[inds[b.size + np.arange(a.size)]]
	result[result > b.size] = 0

	return result

def make_similarity_matrix_plot(config, **options):
	''' Make an annotated plot of the variant similarity matrix '''

	# get data
	old_labels, colnames, sim_mat = get_original_labels(config, options)
	new_labels = filter_labels(old_labels, sim_mat, options)

	# sort variants by cluster_id
	perm = np.argsort(old_labels)
	seq = np.arange(len(perm))

	# create the plot
	fig, ax = plt.subplots()
	img = ax.imshow(sim_mat[perm, :][:, perm],
		norm = plt.Normalize(0, 1),
			cmap = 'jet', aspect = 'equal', interpolation = 'none')
	plt.colorbar(img)

	# compute further settings
	fontsize = 12 * 25 / len(seq) # 12pt is good for up to ~25 items

	# annotate variant ids on the x axis 
	ax.set_xticks(seq)
	ax.set_xticklabels(colnames[perm], fontsize = fontsize, rotation = 90)
	plt.xlabel('variant_id')

	# annotate cluster ids on the y axis
	ax.set_yticks(seq)
	flags = (-1) ** (new_labels != old_labels)
	ax.set_yticklabels(flags[perm] * old_labels[perm], fontsize = fontsize)
	plt.ylabel('cluster_id')

	return fig

def make_filter_metrics_plot(config, **options):
	''' Make a plot of the filtering metrics '''

	# get labels & similarity matrix
	old_labels, colnames, sim_mat = get_original_labels(config, options)

	# get filter metrics
	ll1 = compute_create_lr(old_labels, sim_mat)
	ll2 = compute_assign_lr(old_labels, sim_mat)

	# sort variants by cluster ids
	perm = np.argsort(old_labels)
	seq = np.arange(len(perm))

	# plot curves
	fig, ax = plt.subplots()
	ax.plot(seq, ll1[perm], 'bx', label = 'create')
	ax.plot((seq[0], seq[-1]), options['min_create_lr'] * np.array((1, 1)), 'b--')
	ax.plot(seq, ll2[perm], 'g+', label = 'assign')
	ax.plot((seq[0], seq[-1]), options['min_assign_lr'] * np.array((1, 1)), 'g:')

	# compute further settings
	fontsize = 12 * 25 / len(seq)
	xbleed = .5
	ybleed = .025

	# set bounds
	ax.set_xlim(seq[0] - xbleed, seq[-1] + xbleed)
	ax.set_ylim(0 - ybleed, 1 + ybleed)

	# annotate x-axis
	ax.set_xticks(seq)
	ax.set_xticklabels(colnames[perm], fontsize = fontsize, rotation = 90)
	plt.xlabel('variant_id')

	# put rest of the labels
	plt.ylabel('likelihood ratio')
	plt.legend(loc = 'best')

	return fig

def _normpdf(x, m, s):
	''' Compute normal density '''

	v = s * s
	if v > 0:
		return scipy.stats.norm.pdf(x, m, s)
	else:
		d = x - m
		y = np.zeros(x.shape)
		y[d * d == np.amin(d * d)] = np.inf
		return y

def _normalize(x):
	''' Normalizes a PMF '''

	# compute sum, check for overflow
	s = np.sum(x)
	if s >= np.inf:
		x = x >= np.amax(x)
		s = np.sum(x)

	# normalize
	return x / s

def make_cluster_density_plot(config, **options):
	''' Make a plot of the cluster densities '''

	# create config structure
	for key in options:
		config[key] = options[key]

	# get cluster data
	df = pyclone.post_process.clusters.load_table(config, **options)
	df = df.set_index(['sample_id', 'cluster_id', 'size'])

	# get densities
	x = df.columns.astype(float).values[np.newaxis, :]
	p = np.exp(df.values)

	# compute settings
	scale = 0.2 * 101 / config['mesh_size'] # 0.2 looks ok for 101 points

	# plot densities
	fig, ax = plt.subplots()
	for i in xrange(p.shape[0]):
		# 
		# NOTE: (i+1) ~ row in the cluster table, *not* the cluster ID
		#   cluster IDs could be problematic with multiple inputs
		#

		# plot density
		ax.plot(x.flatten(), p[i, :] + scale * i, 'b-')
		top = max(p[i, :])

		# get model
		m, v = pmf_trimmed_stat(x.flatten(), p[i, :], 1 - options['trim'])
		phat = _normpdf(x.flatten(), m, np.sqrt(v))
		phat = _normalize(phat)

		# plot model
		ax.plot(x.flatten(), phat + scale * i, 'g--')
		ax.plot((m, m), np.array((0, top)) + scale * i, 'g-')

	# put legends
	ax.set_xlabel('cellular prevalence')
	ax.set_ylabel('cluster, probability mass'.format(scale))

	return fig

def to_pdf(filename, plot):
	''' Converts a Matplotlib figure into a pdf '''

	# add extension
	filename = add_ext(filename, 'pdf')
	# write
	plot.savefig(filename, format = 'pdf', bbox_inches = 'tight')

def split_names(string):
	''' Convert a comma separated string into a list of names '''

	if not string:
		return ()
	return tuple(arg.strip() for arg in string.split(','))

if __name__ == '__main__':
	import argparse

	# create parser
	parser = argparse.ArgumentParser()
	parser.add_argument('--config_file', default = './config.yaml')
	parser.add_argument('--trace_dir')
	parser.add_argument('--samples_dir')
	parser.add_argument('--out_dir', default = '.')
	parser.add_argument('--plot_type', default = '')
	parser.add_argument('--table_type', default = '')
	parser.add_argument('--burnin', type = int, default = 0)
	parser.add_argument('--mesh_size', type = int, default = 101)
	parser.add_argument('--thin', type = int, default = 1)
	parser.add_argument('--min_create_lr', type = float, default = 0)
	parser.add_argument('--min_assign_lr', type = float, default = 0)
	parser.add_argument('--inf_coverage', type = float, default = float('inf'))
	parser.add_argument('--trim', type = float, default = 0)
	parser.add_argument('--cluster_pvalues', type = bool, default = False)
	parser.add_argument('--min_cluster_size', type = int, default = 1)
	parser.add_argument('--max_clusters', type = int, default = float('inf'))

	# parse arguments
	args = parser.parse_args()
	args.samples_files = get_samples_files(args.samples_dir)

	# get config
	config = load_config(args.config_file, args.trace_dir, args.samples_files)
	options = {}
	for key in ('burnin', 'mesh_size', 'thin', 'min_create_lr', 'min_assign_lr',
			'inf_coverage', 'trim', 'cluster_pvalues', 'min_cluster_size', 'max_clusters'):
		options[key] = getattr(args, key)

	# make plots
	plots = {'similarity_matrix': make_similarity_matrix_plot,
		'filter_metrics': make_filter_metrics_plot,
		'cluster_density': make_cluster_density_plot}
	for plot_type in split_names(args.plot_type):
		to_pdf(os.path.join(args.out_dir, plot_type), plots[plot_type](
			config, **options))

	# print tables
	tables = {'cluster': compute_cluster_table,
		'loci': compute_loci_table,
		'density': compute_density_table}
	for table_type in split_names(args.table_type):
		to_csv(os.path.join(args.out_dir, table_type), tables[table_type](
			config, **options))
