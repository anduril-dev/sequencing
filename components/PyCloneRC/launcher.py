#!/usr/bin/env python

import anduril.args, pyclonerc
import os

# get options
options = {'burnin': anduril.args.burnin,
	'mesh_size': anduril.args.meshSize,
	'thin': anduril.args.thin,
	'min_create_lr': anduril.args.minCreateLr,
	'min_assign_lr': anduril.args.minAssignLr,	
	'inf_coverage': anduril.args.infCoverage,
	'trim': anduril.args.trim,
	'cluster_pvalues': anduril.args.clusterPValues,
	'min_cluster_size': anduril.args.minClusterSize,
	'max_clusters': anduril.args.maxClusters}

# load configuration
config = pyclonerc.load_config(anduril.args.input_config,
	anduril.args.input_trace, anduril.args.input_mutationFiles)

# create output directory for the tables
res_dir = anduril.args.output_clusteringResults
os.makedirs(res_dir)

# output tables
outputs = (('cluster', pyclonerc.compute_cluster_table),
	('loci', pyclonerc.compute_loci_table),
	('density', pyclonerc.compute_density_table))
for table_type, method in outputs:
	table = method(config, **options)
	pyclonerc.to_csv(os.path.join(res_dir, table_type), table)

# create output directory for the plots
plot_dir = anduril.args.output_plots
os.makedirs(plot_dir)

# output plots
plots = (('similarity_matrix', pyclonerc.make_similarity_matrix_plot),
	('filter_metrics', pyclonerc.make_filter_metrics_plot),
	('cluster_density', pyclonerc.make_cluster_density_plot))
for plot_type, method in plots:
	if plot_type in [arg.strip() for arg in anduril.args.plotType.split(',')]:
		plot = method(config, **options)
		pyclonerc.to_pdf(os.path.join(plot_dir, plot_type), plot)
