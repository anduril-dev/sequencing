#!/usr/bin/bash
set -e
[[ -e $ANDURIL_HOME/lang/bash/functions.sh ]] && . $ANDURIL_HOME/lang/bash/functions.sh $1
export_command
LIBPATH=$( readlink -f ../../lib )

# INPUTS
reads="$input_reads"
mates="$input_mates"
# OUTPUTS
out_folder="$output_out"
# PARAMETERS
tool="$parameter_tool"
threads="$parameter_threads"
read_length="$parameter_length"
sample_name="$parameter_sample"

tmpDir=$( gettempdir )
mkdir "$out_folder"

#RUN FUSION DETECTION TOOLS
case $tool in
    #FusionCatcher
    FC)
        [[ -e "$LIBPATH/fusioncatcher" ]] || {
            echo -e "FusionCatcher not installed. Run:\nbash $LIBPATH/install-scripts/fusioncatcher_install.sh $LIBPATH"
            exit 1
        }
        ln -s "$reads" "$tmpDir"/reads_1.fq.gz
        ln -s "$mates" "$tmpDir"/reads_2.fq.gz
        FUSIONCATCHER_HOME="$LIBPATH/fusioncatcher/"
        "$FUSIONCATCHER_HOME/bin/fusioncatcher" -p "$threads" -d "$( readlink -f $FUSIONCATCHER_HOME )/data/current" -i "$tmpDir/reads_1.fq.gz,$tmpDir/reads_2.fq.gz" -o "$out_folder"
        ln -s final-list_candidate-fusion-genes.txt "$output_out"/final
    ;;
    #ChimeraScan
    CS)
        [[ -e "$LIBPATH/chimerascan" ]] || {
            echo -e "ChimeraScan not installed. Run:\nbash $LIBPATH/install-scripts/chimerascan_install.sh $LIBPATH"
            exit 1
        }
        ln -s "$reads" "$tmpDir"/reads_1.fq.gz
        ln -s "$mates" "$tmpDir"/reads_2.fq.gz
        CHIMERASCAN_HOME="$LIBPATH/chimerascan"
        export PYTHONPATH="$CHIMERASCAN_HOME/lib/python2.7/site-packages":"$PYTHONPATH"
        python "$CHIMERASCAN_HOME/bin/chimerascan_run.py" -p "$threads" "$CHIMERASCAN_HOME/db" "$tmpDir/reads_1.fq.gz" "$tmpDir/reads_2.fq.gz" "$out_folder"
        ln -s chimeras.bedpe "$output_out"/final
    ;;
    #deFuse
    DF)
        [[ -e "$LIBPATH/deFuse" ]] || {
            echo -e "deFuse not installed. Run:\nbash $LIBPATH/install-scripts/defuse_install.sh $LIBPATH"
            exit 1
        }
        gunzip -k -c "$reads" > "$tmpDir"/reads_1.fq
        gunzip -k -c "$mates" > "$tmpDir"/reads_2.fq
        DEFUSE_HOME="$LIBPATH/deFuse"
        perl "$DEFUSE_HOME"/scripts/defuse_run.pl -p "$threads" -c "$DEFUSE_HOME/scripts/config.txt" -d "$DEFUSE_HOME/db" -1 "$tmpDir/reads_1.fq" -2 "$tmpDir/reads_2.fq" -o "$out_folder"
        ln -s "results.classify.tsv" "$output_out/final"
    ;;
    #SOAPfuse
    SF)
        [[ -e "$LIBPATH/SOAPfuse" ]] || {
            echo -e "SOAPfuse not installed. Run:\nbash $LIBPATH/install-scripts/soapfuse_install.sh $LIBPATH"
            exit 1
        }
        
        # Create SOAPfuse input directory structure
        mkdir "$tmpDir/tumor"
        mkdir "$tmpDir/tumor/sample"
        mkdir "$tmpDir/tumor/sample/lib"
        ln -s "$reads" "$tmpDir/tumor/sample/lib/run_1.fq.gz"
        ln -s "$mates" "$tmpDir/tumor/sample/lib/run_2.fq.gz"
        # Create information list of samples for SOAPfuse
        echo -e "sample\tlib\trun\t$read_length" >> "$tmpDir/sample_list.txt"   
        SOAPFUSE_HOME="$LIBPATH/SOAPfuse"
        export PERL5LIB=$PERL5LIB:"$SOAPFUSE_HOME/source/bin/perl_module"
        perl "$SOAPFUSE_HOME"/SOAPfuse-RUN.pl -c "$SOAPFUSE_HOME/config/config.txt" -fd "$tmpDir/tumor" -l "$tmpDir/sample_list.txt " -o "$out_folder" -fm
        ln -s final_fusion_genes/sample/sample.final.Fusion.specific.for.genes "$output_out"/final
    ;;
    #EricScript
    ES)
        [[ -e "$LIBPATH/ericscript" ]] || {
            echo -e "EricScript not installed. Run:\nbash $LIBPATH/install-scripts/ericscript_install.sh $LIBPATH"
            exit 1
        }
        ln -s "$reads" "$tmpDir/reads_1.fq.gz"
        ln -s "$mates" "$tmpDir/reads_2.fq.gz"
        rm -r "$output_out"
        ERICSCRIPT_HOME="$LIBPATH/ericscript"
        export PATH="$ERICSCRIPT_HOME/samtools-0.1.19:$PATH"
        perl "$ERICSCRIPT_HOME/ericscript.pl" -p $threads -db "$ERICSCRIPT_HOME/db" -o "$out_folder" "$tmpDir/reads_1.fq.gz" "$tmpDir/reads_2.fq.gz"
        if [[ -e "${out_folder}/MyEric.results.filtered.tsv" ]]; then
          ln -s MyEric.results.filtered.tsv "$output_out"/final
        else 
          ln -s MyEric.results.total.tsv "$output_out"/final
        fi
    ;;
    #StarFusion
    STAR)
        [[ -e "$LIBPATH/STAR-Fusion" ]] || {
            echo -e "STAR-Fusion not installed. Run:\nbash $LIBPATH/install-scripts/starfusion_install.sh $LIBPATH"
            exit 1
        }
        ln -s "$reads" "$tmpDir/reads_1.fq.gz"
        ln -s "$mates" "$tmpDir/reads_2.fq.gz"
        rm -r "$output_out"
        STARFUSION_HOME="$LIBPATH/STAR-Fusion"
        $STARFUSION_HOME/STAR-Fusion --genome_lib_dir $STARFUSION_HOME/ctat_genome_lib_build_dir --left_fq "$tmpDir/reads_1.fq.gz" --right_fq "$tmpDir/reads_2.fq.gz" --output_dir "$out_folder"
        ln -s star-fusion.fusion_predictions.abridged.tsv "$output_out"/final
    ;;
esac
echo -n "$tool" > "$out_folder/.tool"
echo -n "$sample_name" > "$out_folder/.sample"
