#!/bin/bash -x

# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/lang/bash/functions.sh"

# use these functions to read command file
infile=$( getinput accepted_hits ) 
outfile1=$( getoutput dataOut1 )
outfile2=$( getoutput dataOut2 )
cutoff=$( getparameter cutoff )

    
# you can use a special temporary directory:
tempdir=$( gettempdir )
#echo "Hello" > "$tempdir"/tempfile

#-- sorting the n allows to sort based on read name instead of location
#echo $infile $tempdir
#echo samtools sort -n $infile $tempdir/accepted_hits_sort
samtools sort -n $infile -o $tempdir/accepted_hits_sort.bam

#--to sam format
samtools view $tempdir/accepted_hits_sort.bam > $tempdir/accepted_hits_sort.sam

samtools view -H $tempdir/accepted_hits_sort.bam > $tempdir/header

#put all the locations for a read in the same row and discard the other columns (keep name and position)
awk '{if(! a[$1]) {a[$1]=$1"\t"$4;j=1;b[$1]=1} else{cad=a[$1]"\t"$4;a[$1]=cad;b[$1]=b[$1]+1}} END{for (i in a) {print b[i] "\t" a[i]}}' $tempdir/accepted_hits_sort.sam > $tempdir/index.csv

# NR==FNR is true while parsing the first file (index), the count of each read is saved in s, then if the count is < x print
awk -v x=$cutoff -v out1=$tempdir/dataIn1.csv -v out2=$tempdir/dataIn2.csv 'NR==FNR {s[$2]=$1; next} (s[$1]<=x) {print > out1} {print > out2}' $tempdir/index.csv $tempdir/accepted_hits_sort.sam

#add the header and output a sam file
cat $tempdir/header $tempdir/dataIn1.csv > $outfile1
cat $tempdir/header $tempdir/dataIn2.csv > $outfile2


#if [ -f $outfile ]
#then echo "Successful run." >> $logfile
#     echo $slsep >> $logfile
# messages should be separated with $slsep in log/error file.
#fi


