# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/lang/bash/functions.sh"
set -ex


if [[ $array == "" ]]
then
    input=$( getinput alignment )
else
    input=$( getarrayfiles array | tr '\n' ','  | sed 's/,$//' )
fi

reference=$( getinput reference )
referenceFolder=$( getinput referenceFolder )
mask=$( getinput mask )
annotation=$( getinput annotation )
tool=$( getparameter tool )
myFolder=$( getoutput folder )
parameters=$( getparameter parameters )
genes=$( getoutput genes )
isoforms=$( getoutput isoforms )
extra=$( getparameter extra )
outString="out"
mkdir $myFolder

sequencing_path=$( getbundlepath sequencing )
#options=$parameters" -o "$myFolder

echo "Tool selected: "$tool
echo "Alignment: "$input
echo "Inputs:" $reference $referenceFolder $annotation $mask
echo "Parameters: "$parameters
echo "Output folder: "$myFolder

if [[ $tool == "cufflinks" ]]
then
    options=$parameters" -o "$myFolder
    if [[ $annotation != "" ]]
    then
        options=$parameters" -o "$myFolder" -G "$annotation
    fi
    if [[ $reference != "" ]]
    then 
        options=$options" -b "$reference
    fi
    if [[ $mask != "" ]]
    then 
        options=$options" -M "$mask
    fi
    command=$options" "$input
    echo "$tool" $command
	if iscmd "$tool"
	then
	    $tool $command
	else
		$sequencing_path/lib/cufflinks/$tool $command
	fi
    if [[ -f ${myFolder}/isoforms.fpkm_tracking ]]
    then
        outFile1=${myFolder}/isoforms.fpkm_tracking
        outFile2=${myFolder}/genes.fpkm_tracking
    else
        echo "output missing"
    fi

elif [[ $tool == "express" ]]
then
    options=$parameters" -o "$myFolder
    command=$options" "$reference" "$input
    echo "$tool" $command
	if iscmd "$tool"
	then
	    $tool $command
	else
		$sequencing_path/lib/express/$tool $command
	fi
    if [[ -f ${myFolder}/results.xprs ]]
    then 
        outFile1=${myFolder}/results.xprs
        echo $outFile1 outfile
    else
        echo "output missing"
    fi

elif [[ $tool == "rsem" ]]
then
    currDir=$( pwd )
    cd $myFolder
    echo $rsemRef
    command=$parameters" "$input" "$referenceFolder/${extra}" "$outString
    echo "rsem-calculate-expression" $command
	if iscmd "rsem-calculate-expression"
	then
	    rsem-calculate-expression $command
	else
		$sequencing_path/lib/RSEM/rsem-calculate-expression $command
	fi
    if [[ -f out.genes.results ]]
    then 
        outFile1=${myFolder}/out.genes.results
        outFile2=${myFolder}/out.isoforms.results
    else
        echo "output missing"
    fi  
    cd $currDir

elif [[ $tool == "bitseq" ]]
then
    currDir=$( pwd )
    cd $myFolder
    command1=$input" --outFile "$outString".prob --trSeqFile "$reference" --trInfoFile "$outString".tr "$parameters
    echo "parseAlignment" $command1
	if iscmd "parseAlignment"
	then
	    parseAlignment $command1
	else
		$sequencing_path/lib/BitSeq/parseAlignment $command1
	fi
    command2=$outString".prob --outPrefix "$outString" --trInfoFile "$outString".tr "$extra
    echo "estimateExpression" $command2
	if iscmd "estimateExpression"
	then
	    estimateExpression $command2 
	else
		$sequencing_path/lib/BitSeq/estimateExpression $command2
	fi
    if [[ -f ${myFolder}/out.rpkm ]]
    then 
        tmpFile=${myFolder}/out.rpkm
    elif [[ -f ${myFolder}/out.counts ]]
    then 
        tmpFile=${myFolder}/out.counts
    elif [[ -f ${myFolder}/out.theta ]]
    then
        tmpFile=${myFolder}/out.theta
    elif [[ -f ${myFolder}/out.tau ]]
    then
        tmpFile=${myFolder}/out.tau
    fi
    ext=${tmpFile#*.}
	grep -v "#" ${myFolder}/out.tr | cut -d" " -f1  > ${myFolder}/geneFile.txt
	if iscmd "getVariance"
	then
	    getVariance -o ${myFolder}/transcripts.$ext $tmpFile
	else
		$sequencing_path/lib/BitSeq/getVariance -o ${myFolder}/transcripts.$ext $tmpFile
	fi
	if iscmd "getGeneExpression"
	then
	    getGeneExpression -o ${myFolder}/geneTmp.$ext -t ${myFolder}/out.tr -G ${myFolder}/geneFile.txt ${myFolder}/out.rpkm
	else
		$sequencing_path/lib/BitSeq/getGeneExpression -o ${myFolder}/geneTmp.$ext -t ${myFolder}/out.tr -G ${myFolder}/geneFile.txt ${myFolder}/out.rpkm
	fi
	if iscmd "getVariance"
	then
		 getVariance -o ${myFolder}/out.out ${myFolder}/geneTmp.$ext
	else
		$sequencing_path/lib/BitSeq/getVariance -o ${myFolder}/out.out ${myFolder}/geneTmp.$ext
	fi
	grep -v "#" ${myFolder}/out.out > ${myFolder}/tmp
    uniq ${myFolder}/geneFile.txt > ${myFolder}/tmp1
    paste ${myFolder}/tmp1 ${myFolder}/tmp > ${myFolder}/genes.rpkm
    rm ${myFolder}/tmp ${myFolder}/tmp1
    outFile2=${myFolder}/genes.rpkm
    outFile1=${myFolder}/transcripts.rpkm
    #one below works
    #getGeneExpression -o out.geneExpr -t execute/ExpressionQuantifier/case4_bitseq/component/folder/out.tr -G geneFile.txt execute/ExpressionQuantifier/case4_bitseq/component/folder/out.rpkm 
    #getVariance -o out.out out.geneExpr
	sed -ie '1i\gene_id mean_expression sample_variance' $outFile2
	paste ${myFolder}/geneFile.txt <(grep -v "#" $outFile1) |sed -e '1i\transcript_id mean_expression sample_variance' > ${myFolder}/tmp2
	mv ${myFolder}/tmp2 $outFile1
	sed -i 's/ /\t/g' $outFile1
	sed -i 's/ /\t/g' $outFile2
fi

rm -f $genes
rm -f $isoforms
ln -s $outFile1 $isoforms
if [[ -f $outFile2 ]]
then 
    ln -s $outFile2 $genes
else 
    touch $genes
fi

