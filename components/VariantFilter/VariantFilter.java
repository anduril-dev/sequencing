import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.anduril.asser.io.CSVParser;
import org.anduril.asser.io.CSVWriter;
import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;
import org.anduril.component.IndexFile;


public class VariantFilter extends SkeletonComponent {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new VariantFilter().run(args);
	}
	
	private static void incrementCount(Map<String, Integer> alleles, String allele1, String allele2){
		if(!alleles.containsKey(allele1))
			alleles.put(allele1, 1);
		else
			alleles.put(allele1, alleles.get(allele1) + 1);
		if(allele2 != null){
			if(!alleles.containsKey(allele2))
				alleles.put(allele2, 1);
			else
				alleles.put(allele2, alleles.get(allele2) + 1);
		}
		
	}
	
	private static boolean exceedsThreshold(Map<String, Integer> alleles, String variant, int threshold, boolean negate){
		if(negate){
			if(!alleles.containsKey(variant) || 
				(alleles.containsKey(variant) && alleles.get(variant) <= threshold))
				return true;
		}else{
			if(alleles.containsKey(variant) &&
					alleles.get(variant) >= threshold)
				return true;
				
		}
		return false;
	}

	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {

		IndexFile iSamples = IndexFile.read(cf.getInputArrayIndex("in"));
		CSVParser [] samples = new CSVParser[iSamples.size()];
		
		CSVWriter allOut = new CSVWriter(new String[]{"Pos", "Sample", "Id",
														"Allele1", "Allele2", 
														"AlleleCount1", "AlleleCount2", 
														"SampleCount1", "SampleCount2", 
														"ControlAlleleCount1", "ControlAlleleCount2", 
														"Included"}, 
										cf.getOutput("alleleCount"));
		
		boolean acForAll = cf.getBooleanParameter("acForAll");
		
		cf.getOutput("out").mkdir();
		IndexFile iOut = new IndexFile();
		CSVWriter [] samplesOut = new CSVWriter[iSamples.size()];
		
		Map<Integer, String[]> sampleLines = new HashMap<Integer, String[]>();
		
		CSVParser[] annotation;
		String [] sampleNames = new String[iSamples.size()];
		
		if(!cf.getParameter("sampleNames").equals("")){
			sampleNames = cf.getParameter("sampleNames").split(",");
		}
		
		IndexFile iAnno = null;
		
		if(cf.inputDefined("annotations")){
			iAnno = IndexFile.read(cf.getInputArrayIndex("annotationArray"));

			if(iAnno.size() != iSamples.size()){
				System.err.println("Sample and annotation arrays must be equal length");
				return ErrorCode.INVALID_INPUT;
			}

			annotation = new CSVParser[iAnno.size()];
		}else
			annotation = new CSVParser[iSamples.size()];

		for(int i = 0; i < iSamples.size(); i++){

			String key = iSamples.getKey(i);

			if(cf.getParameter("sampleNames").equals("")){
				sampleNames[i] = key;
			}
			samples[i] = new CSVParser(iSamples.getFile(key));

			//init the sample lines here
				
			File outFile = new File(cf.getOutput("out").getAbsolutePath() + "/" + sampleNames[i] + ".csv");
			samplesOut[i] = new CSVWriter(samples[i].getColumnNames(), outFile);
			iOut.add(key, outFile);
			
			if(iAnno != null)
				annotation[i] = new CSVParser(iAnno.getFile(key));
			else
				annotation[i] = new CSVParser(cf.getInput("annotation"));
			
		}
			
		String caseName = cf.getParameter("cases");
		String controlName = cf.getParameter("controls");

		int caseCount = cf.getIntParameter("caseThreshold");
		int controlCount = cf.getIntParameter("controlThreshold");

		int sampleCount = cf.getIntParameter("sampleThreshold");
		//if(sampleCount < 0){
		//	sampleCount = samples.length;
		//}
		String chr = cf.getParameter("chromosome");
		
		String annotChrCol = "Chromosome";
		String annotStartCol = "Start";

		String var1Col = "Allele1";
		String var2Col = "Allele2";

		
		
		//counter for reading data from files
		boolean hasNext = true;
		//counter for positions
		int [] curPos = new int [annotation.length];
		//counter for position that is being claculated
		int pos = -1;
		//init
		for(int i = 0; i < curPos.length; i++){
			curPos[i] = 0;
		}
		
		//iterate into correct chromosome
		for(int i = 0; i < annotation.length; i++){

			while(annotation[i].hasNext()){
				String[] annoLine = annotation[i].next();
				if(chr.equals(annoLine[annotation[i].getColumnIndex(annotChrCol)])){
					curPos[i] = Integer.parseInt(annoLine[annotation[i].getColumnIndex(annotStartCol)]);
					break;
				}
				samples[i].next();
				
			}
			if(!annotation[i].hasNext())
				curPos[i] = -1;
		}
		
		while(hasNext){

			//get the smallest position
			boolean init = false;
			for(int position : curPos){
				if(position > -1 && (!init || position < pos)){
					pos = position;
					init = true;
				}
			}
			//cases and controls in this position
			Map<String, Integer> cases = new HashMap<String, Integer>();
			Map<String, Integer> caseSamples = new HashMap<String, Integer>();
			Map<String, Integer> controls = new HashMap<String, Integer>();
			//System.out.println("process: "+pos);
			
			for(int i = 0; i < samples.length; i++){
				if(curPos[i] == pos){
					if(samples[i].hasNext()){
						sampleLines.put(i, samples[i].next());

						/*
						  System.out.println("read: ");
						 
						for(String s : sampleLines.get(i))
							System.out.print(s+"\t");
						*/
						String allele1 = sampleLines.get(i)[samples[i].getColumnIndex(var1Col)];
						String allele2 = sampleLines.get(i)[samples[i].getColumnIndex(var2Col)];

						if(sampleNames[i].matches(caseName)){
							incrementCount(cases, allele1, allele2);
							if(allele1.equals(allele2)){
								incrementCount(caseSamples, allele1, null);
							}else{
								incrementCount(caseSamples, allele1, allele2);
							}
						}else if(sampleNames[i].matches(controlName)){
							incrementCount(controls, allele1, allele2);
						}
					}
				}else{
					sampleLines.put(i, null);
				}
			}
			for(int i = 0; i < samples.length; i++){
				if(sampleLines.get(i) != null){
						
					//test the treshold for cases and controls
					boolean included = false;
					if( 
						((exceedsThreshold(caseSamples, sampleLines.get(i)[samples[i].getColumnIndex(var1Col)], sampleCount, false) &&
						exceedsThreshold(cases, sampleLines.get(i)[samples[i].getColumnIndex(var1Col)], caseCount, false)) &&
						exceedsThreshold(controls, sampleLines.get(i)[samples[i].getColumnIndex(var1Col)], controlCount, true) ) ||
						(exceedsThreshold(caseSamples, sampleLines.get(i)[samples[i].getColumnIndex(var2Col)], sampleCount, false) &&
								(exceedsThreshold(cases, sampleLines.get(i)[samples[i].getColumnIndex(var2Col)], caseCount, false)) &&
								exceedsThreshold(controls, sampleLines.get(i)[samples[i].getColumnIndex(var2Col)], controlCount, true))
								){

						samplesOut[i].writeRow(sampleLines.get(i));
						included = true;
						
					}
					if(acForAll || included){
						allOut.write(pos);
						allOut.write(sampleNames[i]);
						allOut.write(sampleLines.get(i)[samples[i].getColumnIndex("Id")]);
						allOut.write(sampleLines.get(i)[samples[i].getColumnIndex(var1Col)]);
						allOut.write(sampleLines.get(i)[samples[i].getColumnIndex(var2Col)]);

						if(cases.containsKey(sampleLines.get(i)[samples[i].getColumnIndex(var1Col)]))
							allOut.write(cases.get(sampleLines.get(i)[samples[i].getColumnIndex(var1Col)]));
						else
							allOut.write(0);

						if(cases.containsKey(sampleLines.get(i)[samples[i].getColumnIndex(var2Col)]))
							allOut.write(cases.get(sampleLines.get(i)[samples[i].getColumnIndex(var2Col)]));
						else
							allOut.write(0);

						if(caseSamples.containsKey(sampleLines.get(i)[samples[i].getColumnIndex(var1Col)]))
							allOut.write(caseSamples.get(sampleLines.get(i)[samples[i].getColumnIndex(var1Col)]));
						else
							allOut.write(0);

						if(caseSamples.containsKey(sampleLines.get(i)[samples[i].getColumnIndex(var2Col)]))
							allOut.write(caseSamples.get(sampleLines.get(i)[samples[i].getColumnIndex(var2Col)]));
						else
							allOut.write(0);

						if(controls.containsKey(sampleLines.get(i)[samples[i].getColumnIndex(var1Col)]))
							allOut.write(controls.get(sampleLines.get(i)[samples[i].getColumnIndex(var1Col)]));
						else
							allOut.write(0);

						if(controls.containsKey(sampleLines.get(i)[samples[i].getColumnIndex(var2Col)]))
							allOut.write(controls.get(sampleLines.get(i)[samples[i].getColumnIndex(var2Col)]));
						else
							allOut.write(0);

						allOut.write(included);
					}
				}
			}
		
			
			hasNext = false;
			for(int i = 0; i < annotation.length; i++){

				if(annotation[i].hasNext()){
					//if we've already filtered this position, go to next one
					if(pos == -1 || curPos[i] <= pos){
						
						String[] annoLine = annotation[i].next();
						
						if(chr.equals(annoLine[annotation[i].getColumnIndex(annotChrCol)])){
						
							curPos[i] = Integer.parseInt(annoLine[annotation[i].getColumnIndex(annotStartCol)]);
							hasNext = true;
							
						}else{
							curPos[i] = -1;
						}	
						
						
					}else{
						hasNext = true;
					}
				}else{
					curPos[i] = -1;
				}
			}
		}

		iOut.write(cf, "samples");
		allOut.close();
		for(CSVWriter ow : samplesOut){
			ow.close();
		}
		return ErrorCode.OK;
	}

}
