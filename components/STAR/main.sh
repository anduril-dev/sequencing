#!/bin/bash

# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/lang/bash/functions.sh"
export PATH=$PATH:$( readlink -f ../../lib/STAR/source/ ):$( readlink -f ../../lib/samtools/ )
export_command

set -ex

iscmd samtools || exit 1
iscmd STAR || exit 1

mkdir ${output_folder}
cd ${output_folder}

# For the UNIX pipe syntax here, see: http://stackoverflow.com/questions/7756609/pass-stdout-as-file-name-for-command-line-util-answer-use-named-pipes

# Copy parameter file to cwd so STAR will use it
if [[ "${parameter_parameters}" != "" ]]; then
    echo "Using parameter_parameters from: ${parameter_parameters}"
    cp ${parameter_parameters} Parameters1.in
fi

READS=$(getarrayfiles reads | tr '\n' ','  | sed 's/,$//')
MATES=""
if [ -s "${input_mates}" ]; then
    MATES=$(getarrayfiles mates | tr '\n' ','  | sed 's/,$//')
fi

echo "Reads are ${READS}"
echo "Mates are ${MATES}"

STAR --genomeDir ${input_genome} --genomeLoad ${parameter_genomeLoad} --readFilesIn ${READS} ${MATES} --runThreadN ${parameter_threads} ${parameter_options} --outFileNamePrefix ./ --outStd Log

cd ..
if [[ ${parameter_mainAlignmentType} != "" ]]
then
    outString="."${parameter_mainAlignmentType}
fi
outFile=$( ls ${output_folder}/Aligned${outString}.out.* )
echo $outFile
if [[ -e ${output_alignment} ]] 
then
    rm ${output_alignment}
fi
ln -s ${outFile} ${output_alignment}

if [[ -f ${output_folder}/Aligned.sortedByCoord.out.bam ]]
then 
    samtools index ${output_folder}/Aligned.sortedByCoord.out.bam
fi

# Header for the splice junction CSV file
echo -e "Chromosome\tStart\tEnd\tStrand\tIntronMotif\tAnnotated\tUniqueMapping\tMultiMapping\tMaxOverhang" > "${output_spliceJunctions}"
cat ${output_folder}/SJ.out.tab >> "${output_spliceJunctions}"

