#!/bin/bash


URL="http://csbi.ltdk.helsinki.fi/pub/anduril_static/component_data/STAR/input/genome.tar.gz"
cd "$( dirname $0 )"
[[ -d testcases/case1/input/genome ]] && {
    echo "Testcases already exist"
} || {
    #mkdir -p testcases/case1/expected-output/
    cd testcases/case1/input/
    wget -O - "$URL" | tar xzf - 
}

