cd @folder1@

PAIRED_MODE=@param1@
QUALITY=@param2@
ARRAY2=$( getinput array2 )

EXTRA_ARGS=""
if [ "$QUALITY" = "phred33" ] ; then
    :
elif [ "$QUALITY" = "phred64" ] ; then
    EXTRA_ARGS="$EXTRA_ARGS --phred64"
else
    echo "Invalid quality parameter: $QUALITY"
    exit 1
fi

if [ "$ARRAY2" = "" ] ; then
    # No mates, or interleaved mates
    k=0
    for reads in $( getarrayfiles array1 ) ; do
        k=$(( k+1 ))
        formatk=$( printf "%03d" $k )
        sga preprocess --pe-mode $PAIRED_MODE -o reads-$formatk.fa $EXTRA_ARGS $reads
    done
else
    # Split mates
    declare -a files1 # Put files to array
    k=0
    for file in $( getarrayfiles array1 ) ; do
        k=$(( k+1 ))
        files1[$k]=$file
    done

    declare -a files2 # Put files to array
    k=0
    for file in $( getarrayfiles array2 ) ; do
        k=$(( k+1 ))
        files2[$k]=$file
    done

    # Iterate over both arrays
    n=${#files1[@]}
    for (( k=1 ; k<=$n; k=k+1 )) ; do
        reads=${files1[$k]}
        mates=${files2[$k]}
        formatk=$( printf "%03d" $k )
        sga preprocess --pe-mode $PAIRED_MODE -o reads-$formatk.fa $EXTRA_ARGS $reads $mates
    done
    k=$(( k-1 )) # Get the final number of files
fi

# Combine
if [ "$k" -eq 1 ] ; then
    mv reads-001.fa reads.fa
else
    cat reads-*.fa > reads.fa
    rm reads-*.fa
fi
