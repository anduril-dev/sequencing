def SGA (
      reads: SequenceSet ,
      mates: SequenceSet = null,
      threads: Int  = 8,
      pairedEnd: Boolean  = true,
      minOverlap: Int  = 45,
      minOverlapAssemble: Int  = 75,
      minOverlapMerge: Int  = 65,
      minContigLength: Int  = 200,
      correctionK: Int  = 41,
      indexBatchSize: Int  = 0,
      minBranchLength: Int  = 150,
      quality: String  =  "phred33" ,
      memoryGB: Int  = 16 ): (FASTA,FASTA) =
{
 
     implicit def Tuple2StrMap[T](t: (String,T)) = Map ( t._1 -> t._2.toString) 
 
     def SGACall(workDir: BinaryFolder, previous: BinaryFile, command: String, input: String, args: Map[String,String] = Map.empty,
                 threads: Int = 1, memoryGB: Int = 0): (BinaryFile) = {

         var sgaArgs = ""
         for ( (name, value) <- args ) {
             
             assert(name != "t", "'t' argument must not be used")

             if (name.length == 1) {
                 sgaArgs = sgaArgs + " -" + name
             } else {
                 sgaArgs = sgaArgs + " --" + name
             }
             if (value != null) {
                 sgaArgs = sgaArgs + " " + value
             }
         }

         if (threads > 1) sgaArgs = sgaArgs + " -t " + threads         
     
         val cmd = s""" cd @var1@
                        sga $command $sgaArgs $input"""

         
         val sgaCall = BashEvaluate(var1=workDir, var2=previous, script=cmd)
         sgaCall._custom("cpu")=threads.toString
         if (memoryGB > 0) sgaCall._custom("memory")=(memoryGB * 1024).toString

         return sgaCall.stdOut
     }
 
 
     val PAIRED_MODE_NONE = 0
     val PAIRED_MODE_TWO_FILES = 1
     val PAIRED_MODE_ONE_FILE = 2

     val pairedMode =
         if (!pairedEnd) PAIRED_MODE_NONE
         else if (mates == null) PAIRED_MODE_ONE_FILE
         else PAIRED_MODE_TWO_FILES
         

     val indexArgs = 
         if (indexBatchSize > 0) ("d", indexBatchSize.toString)
         else if (indexBatchSize == 0) ("a", "ropebwt")
         else ("a", "sais")
         

     // Preprocess & establish work directory where all files are written to //

     val preprocessScript = INPUT(path="preprocess.sh")
     val preprocessed = BashEvaluate(array1=reads, array2=mates, command=preprocessScript, param1=pairedMode.toString, param2=quality)
     val workDir = preprocessed.folder1

     // Error correction //

     val noReverseIndexArgs = Array( indexArgs, ("no-reverse", null) ).toMap
     
     val indexErrorCorrection = SGACall(workDir, preprocessed.stdOut, command="index", input="reads.fa",
            threads=threads, memoryGB=memoryGB, args=noReverseIndexArgs)
     
     val corrected = SGACall(workDir, indexErrorCorrection/* .chain */, command="correct", input="reads.fa",
             threads=threads, memoryGB=memoryGB, args=Map( "k"->correctionK.toString, "discard"->null, "learn"->null ) )

     // Contig assembly //

     val index = SGACall(workDir, corrected/* .chain */, command="index", input="reads.ec.fa",
            threads=threads, memoryGB=memoryGB, args=indexArgs)
     
     val filtered = SGACall(workDir, index/* .chain */, command="filter", input="reads.ec.fa",
             threads=threads, memoryGB=memoryGB,
             args=Map( "homopolymer-check"->null, "low-complexity-check"->null ) )
     
     val merged = SGACall(workDir, filtered/* .chain */, command="fm-merge", input="reads.ec.filter.pass.fa",
            threads=threads, memoryGB=memoryGB, args=("m",minOverlapMerge))
     
     val splitScript = INPUT(path="splitFASTA.scala")
     
     /** Split sequences longer than 65536 bp. */
     val split = ScalaEvaluate(scriptFile=splitScript, var1=workDir, var2=merged/* .chain */, param1="reads.ec.filter.pass.merged.fa")
     
     val indexMerged = SGACall(workDir, split.table, command="index", input="reads.ec.filter.pass.merged.fa",
            threads=threads, memoryGB=memoryGB, args=indexArgs)
     
     val rmDup = SGACall(workDir, indexMerged/* .chain */, command="rmdup", input="reads.ec.filter.pass.merged.fa",
            threads=threads)
     
     val overlap = SGACall(workDir, rmDup/* .chain */, command="overlap", input="reads.ec.filter.pass.merged.rmdup.fa",
         threads=threads, args=("m",minOverlap))
     
     val assembled = SGACall(workDir, overlap/* .chain */, command="assemble", input="reads.ec.filter.pass.merged.rmdup.asqg.gz",
         args=("m",minOverlapAssemble))

     // Scaffolding //

     if (pairedEnd) {
         val alignScript = INPUT(path="align.sh")
         val contigAlign = BashEvaluate(var1=workDir, array1=reads, array2=mates, var2=assembled/* .chain */, command=alignScript, param1=threads.toString)
         contigAlign._custom("cpu") = threads.toString

         val cmd1 = s"""
                 cd @var1@
                 sga-bam2de.pl -n 5 -m $minContigLength --prefix default -t $threads default.bam
                 """                 
         val distance = BashEvaluate(var1=workDir, var2=contigAlign.stdOut, script=cmd1)
         distance._custom("cpu") = threads.toString

         val cmd2 = """
                 cd @var1@
                 BAM=default.refsort.bam
                 SIZE=$( du -b $BAM | cut -f1 )
                 if [ "$SIZE" -lt 10000 ] ; then
                     # Test data, or other very small data
                     sga-astat.py -m """+minContigLength+""" -b 1 -n 0 $BAM > astat
                 else
                     sga-astat.py -m """+minContigLength+""" $BAM > astat
                 fi
                 """
         val astat = BashEvaluate(var1=workDir, var2=distance.stdOut, script=cmd2)

         val scaffold = SGACall(workDir, astat.stdOut, command="scaffold", input="default-contigs.fa",
                args=Map("a"->"astat", "pe"->"default.de", "m"->minContigLength.toString) ) 

         val scaffoldFasta = SGACall(workDir, scaffold/* .chain */, command="scaffold2fasta", input="default-contigs.fa.scaf",
                args=Map("a"->"default-graph.asqg.gz", "use-overlap"->null, "m"->minContigLength.toString) )
                  
         val extracted = FolderExtractor(workDir, filename1="default-contigs.fa", filename2="scaffolds.fa")
         extracted._filename("file1", "file1.fasta")
         extracted._filename("file2", "file2.fasta")
         extracted._bind(scaffoldFasta)

         return (extracted.file1.force(), extracted.file2.force())
     } else {
         
         val extracted = FolderExtractor(workDir, filename1="default-contigs.fa", filename2="default-contigs.fa")
         extracted._filename("file1", "file1.fasta")
         extracted._bind(assembled)
         
         val empty = StringInput(content="")
         empty._filename("out", "in.fasta")

         return (extracted.file1.force(), empty.force())
     }
 }
