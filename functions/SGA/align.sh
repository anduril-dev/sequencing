cd @var1@ # Work dir

THREADS=@param1@
ARRAY2=$( getinput array2 )

if [ "$ARRAY2" = "" ] ; then
    # No mates, or interleaved mates
    k=0
    for reads in $( getarrayfiles array1 ) ; do
        k=$(( k+1 ))
        formatk=$( printf "%03d" $k )
        sga-align --name $formatk default-contigs.fa -t $THREADS $reads
    done
else
    # Split mates
    declare -a files1 # Put files to array
    k=0
    for file in $( getarrayfiles array1 ) ; do
        k=$(( k+1 ))
        files1[$k]=$file
    done

    declare -a files2 # Put files to array
    k=0
    for file in $( getarrayfiles array2 ) ; do
        k=$(( k+1 ))
        files2[$k]=$file
    done

    # Iterate over both arrays
    n=${#files1[@]}
    for (( k=1 ; k<=$n; k=k+1 )) ; do
        reads=${files1[$k]}
        mates=${files2[$k]}
        formatk=$( printf "%03d" $k )
        sga-align --name $formatk default-contigs.fa -t $THREADS $reads $mates
    done
    k=$(( k-1 )) # Get the final number of files
fi

# Merge
if [ "$k" -eq 1 ] ; then
    mv 001.bam default.bam
    mv 001.refsort.bam default.refsort.bam
else
    samtools merge default.refsort.bam [0-9]*.refsort.bam
    rm [0-9]*.refsort.bam
    samtools merge default.bam [0-9]*.bam
    rm [0-9]*.bam
fi
