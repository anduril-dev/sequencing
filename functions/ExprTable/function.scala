def ExprTable ( 
  array: BinaryFile ,
  auxiliary: CSV = null, 
  idsCol: String  =  "tracking_id" ,
  valueCols: String  =  "FPKM" ,
  extraCols: String  =  "gene_id,gene_short_name" ,
  filter: String  =  "" ,
  matchColAux: String = "",
  matchCol: String = "",
  numberTopHits: Int  = 10,
  lowBound: String = "",
  highBound: String = "",
  log2transform: Boolean = true,
  collapseNumeric: String = "consensus" ): (CSV,CSV,CSV,Latex) = {
    
    import org.anduril.runtime._
    import anduril.tools._
    import anduril.builtin._

    val filtered = NamedMap[CSVFilter]("filtered")
    val unique   = NamedMap[IDConvert]("unique")
    val log2Script = INPUT(path=_functionFolder+"log2.R")

    val cols = 
        if (extraCols != "") idsCol+","+extraCols+","+valueCols 
        else idsCol+","+valueCols

    for ( (k,v) <- iterArray(array) ) {
               
        val renameCol = valueCols+"="+k

        /* filter out rows according to the filter parameter and add the sample key to the column name of the expression values */
        filtered(k) = CSVFilter(
            in              = INPUT( path= v.getAbsolutePath() ),
            aux             = auxiliary,
            idColumn        = matchCol,
            matchColumn     = matchColAux,
            lowBound        = lowBound,
            highBound       = highBound,
            regexp          = filter,   
            includeColumns  = cols,
            rename          = renameCol)

        unique(k) = IDConvert(
            in             = filtered(k).out, 
            conversionTable = filtered(k).out,
            collapseNumeric = collapseNumeric,
            conversionColumn= idsCol,
            keyColumn       = idsCol,
            sourceColumn    = idsCol,
            unique          = true
        )
        
    }                          

    val joined = CSVJoin(   in              = makeArray(unique.mapValues {_.out}) ,
                            intersection    = false,
                            keyColumnNames  = "", 
                            useKeys         = true)
    // Preliminary values if no log2transform needed
    var empty = BashEvaluate(script ="""echo "log2transform parameter was set to false" > @out1@""")
    var log2Table = empty.out1
    var myStats = empty.out2
    var tmp = LatexTemplate()
    var doc = tmp.header

    if (log2transform) {
        val log2 = REvaluate( script = log2Script, table1 = joined.out)
        val statsIn = StringInput(
            content="""
                setwd(document.dir)
                library(reshape)
                library(ggplot2)
                library(digest) 
                library(GenomicRanges) 
                myDir = get.output(cf,'document')
                nums <- sapply(table1, is.numeric)
                print(nums)
                t1<-table1[,nums]
                print(colnames(t1))
                print(param1)
                rownames(table1) <- table1[,param1]
                t2<-cbind(id=rownames(table1),t1)
                print(head(t2))
                a<-c()
                for (i in 2:ncol(t2)) { a<-union(a,as.character(t2[order(t2[,i],decreasing=TRUE),1][1:"""+numberTopHits+"""])) }
                print (a)
                print(t2[t2[,1]%in%a,])
                table.out <- t2[t2[,1]%in%a,]

                groupLimits = colnames(t2[,2:ncol(t2)])
                data = melt(t2[,2:ncol(t2)])
                clean <-subset(data,data$value>0)
                p1 = ggplot(clean,aes(value,fill=variable)) +
                geom_histogram(alpha=0.4,position="identity",aes(y=(..count..)/sum(..count..))) +
                theme_bw() + theme(plot.background = element_blank(), panel.grid.major = element_blank(), panel.grid.minor = element_blank()) + 
                xlab("log2 FPKMs")+ylab("Proportion")+ggtitle("Gene expression histogram")
                ggsave(file="histogram.pdf",plot=p1)
                
                p2 = ggplot(clean,aes(value,fill=variable)) + geom_density(alpha=0.2) + theme_bw() + 
                theme(plot.background = element_blank(), panel.grid.major = element_blank(), panel.grid.minor = element_blank()) + 
                xlab("log2 FPKMs")+ylab("Density")+ggtitle("Gene expression density")
                ggsave(file="density.pdf",plot=p2) 

                p3 = ggplot(clean,aes(x=variable,y=value,fill=variable)) + geom_boxplot(outlier.shape = NA) + stat_summary(fun.y=mean, geom="point", shape=5, size=4) +
                xlab("samples")+ylab("log2 FPKMs")+ggtitle("Gene expression")
                ggsave(file="boxplot.pdf",plot=p3)

                fig1<-latex.figure(filename="histogram.pdf")
                fig2<-latex.figure(filename="density.pdf")
                fig3<-latex.figure(filename="boxplot.pdf")
                document.out <- c(fig1,fig2,fig3)
            """
        )

        val stats = REvaluate( script  = statsIn, param1  = idsCol, table1  = log2.table)
        log2Table = log2.table
        myStats = stats.table
        doc = stats.document

    } else {

    }

    return (joined.out, log2Table, myStats, doc)
}
