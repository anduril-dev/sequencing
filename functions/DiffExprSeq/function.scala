
def DiffExprSeq ( 
      exonCounts: BinaryFile = null,
      geneCounts: BinaryFile = null,
      annotation: GFF = null,
      conditions: CSV = null, 
      level: String  =  "Exon" ,
      conditionList: String  =  "A,A,B,B" ,
      nCores: Int  = 5,
      minCount: Int  = 5 ): (CSV,CSV) = {

    val levels = level.split("\\W+").toSet

    val exon = levels("Exon")
    val gene = levels("Gene")

    if (exon == false && gene == false)
        sys.error("Only supported levels: Exon and Gene.  Found  " + levels.mkString(","))


    val conds = 
        if (conditions == null) {
            conditionList
        } else {
            val list = BashEvaluate(var1    = conditions,
                                    script  ="""a=$( awk '{for (i=1; i<=NF;i++) {if ($i~"Condition") print i}}' @var1@ )
                                                tail -n +2 @var1@ | cut -f $a | tr '\n' ',' | sed '$s/,$//' """)
            list.stdOut.textRead()
        }

    val outE = 
        if (exon) {
            if (annotation==null) 
                sys.error("annotation missing") 

            val script = INPUT(path="dexSeqRscript.R")
            val diffExon = REvaluate(   var1    = annotation, 
                                        var2    = exonCounts,
                                        param1  = conds,
                                        param2  = nCores.toString,
                                        param3  = minCount.toString,
                                        script  = script)
            diffExon.table
        }
        else {
            StringInput(content="Exon level not selected").out
        }

    val outG = 
        if (gene) {
            val scriptG = INPUT(path= _functionFolder +"DeSeq.R")
            val diffGene = REvaluate(   var1    = geneCounts,
                                        param1  = conds,
                                        script  = scriptG)
            diffGene.table
        }
        else {
            StringInput(content="Gene level not selected").out
        } 

    return (outE,outG)
}
