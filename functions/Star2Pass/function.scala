import anduril.builtin._
import anduril.microarray._
import anduril.sequencing._
import anduril.tools._
import anduril.anima._
import org.anduril.runtime._


  
  def Star2Pass(
        reference: FASTA,
        reads: FASTQ,
        mates: FASTQ = null,
        genome: BinaryFolder = null,
        annotation: BinaryFile = null,
        parameterFile: TextFile = null,
        custom: TextFile = null,
        genomeParameters: String = "",
        alignParameters: String = "",
        genomeLoad: String="NoSharedMemory",
        readFilesCommand: String = "",
        lowBound: String = "UniqueMapping=5",
        highBound: String = "",
        mainAlignmentType: String = "",
        useEncodeParams: Boolean = true,
        threads: Int = 4,
        memory: Int=10000,
        execute: String = "changed"):(BinaryFolder,BAM,CSV) = 
  {
    val singleEnd=mates==null
    
    val sampleSpecific = NamedMap[String]("sampleSpecific")
    val encodeParams = " --outFilterType BySJout --alignSJoverhangMin 8 --alignSJDBoverhangMin 1 --alignIntronMin 20 --alignIntronMax 1000000 --alignMatesGapMax 1000000"

    val myGenome = if (genome == null) {

      info("generating genome")
      // Build genome for first pass
      val prepareGenome = STARGenome(genomeFasta = reference, annotation = annotation, genomeParameters = genomeParameters, threads=threads)
      prepareGenome._execute=execute
      prepareGenome.genome
                              
    } else {
        info("genome provided")
        genome 
    }
    
    val readCommand = if (readFilesCommand=="") {
        info(readFilesCommand) 
        readFilesCommand 
    }else " --readFilesCommand "+readFilesCommand 

    val params = if (useEncodeParams) readCommand+encodeParams else readCommand
    info(params)
        
    val splices=NamedMap[CSV]("splices")
    val canonical=NamedMap[CSV]("canonical")
    for ( (k,v) <- iterArray(reads) )
    {
      withName(k) 
      {
        val mateArray = if (singleEnd) null else makeArray(mates(k))
        val readArray = makeArray(reads(k))        
        // First alignment pass
        val firstPass = STAR( genome    = myGenome,
                              reads     = readArray,
                              mates     = mateArray,
                              parameters= parameterFile,
                              genomeLoad= genomeLoad,
                              options   = "--outSAMtype BAM Unsorted"+params,
                              threads   = threads)

        firstPass._custom("cpu")=threads.toString
        firstPass._custom("memory")=memory.toString
        firstPass._execute=execute
      
        // Filter splice junctions for each sample
        canonical(k) = CSVFilter( in          = firstPass.spliceJunctions.asInstanceOf[CSV],
                                  regexp          = "Chromosome=[0-9]+|[X]|[Y]",
                                  lowBound      = lowBound,
                                  highBound     = highBound,
                                  includeColumns  ="Chromosome,Start,End,Strand").out
      
        //create empty sampleSpecific parameters, if custom files was provided then the correct values will be added below
        sampleSpecific(k)=""
      }
    }
    
    if ( custom != null ) for ( row <- iterCSV(custom) ) sampleSpecific(row("Key"))=" "+row("Custom")

    // Pool splic junctions from all the samples
    val joinSplices = CSVJoin(in = makeArray(canonical), useKeys = false).out
    val pooledSplices = CSVCleaner(in = joinSplices, skipQuotes = "*").out

    // Build a genome using the pool
    val makeGenome = STARGenome(genomeFasta=reference,spliceJunctions=pooledSplices,annotation=annotation,genomeParameters=genomeParameters,threads=threads)
    makeGenome._custom("cpu")=threads.toString
    makeGenome._custom("memory")=memory.toString
    makeGenome._execute=execute
  
    // Second pass of alignments
    val bams = NamedMap[BAM]("bams")
    val myFolders = NamedMap[BinaryFolder]("myFolders")
    for ( (k,v) <- iterArray(reads) )
    {
      withName(k) 
      {
        val mateArray = if (singleEnd) null else makeArray(mates(k))
        val readArray = makeArray(reads(k))        
        val secondPass = STAR(genome            = makeGenome.genome,
                              reads             = readArray,
                              mates             = mateArray,
                              parameters        = parameterFile,
                              genomeLoad        = genomeLoad,
                              mainAlignmentType = mainAlignmentType,
                              options           = alignParameters+params+sampleSpecific(k),
                              threads           = threads)

        secondPass._custom("cpu")=threads.toString
        secondPass._custom("memory")=memory.toString

        bams(k)= secondPass.alignment
        myFolders(k) = secondPass.folder
      }
    }
    val alignments = makeArray(bams)
    val folders = makeArray(myFolders)
    return (folders,alignments,pooledSplices)
  }
