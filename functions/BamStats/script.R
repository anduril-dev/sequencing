

################################################## R script to produce HTML report on alignment statistics  ##########################################################


#################################
### loading required packages ###
#################################
library(ggplot2)
library(componentSkeleton)
library(hwriter)

#########################
### reading arguments ###
#########################

args = commandArgs(trailingOnly=T)
inputPicard = args[1]
inputBedtools = args[2]
output = args[3]
cssStyle = args[4]
sampleName = args[5]
targeted = args[6]
maxCov = as.numeric(args[7])
markExist = args[8]
summaryOut = args[9]
paired = args[10]
markResults = args[11]

#####################
### reading files ###
#####################

if(markExist == "true")
	markStats = read.table(markResults, sep="\t",header=T,stringsAsFactors=F,nrow=1)
alignment_summary_metrics = CSV.read(paste0(inputPicard,"/alignment_summary_metrics"))
alignment_summary_metrics = t(alignment_summary_metrics) 
write.table(alignment_summary_metrics,file=paste0(output,"/alignment_summary_metrics"),row.names=T,col.names=F,sep='\t',quote=F)
alignment_summary_metrics = read.table(paste0(output,"/alignment_summary_metrics"),header=T,sep="\t",colClasses="character")
if (paired == "true"){
	insert_size_metrics = read.table(paste0(inputPicard,"/insert_size_metrics"),header=T,stringsAsFactors=F,sep="\t",skip=8)
	insert_size_summary = read.table(paste0(inputPicard,"/insert_size_metrics"),header=T,stringsAsFactors=F,sep="\t",nrow=1)
}
quality_distribution_metrics = CSV.read(paste0(inputPicard,"/quality_distribution_metrics"))
coverage_distribution = read.table(inputBedtools,header=F,stringsAsFactors=F,sep="\t")
print(paste("paired is", paired))
################################
### producing summary output ###
################################
if (paired == "true")
	summaryTable = alignment_summary_metrics[c(1,3,6,15,17),c(1,4)]
if (paired == "false")
	summaryTable = alignment_summary_metrics[c(1,3,6,15,17),c(1,2)]

summaryTable = rbind(summaryTable,  c("meanCoverage", sum(as.numeric(coverage_distribution[,2])*as.numeric(coverage_distribution[,3]))/sum(as.numeric(coverage_distribution[,3]))),
									c("PCT_COV_x1", sum(coverage_distribution[coverage_distribution[,2]>=1,5])),
									c("PCT_COV_x5", sum(coverage_distribution[coverage_distribution[,2]>=5,5])),
									c("PCT_COV_x10", sum(coverage_distribution[coverage_distribution[,2]>=10,5])),
									c("PCT_COV_x30", sum(coverage_distribution[coverage_distribution[,2]>=30,5])),
									c("PCT_COV_x50", sum(coverage_distribution[coverage_distribution[,2]>=50,5])),
									c("PCT_COV_x80", sum(coverage_distribution[coverage_distribution[,2]>=80,5])))
if (paired == "true")									
	summaryTable = rbind(summaryTable,c(colnames(insert_size_summary)[1], insert_size_summary[1,1]),
									c(colnames(insert_size_summary)[5], insert_size_summary[1,5]))
if(markExist == "true")
	summaryTable = rbind(summaryTable,c(colnames(markStats)[9],markStats[1,9]))
colnames(summaryTable) = c("Metrics",sampleName)
write.table(summaryTable, file=summaryOut, sep="\t",col.names=T,row.names=F,quote=F)

### Truncating if specified ###
if(maxCov > 0)
	coverage_distribution = coverage_distribution[coverage_distribution[,2]<=maxCov,]

################
### plotting ###
################

setwd(output)
coverage_plot = qplot(coverage_distribution[2:nrow(coverage_distribution),2],weight=coverage_distribution[2:nrow(coverage_distribution),3],xlab="Coverage",main="Coverage Distribution") + geom_histogram(colour="black",fill="skyblue3")
if (paired == "true"){
	insert_size_plot = qplot(insert_size_metrics[,1],weight=insert_size_metrics[,2],xlab="Insert Size",main="Insert Size Distribution") + geom_histogram(colour="black",fill="skyblue3")
	ggsave(paste0(output,"/insert_size_plot.png"),plot=insert_size_plot)
}
quality_distribution_plot = qplot(quality_distribution_metrics[,1],weight=quality_distribution_metrics[,2],xlab="Quality Score",main="Quality Distribution") + geom_histogram(colour="black",fill="skyblue3")
ggsave(paste0(output,"/coverage_plot.png"),plot=coverage_plot)
ggsave(paste0(output,"/quality_distribution_plot.png"),plot=quality_distribution_plot)
png(paste0(output,"/coverage_cumulative_plot.png"))
	cumCov = 1-cumsum(coverage_distribution[,5])
	plot(coverage_distribution[2:nrow(coverage_distribution),2],cumCov[1:length(cumCov)-1], type='n', xlab = "Depth", ylab = "Fraction of bases \u2265 depth", main="Cumulative Coverage")
	abline(v = 30, col = "gray60")
	abline(v = 50, col = "gray60")
	abline(h = 0.5, col = "gray60")
	abline(h = 0.9, col = "gray60")
	axis(1, at=c(30,50), labels=c(30,50))
	axis(2, at=c(0.5,0.9), labels=c(0.5,0.9))
	points(coverage_distribution[2:nrow(coverage_distribution),2],cumCov[1:length(cumCov)-1], type='l',lwd=2,col="skyblue3")
dev.off()

#############################
### producing HTML report ###
#############################

p = openPage(paste0(output,"/index.html"),link.css = cssStyle)
hwrite(paste0("Alignment Quality Report for ",sampleName),p,center=T,heading=1, br=T)
hwrite("Alignment Statistics Summary",p,heading=2,br=T)
hwrite("The description of the measured statistics can be found ",p)
hwrite("here",p,link="http://picard.sourceforge.net/picard-metric-definitions.shtml#AlignmentSummaryMetrics",br=T)
hwrite(alignment_summary_metrics,p,center=T,row.bgcolor='#A8A8A8',br=T)
hwrite("Coverage Distribution",p,heading=2,br=T)
hwriteImage("coverage_plot.png",p,center=T,br=T,width=800,hight=800,image.border=1)
hwriteImage("coverage_cumulative_plot.png",p,center=T,br=T,width=800,hight=800,image.border=1)
if ( paired == "true"){
	hwrite("Insert Size Distribution",p,heading=2,br=T)
	hwrite(insert_size_summary[,1:8],p,center=T,row.bgcolor='#A8A8A8',br=T)
	hwriteImage("insert_size_plot.png",p,center=T,br=T,width=800,hight=800,image.border=1)
}
hwrite("Quality Score Distribution",p,heading=2,br=T)
hwriteImage("quality_distribution_plot.png",p,center=T,br=T,width=800,hight=800,image.border=1)
if (markExist == "true"){
	hwrite("Duplicates Statistics",p,heading=2,br=T)
	hwrite(markStats,p, center=T,row.bgcolor='#A8A8A8',br=T)
}
closePage(p)
