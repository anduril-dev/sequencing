
/*   collect alignment and coverage stats from a bam file */


def BamStats ( 
      bam: BAM ,
      refGenome: FASTA ,
      targets: BED = null,
      chrLength: CSV = null,
      markStats: BinaryFile = null, 
      picard: String = "",
      bedtools: String = "",
      memory: String  =  "4g" ,
      targeted: Boolean  = false,
      paired: Boolean  = true,
      stopAfter: Int  = 0,
      maxCoverage: Int  = 150,
      sampleName: String  =  "Sample" ): (BinaryFolder,CSV) = {        
    import org.anduril.runtime._
	val reportScript = INPUT(path=_functionFolder+"script.R")
	val cssStyle = INPUT(path = _functionFolder+"style.css")
	
	val markExist = (markStats != null)

	val picard_home = if (picard == "") getEnv("PICARD_HOME") else picard
	
	val bedtools_home = if (bedtools == "") getEnv("BEDTOOLS_HOME") else bedtools

	
	val coverage = 
		if (targeted == false){
		
			BashEvaluate(//run bedtools genomecov for whole-genome coverage
								var1 = bam,
								var2 = chrLength,
								param1 = maxCoverage.toString,
								script = bedtools+"/bedtools genomecov -ibam @var1@ -g @var2@ | grep ^genome > @out1@")
			
		}
		else {
		
			BashEvaluate(//run bedtools coverage for on-targets coverage
								var1 = bam,
								var2 = targets,
								script = bedtools+"/bedtools coverage -abam @var1@ -b @var2@ -hist | grep ^all > @out1@")
								
		}
	
	
	val alignmentSummary = BashEvaluate(//run picard CollectAlignmentSummaryMetrics
						var1 = bam,
						var2 = refGenome,
						param1 = stopAfter.toString,
						param2 = sampleName,
						param3 = memory,
						param4 = picard,
						script ="java -Xmx" + memory + " -jar " + picard + "/picard.jar CollectMultipleMetrics VALIDATION_STRINGENCY=SILENT " +
								"INPUT=@var1@ REFERENCE_SEQUENCE=@var2@ OUTPUT=@folder1@/@param2@ STOP_AFTER=@param1@ ASSUME_SORTED=true;" +
								"cd @folder1@;" + "rename \"s/@param2@\\.//\" *;" + "mv @folder1@/*.pdf @folder2@")
	
	val report = 
		if(markExist == false){
			
			BashEvaluate(var1 = alignmentSummary.folder1,
								var2 = coverage.out1,
								var3 = cssStyle,
								var4 = reportScript,
								param1 = sampleName,
								param2 = targeted.toString,
								param3 = maxCoverage.toString,
								param4 = markExist.toString,
								param5 = paired.toString,
								script = "R_LIBS=$R_LIBS:$ANDURIL_HOME/lang/r/R_LIBS Rscript @var4@ @var1@ @var2@ @folder1@ @var3@ @param1@ @param2@ @param3@ @param4@ @out1@ @param5@;")
		}
		else {	
			
			BashEvaluate(var1 = alignmentSummary.folder1,
								var2 = coverage.out1,
								var3 = cssStyle,
								var4 = reportScript,
								var5 = markStats,
								param1 = sampleName,
								param2 = targeted.toString,
								param3 = maxCoverage.toString,
								param4 = markExist.toString,
								param5 = paired.toString,
								script = "R_LIBS=$R_LIBS:$ANDURIL_HOME/lang/r/R_LIBS Rscript @var4@ @var1@ @var2@ @folder1@ @var3@ @param1@ @param2@ @param3@ @param4@ @out1@ @param5@ @var5@;")
		}
	report._filename("folder1", "html")
	report._filename("out1", "summary.csv")

	return (report.folder1,report.out1)
}
