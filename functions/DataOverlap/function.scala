/*  Merges and collapses 2 datasets with optional filtering and collapsing step */


def DataOverlap ( 
      annot1: CSV ,
      annot2: CSV , 
      pairedKeys: String = "",
      ColumnsNotNA: String = "",
      ColumnsNum: Int  = 1,
      collapseBy: String = "" ): CSV = {       

    val Union = REvaluate(
            script = INPUT(path="mergePairedColumns.r"),
            table1 = annot1,
            table2 = annot2,
            param1 = pairedKeys,
            param2 = ColumnsNotNA,
            param3 = ColumnsNum.toString
            )

    if (ColumnsNotNA != "") {
        OUTPUT(Union.outArray("allData"))
    }

    if (collapseBy != "") { // Collapse dataset

        val Union_collapsed = ExpandCollapse(
            in = Union.table,
            listCols = collapseBy,
            expand = false
            )

        Union_collapsed.out

    } else { // Do not collapse dataset
        Union.table
    }

}
