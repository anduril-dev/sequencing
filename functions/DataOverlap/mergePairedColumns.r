# Extract pairedKeys such that columns are strictly annot1=annot2
pairedKeys <- do.call(rbind,strsplit(unique(unlist(strsplit(gsub("[[:blank:]]","",param1),","))),"="))

# Include intersecting columns
if (length(intersect(colnames(table1),colnames(table2))) > 0) {
    pairedKeys2 <- intersect(colnames(table1),colnames(table2))
    pairedKeys2 <- matrix(pairedKeys2,ncol=2,nrow=length(pairedKeys2))
}

# MERGE AND REMOVE DUPLICATES
pairedKeys <- rbind(pairedKeys,pairedKeys2)
if (length(which(duplicated(pairedKeys[,1]))) > 0) {
    pairedKeys <- pairedKeys[-which(duplicated(pairedKeys[,1])),]
}
if (nrow(pairedKeys) == 0) {
    write.error("No columns to merge. Please provide valid pairedKeys values and/or make sure both datasets have the same column name for the columns to be merged.")
    return(1)
    }
mergedDB <- merge(table1,table2,by.x=pairedKeys[,1],by.y=pairedKeys[,2],all.x=TRUE,all.y=TRUE)

# If filter information provided, create separate, filtered table (i.e. certain number of columns are not NA)
if (param2 != "") {
    toFilter <- unlist(strsplit(gsub("[[:blank:]]","",param2),","))
    NAs <- !is.na(matrix(mergedDB[,toFilter],ncol=length(toFilter)))
    if (is.numeric(try(as.numeric(param3)))) {
        filterOut <- which(rowSums(NAs) < as.numeric(param3))
    }
    if (param3 == "") {
        param3 <- 1 # At least one column must not be NA
    }
    if (length(filterOut) == 0 | !is.numeric(try(as.numeric(param3)))) {
        write.error("No rows filtered out. Please check parameters. ColumnsNum must be numeric.")
        return(1)
    } else {
        table.out <- mergedDB[-filterOut,]
        array.out <- list(allData = mergedDB, filteredData = table.out)
    }
} else {
    table.out <- mergedDB
}
