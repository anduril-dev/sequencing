# Format gff3 file to match hairpin fasta file (i.e. Ensembl Transcript ID
# first header content) to align and quantify expression properly with
# htseq-count

allsWell = TRUE
conversn <- table1
conversn <- conversn[which(conversn[,2] != ""),]


# retrieve reference transcriptome and extract Ensembl IDs
print(getwd())
hairpins <- read.csv(param2,header=FALSE,stringsAsFactors=FALSE, sep=" ")
hairpins <- gsub(">","",hairpins[,1])

keep <- c()
for (c in 1:nrow(conversn)) {
    if (length(grep(conversn[c,1],hairpins)) > 0) {
        keep <- c(keep,1)
    } else {
        keep <- c(keep,0)
    }
}
conversn <- conversn[which(keep == 1),]

# Load GFF file with comments removed and reduce to transciptome set
gff <- read.csv(param1, header=FALSE, sep="\t", comment.char="#", quote = "\"",stringsAsFactors=FALSE)

for (n in 1:nrow(gff)) {
    # extract description and split into a list of strings
    desc <- strsplit(as.character(gff[n,9]),";")
	
    # extract either the miRBase ID (if miRNA_primary_transcript) or the Derives_from (if miRNA)
    ID <- c()
    if (gff[n,3] == "miRNA_primary_transcript") {
    	ID <- strsplit(strsplit(desc[[1]][1],"=")[[1]][2],"_")[[1]][1]
    } else if (gff[n,3] == "miRNA") {
    	ID <- strsplit(strsplit(desc[[1]][4],"=")[[1]][2],"_")[[1]][1]
	} else if (gff[n,3] == "exon") {
    	ID <- strsplit(desc[[1]][6]," ")[[1]][3]
    } else {
    	allsWell <- FALSE
    	print("[ERROR] Input is not a proper GFF file.")
    	break
    }

    # move chr ID to end of reference
    desc[[1]][length(desc[[1]])+1] <- paste("Chromosome",gsub("chr","",as.character(gff[n,1])),sep="=")

    # Save changes to description
    gff[n,9] <- paste(desc[[1]],collapse=";")

    # Look up Ensembl transcript ID using miRBase transcript ID
    found <- which(conversn[,3] == ID)
    if (length(found) > 1) {
    	temp <- unlist(conversn[found,1])
    	gff[n,1] <- as.character(temp[1])
	    for (id in 2:length(temp)) {
	        toBind <- gff[n,]
	        toBind[,1] <- as.character(temp[id])
	        gff <- rbind(gff,toBind)
	    }
    } else if (length(found) == 0) {
    	gff[n,1] <- NA
    } else {
        gff[n,1] <- as.character(conversn[found,1])
    }

    if (allsWell == FALSE) { break }
}

# Separate those with Ensembl Transcript IDs from those that don't.
not.in.miRBase <- gff[which(is.na(gff[,1])),]
gff <- gff[which(!is.na(gff[,1])),]

# Adjust miRNA positions relative to transcript postions
transIDs <- unique(gff[,1])
transIDs <- transIDs[grep("ENST",transIDs)]
for (id in transIDs) {
    transcript <- gff[which(gff[,1] == id),]
    matures <- transcript[which(transcript[,3] == "miRNA"),]
    transcript <- transcript[which(transcript[,3] == "miRNA_primary_transcript"),]
    ensembl <- conversn[grep(id,conversn[,1]),]

    # Find which miRbase ID corresponds to the specific transcript location in ensembl
    transID <- which(transcript[,4] >= unique(ensembl[,5]) & transcript[,5] <= unique(ensembl[,6]))

    if (length(transID) == 0) { # miRNA is in a transcript cluster
        transcript[,1] <- NA
        matures[,1] <- NA
    } else {
        transcript[transID,4] <- transcript[transID,4] -  unique(ensembl[,5])+1
        transcript[transID,5] <- transcript[transID,5] - unique(ensembl[,5])+1
        matID <- which(matures[,4] >= unique(ensembl[,5]) & (matures[,5] <= unique(ensembl[,6])))
        if (length(matID) != 0) {
            matures[matID,4] <- matures[matID,4]  -  unique(ensembl[,5])+1
            matures[matID,5] <- matures[matID,5] -  unique(ensembl[,5])+1
            if (nrow(transcript) - length(transID) != 0) {
                transcript[-transID,1] <- NA
            }
            if (nrow(matures) - length(matID) != 0) {
                matures[-matID,1] <- NA
            }
        } else {
                matures[,1] <- NA
        }
    }
    
    # If multiple transcripts
#    if (nrow(transcript) > 1) {
	# Completely strip attributes
#	mID <- unlist(strsplit(unlist(strsplit(transcript[,9],";")),"="))
	# Find the miRBase IDs
#	mID <- mID[which(mID == "ID")+1]
	# For each miRBase transcript
#	for (x in 1:length(mID)) {
#	    mtr <- grep(mID[x],matures[,9])
#	    matures[mtr,4] <- matures[mtr,4] - transcript[x,4] + 1
#	    matures[mtr,5] <- matures[mtr,5] - transcript[x,4] + 1
#	    transcript[x,5] <- transcript[x,5] - transcript[x,4] + 1
#	    transcript[x,4] <- 1 
#	}
#    } else {
    # Recalculate start and end positions relative to transcript
#      for (m in 1:nrow(matures)) {
#	    matures[m,4] <- matures[m,4] - transcript[1,4]+1
#	    matures[m,5] <- matures[m,5] - transcript[1,4]+1 
#        }

    # Recaclulate start and end position of transcript
#	transcript[1,5] <- transcript[1,5] - transcript[1,4] + 1
#	transcript[1,4] <- 1
#    }

    # Replace values
    gff[which(gff[,1] == id),] <- rbind(transcript,matures)
}

# Create output directory
outdir=get.output(cf,'optOut1')
dir.create(outdir)

gff <- gff[which(!is.na(gff[,1])),]
write.table(not.in.miRBase,paste(outdir,"/hsa_noEnsemblID.gff",sep=""),quote=FALSE,sep="\t",row.names=FALSE,col.names=FALSE)
write.table(gff,paste(outdir,"/hsa_annotatable.gff",sep=""),quote=FALSE,sep="\t",row.names=FALSE,col.names=FALSE)

table.out <- data.frame()
rm(optOut1)
