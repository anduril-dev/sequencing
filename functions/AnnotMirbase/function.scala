def AnnotMirbase (
      reference_hairpin: String = "",
      annotation_mirbase: String = "",
      ensembl_host: String  =  "feb2014.archive.ensembl.org" ,
      ensembl_dataset: String  =  "hsapiens_gene_ensembl"  ): (CSV) = {

/* modify a miRBase GFF3 file to reference Ensembl transcript IDs */

       val transcripts = REvaluate( // Pull all known EnsemblIDs mapped to miRBase IDs
          script = INPUT(path="biomart.r"),
          param1 = "ensembl_transcript_id,mirbase_id,mirbase_accession",
          param2 = ensembl_host,
          param3 = ensembl_dataset
          )

        val transcripts_clean = CSVCleaner( // Rename files for easier reference
            in = transcripts.table,
            skipQuotes = "*",
            rename = "ensembl_transcript_id=Ensembl_Transcript_ID,mirbase_id=miRBase_ID,mirbase_accession=miRBase Accession_ID,chromosome_name=chr,transcript_start=start_pos,transcript_end=end_pos"
            )

        val annotate_miRBase = REvaluate( // Replace chromosome, start and end positions with known Ensembl transcripts and, for mature sequences, relative positions inside transcripts
            script = INPUT(path="ref_hairpin_GFFchrNposBYtransID.r"),
            table1 = transcripts_clean.out.force(),
            param1 = annotation_mirbase,
            param2 = reference_hairpin
            )

        val annotate_miRBase_array = Folder2Array(
            folder1 = annotate_miRBase.optOut1,
            filePattern = "hsa_annotatable.gff"
        )

        val annotate_miRBase_csv = Array2CSV( // Create a CSV file with a single reference row containing the new annotation file
            in = annotate_miRBase_array.out
            )

        return annotate_miRBase_csv.out
}
