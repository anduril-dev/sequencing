def VariantCaller (
      reference: FASTA ,
      bam1: BAM = null,
      bam2: BAM = null,
      bams: BAMList = null,
      intervals: BED = null,
      dbsnp: VCF = null,
      caller: String  =  "gatk" ,
      varscan: String = "",
      gatk: String = "",
      callIndels: Boolean  = true,
      variantsOnly: Boolean  = true,
      memory: String  =  "4g" ,
      threads: Int  = 1,
      region: String  =  "all" ,
      exclude: String  =  "none" ,
      options: String = "",
      samOptions4VS: String = "" ): (VCF,VCF,TextFile) =
{

    // ------------------------------------------------------------
    // General setings.
    // ------------------------------------------------------------

    import org.anduril.runtime._

    var _gatk = gatk
    if (gatk == "") _gatk = getEnv("GATK_HOME")

    var _varscan = varscan
    if (varscan == "") _varscan = getEnv("VARSCAN_HOME") + "/VarScan.jar"

    var _caller = caller
    if (bams != null) _caller = "gatk"

    var _options = options

    // ------------------------------------------------------------
    // Run appropriate variant _caller.
    // ------------------------------------------------------------

    if (_caller == "samtools") {

        // ------------------------------------------------------------
        // Run Samtools, Bcftools and Vcfutils to call variants.
        // ------------------------------------------------------------

        if (_options != "") _options=" " + _options
        var samOptions = _options // _options parameter is only used in samtools
        var bcfOptions=""
        if (!callIndels) samOptions=samOptions + " -I"
        if (variantsOnly) bcfOptions=bcfOptions + " -v"
        
        val samtools = BashEvaluate(
                var1=reference,
                var2=bam1,
                script="samtools mpileup" + samOptions + " -uf @var1@ @var2@ | " +
                    "bcftools view " + bcfOptions + " -cg - | vcfutils.pl varFilter -D200 > " +
                    "@out1@"
                )
        samtools._filename("out1", "calls.vcf")

        return ( samtools.out1, samtools.out2, samtools.out3 )

    } else if (_caller == "gatk") {

        // ------------------------------------------------------------
        // Run GATK UnifiedGenotyper using known dbsnp when given.
        // ------------------------------------------------------------

       	var bam_files=""
        var regionStr=""
        var excludeStr=""

        if (_options != "") _options=" " + _options
        if (!variantsOnly) _options=_options + " -out_mode EMIT_ALL_CONFIDENT_SITES"
        if (callIndels) _options=_options + " -glm BOTH"
        if (intervals != null) _options=_options + " -L @var2@"
        if (dbsnp != null) _options=_options + " --dbsnp @var3@"
        if (bam1 != null) bam_files=bam_files + "-I @var4@ "
        if (bams != null) bam_files=bam_files + "-I @var5@ "
        if (region != "all") { 
            regionStr="-L " + region
            regionStr=regionStr.replaceAll(","," -L ")
            regionStr=regionStr + " "
        }
        if (exclude != "none") { 
            excludeStr="-XL " + exclude
            excludeStr=excludeStr.replaceAll(","," -XL ")
            excludeStr=excludeStr + " "
        }
        
        val gatkC = BashEvaluate(
                var1=reference,
                var2=intervals,
                var3=dbsnp,
                var4=bam1,
                var5=bams,
                script="java -Xmx" + memory + " -jar " + _gatk +
                    " -R @var1@ " + bam_files + "-T UnifiedGenotyper" +
                    " --num_threads " + threads + _options + " -o @out1@ " +
                    regionStr + excludeStr + "-metrics @out3@"
                )
        gatkC._filename("out1", "calls.vcf")
        gatkC._filename("out3", "metrics.txt")

        return ( gatkC.out1, gatkC.out2, gatkC.out3 )

    } else if (_caller == "varscan") {

        // ------------------------------------------------------------
        // Run VarScan on one sample or on paired samples.
        // ------------------------------------------------------------

        _options=_options + " --output-vcf 1"
        var samOptions = ""
        if (samOptions4VS != "") samOptions = " " + samOptions4VS else samOptions=" -q 1"
        if (!callIndels) samOptions=samOptions + " -I"

        var varscanC: BashEvaluate = null
        if (bam2 == null) { // Call germline variants
        
            val method = if (!callIndels) " mpileup2snp" else " mpileup2cns"
            
            val pileup = BashEvaluate( // Make pileup
                var1=reference,
                var2=bam1,
                script="samtools mpileup" + samOptions +" -f @var1@ @var2@ > @out1@"
                )
            pileup._filename("out1", "reads.pileup")
            
            varscanC = BashEvaluate( // Call germline variants
                var1=pileup.out1,
                script="java -jar " + _varscan + method + " @var1@ " + _options +
                    " > @out1@"
                )
            varscanC._filename("out1", "calls.vcf")

        } else { // Call somatic variants
            
            val normPileup = BashEvaluate( // Make normal pileup
                var1=reference,
                var2=bam1,
                script="samtools mpileup" + samOptions +" -f @var1@ @var2@ > @out1@"
                )
            normPileup._filename("out1", "normal.pileup")
            
            val tumorPileup = BashEvaluate( // Make tumor pileup
                var1=reference,
                var2=bam2,
                script="samtools mpileup" + samOptions +" -f @var1@ @var2@ > @out1@"
                )
            tumorPileup._filename("out1", "tumor.pileup")
                        
            varscanC = BashEvaluate( // Call somatic variants
                var1=normPileup.out1,
                var2=tumorPileup.out1,
                script="java -jar " + _varscan + " somatic @var1@ @var2@ @folder1@/calls " + _options + "; " +
                    "mv @folder1@/calls.snp.vcf @out1@; " +
                    "mv @folder1@/calls.indel.vcf @out2@"
                )
            varscanC._filename("out1", "snps.vcf")
            varscanC._filename("out2", "indels.vcf")
        }
        
        return ( varscanC.out1.force(), varscanC.out2.force(), varscanC.out3.force() )
    }
    else sys.error("Unsupported caller.")
}
