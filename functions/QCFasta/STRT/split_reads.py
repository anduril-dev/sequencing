#!/usr/bin/python
import os
import sys
import gzip

def assure_path_exists(path):
        tar = os.path.dirname(path)
        if not os.path.exists(tar):
            os.makedirs(tar)

def myopen(myfile, mode):
    if myfile.endswith('.gz'):
        return gzip.open(myfile, mode)
    else: 
        return open(myfile, mode)

filein = sys.argv[1]
UMIs_out= sys.argv[2]
Tseqs_out = sys.argv[3]
invalid_out = sys.argv[4]  # reads with out valid UMI tagged (no GGGs following)
UMIlen = int(sys.argv[5].strip('"'))
temSwitPrimer = sys.argv[6]  # minimal template swtiching primer after UMIs, default is "GGG"
stat_out = sys.argv[7]

assure_path_exists(UMIs_out)
assure_path_exists(Tseqs_out)
readfile = myopen(filein, 'r')
UMIs = myopen(UMIs_out,'w')
Tseqs = myopen(Tseqs_out, 'w')
invalidReads = myopen(invalid_out,'w')
stat = open(stat_out, 'w')
print >> stat, "Input_Reads\tSurviving_Percentage\tDropped"
t = 0 # number of inout reads
d = 0 # number of discarded reads
i = 0
valid = True
for line in readfile:
    i = i + 1
    if i==1:
        ID = line
    elif i==2:
        if line[UMIlen:UMIlen+int(len(temSwitPrimer))]==temSwitPrimer: 
            UMI = line[0:UMIlen]
            Tseq = line[UMIlen:].lstrip('G')
            ID = ID.split(' ')
            ID = ID[0]+"_"+UMI+' '+ID[1]
        else:
            valid = False
            invalidSeq = line
    elif i==3:
        plus = line
    elif i==4:
        if valid:
            UMIQ = line[0:UMIlen]
            TseqQ = line[len(line)-len(Tseq):].strip()
        else:
            invalidQ = line.strip()
        i=0
    if i==0:
        if valid:
            print >> UMIs, ID+UMI+'\n'+plus+UMIQ
            print >> Tseqs, ID+Tseq+plus+TseqQ
        else:
            print >> invalidReads, ID+invalidSeq+plus+invalidQ
            d += 1
        valid = True
        t += 1
print >> stat, str(t)+'\t'+str(float(t-d)*100/t)+'\t'+str(d)
#print "Input_Reads\tSurviving_Percentage\tDropped\n"+str(t)+'\t'+str(float(t-d)/d)+'\t'+str(d)
UMIs.close()
Tseqs.close()
invalidReads.close()
readfile.close()
