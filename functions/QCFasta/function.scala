import anduril.builtin._
import anduril.microarray._
import anduril.sequencing._
import anduril.tools._
import anduril.anima._
import org.anduril.runtime._

  /*
   * Quality control function for fastq files
   * statistics and quality and tag trimming
   */

  def QCFasta(
  reads: FASTQ ,
  mates: FASTQ = null,
  fastQCfolders: BinaryFolder = null,
  adapter: FASTA = null,
  tool: String = "trimmomatic" ,
  qual: String = "" ,
  minQuality: Int = 20,
  minLength: Int = 20,
  percent: Double = 30,
  stringency: Int = 2,
  headcrop: Int = 0,
  extra: String = "",
  trailing: Int = 30,
  slidingWindow: String = "null" ,
  adapterSeq: String = "",
  gzip: Boolean = false,
  readKey: String = "read" ,
  mateKey: String = "mate" ,
  simpleClip: Int = 12,
  palindromeClip: Int = 12, 
  crop: Int = -1,
  minPercent: Int = 20,
  keepBothReads: Boolean = false,
  isSinglecell: Boolean = false,
  umiLen: Int = 6,
  umiSliding: String = "1:17",
  temSwitPrimer: String = "GGG",
  threads: Int = 2 ): (FASTQ,FASTQ,HTML,CSV,FASTQ,FASTQ) =
  {

    import anduril.builtin._
    import anduril.microarray._
    import anduril.sequencing._
    import anduril.tools._
    import org.anduril.runtime._

    /* Steps in QCFasta
     * 1. Check initial qc or receive FastQC folders
     * 2. Trim
     * 3. Filter out low quality samples
     * 4. Make report
     */

    //Outputs
    val trimmedReads = NamedMap[FASTQ]("trimmedReads")
    val trimmedMates = NamedMap[FASTQ]("trimmedMates")
    val unpairedReads = NamedMap[FASTQ]("unpairedReads")
    val unpairedMates = NamedMap[FASTQ]("unpairedMates")

	val filteredReads = NamedMap[FASTQ]("filteredReads")
	val umiFilterStats = NamedMap[BinaryFile]("umiFilterStats")
    val trimmedArrays = NamedMap[FASTQ]("trimmedArrays")
    val stats = NamedMap[CSV]("stats")
    val filter = NamedMap[BinaryFolder]("filter")
    val folders = NamedMap[BinaryFile]("folders")
    val bySample = NamedMap[BinaryFile]("bySample")
    val status = NamedMap[CSV]("status")
    val qcWarnings = NamedMap[HTMLFile]("qcWarnings")
    val statusString = scala.collection.mutable.Map[String,String]()
    val statusCSV = NamedMap[CSV]("statusCSV")
  
    val isPaired = (mates != null)

    /* Step 1: Check QC 
     * Run FastQC if fastqcfolders were not provided
     * Gather the file with the statistics and the image folder from our own run of fastqc or from the fastqcfolders if they were provided
     * Guess quality encoding if not provided in qual parameter
     */

    val rawStats = CheckQC( reads = reads,
                            mates = mates, 
                            fastQCfolders = fastQCfolders,
                            qual          = qual,
                            isPaired      = isPaired,
                            readKey       = readKey,
                            mateKey       = mateKey,
                            threads       = threads)

    val preImages= rawStats._1
    val encoding = rawStats._2
    val warnings = rawStats._3    	
       
	//////////////////////////////////////////
    // For STRT: preFiltering based on umis.//
    //////////////////////////////////////////

    if (isSinglecell){
        for ( (k,v) <- iterArray(reads) ) {
			withName(k){
            val preFilter = umiFilter( read = reads(k),
                	                   sampleName = k,
                    	               umiLen = umiLen,
                        	           umiSliding = umiSliding,
                            	       temSwitPrimer = temSwitPrimer,
                                	   qual = encoding(k))

            filteredReads(k) = preFilter._1
            umiFilterStats(k) = preFilter._2
       		}
		}
        val umiSurvivingCSV = CSVJoin(in = umiFilterStats, useKeys=false)
	    /*
        val formatUMISurvivingCSV = CSVCleaner(in    = umiSurvivingCSV,
                                           	   numberFormat= "Surviving_Percentage=#0.00",
                                           	   rename      = "Input_Reads=Input_Reads_UF,Surviving_Percentage=Surviving_UF %, Dropped=Dropped_UF")        
		*/
	}else{
        for ( (k,v) <- iterArray(reads) ) {
            filteredReads(k) = reads(k)
        }
    	}
	
                 
    /* Step 2: Trim
     * Trim with either Trimmomatic, TrimGalore or Fastx
     * return quality figures already pasted together when using paired end reads
     * decide if sample passed quality filtering
     * paste together before and after qc figures
     */

    var totCount = 0

    for ( (k,v) <- iterArray(filteredReads) )
    { 

      val mate = if (isPaired) mates(k) else null

      withName(k)
      {

        // Performs quality trimming and checks if enough sequences survived the trimming step
        val trimmed = TrimQC(read          = filteredReads(k),
                                            mate          = mate,
                                            adapter       = adapter,
                                            isPaired      = isPaired,     //false for single, true for paired-end data
                                            headcrop      = headcrop,     //clipR1 and clipR2 in trim Galore
                                            minQuality    = minQuality,   //trimGalore; in Trimmomatic = leading
                                            trailing      = trailing,      //only Trimmomatic
                                            slidingWindow = slidingWindow,//only Trimmomatic
                                            minLength     = minLength,
                                            extra         = extra,        //only trimGalore
                                            qual          = encoding(k),
                                            stringency    = stringency,   //only trimGalore
                                            adapterSeq    = adapterSeq,
                                            tool          = tool,
                                            sampleName    = k,
                                            gzip          = gzip,
                                            simpleClip    = simpleClip,
                                            palindromeClip = palindromeClip,
                                            crop          = crop,
                                            minPercent    = minPercent,
                                            percent       = percent,
                                            keepBothReads = keepBothReads, //only Trimmomatic
                                            threads       = threads)
      
        trimmedArrays(k) = trimmed._1 
        stats(k) = trimmed._2
        status(k) = trimmed._3 //file with passed or failed depending on number of surviving reads and percent parameter
      }
      totCount = totCount+1
    }
    
    for ( (k,v) <- iterArray(reads) ) 
    { 
      statusString(k) = status(k).textRead() 
      statusCSV(k) = StringInput(content="Sample\tStatus\n"+k+"\t"+statusString(k))
    }

    var survCount = 0

    for ( (k,v) <- iterArray(reads) )
    {
      withName(k) 
      {
        //only samples with enough reads get stats again
        filter(k) = if (statusString(k)=="passed")
        {
        
          survCount = survCount+1
          trimmedReads(k) = trimmedArrays(k)("Reads")
          trimmedMates(k) = if (isPaired) trimmedArrays(k)("Mates") else null
          unpairedReads(k) = if (isPaired) trimmedArrays(k)("Ureads") else null
          unpairedMates(k) = if (isPaired) trimmedArrays(k)("Umates") else null
          
          val post = FastQC(read = trimmedReads(k),
                            mate = trimmedMates(k),
                            clean= false)
          post._custom("cpu")= threads.toString
         
          val mateWarns = if (isPaired) post.summary("mate_warnings") else null
          
          qcWarnings(k) = PrepWarnings(post.summary("read_warnings"),mateWarns,isPaired) 

          val readsFolder = QuickBash(in = post.folder,
                                    script = """set -e
                                                r=$( ls -d $in/*eads_fastqc/Images/ )
                                                ln -s $r $out""").out

          val matesFolder = if (isPaired) 
          {
            QuickBash(in = post.folder, 
                      script = """set -e
                                  m=$( ls -d $in/*ates_fastqc/Images/ )
                                  ln -s $m $out""").out

          } else null

          //put images of trimmed reads on top of each other if paired
          val pairPostImages = ConvertImages(folder1 = readsFolder, folder2 = matesFolder)

          //put images of trimmed reads side by side (raw and trimmed reads images)
          val joinPrePost = ConvertImages(folder1 = preImages(k), folder2 = pairPostImages, doConvert=false)

          joinPrePost
        } else 
        { 
          qcWarnings(k)=warnings(k)
          preImages(k)
        }
      }
    }
	
    val annotationBySample= NamedMap[CSV]("annotationBySample")

    for ( (k,v) <- iterArray(filter) ) 
    { 
        folders(k) = Folder2Array(filter(k),keyMode="filename").out

	  val captionTextBySample = if (isPaired) 
    {
        "Top-left: Reads before trimming. Top-right: Reads after trimming. Bottom-left: Mates before trimming. Bottom-right: Mates after trimming."        
	  } else "Left: Reads before trimming. Right: Reads after trimming."
        
      annotationBySample(k)=QuickBash(in=filter(k),
                                      script="""tsvecho File Annotation > $out
                                                for f in $in/*; do
                                                  f=$( basename $f )
                                                  tsvecho "$f" "$f. """+captionTextBySample+""""
                                                done >> $out""").out

        bySample(k) = ImageGallery(inRoot = filter(k), csvRoot=annotationBySample(k), annotationCol="Annotation").out
    }
    /*
     * Step 3. Make report
     * Make bar plot
     * Make image galleries by figure type
     * Put everything in webpage
     */

    val infoString = StringInput(content="<script>$( document ).ready(function() { showimage(0);  });</script>")
    val galleryFolderArray = NamedMap[BinaryFolder]("galleryFolderArray")
    val list=StringInput(content="Key\tFile\nper_base_quality_png\tper_base_quality.png\nper_base_sequence_content_png\tper_base_sequence_content.png\nadapter_content_png\tadapter_content.png\nper_sequence_quality_png\tper_sequence_quality.png\nper_base_n_content_png\tper_base_n_content.png\nduplication_levels_png\tduplication_levels.png\nper_sequence_gc_content_png\tper_sequence_gc_content.png\nsequence_length_distribution_png\tsequence_length_distribution.png\nper_tile_quality_png\tper_tile_quality.png\nkmer_profiles_png\tkmer_profiles.png")

    for ( row <- iterCSV(list) )
    {
      withName(row("Key"))
      {
        //one entry per sample, but in each loop only saves same image type (ex. per_base_quality_png)
        val perTypeImages = NamedMap[BinaryFile]("myImages")

        for ( (k,v) <- folders) { perTypeImages(k) = folders(k)(row("Key")) }

        val perTypeImagesFolders = Array2Folder(makeArray(perTypeImages)).out

	      val captionTextByType = if (isPaired){
		      "Top-left: Reads before trimming. Top-right: Reads after trimming. Bottom-left: Mates before trimming. Bottom-right: Mates after trimming."        
	      } else {
        	"Left: Reads before trimming. Right: Reads after trimming."
        }

	      val annotationByType=QuickBash(in=perTypeImagesFolders,
                                        script="""tsvecho File Annotation > $out
                                                	for f in $in/*; do
                                        	          f=$( basename $f )
                                                  	tsvecho "$f" "$f. """+captionTextByType+""""
                                                  	done >> $out """).out

        galleryFolderArray(row("Key")) = ImageGallery(inRoot=perTypeImagesFolders, csvRoot=annotationByType, annotationCol="Annotation", infoRoot=infoString).out
     
      }
    }
  
    val myTmpArray = ArrayConstructor(file1 = galleryFolderArray("per_base_quality_png"),
                                      file2 = galleryFolderArray("per_base_sequence_content_png"),
                                      file3 = galleryFolderArray("adapter_content_png"),
                                      file4 = galleryFolderArray("per_sequence_quality_png"),
                                      file5 = galleryFolderArray("per_base_n_content_png"),
                                      file6 = galleryFolderArray("duplication_levels_png"),
                                      file7 = galleryFolderArray("per_sequence_gc_content_png"),
                                      file8 = galleryFolderArray("sequence_length_distribution_png"),
                                      file9 = galleryFolderArray("per_tile_quality_png"),
                                      file10= galleryFolderArray("kmer_profiles_png"),
                                      key1 = "1.  Per base quality figures",
                                      key2 = "2.  Per base sequence content figures",
                                      key3 = "3.  Adapter Content",
                                      key4 = "4.  Per sequence quality figures",
                                      key5 = "5.  Per base N content figures",
                                      key6 = "6.  Duplication levels figures",
                                      key7 = "7.  Per sequence gc content figures",
                                      key8 = "8.  Sequence length distribution figures",
                                      key9 = "9.  Per tile sequence content figures",
                                      key10= "10. Kmer profiles")

    val barPlotScript = StringInput(content="""
library(RColorBrewer)
library(ggplot2)
library(reshape)
library(cowplot)

cols <- rev(brewer.pal(4,"Spectral"))

# merge the array files and add sample names in the first column
x <- data.frame(Sample=names(array),do.call(rbind, array))

# order by number of reads
x[,1] <- reorder(x[,1],-x[,2])

# use shorter names 
if(ncol(x)==6)
    colnames(x)[5:6] <- c("Forward_Only","Reverse_Only")

# plot absolute numbers
melted <- melt(x[,-2])
g1 <- ggplot(melted) + geom_bar(aes_string(x = "Sample", y = "value", fill = "variable"),color = "black",size=0.1, stat = "identity", position = "stack") + ylab("Number of reads") + theme_bw() + theme(panel.grid.major=element_blank(),strip.background=element_blank(),panel.border=element_blank(),panel.background=element_blank(),axis.text.x=element_blank(), legend.position="none") +  scale_fill_manual(values=cols)

# calculate percentages and use the same order as in absoulte number
percentages <- data.frame(Sample=x$Sample, x[,-c(1,2)]/x[,2])
percentages[,1] <- reorder(percentages[,1],-x[,2])
percentages[,-1] <- round(percentages[,-1]*100,3)

# plot percentages
meltedPer <- melt(percentages)
g2 <- ggplot(meltedPer) + geom_bar(aes_string(x = "Sample", y = "value", fill = "variable"),color = "black",size=0.1, stat = "identity", position = "stack") + ylab("Reads %") + theme_bw() + theme(panel.grid.major=element_blank(),strip.background=element_blank(),panel.border=element_blank(),panel.background=element_blank(),axis.text.x=element_blank(), legend.position="bottom", legend.title=element_blank()) +  scale_fill_manual(values=cols)

# put plots together
g <- plot_grid(g1,g2,labels=c("A","B"),ncol=1,nrow=2)

# output figure to optOut1
setwd(document.dir)
ggsave(g,file="plot.png",width = 6, height = 9, units = "in", dpi=100)

# output absolute numbers to table.out
table.out <- x

# output percentages to optOut2
write.table(percentages,file=get.output(cf,"optOut2"),col.names=T,row.names=F,sep="\t",quote=F) 
#rm(optOut1)
rm(optOut2)""")


    val barPlot = REvaluate(inArray = stats, script = barPlotScript)

    val renamePercentages = if (isPaired) CSVFilter(in = barPlot.optOut2, rename = "Discarded=Discarded_%,Output_Reads=Output_Reads_%,Forward_Only=Forward_Only_%,Reverse_Only=Reverse_Only_%") else CSVFilter(in = barPlot.optOut2, rename = "Discarded=Discarded_%,Output_Reads=Output_Reads_%")

    val joinStats = CSVJoin(in1 = barPlot.table, in2 = renamePercentages.out)
    val joinStatsEdited = REvaluate(script=StringInput(content="""
    							if (ncol(table1) == 10){
								  cols <- 2:6
							} else {
								  cols <- 2:5
							}

							table.out <- table1

							for(i in 1:length(cols)){
							  table.out[,cols[i]] <- prettyNum(table1[,cols[i]], big.mark=",",scientific=FALSE)
							}
    							"""),
 			                               table1=joinStats.out)

    var failCount=totCount-survCount
    val survivingHTML = HTMLTable(in=joinStatsEdited.table, title="Number of samples processed: "+totCount+";        Number of samples discarded: "+failCount)

    val warnFolder = Array2Folder(makeArray(qcWarnings))

    val addFiles = QuickBash( in    = makeArray(barPlot.document,
                                          SimpleWebPage(Array2Folder(myTmpArray.out).out,title = "Quality Control Plots Before and After Trimming",index = "index.html"),
                                          SimpleWebPage(content = survivingHTML),
                                          SimpleWebPage(warnFolder.out, title = "Warnings", index = "index.html"),
					  //ImageGallery(inRoot=warnFolder.out),
                                          SimpleWebPage(Array2Folder(makeArray(bySample)),title = "Quality Control Plots by Sample",index = "index.html")),
                              script= """mkdir $out; cd $out; cp "$key1"/*png .; cp -r "$key2" "Quality Control Plots Before and After Trimming"; cp -r "$key3" "Summary Table"; cp -r "$key4" Warnings; cp -r "$key5" "Quality Control Plots by Sample" """)

    val content =  StringInput(content="<img src=plot.png>")

    val report = SimpleWebPage( in      = addFiles,
                                content = content,
                                title   = "Quality control results",
                                index   = "index.html")

    val statusAll = CSVListJoin(in=statusCSV,fileCol="").out

    val statsOut = CSVJoin(in1 = joinStatsEdited.table, in2 = statusAll)

    val outReads = makeArray(trimmedReads)
    val outMates = if (isPaired) makeArray(trimmedMates) else outReads
	
	val outUnpairedReads = if (isPaired) makeArray(unpairedReads) else outReads
	val outUnpairedMates = if (isPaired) makeArray(unpairedMates) else outReads

    return(outReads,outMates,report.out,statsOut.out,outUnpairedReads,outUnpairedMates)
  }
// End of main code

  /* CheckQC
   *
   */
  def CheckQC(reads: FASTQ,
              mates: FASTQ = null,
              fastQCfolders: BinaryFolder = null,
              qual: String="", isPaired: Boolean, readKey: String="read", mateKey: String="mate", threads: Int): (NamedMap[CSV],scala.collection.mutable.Map[String,String],NamedMap[HTMLFile]) =
  {

    val txtStatsArray = NamedMap[TextFile]("txtStatsArray")
    val imagesStatsArray = NamedMap[BinaryFolder]("imagesStatsArray")
    val qualEncodings = scala.collection.mutable.Map[String, String]()
    val qcWarnings = NamedMap[HTMLFile]("qcWarnings")

    //summary is an array with keys "read" and if paired "mate" that point to the text file with the stats produced by fastqc
    //preStats is an array with keys "read" and if paired "mate" that point to the image folders produced by fastqc
    val (summary, preStats, warnings) = if (fastQCfolders == null) 
    {
      //were here if we have to run fastqc ourselves
      for ( (k,v) <- iterArray(reads) ) 
      {
        withName(k) 
        {
          val mate = if (isPaired) mates(k) else null   

          val check = FastQC( read = reads(k),
                              mate = mate,
                              clean= false)
          check._custom("cpu")=threads.toString
          
          val readsFolder = QuickBash(in = check.folder,
                                    script = "r=$( ls -d $in/*"+readKey+"""_fastqc/Images )
                                              ln -s $r $out""").out

          val matesFolder = if (isPaired) 
          {   
              QuickBash(in = check.folder, 
                        script = "m=$( ls -d $in/*"+mateKey+"""_fastqc/Images ); ln -s $m $out""").out
          } else null

          //this puts the images of the read and mate one on top of the other, or if single-end returns the same image folder
          val pairPreImages = ConvertImages(folder1  = readsFolder, folder2  = matesFolder)
          
          val mateWarns = if (isPaired) check.summary("mate_warnings") else null

          qcWarnings(k) = PrepWarnings(check.summary("read_warnings"),mateWarns,isPaired)
          txtStatsArray(k) = check.summary 
          imagesStatsArray(k) = pairPreImages
        }
      }
      (txtStatsArray, imagesStatsArray, qcWarnings)
    } else 
    {
      //we don't have to run fastqc ourselves but we need to extract the image folder and the stats file  
      for ( (k,v) <- iterArray(fastQCfolders) )
      {
        withName(k)
        {
          //this just gets the info for read and not for mate but it is ok since it is only used to get the qualEnconding
          val summaryStuff = Folder2Array(folder1 = fastQCfolders(k),
                                          filePattern ="fastqc_data.txt")

          val readsFolder = QuickBash(in = fastQCfolders(k),
                                    script = "r=$( ls -d $in/*"+readKey+"""_fastqc/Images )
                                              ln -s $r $out""").out

          val matesFolder = if (isPaired) 
          {   
              QuickBash(in = fastQCfolders(k), 
                        script = "m=$( ls -d $in/*"+mateKey+"""_fastqc/Images ); ln -s $m $out""").out
          } else null 

          //this puts the images of the read and mate one on top of the other, or if single-end returns the same image folder
          val pairPreImages = ConvertImages(folder1  = readsFolder, folder2  = matesFolder)

          val warningStuff = Folder2Array(folder1 = fastQCfolders(k),
                                          filePattern ="summary.txt",
                                          keyMode = "number")


          // it may be that the keys do not match read and mate, but it doesnt matter at this point
          val mateWarns = if (isPaired) warningStuff.out("2") else null

          qcWarnings(k) =  PrepWarnings(warningStuff.out("1"),mateWarns,isPaired)
          txtStatsArray(k) = summaryStuff.out
          imagesStatsArray(k) = pairPreImages
        }
      }
      (txtStatsArray, imagesStatsArray, qcWarnings)
    }
        
    //following part just guesses the encoding if not provided
    for ( (k,v) <- iterArray(reads) )
    {
      withName(k)
      {
        qualEncodings(k) = if (qual=="") 
        {     
      
          val qcInfo = QCParser(summary = summary(k),
                                paired  = isPaired)

          val getEncoding = QuickBash(in      = qcInfo.basicStats("read"),
                                      script  = """grep Encoding $in | cut -f2 | cut -d" " -f2 | awk -v enc="""+qual+""" '{if (enc!="") {print enc;} else {if ($1>=1.3 && $1<=1.8) {print "phred64";} else {print "phred33"}}}' > $out""")
            
          getEncoding.out.textRead()
        }
        else qual
      }
    }         
    return(preStats,qualEncodings,warnings)
  }



  // umiFilter (Filter reads based on umis and remove multiple Gs at 5' end, just for STRT)

  def umiFilter( read: FASTQ, umiLen: Int, umiSliding: String, qual: String, sampleName: String, temSwitPrimer: String): (BinaryFile, CSV) = {

    val split = BashEvaluate( var1 = read,
        	                  var2 = INPUT(path=_functionFolder+"STRT/split_reads.py"),
            	              param1 = umiLen.toString,
                	          param2 = temSwitPrimer,
                    	      script = """ python @var2@ @var1@ @arrayOut1@/umis.fasta.gz @arrayOut1@/Tseqs.fasta.gz @arrayOut1@/Invalid_Reads.fasta.gz @param1@ @param2@ @out1@
                                       addarray arrayOut1 umis umis.fasta.gz
                                       addarray arrayOut1 Tseqs Tseqs.fasta.gz
                                       addarray arrayOut1 Invalid Invalid_Reads.fasta.gz""")
	split._filename("out1", "stats.csv")
	
    // Discard reads with  bad umis
    val umiQC = Trimmomatic(read = split.arrayOut1("umis"),
         	                 qual = qual,
            	             slidingWindow = umiSliding,
                	         minlen = umiLen,
                    	     gzip = true,
                        	 leading = -1,
                        	 trailing = -1)
    
    val TseqOkUMI = BashEvaluate( var1=split.arrayOut1("Tseqs"),
								   var2=umiQC.trimmedReads("Reads"), 
								   var3=INPUT(path=_functionFolder+"STRT/filter_Tseq_by_UMI.py"),
                            	   script = """python @var3@ @var1@ @var2@ " " @out1@ """
                                  )                         
	TseqOkUMI._filename("out1", "TseqOkUMI.fastq.gz")

	val umiFilterS = TableQuery(table1 = umiQC.stats,
       			                    table2 = split.out1,
                		             query = """ SELECT table2."Input_Reads" AS "Input_Reads",                        		                
                                		         table1."Discarded"+table2."Dropped" AS "Discarded" FROM table1,table2
                                     """ )
    val umiFilterSFormat = BashEvaluate(var1 = umiFilterS,
        	                       script = """echo -e "Sample\n"'''+sampleName+''' > @folder1@/tmp
            	                               paste @folder1@/tmp @var1@ | tr -d '"' > @out1@""")

	val preFilteredTseq = TseqOkUMI.out1
	val umiFilterStat = umiFilterSFormat.out1
    return (preFilteredTseq.asInstanceOf[BinaryFile], umiFilterStat.asInstanceOf[CSV])
      }
		

  //TrimQC
  def TrimQC( read: FASTQ, 
              mate: FASTQ = null, 
              adapter: FASTA = null,
              isPaired: Boolean, headcrop: Int, minQuality: Int,trailing: Int, 
              slidingWindow: String, minLength: Int, extra: String, qual: String, 
              stringency: Int, adapterSeq: String, tool: String, sampleName: String, 
              gzip: Boolean, simpleClip: Int, palindromeClip: Int, crop: Int, minPercent: Int, percent: Double, keepBothReads: Boolean, threads: Int): (BinaryFile,CSV,BinaryFile) =
  { 


    val (trimmed,stats) = if (tool=="trimmomatic") 
    {

      val trim = Trimmomatic( read            = read,
                              mate            = mate,
                              adapter         = adapter,
                              illuminaAdapter = adapterSeq,
                              minlen          = minLength,
                              trailing        = trailing,
                              leading         = minQuality,
                              headcrop        = headcrop,
                              qual            = qual,
                              seedMM          = stringency,
                              simpleClip      = simpleClip,
                              palindromeClip = palindromeClip,
                              crop            = crop,
                              gzip            = gzip,
                              slidingWindow   = slidingWindow,
                              keepBothReads = keepBothReads,
                              threads         = threads)

      val trimmed = trim.trimmedReads
      val stats = trim.stats
      ( trimmed,stats )

    } else if (tool=="trimGalore") 
    {

      val options = if (isPaired) {"--retain_unpaired "+extra} else {extra}

      val trim = TrimGalore(reads       = read,
                            mates       = mate,
                            adapter     = adapterSeq,
                            paired      = isPaired,
                            runFastqc   = false,
                            clipR1      = headcrop,
                            clipR2      = headcrop,
                            minQuality  = minQuality,
                            minLength   = minLength,
                            extraArgs   = options,
                            qualityScore= qual,
                            stringency  = stringency,
                            gzip        = gzip)

      val trimmed = trim.trimmed
      val stats = trim.stats
      ( trimmed,stats )

    } else if (tool=="fastx") 
    {
      
      val enc = if (qual=="phred64") {"-Q64"} else {"-Q33"}
      
      val trim = SmallRNAPrep(reads     = read,
                              adapter   = adapterSeq,
                              M         = stringency,
                              Lmin      = minLength,
                              Lmax      = crop,
                              minQ      = minQuality,
                              minPercent= minPercent,
                              extra     = extra,
                              qual      = enc,
                              zip       = gzip)

      val trimmed = makeArray("Reads"->trim.fastq)
      val stats = trim.stats
      ( trimmed,stats )
    
    } else 
    {
      error("Not a valid tool name")
      throw new IllegalArgumentException(s"'$tool' is not a valid tool name")
    }
    
    //decide if the samples passed or failed the filtering step
    val pass = QuickBash( in = stats.asInstanceOf[CSV], script ="""awk -F"\t" '{if (NR>1) {p=($2*100)/$1; if (p>"""+percent+""") {print "passed";} else {print "failed";}}}' $in > $out""").out
                                                    
    return(trimmed.asInstanceOf[BinaryFile],stats.asInstanceOf[CSV],pass.asInstanceOf[BinaryFile])
  }
  
  //function to put images on top and bottom (paired) and side by side (before and after)
  def ConvertImages(folder1: BinaryFolder, folder2: BinaryFolder=null, doConvert: Boolean=true): (BinaryFolder) =
  {
    
    val outImages = if (folder2 == null )
    { 
      folder1
    } else if (doConvert)
    {
      val convert = BashEvaluate( var1  = folder1,
                                  var2  = folder2,
                                  script="""for i in $( ls @var1@/*png )
                                            do
                                              a=$( basename $i )
                                              if [ ! -e @folder1@/$a ]
                                              then
                                                convert $i @var2@/$a -append @folder1@/$a
                                              fi
                                           done""")
      convert.folder1
    } else
    {
      val montage = BashEvaluate(var1  = folder1,
                                 var2  = folder2,
                                 script ="""sleep 5
                                            for i in $( ls @var1@/*png )
                                            do
                                              a=$( basename $i )
					                                    echo -e "montage $i @var2@/$a -tile 2x2 -geometry +0+0 @folder1@/$a"
                                              montage $i @var2@/$a -tile 2x2 -geometry +0+0 @folder1@/$a
                                            done""")
      montage.folder1
    }
    
    return(outImages)
  }

  def PrepWarnings(readsWarns: CSV, matesWarns: CSV=null, isPaired: Boolean): (HTMLFile) =
  {
    val qcWarnings = if (isPaired)
    { 

      val addWarnHeader = QuickBash(in      =  makeArray(readsWarns,matesWarns),
                                    script  ="""echo -ne Status\\tModule\\tFile\\n | cat - $key1 | awk -F"\t" '{print $2,$3,$1}' OFS="\t" > tmp1
                                                echo -ne Status\\tModule\\tFile\\n | cat - $key2 | awk -F"\t" '{print $3,$1}' OFS="\t" > tmp2
                                                paste tmp1 tmp2 >> $out
                                                rm tmp? """)

      HTMLTable(addWarnHeader.out).out
    } else
    { 

      val addWarnHeader = QuickBash(in      =  readsWarns,
                                   script  ="""echo -ne Status\\tModule\\tFile\\n | cat - $in | awk -F"\t" '{print $2,$3,$1}' OFS="\t" > $out""")

      HTMLTable(addWarnHeader.out).out
    }
    return qcWarnings
  }

