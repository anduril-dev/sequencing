
def BamReorder ( 
      reference: FASTA ,
      bam: BAM , 
      picard: String  = "" ,
      memory: String  =  "4g" ,
      incomplete: Boolean  = false ): BAM = {
        
    import org.anduril.runtime._
    var options=""
    //val picard_home = if (picard == "") getEnv("PICARD_HOME") else picard
    val picard_home = if (picard == "") getEnv("SEQUENCING_BUNDLE_HOME")+"/lib/" else picard
    if (incomplete) options=" ALLOW_INCOMPLETE_DICT_CONCORDANCE=true"

    val picard_reorder = BashEvaluate( // Reorder regions
            var1=bam,
            var2=reference,
            script="java -Xmx" + memory + " -jar " + picard_home +
                "picard.jar ReorderSam VALIDATION_STRINGENCY=SILENT VERBOSITY=WARNING " +
                "INPUT=@var1@ REFERENCE=@var2@ OUTPUT=@out1@ CREATE_INDEX=true" + options
            )

    return picard_reorder.out1

}
