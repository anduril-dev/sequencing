
def HTSeqBam2Counts (
      alignments: BinaryFile ,
      annotationGTF: GTF = null,
      annotationGFF: GFF = null,
      dexseq_dir: String  =  "/opt/DEXSeq/inst/python_scripts/" ,
      entity: String  =  "Exon" ,
      format: String  =  "bam" ,
      sorted: Boolean  = false ): (TextFile,TextFile,GFF) = {

    import org.anduril.runtime._

    val exonCounts  = Record[BinaryFile]("exonCounts")
    val geneCounts  = Record[BinaryFile]("geneCounts")

    var exon = false
    var gene = false
    var bam  = true
    var sam  = false

    if (format=="sam") { sam=true }
    else if (format=="bam") { bam=true }
    else sys.error("Unsupported format:" + format)

    for ( s <- entity.split(",") ) {
        if (s=="Exon") {
            exon=true
        } else
        if (s=="Gene") {
            gene=true
        } else sys.error("Unsupported level value:" + s)
    }

    assert( (exon || gene), "No entity has been specified" )

    val annotationF =
        if (annotationGFF == null) {
            assert( (annotationGTF != null), "Annotation file missing" )
            val makeGFF = BashEvaluate( var1    = annotationGTF,
                                        param1  = dexseq_dir,
                                        script  = "python @param1@/dexseq_prepare_annotation.py @var1@ @out1@")
            makeGFF._require("var2", true)
            makeGFF.out1
        }
        else {
            annotationGFF
        }
    assert( (!gene || (annotationGTF != null)), "annotationGTF is needed" )

    if (sam) {
        if (sorted) {
            for (  (s, _) <- iterArray(alignments) ) {
                val sName = quote(s, "Anduril")
                if (exon) {
                    val exonC = BashEvaluate(var1    = alignments(s),
                                             var2    = annotationF,
                                             param1  = s,
                                             param2  = dexseq_dir,
                                             script  = """grep -v "XF:Z" @var1@ | python @param2@dexseq_count.py -p yes -s no @var2@ - @arrayOut1@/@param1@.txt
                                                       addarray array1 @param1@ @param1@.txt""",
                                             _name   = "exonC_"+sName)
                    exonCounts(s) = exonC.arrayOut1
                }
                if (gene) {
                    val geneC = BashEvaluate(var1    = alignments(s),
                                             var2    = annotationGTF,
                                             param1  = s,
                                             script  = """grep -v "XF:Z" @var1@ | htseq-count -s no - @var2@ > @arrayOut1@/@param1@.txt
                                                       addarray array1 @param1@ @param1@.txt""",
                                             _name   ="geneC_"+sName)
                    geneC._require("var2", true)
                    geneCounts(s) = geneC.arrayOut1
                }
            }
        }

        else { //not sorted
            for ( (s, _) <- iterArray(alignments)) {
                val sName = quote(s, "Anduril")
                val sorted = BashEvaluate(  var1    = alignments(s),
                                            script  = """sort -k1 @var1@ | grep -v "XF:Z" > @out1@""",
                                            _name   = "sorted_"+sName)
                if (exon) {
                    val exonC = BashEvaluate(var1    = sorted.out1,
                                             var2    = annotationF,
                                             param1  = s,
                                             param2  = dexseq_dir,
                                             script  = """python @param2@dexseq_count.py -p yes -s no @var2@ @var1@ @arrayOut1@/@param1@.txt
                                                       addarray array1 @param1@ @param1@.txt""",
                                             _name   = "exonC_"+sName)
                    exonCounts(s) = exonC.arrayOut1
                }
                if (gene) {
                    val geneC = BashEvaluate(var1    = sorted.out1,
                                             var2    = annotationGTF,
                                             script  = "htseq-count -s no @var1@ @var2@ > @arrayOut1@/"+s+".txt; "+
                                                       "addarray array1 "+s+" "+s+".txt",
                                             _name   = "geneC_"+sName)
                    geneC._require("var2", true)
                    geneCounts(s) = geneC.arrayOut1
                }
            }
        }
    }
    else {  //if bam
        if (!sorted) {
            for ( (s,_) <- iterArray(alignments) ) {
                val sName = quote(s, "Anduril")
                val sorted = BashEvaluate(  var1    = alignments(s),
                                            script  = "samtools sort -n @var1@ @folder1@/sort ; "+
                                                      "rm @optOut1@ ; ln -s @folder1@/sort.bam @out1@",
                                            _name   = "sorted_"+sName)

                if (exon) {
                    val exonC = BashEvaluate(var1    = sorted.out1,
                                             var2    = annotationF,
                                             param1  = dexseq_dir,
                                             param2  = s,
                                             script  = """samtools view @var1@ | grep -v "XF:Z" |
                                                       python @param1@dexseq_count.py -p yes -s no @var2@ - @arrayOut1@/@param2@.txt
                                                       addarray array1 @param2@ @param2@.txt""",
                                             _name   = "exonC_"+sName)
                    exonCounts(s) = exonC.arrayOut1
                }
                if (gene) {
                    val geneC = BashEvaluate(var1    = sorted.out1,
                                             var2    = annotationGTF,
                                             param1  = s,
                                             script  = """samtools view @var1@ | grep -v "XF:Z" |
                                                       htseq-count -s no - @var2@ > @arrayOut1@/@param1@.txt
                                                       addarray array1 @param1@ @param1@.txt""",
                                             _name   = "geneC_"+sName)
                    geneC._require("var2", true)
                    geneCounts(s) = geneC.arrayOut1
                }
            }
        }

        else {
            //already sorted, just convert to sam
            for ( (s,_) <- iterArray(alignments) ) {
                val sName = quote(s, "Anduril")
                if (exon) {
                    val exonC = BashEvaluate(var1    = alignments(s),
                                             var2    = annotationF,
                                             param1  = dexseq_dir,
                                             param2  = s,
                                             script  = """samtools view @var1@ | grep -v "XF:Z" |
                                                          python @param1@dexseq_count.py -p yes -s no @var2@ - @arrayOut1@/@param2@.txt
                                                          addarray array1 @param2@ @param2@.txt""",
                                             _name   = "exonC_"+sName)
                    exonCounts(s) = exonC.arrayOut1
                }

                if (gene){
                    val geneC = BashEvaluate(var1    = alignments(s),
                                             var2    = annotationGTF,
                                             param1  = s,
                                             script  = """\
                                                       htseq-count -f bam -s no @var1@ @var2@ |\
                                                       grep -v '^__'> @arrayOut1@/@param1@.txt
                                                       addarray array1 @param1@ @param1@.txt
                                                       """,
                                             _name   = "geneC_"+sName)
                    geneC._require("var2", true)
                    geneCounts(s) = geneC.arrayOut1
                }
            }
        }
    }

    val empty = StringInput(content="Level not selected")
    val tmp = ArrayConstructor(file1 = empty)
    if (exonCounts.isEmpty) {
        exonCounts("1") = tmp.out
    }

    if (geneCounts.isEmpty) {
        geneCounts("1") = tmp.out
    }

    return ( ArrayCombiner(exonCounts.rec), ArrayCombiner(geneCounts.rec), annotationF.force() )
}
