if (length(grep("mate",table2[,1])) > 0) { # Paired-end data
    paired <- TRUE
} else {
    paired <- FALSE
}

# Calculate percentage of reads mapped
f1 <- unlist(strsplit(colnames(table1)," "))

if (paired) {
    f1_reads <- as.numeric(as.character(f1[1]))/2
} else {
    f1_reads <- as.numeric(as.character(f1[1]))
}

f1 <- unlist(strsplit(table1[2,]," "))
f1_mapped <- as.numeric(as.character(f1[1]))
f1_percent <- f1_mapped/f1_reads*100

toAdd1 <- matrix(c(param1,gsub("\n","",param2)), nrow=1)
colnames(toAdd1) <- c("Key","File")

if (param3 == 2) { # QC2

    toAdd2 <- table2[grep(paste(param1,"$",sep=""),table2[,"Sample"]),c("raw","trimmed")]
    colnames(toAdd2) <- c("raw","trimmed")
    tabled <- matrix(c(f1_reads,f1_mapped,f1_percent), nrow=1)
    colnames(tabled) <- c("Trimmed+Quality Reads","Aligned Reads","Percent_mapped")
    table.out <- cbind(toAdd1,toAdd2,tabled)

} else { # QC3

    if (length(intersect(c("raw","Trimmed+Quality Reads"),colnames(table2))) != 2) {
        # One or more of the alignment and trimming steps was skipped. Take only what was done.
        toAdd2 <- as.matrix(table2[,intersect(c("raw","Trimmed+Quality Reads"),colnames(table2))], nrow=1)
        colnames(toAdd2) <- intersect(c("raw","Trimmed+Quality Reads"),colnames(table2))

        # Reduce to just one row
        toAdd2 <- as.matrix(toAdd2[which(table2[,1] == paste(param1,"__read", sep="")),])
        colnames(toAdd2) <- intersect(c("raw","Trimmed+Quality Reads"),colnames(table2))
        colnames(toAdd2) <- gsub("^raw$", "Total Sequenced Reads", colnames(toAdd2))
        colnames(toAdd2) <- gsub("^Trimmed+Quality Reads$", "Trimmed Reads", colnames(toAdd2))
        rownames(toAdd2) <- NULL

        # divide by trimmed quality reads
        f3_percent <- f1_mapped/as.numeric(toAdd2[1,ncol(toAdd2)])*100

        lastCol <- paste("Percent_mapped_from",gsub(" ","_",colnames(toAdd2)[ncol(toAdd2)]),sep="_")
    } else {
        toAdd2 <- table2[,c("raw","Trimmed+Quality Reads")]
        colnames(toAdd2) <- c("Total Sequenced Reads", "Trimmed+Quality Reads")

        f3_percent <- f1_mapped/as.numeric(toAdd2[1,ncol(toAdd2)])*100 # divide by trimmed quality reads
        lastCol <- "Percent_mapped_from_Trimmed"
    }
    
    tabled <- matrix(c(f1_reads,f1_mapped,f1_percent, f3_percent), nrow=1)
    rownames(tabled) <- NULL
    colnames(tabled) <- c("Mapped Reads to Genome","Aligned Reads", "Percent_mapped", lastCol)
    table.out <- cbind(toAdd1,toAdd2,tabled)
}
