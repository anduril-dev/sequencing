# Create empty var
pass <- NULL

# Load reads file
stats <- read.csv(as.character(table1[which(table1[,1]=="read"),2]))

# Extract first line of summary info from read.txt and obtain whether sample passed or failed QC
header <- strsplit(as.character(stats[1,]),"\\t")

#  Basic Statistics	[pass/fail]
if (header[[1]][2] == "pass") {
    pass <- "T"
} else {
    pass <- "F"
}

# Extract number of reads failing QC
stats <-  strsplit(as.character(stats[7,]),"\\t")
QCfailed <- stats[[1]][2]

# Extract number of raw reads to calculate percentage surviving trimming AND QC.
file <- which(table2[,1] == param1)
stats <- read.csv(as.character(table2[file,2]),header=FALSE,sep="\t")
raw <- as.character(stats[1,2])
trimmed <- as.character(stats[5,2])
percent <- as.numeric(trimmed)/as.numeric(raw)

Key <- paste(param1,table1[grep("warning",table1[,1], invert=TRUE),1],sep="__")	
File <- param2

if (length(which(table1[,1]=="mate")) > 0) { # Paired-end reads
    statsM <- read.csv(as.character(table1[which(table1[,1]=="mate"),2]))

    # Extract first line of summary info from mate.txt and obtain whether sample passed or failed QC
    header <- strsplit(as.character(statsM[1,]),"\\t")

    #  Basic Statistics	[pass/fail]
    if (header[[1]][2] == "pass") {
        passM <- "T"
    } else {
        passM <- "F"
    }

    # Extract number of reads failing QC
    statsM <-  strsplit(as.character(statsM[7,]),"\\t")
    QCfailedM <- statsM[[1]][2]
}


if (!is.na(trimmed)) {
    if (length(which(table1[,1]=="mate")) > 0) { # Paired-end reads
        table.out <- cbind(Key,File,pass,passM,raw,trimmed,percent,QCfailed, QCfailedM)    
    } else {
        table.out <- cbind(Key,File,pass,raw,trimmed,percent,QCfailed)
    }
} else { # No trimming was performed
    if (length(which(table1[,1]=="mate")) > 0) { # Paired-end reads
        table.out <- cbind(Key,File,pass,passM,raw,QCfailed, QCfailedM)    
    } else {
        table.out <- cbind(Key,File,pass,raw,QCfailed)
    }
}
