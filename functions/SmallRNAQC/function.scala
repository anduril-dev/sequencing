def SmallRNAQC (
      reads: FASTQ = null,
      mates: FASTQ = null,
      stats: CSV = null,
      bam: BAM = null,
      mergeStats: CSV = null,
      QCstep: Int  = 2,
      filePath: String = "",
      suffix: String = ""): (CSV,Latex,BinaryFolder) = {

    import anduril.builtin._
    import anduril.tools._
    import org.anduril.runtime._


    /* stores all QC filtering for miRNA-seq preprocessing */

    // Initialise scripts and variables
    val samtools_flagstat = INPUT(path="samtools_flagstat.sh")
    val qc_alignment = INPUT(path="qc_alignment.r")

    // ***************  SECOND FILTER HERE  ******************
    
    if (!Set(2,3)(QCstep))
        sys.error("Unsupported QCstep " + QCstep)

    val Gstats = BashEvaluate(
            command = samtools_flagstat,
            var1 = bam
            )

    val _mergeStats = 
        if (QCstep == 2) { // Enabled for use of QCFasta output as input.
            val mergeStats_clean = CSVCleaner(
                in = mergeStats,
                rename="Input_Reads=raw,Trimmed_Reads=trimmed"
                )
            mergeStats_clean.out
        }
        else mergeStats

    val filePath2 = Gstats.out2.textRead()

    val QC = REvaluate( // Compile alignment statistics in a tab-delimited file and combine with QC statistics.
            script=qc_alignment,
            table1 = Gstats.out1,
            table2 = _mergeStats,
            param1 = suffix,
            param2 = filePath2,
            param3 = QCstep.toString
            )

    return ( QC.table, Gstats.out1, Array2Folder(in=QC.outArray).force())
    
}
