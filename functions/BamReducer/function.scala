
def BamReducer ( 
      reference: FASTA ,
      bam: BAM , 
      gatk: String = "" ,
      memory: String  =  "4g" ,
      cleanup: Boolean  = false ,
      downsample: Int  = -1 ): BAM = {


    // ------------------------------------------------------------
    // Run GATK ReduceReads
    // ------------------------------------------------------------
    import org.anduril.runtime._

    val downsampleStr = if (downsample != -1) "-dcov " + downsample else ""

    val gatkStr="java -Xmx" + memory + " -jar " + { if (gatk == "") getEnv("GATK_HOME") else gatk } +
                "/GenomeAnalysisTK.jar -T ReduceReads -R @var2@ -I @var1@ " +
                "-o @out1@"
                
    val gatk_reduce = BashEvaluate( // Compress bam
            var1=bam,
            var2=reference,
            script=gatkStr
            )
    gatk_reduce._filename("out1", "reduced.bam")

     return gatk_reduce.out1
    
}
