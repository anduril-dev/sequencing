def EnsemblGenes ( connection: Properties ): (DNARegion,DNARegion) = {


    val gene = SQLSelect(query = INPUT(path="query.sql"),
                         connection = connection)

    val filter = CSVFilter(gene.table,
                           includeColumns = "",
                           negate = true,
                           regexp = "chromosome=HS.*|HG.*")

    val protein_coding = TableQuery(table1=filter.out,
				  query="SELECT table1.\"ID\", table1.\"strand\", table1.\"chromosome\", table1.\"start\", table1.\"end\" FROM table1 WHERE table1.\"biotype\"='protein_coding'")
    
    val others = TableQuery(table1=gene.table, 
                            query="SELECT * FROM table1 WHERE table1.\"biotype\"<>'protein_coding'")

    return ( protein_coding.table.force(), others.force() )
}
