stringtomap @param2@

# Convert fastq file to collapsed fasta file
fastx_collapser -i @var1@ -o @folder1@/unmapped.fa $Qscore

# Format fasta file for miRanalyzer
sed -i 's/-/\t/g' @folder1@/unmapped.fa
sed -i 's/^>/>ID/g' @folder1@/unmapped.fa

# miRanalyzer requires Vienna-1.8.5 and LESS, NOT MORE
# species = assemby/basename of bowtie index in the miRanalyzerDB genome folder
# speciesShort = Ensembl 3-letter abbreviation
cd @folder1@
java -Xmx4096m -jar @param3@ input=unmapped.fa dbPath=@param4@ species=genome speciesShort=$species kingdom=$kingdom minReadLength=$minReadLength mmToUse=$mmToUse bowtiePath=@param1@ adapterTrimmed=true output=@folder1@ #justNew=true

# Delete all unnecessary files except for all.zip, newMicroRNA and Candidates files
#rm unmapped.f*
#rm parameters.txt
#rm reads.fa
#rm filteredReads.txt
rm tmp*
