# process options by removing spaces and using uppercase letters
methods <- unlist(strsplit(tolower(gsub("[[:blank:]]","",param1)),","))
arrayFiles <- list()
if (length(methods) == 0) {
	stop("Novel miRNA discovery options are strictly: mirdeep2, miranalyzer.")
} else {

# iterate through folders and extract required novel miRNA information
# to create an expression matrix
	for (method in methods) {
#		exprMatrx <- c()
		exprMatrxM <- c()

		######   MIRDEEP2   ######
		if (method == "mirdeep2") {
				mirD <- table1
				for (n in 1:nrow(mirD)) {
					bedName <- system(paste("find ",as.character(mirD[n,2])," -name 'result*.bed'",sep=""),intern=TRUE)
					bed <- read.table(bedName,header=FALSE, sep="\t",stringsAsFactors=TRUE)
					skipLines <- system(paste("grep -n 'provisional id' ",gsub("bed","csv",bedName),sep=""),intern=TRUE)
					skipLines <- as.numeric(strsplit(skipLines,":")[[1]][1])
					csv <- read.delim(gsub("bed","csv",bedName),sep="\t", header=TRUE, skip=skipLines-1, stringsAsFactors=TRUE)

					# reduce files to just the novel info with a miRdeep score > 0
					bed <- bed[grep("novel",bed[,4]),]
					csv <- csv[-c(grep("_",invert=TRUE,csv[,1])[1]:nrow(csv)),]
					csv <- csv[which(as.numeric(as.character(csv[,2])) > 0),]

					id <- do.call(rbind,strsplit(as.character(bed[,4]),":"))[,2]
					bed <- cbind(id,bed[,c(1:3,6)])
					colnames(bed) <- c("id","chr","start","end","strand")

					# merge info
					info <- merge(bed,csv,by.x="id",by.y="provisional.id")
				# arrayFiles[[as.character(table1[n,1])]] <- info

				# add to exprMatrx by exprID
#				exprID <- paste(as.character(info[,"chr"]),as.character(info[,"start"]),as.character(info[,"end"]),sep="_")
#				exprTotal <- cbind(exprID,info[,"total.read.count"])
#				exprMature <- cbind(exprID,info[,"mature.read.count"])
#				colnames(exprTotal)[2] <- strsplit(as.character(table1[n,1]),"__")[[1]][2]
#				colnames(exprMature)[2] <- strsplit(as.character(table1[n,1]),"__")[[1]][2]
#				if (n == 1) {
#					exprMatrx <- exprTotal
#					exprMatrxM <- exprMature
#				} else {
#					exprMatrx <- merge(exprMatrx,exprTotal,by="exprID",all=TRUE)
#					exprMatrxM <- merge(exprMatrxM,exprMature,by="exprID",all=TRUE)
#				}

##### CURRENTLY NOT USED AS POSITION INFORMATION NOT GIVEN
#				exprMature <- cbind(csv[,c("chr","start","end","mature.read.count")],method)
					exprTotal <- cbind(info[,c("chr","start","end","strand","total.read.count")],method)
					exprTotal[,1] <- gsub("X",23,as.character(exprTotal[,1]))
					exprTotal[,1] <- gsub("Y",24,as.character(exprTotal[,1]))
                    
                    # Remove anything else that is not a valid numerical chromosome
                    if (length(which(is.na(as.numeric(exprTotal[,1])))) > 0) {
                        exprTotal_extra <- exprTotal[which(is.na(as.numeric(exprTotal[,1]))),]
                        exprTotal <- exprTotal[-which(is.na(as.numeric(exprTotal[,1]))),]
                        arrayFiles[[ paste("extra_",as.character(mirD[n,1]), sep="") ]]  <- exprTotal_extra[order(exprTotal_extra[,1]),]
                    }
                                            
				arrayFiles[[ as.character(mirD[n,1]) ]] <- exprTotal[order(exprTotal[,1]),]	
				} # end of for n in nrow(mirD)

		
		######   MIRANALYZER   ######
		} else if (method == "miranalyzer") {
				mirA <- table1	
				for (n in 1:nrow(mirA)) {
                    skipSample <- FALSE
                    if (!file.exists(paste(as.character(mirA[n,2]),"/newMicroRNA_genome.txt",sep=""))) {
                        print(paste("No novel miRNAs found in ",as.character(mirA[n,2]),". Skipping sample.", sep=""))
                        skipSample <- TRUE
                    } else {
                        csv <- read.csv(paste(as.character(mirA[n,2]),"/newMicroRNA_genome.txt",sep=""),header=TRUE, sep="\t", stringsAsFactors=TRUE)
                    if (nrow(csv) == 0) {
                        print(paste("No novel miRNAs found in ", as.character(mirA[n,2]),". Skipping sample.", sep=""))
                        skipSample <- TRUE
                    }

                    if (!skipSample) {
					    exprTotal <- cbind(csv[,c("chrom","chromStart","chromEnd","strand","read.count")],method)
		                colnames(exprTotal) <- c("chr","start","end","strand","total.read.count","method")
					    exprTotal[,1] <- gsub("chr","",as.character(exprTotal[,1]))
					    exprTotal[,1] <- gsub("X",23,as.character(exprTotal[,1]))
					    exprTotal[,1] <- gsub("Y",24,as.character(exprTotal[,1]))
		                exprTotal[,1] <- gsub("MT",25,as.character(exprTotal[,1]))
                        
                        # Remove anything else that is not a valid numerical chromosome
                        if (length(which(is.na(as.numeric(exprTotal[,1])))) > 0) {
                            exprTotal_extra <- exprTotal[which(is.na(as.numeric(exprTotal[,1]))),]
                            exprTotal <- exprTotal[-which(is.na(as.numeric(exprTotal[,1]))),]
                            arrayFiles[[ paste("extra_",as.character(mirA[n,1]),sep="") ]]  <- exprTotal_extra[order(exprTotal_extra[,1],exprTotal_extra[,2]),]
                        }
					    arrayFiles[[ as.character(mirA[n,1]) ]] <- exprTotal[order(exprTotal[,1],exprTotal[,2]),]
                    } # end of skipSample
				} # end of if file.exists
            } # end of for loop
		} # end of miRAnalyzer
	} # method loop
} # else

array.out <- arrayFiles
table.out <- data.frame()
