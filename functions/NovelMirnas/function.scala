def NovelMirnas (
      fqFiles: CSV ,
      mirDP_genome: FASTA = null,
      mirDP_mature: FASTA = null,
      mirDP_related_mature: FASTA = null,
      mirDP_precursor: FASTA = null,
      method: String  =  "miranalyzer" ,
      miranalyzer: String  =  "miRanalyzerDB/miRanalyzer.jar" ,
      miranalyzerDB: String  =  "miRanalyzerDB/" ,
      bowtiePath: String = "",
      species: String  =  "hsa" ,
      minReadLength: Int  = 17,
      kingdom: String  =  "animal" ,
      Qscore: String  =  "-Q64" ,
      mmToUse: Int  = 1 ): (BinaryFile) = {

    import org.anduril.runtime._

/* Execute novel miRNA discovery tools miRanalyzer or mirDeep2 */

    val fasta_files = Record[BinaryFolder]("fasta_files")

    var _Qscore = Qscore
    var _kingdom = kingdom
    var _species = species

    // Check input variables
    if (_Qscore == "") {
        _Qscore = "-Q64"
    }
    if (_Qscore != "-Q33" && _Qscore != "-Q64") {
       sys.error("miRanalyzer component parameter Q = '" + _Qscore + "' is invalid. Valid options are '-Q33' or '-Q64'")
    }

    def exists(filename: String) = new java.io.File(filename).exists()
// *****    miRanalyzer    *****//
    if (method == "miranalyzer" || method == "") {       
        if (! exists(miranalyzer) ) {
            sys.error(s"miRanalyzer jar file $miranalyzer not found.")
        }
        if (! exists(miranalyzerDB) ) {
            sys.error(s"miRanalyzer absolute path $miranalyzerDB not found.")
        }
        if (! exists(bowtiePath)) {
            sys.error(s"bowtie path with binaries $bowtiePath not found.")
        }
        
        if (_kingdom == "") { // use default
            _kingdom = "animal"
        }
        
        if (_species == "") { // use default
            _species = "hsa"
        }

            
        for ( row <- iterCSV(fqFiles) ) {

            val mirA = BashEvaluate(
                    command = INPUT(path="mirAn.sh"),
                    var1 = INPUT(path=row("File")),
                    param2 = "_Qscore=" + _Qscore + ",_species=" + _species + ",minReadLength=" + minReadLength + ",mmToUse=" + mmToUse + ",_kingdom=" + _kingdom,
                    param3 = miranalyzer,
                    param4 = miranalyzerDB,
                    param1 = bowtiePath,
                    _name = row("Key")
                    )

            fasta_files(row("Key")) = mirA.folder1
        }

// *****    miRdeep2    ***** //
    } else if (method == "mirdeep2" || method == "miRDeep2") {
        
        if (mirDP_genome == null)  {
            sys.error("Input file mirDP_genome is required for mirdeep2.")
        }
        if (mirDP_mature == null)  {
            sys.error("Input file mirDP_mature is required for mirdeep2.")
        }
        if (mirDP_related_mature == null)  {
            sys.error("Input file mirDP_related_mature is required for mirdeep2.")
        }
        if (mirDP_precursor == null)  {
            sys.error("Input file mirDP_precursor is required for mirdeep2.")
        }
        if (_species == "") { // use default
            _species = "Human"
        }


        val mirDprep = BashEvaluate(
                var1 = mirDP_genome,
                var2 = mirDP_mature,
                var3 = mirDP_related_mature,
                var4 = mirDP_precursor,
                script = """awk '{print $1}' @var2@ > @folder1@/mature.fa
                                awk '{print $1}' @var3@ > @folder1@/other_mature.fa
                                awk '{print $1}' @var4@ > @folder1@/precursor.fa
                                awk '{print $1}' @var1@ > @folder1@/genome.fa

                                # Convert U to T
                                sed -i 's/U/T/g' @folder1@/mature.fa
                                sed -i 's/U/T/g' @folder1@/other_mature.fa
                                sed -i 's/U/T/g' @folder1@/precursor.fa

                                # Change all other valid IUPAC letters to N to avoid errors
                                sed -i 's/[RYMSWBDHVN]/N/g' @folder1@/genome.fa
                                sed -i 's/[RYMSWBDHVN]/N/g' @folder1@/mature.fa
                                sed -i 's/[RYMSWBDHVN]/N/g' @folder1@/other_mature.fa
                                sed -i 's/[RYMSWBDHVN]/N/g' @folder1@/precursor.fa"""
            )

        val mirDPfiles = Folder2Array(folder1 = mirDprep.folder1, filePattern="(.*)[.]fa")
        
        for ( row <- iterCSV(fqFiles) ) {
            // Error 127 = no results found
            val mirD = BashEvaluate(
                    command = INPUT(path="mirDp.sh"),
                    param1 = row("File"),
                    param2 = "_Qscore=" + _Qscore + ",_species=" + _species,
                    var1 = mirDP_genome,
                    vararray=mirDPfiles.out,
                    _name = row("Key")
                    )

            fasta_files(row("Key")) = mirD.folder1		
        }

// *****    FAIL    ***** //
    } else {
        sys.error("INVALID INPUT: " + method + ". Parameter 'method' must be either 'miranalyzer' or 'mirdeep2'.")
    }

    val novelMiRNA = Array2CSV(
            in = makeArray(fasta_files)
            )

    // Extract count expression per sample by chr positions
    val novelExpr = REvaluate(
            script = INPUT(path="mirExpr.r"),
            table1 = novelMiRNA.out,
            param1 = method
            )

    return novelExpr.outArray

}
