#!/bin/bash

# var1: reads
# var2: mates (may be null)
# var3: config file
# param1: mono path (may be empty)
# param2: FusionMap path (may be empty => $FUSIONMAP_HOME)
# param3: reference genome code
# param4: gene model code

READS=$( getinput var1 )
MATES=$( getinput var2 )
IN_CONFIG=$( getinput var3 )
CONFIG=$( getoutput optOut1 )
MONO_EXEC=$( getparameter param1 )
FUSIONMAP_HOME_PARAM=$( getparameter param2 )
REFERENCE=$( getparameter param3 )
GENE_MODEL=$( getparameter param4 )
OUTPUT=$( getoutput folder1 )

mkdir -p "$OUTPUT"

case "$READS" in
    *.bam )
        FORMAT=BAM
        GZIP=False ;;
    *.fasta | *.fa )
        FORMAT=FASTA
        GZIP=False ;;
    *.fasta.gz | *.fa.gz )
        FORMAT=FASTA
        GZIP=True ;;
    *.fastq | *.fq )
        FORMAT=FASTQ
        GZIP=False ;;
    *.fastq.gz | *.fq.gz )
        FORMAT=FASTQ
        GZIP=True ;;
    * )
        FORMAT=FASTQ
        GZIP=False ;;
esac

if [ -z "$MONO_EXEC" ]; then
    MONO_EXEC=mono
fi

MONO_VERSION=$(mono --version | head -n1 | grep -Eo '[0-9.]+' | head -n1)
if [[ ! "$MONO_VERSION" =~ 2.1[0-9][.].* ]] ; then
    echo '*****' "Warning: the component has been tested with Mono 2.10.10-12; it may not work with $MONO_VERSION" '*****'
fi

if [ -n "$FUSIONMAP_HOME_PARAM" ]; then
    FUSIONMAP_HOME="$FUSIONMAP_HOME_PARAM"
else
    if [ -z "$FUSIONMAP_HOME" ]; then
        echo 'Error: either fusionMapPath or $FUSIONMAP_HOME must be set' 
        exit 1
    fi
fi

if [ ! -w "$FUSIONMAP_HOME" ]; then
    echo '*****' "Warning: FusionMap directory $FUSIONMAP_HOME is not writable" '*****'
fi

if [ -n "$MATES" ]; then
TMPDIR=$( gettempdir )
    # Paired-end mode: FusionMap requires that read/mate files
    # are named PREFIX_1.EXT and PREFIX_2.EXT.
    ln -s "$READS" "$TMPDIR/reads_1"
    ln -s "$MATES" "$TMPDIR/reads_2"
    READS="$TMPDIR/reads_1"
    MATES="$TMPDIR/reads_2"
fi

cp "$IN_CONFIG" "$CONFIG"
sed -i "s|@READS@|$READS|" "$CONFIG"
if [ -z @var2@ ]; then
    sed -i 's|@MATES@||' "$CONFIG"
else
    sed -i "s|@MATES@|$MATES|" "$CONFIG"
fi
sed -i "s|@OUTPUT@|$OUTPUT|" "$CONFIG"

sed -i "s|@FORMAT@|$FORMAT|" "$CONFIG"
sed -i "s|@GZIP@|$GZIP|" "$CONFIG"

"$MONO_EXEC" "$FUSIONMAP_HOME/bin/FusionMap.exe" --semap "$FUSIONMAP_HOME" "$REFERENCE" "$GENE_MODEL" "$CONFIG"
mv "$OUTPUT"/*.FusionReads.bam "$OUTPUT/FusionReads.bam"
