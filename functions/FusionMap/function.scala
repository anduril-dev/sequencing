
def FusionMap ( 
      reads: RegionSet ,
      mates: RegionSet = null, 
      fusionMapPath: String = "",
      rna: Boolean  = true,
      referenceName: String  =  "Human.B37.3" ,
      geneModelName: String  =  "Ensembl.R73" ,
      minSeedLength: Int  = 25,
      maxAlignments: Int  = 1,
      mismatchPercentage: Int  = 8,
      monoExec: String = "",
      threads: Int  = 4,
      memoryGB: Int  = 8 ): (CSV,BAM) = {
        

    var config = 
s"""\
|<Files>
|@READS@
|@MATES@
|
|<Options>
|MonoPath=@MONOPATH@
|PairedEnd=@PAIRED@
|RnaMode=@RNA@
|ThreadNumber=$threads
|FileFormat=@FORMAT@
|CompressionMethod=Gzip
|Gzip=@GZIP@
|//AutoPenalty=True
|//FixedPenalty=5
|//FilterUnlikelyFusionReads=True
|FullLengthPenaltyProportion=$mismatchPercentage
|MinimalFusionAlignmentLength=$minSeedLength
|FusionReportCutoff=$maxAlignments
|//NonCanonicalSpliceJunctionPenalty=2
|//MinimalHit=2
|//MinimalRescuedReadNumber=1
|//MinimalFusionSpan=5000
|RealignToGenome=True
|OutputFusionReads=True
|
|<Output>
|TempPath=/tmp/FusionMapTemp
|OutputPath=@OUTPUT@
|OutputName=fusions
""".stripMargin

    if (monoExec == "") {
        config = config.replaceAll("@MONOPATH@", "mono")
    } else {
        config = config.replaceAll("@MONOPATH@", monoExec)
    }

    if (mates == null) {
        config = config.replaceAll("@PAIRED@", "False")
    } else {
        config = config.replaceAll("@PAIRED@", "True")
    }

    if (rna) {
        config = config.replaceAll("@RNA@", "True")
    } else {
        config = config.replaceAll("@RNA@", "False")
    }

    val configFile = StringInput(content=config)
    val fusionMapScript = INPUT(path="fusionMap.sh")

    val fusions = BashEvaluate(var1=reads, var2=mates, var3=configFile,
            param1=monoExec, param2=fusionMapPath, param3=referenceName, param4=geneModelName,
            command=fusionMapScript, echoStdOut=true)
    fusions._custom("cpu") = threads.toString
    fusions._custom("memory") = (memoryGB*1024).toString

    
    val extracted = FolderExtractor(fusions.folder1,
            filename1="fusions.FusionReport.txt", filename2="FusionReads.bam")
    extracted._filename("file1", "fusions.csv")
    extracted._filename("file2", "fusionReads.bam")

    return (extracted.file1.force(), extracted.file2.force())
}
