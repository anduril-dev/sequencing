
def MutSig ( 
      variants: CSV ,
      coverage: CSV = null,
      covariates: CSV = null, 
      chromColumn: String  =  "CHROM" ,
      positionColumn: String  =  "POS" ,
      geneColumn: String = "",
      effectColumn: String = "",
      patientColumn: String,
      refAlleleColumn: String  =  "REF" ,
      altAlleleColumn: String  =  "ALT" ,
      matlab: String = "",
      mutsig: String = "",
      builtinCovariates: String  =  "*" ,
      annovarBin: String = "",
      annovarDB: String = "",
      label: String  =  "MutSig"  ): (CSV,Excel) = {        

    assert( altAlleleColumn != "", "altAlleleColumn must be defined" )
    assert( chromColumn     != "", "chromColumn must be defined" )
    assert( patientColumn   != "", "patientColumn must be defined" )
    assert( positionColumn  != "", "positionColumn must be defined" )

    import org.anduril.runtime._

    val matlab_env = 
        if (matlab == "") {
            assert( existsEnv("MCRROOT"), "If 'matlab' is empty, $MCRROOT must be defined" )            
            getEnv("MCRROOT")
        }
        else matlab

    val mutsig_home = 
        if (mutsig == "") {
            assert( existsEnv("MUTSIG_HOME"), "If 'mutsig' is empty, $MUTSIG_HOME must be defined")            
            getEnv("MUTSIG_HOME")
        }
        else mutsig

    val annovar_home = 
        if (annovarBin == "") {
            assert( existsEnv("ANNOVAR_HOME"), "If 'annovarBin' is empty, $ANNOVAR_HOME must be defined" )                    
            getEnv("ANNOVAR_HOME")
        }
        else annovarBin

    val annovarDB_ = 
        if (annovarDB == "") {
            assert( existsEnv("ANNOVAR_DB") , "If 'annovarDB' is empty, $ANNOVAR_DB must be defined" )
            getEnv("ANNOVAR_DB")
        } 
        else annovarDB

    val expandCoverageScript    = INPUT(path=_functionFolder+"/expandCoverage.scala")
    val mutsigCoverageScript    = INPUT(path=_functionFolder+"/mutsigCoverage.scala")
    val csv2vcfScript           = INPUT(path=_functionFolder+"/csv2vcf.scala")
    val combinedVariantsScript  = INPUT(path=_functionFolder+"/finalVariants.scala")
    val normalizeVariantsScript = INPUT(path=_functionFolder+"/normalizeVariants.scala")
    val style                   = INPUT(path=_functionFolder+"/style.csv")
    val columnDesc              = INPUT(path=_functionFolder+"/columns.csv")

    val referenceGenome = INPUT(path=mutsig_home+"/chr_files_hg19")

    var rename = ""
    if (geneColumn != "")      { rename = rename + geneColumn+"=gene," }
    if (effectColumn != "")    { rename = rename + effectColumn+"=effect," }
    if (refAlleleColumn != "") { rename = rename + refAlleleColumn+"=ref_allele," }
    rename = rename + altAlleleColumn+"=newbase,"
    rename = rename + chromColumn+"=chr,"
    rename = rename + positionColumn+"=start,"
    rename = rename + patientColumn+"=patient"
    
    val cleanedVariants = CSVCleaner(variants, rename=rename, skipQuotes="*").out

    val ANNOTATED_VARIANTS = 
        if (effectColumn == "" || geneColumn == "") { 
            val vcfVariants = ScalaEvaluate(csv2vcfScript, table1=cleanedVariants,
                    param1="chr", param2="start", param3="ref_allele", param4="newbase")
            val annotatedVariants = VCF2AnnotatedCSV(vcfVariants.table.force(),
                    buildver="hg19", operation="g,g", protocol="ensGene,refGene",
                    annovar_db=annovarDB_, annovar_bin=annovarBin)
                    
            val combinedVariants = ScalaEvaluate(combinedVariantsScript,
                    table1=cleanedVariants, table2=annotatedVariants)
            combinedVariants.table
        } else {
            cleanedVariants
        }

    val normalizedVariants_tmp = ScalaEvaluate(normalizeVariantsScript, table1=ANNOTATED_VARIANTS.asInstanceOf[CSV], var1=referenceGenome)
    
    // remove lines with NA effect //
    // rscript = StringInput(content="table1[is.na(table1[,\"effect\"]),\"effect\"] <- \"noncoding\"; table.out <- table1")
    val rscript = StringInput(content="table.out <- table1[!is.na(table1[,\"effect\"]),]")
    val normalizedVariants_tmp2 = REvaluate(table1=normalizedVariants_tmp.table,script=rscript.out)
    val normalizedVariants = BashEvaluate(var1=normalizedVariants_tmp2.table, script="sed -e 's/\"//g' @var1@ > @out1@")

    val mutsigCoverage = 
        if (coverage == null) {
            INPUT(path=mutsig_home+"/exome_full192.coverage.txt").out
        } else {      
            // TODO: remove overlapping regions
            val expandedCoverage = ScalaEvaluate(scriptFile=expandCoverageScript, table1=coverage.force(),
                    var1=referenceGenome)
            val annotatedCoverage = VariantAnnotator(expandedCoverage.table.force(),
                    referenceDir=annovarDB_, annovar=annovarBin)
            val mutsigCoverageRun = ScalaEvaluate(scriptFile=mutsigCoverageScript,
                    table1=StringInput("").force(),  // annotatedCoverage.calls.variant_function,         // XXX alternatives !!!!
                    table2=StringInput("").force(),  // annotatedCoverage.calls.exonic_variant_function,
                    var1=referenceGenome)
            mutsigCoverageRun.table
        }

    val stockCovariates = INPUT(path=mutsig_home+"/gene.covariates.txt")
    val finalCovariates = 
        if (covariates == null) {
            stockCovariates 
        } else {
            val filteredCovariates =
                if (builtinCovariates != "*") {
                    CSVFilter(stockCovariates, includeColumns="gene,"+builtinCovariates)
                } else {
                    stockCovariates
                }
            val combinedCovariates = CSVJoin(filteredCovariates, covariates)
            CSVCleaner(combinedCovariates, skipQuotes="*", naSymbol="NaN")
        }

    val cmd = 
        s"cd $mutsig_home ; " +
        """LD_LIBRARY_PATH=$LD_LIBRARY_PATH""" + s""":$matlab_env/runtime/glnxa64:$matlab_env/bin/glnxa64:$matlab_env/sys/os/glnxa64:matlab_env/sys/java/jre/glnxa64/jre/lib/amd64/native_threads:$matlab_env/sys/java/jre/glnxa64/jre/lib/amd64/server:$matlab_env/sys/java/jre/glnxa64/jre/lib/amd64 XAPPLRESDIR=$matlab_env/X11/app-defaults ./run_MutSigCV.sh $matlab_env @var1@ @var2@ @var3@ @folder1@/output mutation_type_dictionary_file.txt chr_files_hg19"""
    val mutsig_cmd = BashEvaluate(var1=normalizedVariants.out1, var2=mutsigCoverage, var3=finalCovariates, script=cmd)
    mutsig_cmd._custom("memory") = "10240"

    val genes = FolderExtractor(mutsig_cmd.folder1, filename1="output.sig_genes.txt")
    val genesFilt = CSVFilter(genes.file1,
            includeColumns="gene,p,q,n_nonsilent,n_silent,n_noncoding,N_nonsilent,N_silent,N_noncoding,nnei,x,X",
            lowBound="n_nonsilent=1",
            rename=
                "gene=Gene,q=pFDR,"+
                "N_nonsilent=CoverageNonsilent,N_silent=CoverageSilent,N_noncoding=CoverageNoncoding,"+
                "n_nonsilent=MutationsNonsilent,n_silent=MutationsSilent,n_noncoding=MutationsNoncoding,"+
                "nnei=NumNeighbors,x=MutationsNeighbors,X=CoverageNeighbors")
    val patientCounts = TableQuery(normalizedVariants.out1.force(), query="""
            SELECT
                "gene",
                COUNT(DISTINCT "patient") AS "NumPatients"
            FROM table1
            WHERE "effect" IN ('nonsilent', 'null')
            GROUP BY "gene"
            """)
    val geneMetrics = TableQuery(genesFilt, patientCounts, query="""
            SELECT
                G.*,
                COALESCE(P."NumPatients", 0) AS "NumPatients",
                CASE "CoverageNonsilent"
                    WHEN 0 THEN NULL
                    ELSE CAST("MutationsNonsilent" AS DOUBLE) / "CoverageNonsilent" END
                    AS "MutationRate",
                CASE "CoverageNeighbors"
                    WHEN 0 THEN NULL
                    ELSE CAST("MutationsNeighbors" AS DOUBLE) / "CoverageNeighbors" END
                    AS "BackgroundRate",
                CASE
                    WHEN "CoverageNonsilent" = 0 OR "MutationsNeighbors" = 0 THEN NULL
                    ELSE (CAST("MutationsNonsilent" AS DOUBLE) / "CoverageNonsilent")
                       / (CAST("MutationsNeighbors" AS DOUBLE) / "CoverageNeighbors") END
                    AS "RateRatio"
            FROM
                table1 AS G
                LEFT OUTER JOIN table2 AS P ON G."Gene" = P."gene"
            """)
    val genesExcel = CSV2Excel(geneMetrics, columnDesc, style=style, sheetNames=label+",Description", frozenColumns=1)

    return (geneMetrics, genesExcel)
}
