/*
table1 = variant_function
 - column 1: downstream/exonic/exonic;splicing/intergenic/intronic/
             ncRNA_exonic/ncRNA_intronic/ncRNA_splicing/ncRNA_UTR5/
             splicing/upstream/UTR3/UTR5 
 - column 2: gene name: A1BG or ZSCAN22(dist=2832),A1BG(dist=1628)
 - "CHROM", "POS": chromosome, position
table2: exonic_variant_function
  - column 2: nonsynonymous SNV/stopgain SNV/stoploss SNV/synonymous SNV
  - column 3: gene:transcript:exon:nucleotide change:protein change,OTHER
              A1BG:NM_130786:exon8:c.C1485G:p.S495R,
 - "CHROM", "POS": chromosome, position
var1 = reference genome directory
var/chrN.txt = nucleotides for chromosome N without newlines or metadata
*/

import org.anduril.asser.io.CSVWriter
import java.io.{File,FileOutputStream,RandomAccessFile}
import scala.collection.mutable.{Map,Set}

val EFFECT_NONCODING = 0
val EFFECT_NONSILENT = 1
val EFFECT_SILENT = 2
val EFFECT_LABELS = Seq("noncoding", "nonsilent", "silent")
val NUM_EFFECTS = 3
val NUM_CATEGORIES = 4*4*4*4
val ALLELES = "ACGT"

val chromColVF = table1.getColumn(2)
val posColVF = table1.getColumn(3)
val zoneColVF = table1.getColumn(0)
val geneColVF = table1.getColumn(1)
val altAlleleColVF = table1.getColumn(6)

val chromColEVF =  table2.getColumn(3)
val posColEVF = table2.getColumn(4)
val effectColEVF = table2.getColumn(1)

/* For getReferenceSequence. */
val chromFileMap = Map[String,RandomAccessFile]()

/** Fetch reference sequence for a specific genomic region.
  * @param chromosome Chromosome name in Ensembl ("1") or UCSC ("chr1") format.
  * @start Start position, 1-based indexing.
  * @start Inclusive end position, 1-based indexing. */
def getReferenceSequence(chromosome: String, start: Int, end: Int): String = {
    val chrPattern = "chr(.*)".r
    val normChromosome = chromosome match {
        case chrPattern(x) => x
        case "MT" => "M"
        case x => x
    }
    if (!chromFileMap.contains(normChromosome)) {
        val path = new File(var1File, "chr%s.txt".format(normChromosome))
        chromFileMap(normChromosome) = new RandomAccessFile(path, "r")
    }
    val file = chromFileMap(normChromosome)
    file.seek(start-1)
    val buffer = new Array[Byte](end-start+1)
    file.read(buffer)
    new String(buffer).toUpperCase()
}

def getAlleleIndex(allele: Char): Int = {
    val index = ALLELES.indexOf(allele)
    if (index < 0) throw new IllegalArgumentException("Invalid allele: "+allele)
    index
}

/** Convert a category N(N-N)N to an integer 0..255. There are 4*4*3*4 = 192
  * distinct values in this range. */
def getCategoryIndex(prevAllele: Char, refAllele: Char, altAllele: Char, nextAllele: Char): Int = {
    getAlleleIndex(prevAllele) * 64 +
    getAlleleIndex(refAllele)  * 16 +
    getAlleleIndex(altAllele)  * 4  +
    getAlleleIndex(nextAllele)
}

def getCategoryLabel(categoryIndex: Int): Option[String] = {
    val prevAllele = ALLELES((categoryIndex >> 6) & 0x03)
    val refAllele  = ALLELES((categoryIndex >> 4) & 0x03)
    val altAllele  = ALLELES((categoryIndex >> 2) & 0x03)
    val nextAllele = ALLELES( categoryIndex       & 0x03)
    if (refAllele == altAllele) {
        None
    } else {
        Some("%s(%s->%s)%s".format(prevAllele, refAllele, altAllele, nextAllele))
    }
}

table2.includeColumns(Seq(1, 3, 4).map(table2.getColumn(_).name))

/* (chromosome, position) */
val nonsilentExonSet = Set[(String,Int)]()
val silentExonSet = Set[(String,Int)]()
table2.transform(r => {
    val chr = r(chromColEVF).intern()
    val pos = r.int(posColEVF)
    val effect = r(effectColEVF)
    effect match {
        case "synonymous SNV" => silentExonSet.add((chr, pos))
        case "nonsynonymous SNV" | "stopgain SNV" | "stoploss SNV" | "stopgain" | "stoploss" => nonsilentExonSet.add((chr, pos))
        case "unknown" => {}
        case x => throw new IllegalArgumentException("Unknown exon effect: "+x)
    }
})
table2.readAll()

/* geneMap(gene)(effect)(category) = coverage */
val geneMap = Map[String,Array[Array[Int]]]()

for (vfRow <- table1) {
    val gene = vfRow(geneColVF)
    if (",>;".forall(!gene.contains(_))) {
        val chr = vfRow(chromColVF)
        val pos = vfRow.int(posColVF)

        val genomicEnv = getReferenceSequence(chr, pos-1, pos+1)
        val altAllele = vfRow(altAlleleColVF)(0)

        if (!genomicEnv.contains("N") && altAllele != 'N') {
            assert("ACGT".contains(altAllele))
            val categoryIndex = getCategoryIndex(genomicEnv(0), genomicEnv(1),
                altAllele, genomicEnv(2))

            val effect =
            if (nonsilentExonSet.contains((chr, pos))) {
                EFFECT_NONSILENT
            } else if (silentExonSet.contains((chr, pos))) {
                EFFECT_SILENT
            } else {
                EFFECT_NONCODING
            }

            val geneArray = geneMap.getOrElseUpdate(gene, Array.ofDim(NUM_EFFECTS, NUM_CATEGORIES))
            geneArray(effect)(categoryIndex) += 1
        }
    }
}

val OUT_COLUMNS = Array("gene", "effect", "categ", "coverage")
val outCoverage = new CSVWriter(OUT_COLUMNS, tableOutFile, false)

for (geneName <- geneMap.keys.toSeq.sorted) {
    val geneArray = geneMap(geneName)
    for (effect <- 0.until(NUM_EFFECTS)) {
        for (category <- 0.until(NUM_CATEGORIES)) {
            val categoryLabel: Option[String] = getCategoryLabel(category)
            /* Omit trivial categories with X->X. */
            if (categoryLabel.isDefined) {
                outCoverage.write(geneName, false)
                outCoverage.write(EFFECT_LABELS(effect), false)
                outCoverage.write(categoryLabel.get, false)
                outCoverage.write(geneArray(effect)(category))
            }
        }
    }
}

outCoverage.close()
