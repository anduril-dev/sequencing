/*
table1: annotated variants
- gene, chr, start, ref_allele(?), newbase, patient, effect
var1: reference genome directory

RikuRator AminoChange:
FshiftN
Intronic
RefAA Pos AltAA (Leu258Phe)
RefAA Pos Stop (Ser91Stop)
spl-N RefAA Pos AltAA (spl-2 Arg307His)
splN
splN DEL
splN INS
Stop Pos AltAA (Stop56Trp)
syn RefAA Pos RefAA (syn Ser159Ser)
UTR

MAF format:
3'Flank
3'UTR
5'Flank
5'UTR
Frame_Shift_Del
Frame_Shift_Ins
IGR
In_Frame_Del
In_Frame_Ins
Intron
Missense_Mutation
Nonsense_Mutation 
Nonstop_Mutation 
RNA
Silent
Splice_Site
Translation_Start_Site
*/

import scala.collection.mutable.HashSet

tableOut = table1

/* Make variants unique using (patient,chr,start,newbase) as key. */
val uniqueVariantSet = HashSet[(String,String,String,String)]()
def makeUniqueKey(r: Row): (String,String,String,String) = {
    (r("patient"), r("chr"), r("start"), r("newbase"))
}
filter(r => !uniqueVariantSet.contains(makeUniqueKey(r)))
transform(r => uniqueVariantSet.add(makeUniqueKey(r)))

/* Patterns for RikuRator (after lower casing). */
val FrameShiftPattern = "fshift[0-9]+".r
val NonSynPattern = "[a-z]{3}[-]?[0-9]+[a-z]{3}".r
val SplicePattern ="spl[-]?[0-9]+.*".r
val StopPattern1 = "stop[0-9]+[a-z]{3}".r
val StopPattern2 = "[a-z]{3}[0-9]+stop".r
val SynPattern = "syn .*".r

/* Normalize "effect". */
modifyColumnS("effect", (value: String, row: Row) =>
    value.toLowerCase match {
    case "3'flank"                 => "noncoding"
    case "3'utr"                   => "noncoding"
    case "5'flank"                 => "noncoding"
    case "5'utr"                   => "noncoding"
    case "downstream"              => "noncoding"
    case "exonic"                  => "nonsilent" // unspecific: have to guess
    case "exonic;splicing"         => "nonsilent"
    case "frame_shift_del"         => "null"
    case "frame_shift_ins"         => "null"
    case "frameshift deletion"     => "null"
    case "frameshift insertion"    => "null"
    case "igr"                     => "noncoding" // intergenic region
    case "in_frame_del"            => "null"
    case "in_frame_ins"            => "null"
    case "intergenic"              => "noncoding"
    case "intron"                  => "noncoding"
    case "intronic"                => "noncoding"
    case "missense_mutation"       => "nonsilent"
    case "ncrna"                   => "noncoding"
    case "ncrna_exonic"            => "noncoding"
    case "ncrna_intronic"          => "noncoding"
    case "ncrna_splicing"          => "noncoding"
    case "noncoding"               => "noncoding"
    case "nonframeshift insertion" => "null"
    case "nonsense_mutation"       => "null"
    case "nonsilent"               => "nonsilent"
    case "nonstop_mutation"        => "null"
    case "nonsynonymous"           => "nonsilent"
    case "nonsynonymous snv"       => "nonsilent"
    case "rna"                     => "noncoding"
    case "silent"                  => "silent"
    case "splice_site"             => "null"
    case "splicing"                => "null"
    case "stopgain snv"            => "null"
    case "stoploss snv"            => "null"
    case "synonymous"              => "silent"
    case "synonymous snv"          => "silent"
    case "translation_start_site"  => "null"
    case "unknown"                 => "noncoding"
    case "upstream"                => "noncoding"
    case "upstream;downstream"     => "noncoding"
    case "utr"                     => "noncoding"
    case "utr3"                    => "noncoding"
    case "utr5"                    => "noncoding"
    case "utr5;utr3"               => "noncoding"
    case "ncrna_utr5;ncrna_utr3"   => "noncoding"
    case "ncrna_utr3"              => "noncoding"
    case "ncrna_utr5"              => "noncoding"
    case FrameShiftPattern()       => "null"
    case NonSynPattern()           => "nonsilent"
    case SplicePattern()           => "null"
    case StopPattern1()            => "null"
    case StopPattern2()            => "null"
    case SynPattern()              => "silent"
    case x                         => throw new IllegalArgumentException("Unknown effect: "+value)
    })

if (!hasColumn("ref_allele")) {
    import java.io.RandomAccessFile
    import scala.collection.mutable.Map

    /* For getReferenceSequence. */
    val chromFileMap = Map[String,RandomAccessFile]()

    /** Fetch reference sequence for a specific genomic region.
      * @param chromosome Chromosome name in Ensembl ("1") or UCSC ("chr1") format.
      * @start Start position, 1-based indexing.
      * @start Inclusive end position, 1-based indexing. */
    def getReferenceSequence(chromosome: String, start: Int, end: Int): String = {
        val chrPattern = "chr(.*)".r
        val normChromosome = chromosome match {
            case chrPattern(x) => x
            case "MT" => "M"
            case x => x
        }
        if (!chromFileMap.contains(normChromosome)) {
            val path = new File(var1File, "chr%s.txt".format(normChromosome))
            chromFileMap(normChromosome) = new RandomAccessFile(path, "r")
        }
        val file = chromFileMap(normChromosome)
        file.seek(start-1)
        val buffer = new Array[Byte](end-start+1)
        file.read(buffer)
        new String(buffer).toUpperCase()
    }

    addColumnS("ref_allele",
        r => getReferenceSequence(r("chr"), r.int("start"), r.int("start")))
}
