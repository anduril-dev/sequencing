/*
table1: original variants (? = may be missing)
- gene(?), chr, start, ref_allele(?), newbase, patient, effect(?)
table2: annotated variants
- Chr: chromosome
- Start: position
- Ref: reference allele (may be "0")
- Alt: alternative allele
- Func.ensGene: downstream/exonic/intergenic/intronic/ncRNA_intronic/upstream/UTR3/UTR5
- Gene.ensGene: Ensembl gene ID or multiple (comma-separated)
  ENSG00000134864(dist=3316),ENSG00000125247(dist=16614)
- ExonicFunc.ensGene: nonsynonymous SNV/synonymous SNV/0
- AAChange.ensGene: gene:transcript:exon:DNA-change:amino-acid-change
  ENSG00000121410:ENST00000263100:exon7:c.G1285G:p.D429D
- Gene.refGene: HUGO gene name
*/

tableOut = table1
table2.includeColumns(Seq(
    "Chr", "Start", "Alt", "Gene.refGene", "Func.ensGene", "ExonicFunc.ensGene"))
val annotatedMap = table2.toMapG(r => (r("Chr"), r("Start"), r("Alt")))

def findGene(r: Row): String = {
    // check if newbase has indels //
    val alt = r("newbase")
    val ref = r("ref_allele")
    val newbase: String =
	if(alt.length > ref.length)
		alt.substring(1,alt.length)
	else if((ref.length > alt.length) && (alt.length==1))
		"-"
        else if((ref.length > alt.length) && (alt.length>1))
		alt.substring(1,alt.length)
  	else
  		alt

    val annotatedRow = annotatedMap.get((r("chr"), r("start"), newbase))
    annotatedRow match {
        case None => null
        case Some(ar) => 
            //val gene = ar("Gene.refGene").replace("NONE,","").replace(",NONE","")
            val gene = ar("Gene.refGene")
            if (",=-".forall(!gene.contains(_))) {
                gene
            } else {
                null
            }
    }
}

def findEffect(r: Row): String = {
    // check if newbase has indels //
    val alt = r("newbase")
    val ref = r("ref_allele")
    val newbase: String =
        if(alt.length > ref.length) 
                alt.substring(1,alt.length)
        else if((ref.length > alt.length) && (alt.length==1))
                "-"
        else if((ref.length > alt.length) && (alt.length>1))
                alt.substring(1,alt.length)
        else
                alt

    val annotatedRow = annotatedMap.get((r("chr"), r("start"), newbase))
    annotatedRow match {
        case None => null
        case Some(ar) =>
            val func = ar("Func.ensGene")
            // THIS SHOULD BE PATTERN!!!
            if (func == "exonic") ar("ExonicFunc.ensGene")
            else func

            /*
            val ExonicPattern = "exonic.*".r
            func match {
                case ExonicPattern() => ar("ExonicFunc.ensGene");
                case _ => func
            }
            */
    }
}

if (!table1.hasColumn("gene")) table1.addColumnS("gene", findGene)
if (!table1.hasColumn("effect")) table1.addColumnS("effect", findEffect)
