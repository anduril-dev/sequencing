/*
table1: variants (CSV)
param1: chromosome column
param2: position column
param3: reference allele column (may be empty)
param4: alternative allele column
*/

import java.io.FileOutputStream
import org.anduril.asser.io.CSVWriter

val chromCol = getColumn(param1)
val posCol = getColumn(param2)
val refAlleleCol = getColumnOpt(param3)
val altAlleleCol = getColumn(param4)

val VCF_COLUMNS = Array[String]("#CHROM", "POS", "ID", "REF", "ALT", "QUAL", "FILTER", "INFO", "FORMAT")
val vcfStream = new FileOutputStream(tableOutFile)
vcfStream.write("##fileformat=VCFv4.1\n".getBytes())
val vcf = new CSVWriter(VCF_COLUMNS, vcfStream, "\t", false)

for (row <- table1) {
    val chr = row(chromCol)
    val pos = row(posCol)
    val refAllele = refAlleleCol match {
        case None => "0"
        case Some(col) => row(col)
    }
    val altAllele = row(altAlleleCol)

    vcf.write(chr, false)
    vcf.write(pos, false)
    vcf.write(".", false)
    vcf.write(refAllele, false)
    vcf.write(altAllele, false)
    vcf.write(40)
    vcf.write("PASS", false)
    vcf.write(".", false)
    vcf.write(".", false)
}
vcf.close()
