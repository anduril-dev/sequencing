def SmallRNAPrep (
      reads: FASTQ ,
      adapter: String  =  "ATCTCGTATGCCGTCTTCTGCTT" ,
      M: Int  = 6,
      Lmin: Int  = 15,
      Lmax: Int  = 32,
      qual: String  =  "-Q64" ,
      minQ: Int  = 30,
      minPercent: Int  = 20,
      zip: Boolean  = false,
      extra: String  =  "-n"  ): (FASTQ,BinaryFile) = {

    /* Preprocess and create small statistics for raw fastq files */

    val fastxSH = INPUT(path=_functionFolder+"fastx.sh")
    val fastxReport = INPUT(path=_functionFolder+"fastxReport.R")

    val trimmed = BashEvaluate(
          command = fastxSH,
          var1    = reads,
          param1  = "a=" + adapter + ",M=" + M + ",Lmin=" + Lmin + ",Lmax=" + Lmax + ",qual=" + qual + ",minQ=" + minQ + ",minPercent=" + minPercent + ",extra=" + extra + ",zip=" + zip
          )

    val trimReport = REvaluate(
        script = fastxReport,
        table1 = trimmed.out2
        )

    val trimFolder = Folder2Array( folder1 = trimmed.folder2 )

    val trimmedFile = 
      if (zip) trimFolder.out("trimmed_reads.fq.gz")
      else     trimFolder.out("trimmed_reads.fq")

    val stats = trimReport.table

    return (trimmedFile, stats)
}

