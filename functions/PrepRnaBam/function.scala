def PrepRnaBam (
	reference : FASTA,
	in : BAM,	
	dbsnp : VCF,
	mask: VCF = null,
	gatkPath : String = "",
	picardPath : String = "",
	memory : String = "4g",
	ID : String = "ID1",
	LB : String = "Library1",
	PL : String = "ILLUMINA",
	PU : String = "Machine1",
	SM : String = "Sample1" ) : (BAM, BinaryFile, GatkReport) = {
	
	
	// Find where to find picard and gatk
	import org.anduril.runtime._

	val gatk = if (gatkPath == "") getEnv("GATK_HOME") else gatkPath
	val picard = if (picardPath == "") getEnv("PICARD_HOME") else picardPath
	
	
	// Add groups to the bam
	val regroupScript = "java -Xmx" + memory + " -jar " + picard + "/picard.jar AddOrReplaceReadGroups RGID=" + ID + " RGLB=" + LB + " RGPL=" + PL + " RGPU=" + PU + " RGSM=" + SM + " VALIDATION_STRINGENCY=SILENT VERBOSITY=WARNING CREATE_INDEX=true SO=coordinate "
	val regrouped = BashEvaluate(var1   = in,
								 script = regroupScript + " I=@var1@ OUTPUT=@out1@ TMP_DIR=@folder1@")
	regrouped._keep = false
	regrouped._filename("out1", "regrouped.bam")
	
	
	// Mark duplicates
	val markedScript = "java -Xmx" + memory + " -jar " + picard + "/picard.jar MarkDuplicates VALIDATION_STRINGENCY=SILENT VERBOSITY=WARNING CREATE_INDEX=true" 
	val marked = BashEvaluate(var1 = regrouped.out1,
							  script = markedScript + " I=@var1@ O=@out1@ M=@out2@ TMP_DIR=@folder1@")
	//marked._keep = false
	marked._filename("out1", "marked.bam")
	marked._filename("out2", "stats")
	
	// splitNtrim
	val splitNtrimScript = "java -Djava.io.tmpdir=$( gettempdir ) -Xmx" + memory + " -jar " + gatk + "/GenomeAnalysisTK.jar -T SplitNCigarReads -rf ReassignOneMappingQuality -RMQF 255 -RMQT 60 -U ALLOW_N_CIGAR_READS" 																	  
	val splitNtrim = BashEvaluate(var1 = marked.out1,
								  var2 = reference,
								  script = splitNtrimScript + " -R @var2@ -I @var1@ -o @out1@")
	splitNtrim._keep = false
	splitNtrim._filename("out1", "splitntrim.bam")
	
	// recalibrate							  
	val recal = BaseRecalibrator(reference=reference,
								 bam=splitNtrim.out1,
								 dbsnp=dbsnp,
								 mask=mask,
								 plot=false,
								 memory=memory,
								 gatk=gatkPath)

	return (recal.alignment, marked.out2, recal.report)
}
