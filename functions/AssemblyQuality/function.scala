def AssemblyQuality (
      contigs: FASTA ,
      scaffolds: FASTA ,
      reference: FASTA = null): (Latex,CSV,CSV,CSV,CSV) = {

    val threads = 4
    val minAlignQuality = 30
    val fastaStatsScript = INPUT(path="fastaStats.scala")
    val referenceStatsScript = INPUT(path="referenceStats.scala")
    val nPlotAnnotator = StringInput(content="""
        index.50 <- which(x.column == 50)
        if (length(index.50) > 0) {
            n50 <- y.column[index.50]
            lines(list(x=c(0, 50), y=c(n50, n50)), col="blue")
            legend("topright", legend=sprintf("N50 (%.0f bp)", n50), col="blue", lwd=1)
        }
    """)
    val coverageScript = """\
import grok
reader = grok.reader(var1, "sam").flip(0)
result = grok.unionL(reader)
writer = result.writer(optOut1File, "bed")
writer.add(result)"""

    val contigStats = ScalaEvaluate(fastaStatsScript, var1=contigs)
    val contigNPlot = Plot2D(y=contigStats.optOut1, x=contigStats.optOut1,
            plotAnnotator=nPlotAnnotator,
            xColumns="Quantile", yColumns="Length", plotType="l", pngImage=false,
            xLabel="Assembly coverage %%", yLabel="Contig length",
            title="Minimum contig length (Y) to cover X%% of assembly")

    val scaffoldStats = ScalaEvaluate(fastaStatsScript, var1=scaffolds)
    val scaffoldNPlot = Plot2D(y=scaffoldStats.optOut1, x=scaffoldStats.optOut1,
            plotAnnotator=nPlotAnnotator,
            xColumns="Quantile", yColumns="Length", plotType="l", pngImage=false,
            xLabel="Assembly coverage %%", yLabel="Scaffold length",
            title="Minimum scaffold length (Y) to cover X%% of assembly")

    val report = LatexCombiner(contigNPlot.out, scaffoldNPlot.out)

    val (contigReferenceStatsValue, scaffoldReferenceStatsValue) = 
    if (reference == null) {
        val empty = StringInput(content="").out
        (empty.asInstanceOf[CSV], empty.asInstanceOf[CSV])
    } else {
        val alignedContigs = BashEvaluate(var1=reference, var2=contigs,
                script="bwa mem @var1@ @var2@ -t " + threads)
        val alignedScaffolds = BashEvaluate(var1=reference, var2=scaffolds,
                script="bwa mem @var1@ @var2@ -t " + threads)

        val contigCoverageIslands   = PythonEvaluate(var1=alignedContigs.stdOut,   script=coverageScript)
        val scaffoldCoverageIslands = PythonEvaluate(var1=alignedScaffolds.stdOut, script=coverageScript)

        val contigReferenceStats = ScalaEvaluate(referenceStatsScript,
                table1=contigs.force(), table2=reference.force(), table3=alignedContigs.stdOut.force(), 
                table4=contigCoverageIslands.out1.force(),
                format="table1=fasta,table2=fasta,table3=fasta,table4=bed", param1=minAlignQuality.toString())
        val scaffoldReferenceStats = ScalaEvaluate(referenceStatsScript,
                table1=scaffolds.force(), table2=reference.force(), table3=alignedScaffolds.stdOut.force(), 
                table4=scaffoldCoverageIslands.out1.force(),
                format="table1=fasta,table2=fasta,table3=fasta,table4=bed", param1=minAlignQuality.toString())

        (contigReferenceStats.table, scaffoldReferenceStats.table)
    }

    return (report,contigStats.table,scaffoldStats.table,contigReferenceStatsValue,scaffoldReferenceStatsValue)
}
