def Sam2Fastq (
      alignment: AlignedReadSet ,
      picard: String = "" ,
      memory: String  =  "2g" ,
      perReadgroup: Boolean  = false,
      options: String = "" ): (BinaryFolder,FASTQ,FASTQ) =
{
    

    // ------------------------------------------------------------
    // Use Picard in sequencing/lib if picard is empty.
    // ------------------------------------------------------------

    import org.anduril.runtime._

    val picard_home = if (picard == "") getEnv("SEQUENCING_BUNDLE_HOME")+"/lib/picard/" else picard

    // ------------------------------------------------------------
    // Run Picard SamToFastq to convert SAM/BAM to FASTQ.
    // ------------------------------------------------------------

    var _options = options    
    if (perReadgroup) { 
        _options = _options + " OUTPUT_PER_RG=true RG_TAG=ID"
        _options = _options + " OUTPUT_DIR=@folder1@"
    } else {
        _options = _options + " FASTQ=@out1@ SECOND_END_FASTQ=@out2@"
    }

    
    val picardC = BashEvaluate(
            var1=alignment,
            script="""java -Xmx""" + memory + """ """ +
                """-Djava.io.tmpdir="$( gettempdir )" -jar """ + picard_home +
                """picard.jar SamToFastq INPUT=@var1@ """ +
                """VALIDATION_STRINGENCY=LENIENT """ + _options
            )
    picardC._filename("folder1", "samples")
    picardC._filename("out1", "sample_1.fq")
    picardC._filename("out2", "sample_2.fq")
    
    return (picardC.folder1, picardC.out1, picardC.out2)

}
