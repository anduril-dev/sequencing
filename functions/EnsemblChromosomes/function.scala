def EnsemblChromosomes ( connection: Properties ): (CSV,CSV) = {

    val chr = SQLSelect(query      = INPUT(path="query.sql"),
                        connection = connection)
    val filter = CSVFilter(chr,
                           includeColumns = "chromosome,length",
                           regexp = "attrib=default_version,coord_name=chromosome,version=GRCh37|NCBIM37")
    val chromosomes = CSVFilter(filter,
                                includeColumns = "",
                                negate = true,
                                regexp = "chromosome=HS.*|HG.*")
    val otherChrs = CSVFilter(filter,
                              regexp = "chromosome=HS.*")
    
    return ( chromosomes, otherChrs )
}
