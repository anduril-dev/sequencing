def VariantValidator (
      reference: FASTA ,
      variants: VCF ,
      dbsnp: VCF = null,
      gatk: String ="",
      memory: String  =  "2g"  ): (TextFile) = {

    // ------------------------------------------------------------
    // Use environment variables as defaults.
    // ------------------------------------------------------------

    import org.anduril.runtime._

    var _gatk = gatk
    if (gatk == "") _gatk = getEnv("GATK_HOME")
    
    // ------------------------------------------------------------
    // Run GATK ValidateVariants
    // ------------------------------------------------------------
    
    var inputStr=" --variant @var2@"
    if (dbsnp != null) inputStr=inputStr + " --dbsnp @var3@"
    
    val gatkStr="java -Xmx" + memory + " -jar " + _gatk +
            "/GenomeAnalysisTK.jar -T ValidateVariants -R @var1@"

    val paramStr=" --warnOnErrors"

    
    val gatkVa = BashEvaluate( // Combine variants
            var1=reference,
            var2=variants,
            var3=dbsnp,
            script=gatkStr + paramStr + inputStr
            )
    gatkVa._filename("stdErr", "errors.txt")

     return gatkVa.stdErr.force()
    
}
