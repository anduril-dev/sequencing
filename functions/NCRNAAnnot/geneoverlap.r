# Annotate predicted novel miRNAs as intergenic, intragenic,
# intronic or exonic. Then add nearest neighbouring ncRNA, with
# details.
novel <- table1
hostGenes <- table4
PDneighbours <- table2
neighbours <- table3
hostGenes <- hostGenes[,c(9,1,4,5,7,3,2)]

# If required package not installed, install it
is.installed <- function(pkg) {
                       is.element(pkg, installed.packages()[,1])
                       }

if (!is.installed("biomaRt")) {
                    print("Downloading required R package biomaRt")
                    update.packages(checkBuilt=TRUE, ask=FALSE)
                    source("http://bioconductor.org/biocLite.R")
                    biocLite("biomaRt")
                    }
library(biomaRt)
ensembl <- useMart(host=param1, biomart="ENSEMBL_MART_ENSEMBL", dataset=param2)
attributes <- c("ensembl_gene_id","chromosome_name","start_position","end_position","strand","external_gene_name")
genePos <- getBM(attributes=attributes, mart=ensembl)

colnames(hostGenes) <- c("IDinfo","chr","start","end","strand","type","biotype")
colnames(genePos) <- c("ID","chr","start","end","strand","name")
genePos[,"strand"] <- gsub("-1","-",genePos[,"strand"])
genePos[,"strand"] <- gsub("1","+",genePos[,"strand"])
genePos[,"chr"] <- gsub("X",23,genePos[,"chr"])
genePos[,"chr"] <- gsub("Y",24,genePos[,"chr"])
genePos[,"chr"] <- gsub("MT",25,genePos[,"chr"])
neighbours[,"chr"] <- gsub("X",23,neighbours[,"chr"])
neighbours[,"chr"] <- gsub("Y",24,neighbours[,"chr"])
neighbours[,"chr"] <- gsub("MT",25,neighbours[,"chr"])
hostGenes[,"chr"] <- gsub("X",23,hostGenes[,"chr"])
hostGenes[,"chr"] <- gsub("Y",24,hostGenes[,"chr"])
hostGenes[,"chr"] <- gsub("MT",25,hostGenes[,"chr"])

# Filter out all "type" that are start/stop_codons, leaving just exons and CDS
hostGenes_clean <- hostGenes[which(as.character(hostGenes[,"type"]) == "exon"),]

# Make hostGenes_clean searchable for potentially intragenic novel miRNAs
details <- strsplit(as.character(hostGenes_clean[,1]),"; ")
details_sorted <- matrix(nrow = length(details),ncol=6)
colnames(details_sorted) <- c("exon_id", "exon_number", "gene_biotype", "gene_id", "transcript_id", "transcript_name")
for (n in 1:length(details)) {

	if (length(grep("exon_id",details[[n]]))== 0) {
		details_sorted[n,"exon_id"] <- NA
	} else {
		details_sorted[n,"exon_id"] <- strsplit(details[[n]][grep("exon_id",details[[n]])]," ")[[1]][2]
	}
	details_sorted[n,"exon_number"] <- strsplit(details[[n]][grep("exon_number",details[[n]])]," ")[[1]][2]
	details_sorted[n,"gene_biotype"] <- strsplit(details[[n]][grep("gene_biotype",details[[n]])]," ")[[1]][2]
	details_sorted[n,"gene_id"] <- strsplit(details[[n]][grep("gene_id",details[[n]])]," ")[[1]][2]
	details_sorted[n,"transcript_id"] <- strsplit(details[[n]][grep("transcript_id",details[[n]])]," ")[[1]][2]
	details_sorted[n,"transcript_name"] <- strsplit(details[[n]][grep("transcript_name",details[[n]])]," ")[[1]][2]
}
hostGenes_clean <- cbind(details_sorted,hostGenes_clean[,-1])

# STEP ONE: is putative miRNA intragenic?
annotations <- list()
for (r in 1:nrow(novel)) {
	intra <- which((genePos[,"chr"] == novel[r,"chr"])
				& (genePos[,"start"] <= novel[r,"start"])
				& (genePos[,"end"] >= novel[r,"end"]))
	if (length(intra) > 0) {
		if (length(intra) > 1) { # YES, multiple genes
			hosts <- genePos[intra,]
			maxIndex <- which(hosts[,"end"]-hosts[,"start"] == max(hosts[,"end"]-hosts[,"start"]))
			mainHost <- hosts[maxIndex,]
			mainHost[,"name"] <- paste(c(as.character(mainHost[,"name"]),
					as.character(hosts[,"name"][-maxIndex])),collapse=", ")
			mainHost[,"ID"] <- paste(c(as.character(mainHost[,"ID"]),
					as.character(hosts[,"ID"][-maxIndex])),collapse=", ")
			mainHost[,"start"] <- paste(c(as.character(mainHost[,"start"]),
				as.character(hosts[,"start"][-maxIndex])),collapse=",")
			mainHost[,"end"] <- paste(c(as.character(mainHost[,"end"]),
					as.character(hosts[,"end"][-maxIndex])),collapse=", ")
			mainHost[,"strand"] <- paste(c(as.character(mainHost[,"strand"]), as.character(hosts[,"strand"][-maxIndex])),collapse=", ")
			annotations[[as.character(r)]] <- mainHost
		} else { # YES, single gene
			annotations[[as.character(r)]] <- genePos[intra,]
			annotations[[as.character(r)]][,"ID"] <- as.character(annotations[[as.character(r)]][,"ID"])
			annotations[[as.character(r)]][,"name"] <- as.character(annotations[[as.character(r)]][,"name"])
			annotations[[as.character(r)]][,"strand"] <- as.character(annotations[[as.character(r)]][,"strand"])
		}
	}
}

# STEP TWO: if miRNA is intragenic, where is it?
location <- list()
for (l in 1:length(annotations)) {
	# Retrieve Ensembl gene IDs
	indexL <- names(annotations)[l]
	ids <- unlist(strsplit(as.character(annotations[[indexL]]["ID"]),", "))
	# Anticipate for multiple host genes
	position <- c()
	for (id in ids) {
		if (length(grep(id,genePos[,"ID"])) == 0) {
			stop("SOMETHING WENT WRONG. GENE ID OF INTRAGENIC MIRNA NOT FOUND IN hostGenes_clean")
		} else {
			hostGs <- hostGenes_clean[grep(id,hostGenes_clean[,"gene_id"]),]
			
			# Boolean statements
			end5 <- novel[as.numeric(indexL),"end"] < min(hostGs[,"start"])
			end3 <- novel[as.numeric(indexL),"start"] > max(hostGs[,"end"])
			# Check if exonic and pull details per transcript
			exonic <- which((hostGs[,"start"] < novel[as.numeric(indexL),"start"]) & (hostGs[,"end"] > novel[as.numeric(indexL),"end"]))
			exon_consensus <- c()
			if (length(exonic) > 0 & !(end5) & !(end3)) {
				for (i in 1:length(exonic)) {
					exon_number <- hostGs[exonic[i],"exon_number"]
					exon_consensus <- c(exon_consensus,paste(exon_number: hostGs[exonic[i],"transcript_id"],sep=":"))
				}
				exon_consensus <- paste(unique(exon_consensus),collapse=", ")
			}
			# Check if intronic and pull details per transcript
			intronic <- which(hostGs[,"start"] > novel[as.numeric(indexL),"end"])
			intron_consensus <- c()
			if (!(end5) & !(end3) & length(intronic)>0) {
				for (i in 1:length(intronic)) {
					transIndex <- which(hostGs[,"transcript_id"]==hostGs[intronic[i],"transcript_id"])
					transExons <- hostGs[transIndex,]
					if (which(transIndex == intronic[i])-1 != 0) {
						intron_number <- hostGs[transIndex[which(transIndex == intronic[i])-1],"exon_number"]
						intron_consensus <- c(intron_consensus,paste(intron_number,hostGs[intronic[i], "transcript_id"],sep=":"))
					} else {
						intron_consensus <- c(intron_consensus,"5end")
					}
				}
				intron_consensus <- paste(unique(intron_consensus),collapse=", ")
			}
			

			if (!is.null(exon_consensus) & is.null(intron_consensus) & !(end5) & !(end3)) { # Novel miRNA is strictly exonic
				position <- c(position,paste("exon",exon_consensus,sep=" "))
			} else if (is.null(exon_consensus) & !is.null(intron_consensus) & !(end5) & !(end3)) { # Novel miRNA is strictly intronic
				position <- c(position,paste("intron",intron_consensus,sep=" "))
			} else if (end5) { # miRNA in 5' region before first known exon but after gene start position
				position <- c(position,"5' end")
			} else if (end3) {
				position <- c(position,"3' end")
			} else {
				position <- c(position,"ambiguous intron/exon")
			}
		}
	}
	
	# 'position' should be in the same order as 'ids'. Info we want are under gene_biotype and exon_number
	location[[indexL]] <- paste(position,collapse=", ")
}

# STEP THREE: does miRNA have a neighbouring ncRNA?
rownames(neighbours) <- neighbours[,1]

PDnovel <- do.call(rbind,strsplit(as.character(PDneighbours[,2]),"_"))
PDnb <- neighbours[as.character(PDneighbours[,1]),]

nearestNeighbour <- list()
if (nrow(PDneighbours) > 1) { # ONLY PERFORM IF THERE IS NEIGHBOURING NCRNA!
	for (p in 1:nrow(PDnb)) {
		# Verify predicted neighbour is in the same chromosome
		if (PDnb[p,"chr"] == as.numeric(PDnovel[p,1])) { 
			novIndex <- which((novel[,"chr"] == PDnovel[p,1]) 
					& (novel[,"start"] == PDnovel[p,2]) 
					& (novel[,"end"] == PDnovel[p,3]))
			if (length(novIndex) == 0) {
				stop("SOMETHING WENT WRONG. NO novIndex FOUND")
			} else if(length(novIndex) == 1) {
				nearestNeighbour[[as.character(novIndex)]] <- as.character(PDnb[p,"knownID"])
			} else {
				stop("MULTIPLE NOVEL MIRNAS MATCHING PD")
			}
		}
	}
}

# Create annotations table
allAnnot <- matrix(nrow=nrow(novel),ncol=9)
colnames(allAnnot) <- c("hostGeneID", "hostChr", "hostStart", "hostEnd", "hostStrand", "hostName", "hostBiotype", "location", "Nearest_ncRNA")
for (r in 1:nrow(allAnnot)) {
	if (!is.null(annotations[[as.character(r)]])) {
		allAnnot[r,1:6] <- as.character(annotations[[as.character(r)]])
	}
	if (!is.null(location[[as.character(r)]])) {
		allAnnot[r,7:8] <- as.character(location[[as.character(r)]])
	}
	if (!is.null(nearestNeighbour[[as.character(r)]])) {
		allAnnot[r,9] <- nearestNeighbour[[as.character(r)]]
	}
}

# Replace all 0s (resulting from collapsing NA rows) to NA
novel[novel == 0] <- NA


# Combine all info
table.out <- cbind(novel[,1:4],allAnnot,novel[,5:ncol(novel)])
table.out[,"chr"] <- gsub(23,"X",table.out[,"chr"])
table.out[,"chr"] <- gsub(24,"Y",table.out[,"chr"])
table.out[,"chr"] <- gsub(25,"MT",table.out[,"chr"])
table.out[,"hostChr"] <- gsub(23,"X",table.out[,"hostChr"])
table.out[,"hostChr"] <- gsub(24,"Y",table.out[,"hostChr"])
table.out[,"hostChr"] <- gsub(25,"MT",table.out[,"hostChr"])
