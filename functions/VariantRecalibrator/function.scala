def VariantRecalibrator (
      reference: FASTA ,
      variants: VCF = null,
      hapmap: VCF = null,
      omni: VCF = null,
      hcsnp: VCF = null,
      dbsnp: VCF = null,
      mills: VCF = null,
      gatk: String = "",
      files: String = "",
      memory: String  =  "4g" ,
      threads: Int  = 1,
      capture: Boolean  = true,
      snpAnno: String  =  "QD,HaplotypeScore,MQRankSum,ReadPosRankSum,FS,MQ,InbreedingCoeff" ,
      indelAnno: String  =  "QD,FS,HaplotypeScore,ReadPosRankSum,InbreedingCoeff,MQRankSum" ,
      truth: Double  = 99.0 ): (VCF) =
{    



    // ------------------------------------------------------------
    // Recalibrate VCF file by region or as a whole.
    // - Step 1: Calculate model
    // - Step 2: Apply recalibration
    // ------------------------------------------------------------
    
    // ------------------------------------------------------------
    // Create general settings.
    // ------------------------------------------------------------
    
    import org.anduril.runtime._

    var _gatk = gatk
    if (gatk == "") _gatk = getEnv("GATK_HOME")


    var inputStr = if (variants != null) "-input @var1@ " else ""
    if (files != "") inputStr=inputStr + files + " "
    var hapmapStr = if (hapmap != null) "-resource:hapmap,known=false,training=true,truth=true,prior=15.0 @var4@ " else ""
    var omniStr = if (omni != null) "-resource:omni,known=false,training=true,truth=false,prior=12.0 @var5@ " else ""
    var dbsnpStr = if (dbsnp != null) "-resource:dbsnp,known=true,training=false,truth=false,prior=2.0 @var6@ " else ""
    var hcsnpStr = if (hcsnp != null) "-resource:1000G,known=false,training=true,truth=false,prior=10.0 @var3@ " else ""
    var millsStr = if (mills != null) "-resource:mills,known=true,training=true,truth=true,prior=12.0 @var3@ " else ""

    // ------------------------------------------------------------
    // Step 1: Run GATK to calculate gaussian mixture model.
    // 1.1 First we calculate the SNP model
    // 1.2 Then we calculate the INDEL model
    // ------------------------------------------------------------
 
    // The core script for running GATK
    var gatkStr="java -Xmx" + memory + " -jar " + _gatk +
                "/GenomeAnalysisTK.jar -T VariantRecalibrator -R @var2@ " + inputStr +
                "-recalFile @arrayOut1@/recal.txt -nt " + threads + " " +
                "-tranchesFile @arrayOut1@/tranches.txt " +
                "-rscriptFile @arrayOut1@/plots.r "
            
    // Script for adding files to the array
    val indexStr="""; echo -e recal"\t"recal.txt >> @arrayIndex1@; """ +
                 """echo -e tranches"\t"tranches.txt >> @arrayIndex1@; """  +
                 """echo -e rscript"\t"rscript.r >> @arrayIndex1@"""

    // Add parameters and files for the snp model
    var modeStr="-mode SNP "
    var trainingStr=hapmapStr + omniStr + dbsnpStr + hcsnpStr
    var paramStr="--numBadVariants 1000 "
    var annoStr=" -an " + snpAnno
    annoStr=annoStr.replaceAll(","," -an ")
    if (!capture) annoStr=annoStr+" -an DP "
    else annoStr=annoStr + " "
 
    // Run GATK: 1.1 Calculate SNP model    
    val gatk_model_snp = BashEvaluate(
        var1=variants,
        var2=reference, 
        var3=hcsnp,
        var4=hapmap,
        var5=omni,
        var6=dbsnp,
        script=gatkStr + paramStr + trainingStr + annoStr + modeStr + indexStr
        )
    gatk_model_snp._filename("arrayOut1", "model_snp")

    // Add parameters and files for the indel model
    modeStr="-mode INDEL "
    trainingStr=dbsnpStr + millsStr
    paramStr="--maxGaussians 4 --numBadVariants 1000 "
    annoStr=" -an " + indelAnno
    annoStr=annoStr.replaceAll(","," -an ")
    annoStr=annoStr + " "

    // Run GATK: 1.2 Calculate INDEL model
    val gatk_model_indel = BashEvaluate(
        var1=variants,
        var2=reference,
        var3=mills,
        var6=dbsnp,
        script=gatkStr + paramStr + trainingStr + annoStr + modeStr + indexStr
        )
    gatk_model_indel._filename("arrayOut1", "model_indel")

    // ------------------------------------------------------------
    // Step 2: Run GATK to apply the calculated models.
    // 2.1 First apply the INDEL model
    // 2.2 Then apply the SNP model
    // ------------------------------------------------------------

    // Create the core script for running GATK
    gatkStr="java -Xmx" + memory + " -jar " + _gatk +
            "/GenomeAnalysisTK.jar -T ApplyRecalibration -R @var2@ " + inputStr +
            "-tranchesFile @var4@ " +
            "-recalFile @var5@ " +
            "-o @optOut1@ "
    
    // Set the parameters
    modeStr="-mode INDEL "
    paramStr="--ts_filter_level " + truth + " "

    // Run GATK: 2.1 Apply INDEL model    
    val gatk_calibrated_indel = BashEvaluate( 
        var1=variants,
        var2=reference,
        var4=gatk_model_indel.arrayOut1("tranches"),
        var5=gatk_model_indel.arrayOut1("recal"),
        script=gatkStr + paramStr + modeStr + indexStr
        )
    gatk_calibrated_indel._filename("out1", "recal_indel.vcf")

    modeStr="-mode SNP "

    // Run GATK: 2.2 Apply SNP model    
    val gatk_calibrated_snp = BashEvaluate(
            var1=gatk_calibrated_indel.out1, //variants,
            var2=reference,
            var4=gatk_model_snp.arrayOut1("tranches"),
            var5=gatk_model_snp.arrayOut1("recal"),
            script=gatkStr + paramStr + modeStr + indexStr
            )
    gatk_calibrated_snp._filename("out1", "recal_snp.vcf")

     return gatk_calibrated_snp.out1
    
}
