def VariantSelector (
      reference: FASTA ,
      variants: VCF ,
      intervals: BED = null,
      conc: VCF = null,
      disc: VCF = null,
      gatk: String = "",
      variantType: String = "",
      select: String = "",
      region: String = "",
      memory: String  =  "4g" ,
      options: String = ""): (VCF) =
{ 

    // ------------------------------------------------------------
    // Use environment variable as default.
    // ------------------------------------------------------------

    import org.anduril.runtime._

    var _gatk = gatk
    if (gatk == "") _gatk = getEnv("GATK_HOME")
    
    // ------------------------------------------------------------
    // Run GATK SelectVariants.
    // ------------------------------------------------------------
    
    var refStr=""
    var intervalsStr=""
    var concStr=""
    var discStr=""
    var typeStr=""
    var selectStr=""
    var regionStr=""
    
    if (intervals != null) intervalsStr = " -L \"$var3\""
    if (conc != null) concStr           = " -conc \"$var4\""
    if (disc != null) discStr           = " -disc \"$var5\""
    if (typeStr != "") {
        typeStr="-selectType " + typeStr
        typeStr=typeStr.replaceAll(",","-selectType ")
    }

    var _options = options
    if (select != "") {
        selectStr=" -select \'" + select + "\'"
        _options += selectStr
    }
    if (region != "") { 
        regionStr=" -L " + region
        regionStr=regionStr.replaceAll(","," -L ")
    }
    
    val gatkVCF = QuickBash( in=makeArray(
                                "var1" -> reference,
                                "var2" -> variants,
                                "var3" -> intervals,
                                "var4" -> conc,
                                "var5" -> disc),
                             script="java -Xmx" + memory + " -jar " + _gatk + concStr + discStr + intervalsStr + regionStr + 
                                " -R \"$var1\" -T SelectVariants --variant \"$var2\" -o \"$out\" " + _options
                            )
    gatkVCF._filename("out", "selected.vcf")
    
    return gatkVCF.out

}
