
def BamRegrouper ( 
      bam: BAM , 
      picard: String = "",
      memory: String  =  "4g" ,
      ID: String  =  "1" ,
      LB: String ,
      PL: String ,
      PU: String ,
      SM: String  ): BAM = {            
    import org.anduril.runtime._

    var options=" SORT_ORDER=coordinate"
    options=options + " RGID=" + ID
    options=options + " RGLB=" + LB
    options=options + " RGPL=" + PL
    options=options + " RGPU=" + PU
    options=options + " RGSM=" + SM

    val picard_home = if (picard == "") getEnv("SEQUENCING_BUNDLE_HOME")+"/lib/picard/" else picard

    val picard_regroup = BashEvaluate( // Reorder regions
            var1=bam,
            script="mkdir -p tmp; java -Xmx" + memory + " -Djava.io.tmpdir=`pwd`/tmp -jar " + picard_home +
                "picard.jar AddOrReplaceReadGroups VALIDATION_STRINGENCY=SILENT VERBOSITY=WARNING " +
                "INPUT=@var1@ OUTPUT=@out1@ CREATE_INDEX=true" + options +"; rm -r `pwd`/tmp"
            )

    return picard_regroup.out1

}
