/* modify a miRBase GFF3 file to reference Ensembl transcript IDs */


def AnnotEnsembl ( 
    reference: String = "",
    ensembl_host: String  =  "feb2014.archive.ensembl.org" ,
    ensembl_dataset: String  =  "hsapiens_gene_ensembl"  ): CSV = {

    val transcripts = REvaluate( // Pull all known EnsemblIDs mapped to miRBase IDs
        script = INPUT(path="biomart.r"),
        param1 = "ensembl_transcript_id,transcript_biotype, external_transcript_name",
        param2 = ensembl_host,
        param3 = ensembl_dataset,
        param4 = reference
        )

    val annotate_miRBase_array = Folder2Array(
        folder1 = transcripts.optOut1,
        filePattern = "ncrna.gff"
    )

    val annotate_miRBase_csv = Array2CSV( // Create a CSV file with a single reference row containing the new annotation file
        in = annotate_miRBase_array.out
        )

    return annotate_miRBase_csv.out
}
