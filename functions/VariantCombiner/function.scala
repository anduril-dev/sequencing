def VariantCombiner (
      reference: FASTA ,
      variants: VCF = null,
      variants1: VCF = null,
      variants2: VCF = null,
      variants3: VCF = null,
      variants4: VCF = null,
      variants5: VCF = null,
      gatk: String = "",
      files: String = "",
      memory: String  =  "4g" ,
      disjoint: Boolean  = false,
      genotype: String  =  "UNSORTED" ,
      options: String = "" ): (VCF) = {
    
    // ------------------------------------------------------------
    // Use environment variables as defaults.
    // ------------------------------------------------------------

    import org.anduril.runtime._

    var _gatk = gatk
    if (gatk == "") _gatk = getEnv("GATK_HOME")

    
    // ------------------------------------------------------------
    // Run GATK CombineVariants
    // ------------------------------------------------------------

    var _files = files
    if (variants1 != null) _files = _files + " -V @var2@"
    if (variants2 != null) _files = _files + " -V @var3@"
    if (variants3 != null) _files = _files + " -V @var4@"
    if (variants4 != null) _files = _files + " -V @var5@"
    if (variants5 != null) _files = _files + " -V @var6@"

    val gatkStr="""
java -Xmx""" + memory + """ -jar """ + _gatk + 
 """/GenomeAnalysisTK.jar -T CombineVariants -R @var1@ """ + _files + 
 """ $( paste -d ' ' <(getarraykeys array1) <(getarrayfiles array1)  | sed 's,^, --variant:,' | tr -d '\\n' ) -o @out1@"""

    var paramStr=" -genotypeMergeOptions "+genotype
    if (disjoint) paramStr=paramStr+" -assumeIdenticalSamples"
    if (options != "") paramStr=paramStr + " " + options 
    
    val gatkCV = BashEvaluate( // Combine variants
          var1=reference,
          var2=variants1,
          var3=variants2,
          var4=variants3,
          var5=variants4,
          var6=variants5,
          array1=variants,
          script=gatkStr + paramStr
          )
    gatkCV._filename("out1", "combined.vcf")

     return gatkCV.out1
    
}
