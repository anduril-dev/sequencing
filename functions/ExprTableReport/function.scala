import anduril.builtin._
import anduril.microarray._
import anduril.sequencing._
import anduril.tools._
import anduril.anima._
import org.anduril.runtime._

def ExprTableReport (
  expr: CSV,
  ref: CSV,
  colLabels: CSV = null,
  annotation: GTF = null,
  geneSet: CSV = null,
  bodyMap: CSV = null,
  keyCol: String = "gene_id",
  nameCol: String = "gene_name",
  bodySite: String = "body",
  top: Int = 10,
  minimum: Double = 0.0,
  sampleKeyCol: String = "Key"
  ): (CSV,CSV,Latex,HTML) = {

  //----------------------------------------//
  // Optional inputs
  //----------------------------------------//
  val myGeneSet = if( geneSet == null ) INPUT(_functionFolder+"data/oncogenes.csv").out else geneSet
  val myBodyMap = if( bodyMap == null ) INPUT(_functionFolder+"data/bodyMap.csv").out else bodyMap

  // Adds bio_type annotation to the expression matrix if available
  // incluce_biotype = ""/"skip" <- used by ExpressionStats component
  // myAnnotation <- gene_id, gene_name and bio_type if annotation available
  // exprTable <- original table with annotation if available
  val (include_biotype, myAnnotation, exprTable) = if (annotation != null) {
      // Parse annotation file to get the values for the columns defined in parameters
      val myAnnotation0 = GTFParser( in=annotation, fields=keyCol+","+nameCol+",gene_type" )
      // Rename column biotype because it is hard-coded in ExpressionStats component
//      val myAnnotation1 = CSVCleaner( in=myAnnotation0, rename="gene_type=bio_type" )
      // Until GTFParser supports filtering duplicates we need this sorting step
      val myAnnotation = QuickBash( in=myAnnotation0, 
                                    script="""head -n1 $in | sed 's/gene_type/bio_type/' > $out
                                              tail -n +2 $in | sort -u >> $out""" ).out
      // Add annotation columns to gene expression and samples info
      val exprTableAnnotated = CSVJoin( in1=myAnnotation, in2=expr, keyColumnNames=keyCol+","+keyCol )
      val exprTableFilteredWithRef = CSVFilter( in=exprTableAnnotated, includeColumnsFile=ref, colOrder=true, includeColumns=keyCol+","+nameCol+",bio_type" )
      // output from the if-else
      ( "", myAnnotation, exprTableFilteredWithRef.out )
  } else {
      val myAnnotation = BashEvaluate(
        var1 = expr,
        param1 = nameCol,
        param2 = keyCol,
        script = """n=$( csvcoln @var1@ @param1@ )
                    i=$( csvcoln @var1@ @param2@ )
                    cut -f$i,$n @var1@ > @out1@""").out1
      ( "skip", myAnnotation, expr)
  }

  //----------------------------------------//
  // Filter gene table with various 
  // inputs and parameters
  //----------------------------------------//

  // Parameter 'minimum': keep only genes where at least one
  // sample has expression above "minimum"
  val filter_script = StringInput(content="""
    samples <- table2[ ,param2]                 # list of samples
    expr <- as.matrix( table1[, samples] )      # get only columns with expression values
    ## Make list of T/F: T if row contained at least one value above param1
    tag <- apply( expr > as.numeric( param1 ), 1, function(x) sum(x) > 0)
    table.out <- table1[ which( tag == T ), ]   # return rows where tag was true in previous step
    """)
  val exprTableFilteredWithMinimum = REvaluate( script=filter_script, table1=exprTable, table2=ref, param1=minimum.toString, param2=sampleKeyCol)

  // Genes available in the expression table
  val geneList = CSVCleaner( in=exprTable, columnsOut=keyCol+","+nameCol)

  // Input port: geneSet
  val matchingGeneSet = CSVFilter(in=CSVJoin( in1=geneList, in2=myGeneSet, keyColumnNames=nameCol+",geneName").out,
                                  includeColumns=keyCol)

  // Input port: bodyMap
  val matchingBodyMap = CSVFilter(in=CSVJoin( in1=geneList, in2=myBodyMap, keyColumnNames=nameCol+",geneName" ).out,
                                  includeColumns=nameCol,
                                  negate=true)



  //----------------------------------------//
  // Calculate stats: 
  //----------------------------------------//
  val rStats = ExpressionStats(
    expr            = exprTable, //provided expression matrix
    ref             = ref,  //sample and treatment groups
    geneSet         = matchingGeneSet,
    bodyMap         = matchingBodyMap,
    makeVisuals     = true,
    refID           = "",
    refGroup        = "Treatment",
    exprID          = keyCol, // gene_id
    topNum          = 10,
    bodySite        = bodySite, //body
    biotype         = include_biotype, //true/false
    biotype_min     = minimum)

  //----------------------------------------//
  // Output table for the website
  //----------------------------------------//
  val statsTable = CSVJoin( in1=myAnnotation, in2=rStats.stats )
  //TODO: change this to expression table
  //val htmlTable = HTMLTable( in= statsTable, title="Expression statistics by sample groups." )
  val newTable = REvaluate( table1=exprTable,
                            script=StringInput(content="""
library(matrixStats)
numExpr <-Filter(is.numeric, table1)
zz<-colSums(numExpr==0)/nrow(numExpr)*100
aa<-colQuantiles(as.matrix(numExpr),probs = seq(0.5, 1, 0.1))
table.out <- round(cbind('% of expr=0'=zz,aa),2)""").out)

  val newTableReHeaded = CSVFilter(in=newTable.table,
																		rename="RowName=SampleName,X..of.expr.0=%_of_0,X50.=50%,X60.=60%,X70.=70%,X80.=80%,X90.=90%,X100.=100%")

	val htmlTable = HTMLTable( in= newTableReHeaded.out,
														title="Percentage of 0 expression and expression values at 50-100 quantile" )

  //----------------------------------------//
  // Calculate correlation between samples
  // using genes that passed the minimum expr threshold
  //----------------------------------------//
  val matrix = CSVFilter( in=exprTableFilteredWithMinimum.table, includeColumnsFile=ref, colOrder=true, includeColumns=keyCol)
  val correlation = CorrelationReport( matr=matrix.force() )    

  //----------------------------------------//
  // Heatmap collection TODO: check in example website which ones
  //----------------------------------------//
  val heatmaps = buildHeatmapsETR( annot=myAnnotation, include_biotype=( annotation!=null ), rStats=rStats, exprTable=exprTable, keyCol=keyCol, nameCol=nameCol, colLabels=colLabels )

  //----------------------------------------//
  // Pie chart by bio_type
  // return null if no annotation available
  //----------------------------------------//
  val pieChart = buildPieChartETR( rStats=rStats, skip=(annotation==null) )

  //----------------------------------------//
  // Collect plots to build ImageGallery
  // and website result
  //----------------------------------------//
  val imageFolder = FolderCombiner(
    in1 = rStats.report,
    in2 = correlation.report,
    in3 = pieChart,
    in = heatmaps,
    exclude = ".*tex")
  val gallery = ImageGallery( inRoot=imageFolder )

  val websiteFolder = buildWebpageETR( htmlTable=htmlTable, gallery=gallery )

  //----------------------------------------//
  // Output of the function should be:
  // (CSV,CSV,Latex,HTML)
  //----------------------------------------//
  val expressed = exprTableFilteredWithMinimum.table
  val table = statsTable
  val document = rStats.report
  val report = websiteFolder
  return ( expressed, table, document, report )
}

/* 
* Make array or folder of heatmaps
*/
def buildHeatmapsETR(
  annot: CSV,
  include_biotype: Boolean,
  rStats: ExpressionStats,
  exprTable: CSV,
  keyCol: String,
  nameCol: String,
  colLabels: CSV): NamedMap[Latex] = {
  //----------------------------------------//
  // Heatmap of top oncogenes (myGeneSet)
  //----------------------------------------//
  val heatmaps = NamedMap[Latex]("heatmaps")
  val myGeneSetStats = CSVJoin( in1 = annot, in2 = rStats.statsArray("topOnco"), keyColumnNames = keyCol+","+keyCol )
  val excludeColumns = if( include_biotype ) "bio_type,"+keyCol else keyCol
  val myGeneSetExpr =  CSVFilter( in = myGeneSetStats, includeColumns = excludeColumns, negate = true )

  val include_geneSet = if ( myGeneSetExpr.out.textRows() > 1 ) {
    heatmaps("Onco") = HeatMapReport(
      in            = myGeneSetExpr.force(),
      colLabels     = colLabels,
      colorScheme   = "colorRampPalette(c('white','orange','brown'))((256))",
      plotHeight    = 12, 
      fontSizeRow   = 10, 
      plotWidth     = 16,
      drawLegends   = true,
      drawColnames  = true).out
    ( true )
  } else {
    println("No oncogenes expressed.")
    ( false )
  }

  //----------------------------------------//
  // Heatmap of top body map genes
  //----------------------------------------//
  val myBodyMapStats = CSVJoin( in1=annot, in2=rStats.statsArray("topExpressedByTissue"), keyColumnNames=keyCol+",id")
  // Make new id column: name_ensemblID
  val myBodyMapStatsConcatID = R"""dat <- CSV.read($myBodyMapStats)
    table.out <- cbind( id=paste( dat$$$nameCol, dat$$$keyCol, sep='_'), dat)"""

  val myBodyMapExpr = CSVFilter( in=myBodyMapStatsConcatID.table, includeColumnsFile=ref, colOrder=true, includeColumns="id" )

  heatmaps("Body") = HeatMapReport(
    in          = myBodyMapExpr.force(),
    colLabels   = colLabels,
    colorScheme = "colorRampPalette(c('white','orange','brown'))((256))",
    plotHeight  = 12, 
    fontSizeRow = 10, 
    plotWidth   = 16, 
    scale       = "row",
    drawLegends = true,
    drawColnames= true).out

  //----------------------------------------//
  // Heatmap of top expressed genes
  //----------------------------------------//
  val topExprStats = CSVJoin( in1=annot, in2=rStats.statsArray("topExpressed"), keyColumnNames=keyCol+",id")
  val topExprStatsUnique = CSVSort( in=topExprStats, keyColumns=nameCol , types=nameCol+"=string", unique=true)
  val topExpr = CSVFilter( in=topExprStatsUnique.out, includeColumns  = excludeColumns,negate=true )
  heatmaps("Top") = HeatMapReport(
    in          = topExpr.force(),
    colLabels   = colLabels,
    colorScheme = "colorRampPalette(c('white','orange','brown'))((256))",
    plotHeight  = 12, 
    fontSizeRow = 10, 
    plotWidth   = 16, 
    scale       = "row",
    drawLegends = true,
    drawColnames= true).out

  //----------------------------------------//
  // Heatmap of protein_coding genes
  //----------------------------------------//
  if ( include_biotype ) {
    val proteinCoding = CSVFilter( in = exprTable, regexp = "bio_type=protein_coding" )
    val getTopVariance = CSVTransformer(
      csv1=proteinCoding,
      transform1  ="""variance <-apply(csv1[,-c(1:3)],1,var)
                  quant <- quantile(variance,0.9)
                  csv1[variance>quant,] """)
    val topProtCoding = CSVFilter( in=getTopVariance, includeColumns=nameCol+",bio_type", negate=true)
    heatmaps("Prot") = HeatMapReport(
      in          = topProtCoding.force(),
      colLabels   = colLabels,
      colorScheme = "colorRampPalette(c('white','orange','brown'))((256))",
      plotHeight  = 12,
      fontSizeRow = 10,
      plotWidth   = 16,
      scale       = "row",
      drawLegends = true,
      drawColnames= true,
      drawRownames= false).out
  }

  return heatmaps
}

/* 
* Make pie chart
*/
def buildPieChartETR( rStats: ExpressionStats, skip: Boolean ): Latex = {
  //----------------------------------------//
  // Using Ensembl classification bin the
  // genes into protein_coding, pseudogenes, 
  // long_noncoding, and short_noncoding genes
  // and make a pie chart
  //----------------------------------------//
  val pieChart = if( !skip ) {
    val pieChartScript = BashEvaluate(
      var1=rStats.statsArray("summary"),
      script="""
        protein_coding=$( grep -E 'IG_C_gene|IG_D_gene|IG_gene|IG_J_gene|IG_V_gene|IG_LV_gene|IG_M_gene|IG_Z_gene|nonsense_mediated_decay|nontranslating_CDS|non_stop_decay|polymorphic_pseudogene|protein_coding|TR_V_gene|TR_C_gene|TR_D_gene|TR_J_gene' @var1@ | tr -d '"' | awk '{sum=sum+$2} END {print sum}' )
        [[ -z "$protein_coding" ]] && protein_coding=0
        pseudogene=$( grep -E 'disrupted_domain|IG_C_pseudogene|IG_J_pseudogene|IG_pseudogene|IG_V_pseudogene|transcribed_processed_pseudogene|unitary_pseudogene|transcribed_unitary_pseudogene|translated_processed_pseudogene|TR_V_pseudogene|processed_pseudogene|unprocessed_pseudogene|transcribed_unprocessed_pseudogene' @var1@ | tr -d '"' | awk '{sum=sum+$2} END {print sum}' )
        [[ -z "$pseudogene" ]] && pseudogene=0
        long_noncoding=$( grep -E '3prime_overlapping_ncrna|ambiguous_orf|antisense|antisense_RNA|lincRNA|ncrna_host|processed_transcript|sense_intronic|sense_overlapping' @var1@ | tr -d '"' | awk '{sum=sum+$2} END {print sum}' )
        [[ -z "$long_noncoding" ]] && long_noncoding=0
        short_noncoding=$( grep -E 'misc_RNA|snoRNA|miRNA|Mt_rRNA|snRNA|Mt_tRNA|rRNA|miRNA_pseudogene|miscRNA_pseudogene|scRNA|snlRNA|tRNA|tRNA_pseudogene' @var1@ | tr -d '"' | awk '{sum=sum+$2} END {print sum}')    
        [[ -z "$short_noncoding" ]] && short_noncoding=0
        echo $protein_coding $pseudogene $long_noncoding $short_noncoding
        ( 
            echo '
            library(RColorBrewer)
            slices <- c('${protein_coding}','${pseudogene}','${long_noncoding}','${short_noncoding}')
            labels <- c("protein_coding","pseudogene","long_noncoding","short_noncoding")
            pct <- round(slices/sum(slices)*100)
            labels <- paste(labels, pct)
            labels <- paste(labels,"%",sep="")
            setwd(document.dir)
            pdf("pieChart.pdf",width=8)
            pie(slices,labels = labels, col=brewer.pal(n=length(labels),name="Dark2"),main="BioTypes")
            dev.off()
            table.out <- data.frame()
          '
        ) > @out1@ """)
  val pieChart = REvaluate(script = pieChartScript.out1)
    pieChart.document
  } else {
    null
  }
  return pieChart
}

/* 
* Make website report with table and plots
*/
def buildWebpageETR(
  htmlTable: HTMLTable,
  gallery: HTML):BinaryFolder = {

  //----------------------------------------//
  // HTML template
  //----------------------------------------//
  val iframe = StringInput(
    content="""
      <html><head><title>Expression Statistics</title>
      <style>
      body { margin: 0; overflow: hidden; }
      iframe { height:100%;}
      </style></head><body>
      <iframe src="table.html" style="width: 50%;"></iframe>
      <iframe src="gallery/index.html?p=1" style="width: 48%;"></iframe>
      </body></html>
    """)

  //----------------------------------------//
  // Put table and plots in the HTML template
  //----------------------------------------//
  val webpage = BashEvaluate(
    var1= htmlTable,
    var2= gallery,
    var3= iframe,
    script="""
      cd @folder1@
      cp @var1@ table.html
      cp -r @var2@ gallery
      cp @var3@ index.html 
    """)
  return webpage.folder1 
}
