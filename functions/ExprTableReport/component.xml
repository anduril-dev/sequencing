<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>ExprTableReport</name>
    <version>1.0</version>
    <doc><p>Generates expression statistics into an HTML report. All heatmaps use gene names, so they need to be provided either in a GTF file or as part of the expression matrix.
    The annotation file can be used also to subset the genes of interest, only genes included in the annotation file will be used for the statistics and plots.</p>
    </doc>
    <author email="alejandra.cervera@helsinki.fi">Alejandra Cervera</author>
    <author email="katherine.icay@helsinki.fi">Katherine Icay</author>
    <author email="julia.casado@helsinki.fi">Julia Casado</author>
    <category>Expression</category>
    <inputs> 
        <input name="expr" type="CSV" optional="false">
            <doc>Expression matrix. Values should be in log2 if visualization is enabled.</doc>
        </input>
        <input name="ref" type="CSV" optional="false">
            <doc>CSV file containing sample names and treatment groups. Sample names must match column names of <code>expr</code>.</doc>
        </input>
        <input name="colLabels" type="CSV" optional="true">
            <doc>Label heatmaps with, i.e. clinical information.</doc>
        </input>
        <input name="annotation" type="GTF" optional="true">
            <doc>Ensembl GTF annotation file to be used for grouping genes in biotypes.</doc>
        </input>
        <input name="geneSet" type="CSV" optional="true">
            <doc>One-column list of interesting genes (Ensembl geneId) to create heatmap for. If empty, the oncogenes from the cancer census list will be used.</doc>
        </input>
        <input name="bodyMap" type="CSV" optional="true">
            <doc>Index matrix of geneIds (rows) per tissue (columns). If empty, the Illumina Body Map will be used.</doc>
        </input>
    </inputs>
    <outputs>
        <output name="table" type="CSV" array="false">
            <doc>
            </doc>
        </output>
        <output name="expressed" type="CSV" array="false">
            <doc>Expression table that has at least one column with the gene or transcript ids, and expression columns corresponding to several samples with the expression values in log2.
            </doc>
        </output>
        <output name="document" type="Latex">
            <doc>Document containing all the visuals produced.</doc>
        </output>
        <output name="report" type="HTML">
            <doc>Expression statistics report.</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="keyCol" type="string" default="gene_id">
            <doc>Column name with the unique ids to be used.</doc>
        </parameter>
        <parameter name="nameCol" type="string" default="gene_name">
            <doc>When input <code>annotation</code> is not provided, then the column containing gene names must be defined from <code>expr</code>.</doc>
        </parameter>
        <parameter name="bodySite" type="string" default="body">
        <doc>Any body tissue from the Illumina Body Map, can be the emtpy string or heart, stomach, brain ...</doc>
        </parameter>
        <parameter name="top" type="int" default="10">
            <doc>Number of top genes to be reported.</doc>
        </parameter>
        <parameter name="minimum" type="float" default="0">
       <doc>Threshold for minimum expression to be included in the reduced CSV output.</doc>
        </parameter>
        <parameter name="sampleKeyCol" type="string" default="Key">
       <doc>Column name for sample IDs in input CSV ref.</doc>
        </parameter>
    </parameters>
</component>

