def RNAVariantCaller (
	reference : FASTA,
	in : BAM,	
	gatkPath : String = "",
	memory : String = "4g",
	callConf : String = "20.0",
	haplotypeOptions : String = "",
	window : String = "35",
	cluster : String = "3",
	filterNames : String = "FS,QD",
	filters : String = "FS > 30.0,QD < 2.0",
	filteringOptions : String = "") : (VCF) = {
	
	
	// Find where to find  gatk
	import org.anduril.runtime._

	val gatk = if (gatkPath == "") getEnv("GATK_HOME") else gatkPath
		
	// Run HaplotypeCaller
	var haplotypeScript = gatk + "/gatk  HaplotypeCaller -dont-use-soft-clipped-bases -stand-call-conf " + callConf 
	haplotypeScript = if (haplotypeOptions == "") haplotypeScript else haplotypeScript + " " + haplotypeOptions
	
	val calls = BashEvaluate(var1   = in,
							 var2   = reference,
							 script = haplotypeScript + " -I @var1@ -R @var2@ -O @out1@")
	calls._filename("out1", "calls.vcf")
	
	
	// Filtering
	var filterScript = gatk + "/gatk VariantFiltration -window " + window + " -cluster " + cluster
      if (filterNames != ""){
       val filterNamesSplit = filterNames.split(",")
       val filtersSplit = filters.split(",")
       val n = filterNamesSplit.length
       var i = 0
       for ( i <- 0 to n-1 ){
	    filterScript = filterScript + " --filter-name " + filterNamesSplit(i) + " -filter " + '"' + filtersSplit(i) + '"'   
	  }
    }
    filterScript = if(filteringOptions == "") filterScript else filterScript + " " + filteringOptions
    	
	val filtered = BashEvaluate(var1 = calls.out1,
								var2 = reference,
								script = filterScript + " -R @var2@ -V @var1@ -O @out1@")
	filtered._filename("out1", "filtered.vcf")
	
	
	return (filtered.out1)
}
