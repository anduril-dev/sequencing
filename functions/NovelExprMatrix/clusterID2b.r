# Create unique cluster IDs based on Distance and Point_chr.

# raw counts to merge by columns "chr, start, end"
previous <- table1
present <- table2 

# Point distance results per neighbour pairs
pairDistance <- table3
colnames(pairDistance) <- gsub("\\."," ",colnames(pairDistance))

# Union of EXACT SAME chromosomal positions across samples
countCombin <- merge(previous,present,by=c("chr","start","end","strand"), all=TRUE)

# Make a copy of the count matrix
countCombin_collapsed <- countCombin

# Create chromosomal IDs from point distances and chromosome start position
clusterID <- paste(as.character(pairDistance[,"Distance"]),
					as.character(pairDistance[,"Point chr"]),sep="_")


# Give those with Distance > param1 and duplicated values a unique ID by adding start position
changeIDs <- which(pairDistance[,"Distance"] > as.numeric(param1) & duplicated(clusterID))
for (n in changeIDs[order(changeIDs)]) {
	clusterID[n] <- paste(clusterID[n], as.character(pairDistance[n,"Point start"]), sep="_")
	}

# Find the clusterID of the ones to merge
clusterIDs <- unique(clusterID[which(pairDistance[,"Distance"] <= as.numeric(param1))])

# Retrieve chromosomal positions and union of positions
if (length(clusterIDs) != 0) {
for (id in clusterIDs) {
		clustered <- pairDistance[which(as.character(clusterID) == id),]
		# Skip over those that merge successfully with exact same positions OR are in different chromosomes
		if (((clustered[1,"Point start"] != clustered[1,"Data start"]) 
			| (clustered[1,"Point end"] != clustered[1,"Data end"]))
			& (clustered[1,"Point chr"] == clustered[1,"Data chr"])) {
			clusterIndex <- which((countCombin[,"chr"] == clustered[1,"Point chr"] 
									& countCombin[,"chr"] == clustered[1,"Data chr"]
									)
									& (countCombin[,"start"] == clustered[1,"Point start"] 
									| countCombin[,"start"] == clustered[1,"Data start"])
									)
			# Check				
			if (length(clusterIndex) < 2) {
				msg <- paste(id," had clusterIndex < 2",sep="")
				stop(msg)
				break
			}
			clusteredSet <- countCombin[clusterIndex,]
			newStart <- min(clusteredSet[,"start"], na.rm=TRUE)
			newEnd <- max(clusteredSet[,"end"], na.rm=TRUE)
			sumCounts <- colSums(clusteredSet[,c(5:ncol(clusteredSet))],na.rm=TRUE)

			# Replace info for "start" and "end" in first clustered entry of collapsed copy
			countCombin_collapsed[which(rownames(countCombin_collapsed) 
								== as.character(clusterIndex[1])),"start"] <- newStart
			countCombin_collapsed[which(rownames(countCombin_collapsed) 
								== as.character(clusterIndex[1])),"end"] <- newEnd
			countCombin_collapsed[which(rownames(countCombin_collapsed) 
								== as.character(clusterIndex[1])),c(5:ncol(countCombin_collapsed))] <- sumCounts
	
			# Delete all other clustered entries besides the first based on NUMBERED ROW NAMES
			for (N in clusterIndex[-1]) {
				if (length(which(rownames(countCombin_collapsed) == as.character(N))) != 0) {
					countCombin_collapsed <- countCombin_collapsed[-which(rownames(countCombin_collapsed) 
								== as.character(N)),]
					}
				}
		}
		if (nrow(countCombin_collapsed) == 0) {
			print(paste(id,nrow(countCombin_collapsed),sep=" "))
		stop("")
		}
	}
}

# Remove regions that are over param2
if (length(which((countCombin_collapsed[,"end"]-countCombin_collapsed[,"start"]) > as.numeric(param2))) != 0) {
	countCombin_collapsed <- countCombin_collapsed[-which((countCombin_collapsed[,"end"]-countCombin_collapsed[,"start"]) > as.numeric(param2)),]
}

table.out <- countCombin_collapsed
