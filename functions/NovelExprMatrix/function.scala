def NovelExprMatrix (
      novelArray: BinaryFile ,
      method: String  =  "miranalyzer" ,
      maxPointDistance: Int  = 30,
      maxLength: Int  = 300,
      minPercent: Double  = 0 ): (CSV,CSV) = {

    import org.anduril.runtime._

    // ID nearest neighbour to each chr position
    val nearestNeighbour = Record[CSV]("nearestNeighbour")
    val clusterIDr = INPUT(path="clusterID.r")
    val clusterID2r = INPUT(path="clusterID2b.r")
    val clusterID3r = INPUT(path="clusterID3.r")

    for ( (key,rec) <- iterArray(novelArray) ) {
        val isExtra = key.substring(0,5)
        if (isExtra != "extra") {
            val novelExpr_PD = PointDistance(
                    in = novelArray(key),
                    reference = novelArray(key),
                    cols = "chr,start,end",
                    nth = 2,
                    _name = "novelExpr_PD_"+key
                    )
                
            val novelExpr_clusterID = REvaluate(
                    script = clusterIDr,
                    table1 = novelExpr_PD.out,
                    table2 = novelArray(key),
                    param1 = "10", // PointDistance cut-off
                    param2 = key,
                    _name = "novelExpr_clusterID_"+key
                    )

            nearestNeighbour(key) = novelExpr_clusterID.table
        }
     }
    
    // ID nearest neighbour to each sample
    val Previous = Chain[CSV]("Previous")

    var lastSample = ""
    val nearestNeighbour2 = Record[CSV]("nearestNeighbour2")
    for ( ((key,value),n) <- nearestNeighbour.zipWithIndex ) {
        if (n == 0) {
            Previous += nearestNeighbour(key)
        } else {
            val novelExpr_PD2 = PointDistance(
                    in = value,
                    reference = Previous.chain.last,
                    cols = "chr,start,end",
                    nth = 2,
                    _name = "novelExpr_PD2_"+key
                    )
            
            val novelExpr_clusterID2 = REvaluate(
                    script = clusterID2r,
                    table1 = Previous.chain.last,
                    table2 = nearestNeighbour(key),
                    table3 = novelExpr_PD2.out,
                    param1 = maxPointDistance.toString(), // max. PointDistance of overlapping mapped locations to merge counts from
                    param2 = maxLength.toString(), // Max. allowed length of a potential novel precursor miRNA
                    _name = "novelExpr_clusterID2_"+key
                    )
            
            nearestNeighbour2(key) = novelExpr_clusterID2.table
            Previous += novelExpr_clusterID2.table
        }
        lastSample = key
    }

    // Collapse final expression matrix 
    val novelExpr_clusterID3 = REvaluate(
            script = clusterID3r,
            table1 = nearestNeighbour2(lastSample)
            )

    // Filter for a minimum percentage of sample columns with values
    val interestingNovel = INPUT(path="exprFltr.r")
    val novelExprFiltered = REvaluate(
            script = interestingNovel,
            table1 = novelExpr_clusterID3.table,
            param1 = minPercent.toString() // Min. percentage of samples that must have count value for a predicted miRNA to be included
            )

    return ( novelExprFiltered.table, novelExprFiltered.outArray("coordinatesID") )
}
