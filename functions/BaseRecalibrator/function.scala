
def BaseRecalibrator ( 
      reference: FASTA ,
      bam: BAM ,
      dbsnp: VCF ,
      mask: VCF = null,
      intervals: BED = null, 
      gatk: String = "",
      memory: String  =  "4g" ,
      plot: Boolean  = true,
      region: String = "",
      exclude: String = "",
      optionsTables: String = "",
      optionsRecal: String = "" ): (BAM,GatkReport,PDF) = {

    // ------------------------------------------------------------
    // Misc settings.
    // ------------------------------------------------------------

    import org.anduril.runtime._
    val gatk_home = if (gatk == "") getEnv("GATK_HOME") else gatk
    
    var regionStr=""
    var excludeStr=""
    var intervalsStr=""
    var snpFiles="--known-sites @var3@ "
    var optTables=optionsTables
    
    if (region != "") { 
        regionStr="-L " + region
        regionStr=regionStr.replaceAll(",", " -L ")
        regionStr=regionStr + " "
    }

    if (exclude != "") { 
        excludeStr="-XL " + exclude
        excludeStr=excludeStr.replaceAll(",", " -XL ")
        excludeStr=excludeStr + " "
    }

    if (intervals != null) intervalsStr = "-L @var4@ "
    if (mask != null) snpFiles = snpFiles + " --known-sites @var5@ "
    if (plot) optTables=optTables

    // ------------------------------------------------------------
    // Run GATK to calibrate base qualities.
    // - First we calculate calibration covariate table (BaseRecalibrator)
    // - Then we calibrate the base qualities (TableRecalibrator)
    // ------------------------------------------------------------

    val gatk_tables = BashEvaluate( // Count quality tables
            var1=bam,
            var2=reference,
            var3=dbsnp,
            var4=intervals,
            var5=mask,
            script=gatk_home + "/gatk BaseRecalibrator -R @var2@ -I @var1@ " + snpFiles +
                "-O @out1@ " +
                regionStr + excludeStr + intervalsStr + optTables
            )
    gatk_tables._filename("out1", "tables.grp")

    val gatk_plot = BashEvaluate( // Produce plots
            var1=gatk_tables.out1,
            script=gatk_home + "/gatk AnalyzeCovariates -bqsr @var1@ -plots @out1@ "
            )

    gatk_plot._filename("out1", "plot.pdf")

    val gatk_recal = BashEvaluate( // Calibrate base qualities
            var1=bam,
            var2=reference,
            var3=gatk_tables.out1,
            var4=intervals,
            script=gatk_home + "/gatk ApplyBQSR -R @var2@ -I @var1@ -O @out1@ " +
                "--bqsr-recal-file @var3@ " + regionStr + excludeStr + intervalsStr + optionsRecal
            )
    gatk_recal._filename("out1", "recal.bam")

    return (gatk_recal.out1, gatk_tables.out1, gatk_plot.out1)
    
}
