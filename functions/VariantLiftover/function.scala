def VariantLiftover (
      chain: TextFile ,
      oldReference: FASTA ,
      newReference: FASTA ,
      variants: VCF ,
      gatk: String = "",
      memory: String  =  "2g" ,
      original: Boolean  = false ): (VCF) =
{

    // ------------------------------------------------------------
    // Use environment variables as defaults.
    // ------------------------------------------------------------

    import org.anduril.runtime._

    var _gatk = gatk
    if (gatk == "") _gatk = getEnv("GATK_HOME")

    
    // ------------------------------------------------------------
    // Run GATK LiftoverVariants
    // ------------------------------------------------------------
    
    var inputStr=" --variant @var1@"
    inputStr=inputStr + " --chain @var2@"
    inputStr=inputStr + " --newSequenceDictionary " +
        "$( dirname @var3@)/$( basename @var3@ .fasta ).dict"
    
    var gatkStr="java -Xmx" + memory + " -jar " + _gatk +
            "/GenomeAnalysisTK.jar -T LiftoverVariants -R @var4@ --out @out1@"

    var paramStr=""
    if (original) paramStr=" --recordOriginalLocation"
    
    val lift = BashEvaluate( // Lift over variants
            var1=variants,
            var2=chain,
            var3=newReference,
            var4=oldReference,
            script=gatkStr + paramStr + inputStr
            )
    lift._filename("out1", "lifted.vcf")

    // ------------------------------------------------------------
    // Run VCFsorter to sort
    // ------------------------------------------------------------

    val vcfsorter=INPUT(path="vcfsorter.pl")
    
    val sorted = BashEvaluate(
            var1=lift.out1,
            var2=vcfsorter,
            var3=newReference,
            script="@var2@ $( dirname @var3@ )/$( basename @var3@ .fasta ).dict @var1@ > @out1@"
            )
    sorted._filename("out1", "sorted.vcf")

    // ------------------------------------------------------------
    // Run GATK FilterLiftedVariants
    // ------------------------------------------------------------

    inputStr=" --variant @var1@"
    
    gatkStr="java -Xmx" + memory + " -jar " + _gatk +
            "/GenomeAnalysisTK.jar -T FilterLiftedVariants -R @var2@ --out @out1@"

    
    val filt = BashEvaluate( // Filter lifted variants, 2nd step in liftover
            var1=sorted.out1,
            var2=newReference,
            script=gatkStr + inputStr
            )
    filt._filename("out1", "filtered.vcf")

    return filt.out1
    
}
