def ReferenceIndexer (
      reference: FASTA ,
      picard: String = "",
      aligner: String = "",
      length: String  =  "long" ,
      colorspace: Boolean  = false,
      options: String = ""): (FASTA) = {


    // ------------------------------------------------------------
    // Use environment variables as defaults.
    // ------------------------------------------------------------

    import org.anduril.runtime._

    val picard_home = if (picard == "") getEnv("SEQUENCING_BUNDLE_HOME")+"/lib/picard/" else picard
    
    // ------------------------------------------------------------
    // Create .fai index.
    // ------------------------------------------------------------
    
    val samtoolsStr="; samtools faidx @out1@"
    
    // ------------------------------------------------------------
    // Create .dict sequence dictionary.
    // ------------------------------------------------------------

    val picardStr="; java -jar " + picard_home + "picard.jar CreateSequenceDictionary " +
            "REFERENCE=@var1@ OUTPUT=$( dirname @out1@ )/reference.dict"

    val defCmd="rm -f @out1@; ln -s @var1@ @out1@" + samtoolsStr + picardStr
       

    var _options = options

    val ref = 
    if (aligner == "bwa") {

        // ------------------------------------------------------------
        // Run bwa index.
        // Output .fai and/or .dict files.
        // ------------------------------------------------------------

        if (colorspace) _options           = _options + " -c"
        if (length == "long") _options     = _options + " -a bwtsw"
        if (length == "short") _options    = _options + " -a is"
        
        val bwa = BashEvaluate(
                var1=reference,
                script=defCmd + "; bwa index " + _options + " @out1@"
                )
        bwa._filename("out1", "reference.fasta")

        bwa
        
    } else if (aligner == "bowtie" || aligner == "bowtie2") {

        // ------------------------------------------------------------
        // Run bowtie-build.
        // Output .fai and/or .dict files.
        // ------------------------------------------------------------

        if (colorspace) {
            if (aligner == "bowtie2") sys.error("ERROR: Colorspace alignment is not supported for Bowtie2")
            _options = _options + " -C"
        }

        var buildStr = ""
        if (aligner == "bowtie") buildStr="bowtie-build"
        if (aligner == "bowtie2") buildStr="bowtie2-build"
        
        val bowtie = BashEvaluate(
                var1=reference,
                script=defCmd + "; " + buildStr + " " + _options + " @out1@ $( dirname @out1@ )/$( basename @out1@ .fasta )"
                )
        bowtie._filename("out1", "reference.fasta")
        
        bowtie

    } else {

        // ------------------------------------------------------------
        // Do not create aligner index.
        // Output .fai and/or .dict files.
        // ------------------------------------------------------------
        
        val picardC = BashEvaluate(
                var1=reference,
                script=defCmd
                )
        picardC._filename("out1", "reference.fasta")

        picardC

    }
    
    return ref.out1

}
