// Calculate statistics for aligned reads.
import anduril.builtin._
import org.anduril.runtime._

def AlignStats (
      alignment: AlignedReadSet ,
      reference: FASTA ,
      annotation: GTF ,
      refgene: BED = null,
      chromsize: TextFile = null,
      log: BinaryFolder = null,
      modules: String  =  "1,2,3,4,5,6,7,8,9,10",
      sample: String  =  "sample",
      memory: Int = 35000 ) : (HTML,CSV) = {

    val samtoolsCheck="if [ -n $(which samtools) ];then export PATH=" + _functionFolder + "../lib/samtools:$PATH; fi; "
    val readLength="100"
    val layout="PE"
    val execute: Set[String] = modules.split(",").map(_.trim).toSet

    val moduleOutputs = NamedMap[BinaryFile]("moduleOutputs")

    // Create bed file from gtf
    val awkScript = INPUT(path=_functionFolder+"genePredToBed.awk") 
    val create_bed = BashEvaluate(  var1 = annotation,
                                var2 = awkScript,
                                script = "gtfToGenePred @var1@ stdout | awk -f @var2@ > @out1@")

    val annotBed = create_bed.out1
    val ref = if (refgene == null){
       create_bed.out1
    } else {
       refgene // refGene in principle is shorter than annotBed
    }

    //val RSeQC_setup = """export PATH=$PATH:"$( getmetadata bundlePath )/lib/RSeQC/bin";"""
    val flagstat = QuickBash( in      = alignment,
                              script  = "samtools flagstat $in > $out")
    
    // read distribution across gene body
    //the output of read_distribution.py is sent to out2 and from there we need to parse it to make it a nice table. 
    val readDist = BashEvaluate(var1    = alignment,
                                var2    = annotBed,
                                script  = """read_distribution.py -i @var1@ -r @var2@ &> @out2@
                                          egrep "CDS_Exons|UTR_Exons|Introns|10kb" @out2@ | sed 's/ \+ /\t/g' | cut -f1,3 | tsvtranspose > tmp1
                                          grep '^Total' @out2@ | sed 's/ \+ /\t/g' | sed 's/\ /_/g' | tsvtranspose > tmp2
                                          paste tmp2 tmp1 | tr -d '\r' > @out1@
                                          total=$( csvcoln @out1@ "Total_Tags" )
                                          tss=$( csvcoln @out1@ "TSS_up_10kb" )
                                          tes=$( csvcoln @out1@ "TES_down_10kb" )
                                          intron=$( csvcoln @out1@ "Introns" )
                                          echo $total $tss $tes $intron
                                          awk -v total=$total -v tss=$tss -v tes=$tes -v intron=$intron 'BEGIN {OFS="\t"} {if (NR==1) {inter="Intergenic_Rate"; intronic="Intronic_Rate";} else {inter=($tss+$tes)/$total; intronic=$intron/$total;} print $0,inter,intronic}' @out1@ > @out3@
                                          """)
    readDist._custom("memory")=memory.toString

    val statsT = BashEvaluate(var1 = flagstat.out,
                              var2  = readDist.out3, 
                              param1  = sample,
                              script  ="""echo -e "Sample\n@param1@" > @out3@
                              awk '{s=""; for (i=4; i<=NF; i++) {s=s$i" ";} print s,$1,$3}' OFS="\t" @var1@ | grep -v mapQ | awk -F"\t" '{if (NR==1) {split($1,a,"+"); print a[1],$2"\n"a[2],$3;} else {print $0}}' OFS="\t" | sed 's/\s*$//g' | sed 's/^\s//g' | cut -f1-2 | sed 's/in\ total\ (//' | awk -F"\t" '{split($1,a,"("); if (a[2]!="") {print a[1],$2"\n"a[1]"%",a[2]} else {print $0}}' OFS="\t"  | awk -F"\t" '{split($2,a,":"); print $1,a[1]}' OFS="\t" | tr -d ')' | tsvtranspose > @out2@
                              paste @out3@ @var2@ @out2@ | tr -d '\r' > @out1@ """)


   	val sort = if (execute("4") || execute("10")) {
        BashEvaluate(var1    = alignment,
      				script  = samtoolsCheck+"samtools sort -m 1000000000 @var1@ -o @arrayOut1@/input.sorted.bam; addarray arrayOut1 sorted input.sorted.bam; "+
                      			  samtoolsCheck+"samtools index @arrayOut1@/input.sorted.bam")
    } else {
        BashEvaluate(script="")
    }

    // Read quality module
    if (execute("1")) {

        val readQ = BashEvaluate(   var1    = alignment,
                                    script  = """date; read_quality.py -i @var1@ -o @arrayOut1@/mod1; date;  addarray arrayOut1 readQuality mod1.qual.boxplot.pdf""")

        moduleOutputs("readQuality") = readQ.arrayOut1("readQuality")
    }
    
    // Read duplication module
    if (execute("2")) {

        val readDup = BashEvaluate( var1    = alignment,
                                    script  = "date; read_duplication.py -i @var1@ -o @arrayOut1@/output; date; addarray arrayOut1 readDuplication output.DupRate_plot.pdf") 

        moduleOutputs("readDuplication") = readDup.arrayOut1("readDuplication")
    }

    // Read GC  module
    if (execute("3")) {

        val readGC = BashEvaluate(  var1    = alignment,
                                    script  = "date; read_GC.py -i @var1@ -o @arrayOut1@/output; date; addarray arrayOut1 readGC output.GC_plot.pdf")

        moduleOutputs("readGC") = readGC.arrayOut1("readGC")
    }

    // Gene coverage module
    if (execute("4")) {

        if (chromsize != null) {

            //formats = Folder2Array(folder1 = bam2bigWig.folder1)

            val geneCov = BashEvaluate( //var1    = alignment,//formats.array["output.bw"],
                                        var1 = sort.arrayOut1("sorted").force(),
                                        var2    = ref,
                                        script  = """date; geneBody_coverage.py -i @var1@ -r @var2@ -o @arrayOut1@/output; date; addarray arrayOut1 geneCov output.geneBodyCoverage.curves.pdf""")

            moduleOutputs("geneBodyCoverage") = geneCov.arrayOut1("geneCov")
            geneCov._custom("memory")=memory.toString
        }
        else sys.error("chrom size file is needed for gene coverage module")

    }

    /* Inner distance module */
    if (execute("5")) {

        val innerDist =  BashEvaluate(  var1    = alignment,
                                        var2    = annotBed,
                                        script  = "date; inner_distance.py -r @var2@ -i @var1@ -o @arrayOut1@/output > @out1@; date; addarray arrayOut1 innerDistance output.inner_distance_plot.pdf")

        moduleOutputs("innerDistance") = innerDist.arrayOut1("innerDistance")
    }

    /* Junction annotation module */
    if (execute("6")) {

        val junctionA = BashEvaluate(   var1    = alignment,
                                        var2    = annotBed,
                                        script  = """date; junction_annotation.py -r @var2@ -i @var1@ -o @arrayOut1@/output; date; addarray arrayOut1 splice_junction output.splice_junction.pdf; addarray arrayOut1 splice_events output.splice_events.pdf""")

        moduleOutputs("splice_events") = junctionA.arrayOut1("splice_events")
        moduleOutputs("splice_junction") = junctionA.arrayOut1("splice_junction")
    }

    /* Junction saturation module */
    if (execute("7")) {

        val junctionS = BashEvaluate(   var1    = alignment,
                                        var2    = annotBed,
                                        script  = "date; junction_saturation.py -r @var2@ -i @var1@ -o @arrayOut1@/output; date; addarray arrayOut1 junctionSaturation output.junctionSaturation_plot.pdf")

        moduleOutputs("junctionSaturation") = junctionS.arrayOut1("junctionSaturation")
    }

    /* Infer experiment module */
    if (execute("8")) {

        val inferExp = BashEvaluate(var1    = alignment,
                                    var2    = annotBed,
                                    script  = "date; infer_experiment.py -r @var2@ -i @var1@ &> @out1@; date")

        //csv1 = CSV2Latex(tabledata = inferExp.out1)
    }
    
   
    if (execute("9")) {

        val fragment = BashEvaluate(   var1    = alignment,
                                       var2    = annotBed,
                                       var3 = INPUT(path = _functionFolder+"../../lib/RSeQC/bin/RNA_fragment_size.py"),
                                       script  = "date; python2.7 @var3@ -r @var2@ -i @var1@ -n 0 > @out1@")

    } 
    
    if (execute("10")) {

        val tin = BashEvaluate(   //var1    = alignment,
                                  var1 = sort.arrayOut1("sorted"),
                                  var2 = annotBed,
                                  var3 = INPUT(path = _functionFolder+"../../lib/RSeQC/bin/tin.py"),
                                  script  = "date; python2.7 @var3@ -r @var2@ -i @var1@/input.sorted.bam > @out1@")

    }
    val galleryFolder = Array2Folder(in = makeArray(moduleOutputs),
                                     //fileCol = "Key",
                                     fileMode = "@key@@ext@")
    val gallery = ImageGallery(inRoot = galleryFolder).out                            

    return (gallery, statsT.out1)
 }
